;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for achilles-module.scm                   ###
;;;###                                                       ###
;;;###  last updated August 22, 2024                         ###
;;;###                                                       ###
;;;###  updated June 2, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

(use-modules ((achilles-module)
              :renamer (symbol-prefix-proc 'achilles-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (achilles-module-assert-two-lists-equal
         sub-name test-label-index
         test-num
         shouldbe-list result-list
         result-hash-table)
  (begin
    (if (and (list? shouldbe-list) (list? result-list))
        (begin
          (let ((slen (length shouldbe-list))
                (rlen (length result-list))
                (err-1
                 (format
                  #f "~a : (~a) error : value=~a : "
                  sub-name test-label-index
                  test-num)))
            (let ((err-2
                   (format
                    #f "shouldbe=~a, result=~a : "
                    shouldbe-list result-list))
                  (err-3
                   (format
                    #f "list length error : shouldbe=~a, result=~a"
                    slen rlen)))
              (begin
                (unittest2:assert?
                 (equal? slen rlen)
                 sub-name
                 (string-append
                  err-1 err-2 err-3)
                 result-hash-table)

                (if (equal? slen rlen)
                    (begin
                      (do ((ii 0 (1+ ii)))
                          ((>= ii slen))
                        (begin
                          (let ((s-elem
                                 (list-ref shouldbe-list ii)))
                            (let ((rtmp
                                   (member s-elem result-list))
                                  (err-4
                                   (format
                                    #f "discrepancy at [~a]=~a"
                                    ii s-elem)))
                              (begin
                                (unittest2:assert?
                                 (not
                                  (equal? rtmp #f))
                                 sub-name
                                 (string-append
                                  err-1 err-2 err-4)
                                 result-hash-table)
                                )))
                          ))
                      ))
                ))
            ))
        (begin
          (achilles-module-simple-assert
           sub-name test-label-index
           test-num
           shouldbe-list result-list
           result-hash-table)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (achilles-module-simple-assert
         sub-name test-label-index
         test-num
         shouldbe result
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) error : "
            sub-name test-label-index))
          (err-2
           (format
            #f "shouldbe=~a, result=~a"
            shouldbe result)))
      (begin
        (unittest2:assert?
         (equal? shouldbe result)
         sub-name
         (string-append err-1 err-2)
         result-hash-table)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-reverse-number-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-reverse-number-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 0) (list 1 1) (list 2 2) (list 3 3)
           (list 4 4) (list 5 5) (list 6 6) (list 7 7)
           (list 8 8) (list 9 9) (list 10 1) (list 11 11)
           (list 12 21) (list 13 31) (list 14 41) (list 15 51)
           (list 16 61) (list 17 71) (list 18 81) (list 19 91)
           (list 20 2) (list 99 99)
           (list 123 321) (list 1234 4321) (list 12345 54321)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (achilles-module:reverse-number test-num)))
                (begin
                  (achilles-module-simple-assert
                   sub-name test-label-index
                   test-num
                   shouldbe result
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-prime-exponent-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-find-prime-exponent-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 9 3 2) (list 12 2 2) (list 12 3 1)
           (list 18 3 2) (list 18 2 1) (list 24 2 3)
           (list 24 3 1) (list 25 5 2) (list 32 2 5)
           (list 64 2 6) (list 128 2 7) (list 256 2 8)
           (list 27 3 3) (list 81 3 4) (list 125 5 3)
           (list 625 5 4)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (pp-num (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (achilles-module:find-prime-exponent
                      test-num pp-num)))
                (begin
                  (achilles-module-simple-assert
                   sub-name test-label-index
                   test-num
                   shouldbe result
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-slow-find-prime-exponents-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-slow-find-prime-exponents-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 (list 1)) (list 4 (list 2))
           (list 8 (list 3)) (list 16 (list 4))
           (list 32 (list 5)) (list 64 (list 6))
           (list 128 (list 7)) (list 256 (list 8))
           (list 9 (list 2)) (list 27 (list 3))
           (list 81 (list 4)) (list 18 (list 1 2))
           (list 216 (list 3 3)) (list 2400 (list 5 1 2))
           (list 19 (list 1)) (list 361 (list 2))
           (list 6859 (list 3))
           ))
         (prime-array (prime-module:make-prime-array 10))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (achilles-module:slow-find-prime-exponents-list
                      test-num prime-array)))
                (begin
                  (achilles-module-assert-two-lists-equal
                   sub-name test-label-index
                   test-num
                   shouldbe-list result-list
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-prime-exponents-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-find-prime-exponents-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 (list 1)) (list 4 (list 2))
           (list 8 (list 3)) (list 16 (list 4))
           (list 32 (list 5)) (list 64 (list 6))
           (list 128 (list 7)) (list 256 (list 8))
           (list 9 (list 2)) (list 27 (list 3))
           (list 81 (list 4)) (list 18 (list 1 2))
           (list 216 (list 3 3)) (list 2400 (list 5 1 2))
           (list 19 (list 1)) (list 361 (list 2))
           (list 6859 (list 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (achilles-module:find-prime-exponents-list
                      test-num)))
                (begin
                  (achilles-module-assert-two-lists-equal
                   sub-name test-label-index
                   test-num
                   shouldbe-list result-list
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-powerful-number-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-powerful-number-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2) #t)
           (list (list 2 3 4 4) #t)
           (list (list 2 3 4 1) #f)
           (list (list 1 2 3) #f)
           (list (list 2 2 3 3 4) #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((exp-counts-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (achilles-module:is-powerful-number?
                      exp-counts-list)))
                (begin
                  (achilles-module-simple-assert
                   sub-name test-label-index
                   exp-counts-list
                   shouldbe result
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-perfect-power-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-perfect-power-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2) #t)
           (list (list 2 3 4 4) #f)
           (list (list 2 3 4 1) #f)
           (list (list 2 2 2) #t)
           (list (list 2 2 4 2 4 8) #t)
           (list (list 3 3 6 9 12) #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((exp-counts-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (achilles-module:is-perfect-power?
                      exp-counts-list)))
                (begin
                  (achilles-module-simple-assert
                   sub-name test-label-index
                   exp-counts-list
                   shouldbe result
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
