;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  achilles functions module                            ###
;;;###                                                       ###
;;;###  last updated August 22, 2024                         ###
;;;###                                                       ###
;;;###  updated June 2, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start achilles modules

(define-module (achilles-module)
  #:export (display-results
            reverse-number
            find-prime-exponent
            find-prime-exponents-list
            is-powerful-number?
            is-perfect-power?
            calc-next-int-stop
            main-loop

            slow-main-loop
            slow-find-prime-exponents-list
            ))

;;;#############################################################
;;;#############################################################
;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### srfi-1 for fold functions and delete-duplicates
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

(use-modules ((sprp-module)
              :renamer (symbol-prefix-proc 'sprp-module:)))

;;;#############################################################
;;;#############################################################
(define (reverse-number mm)
  (begin
    (if (>= mm 10)
        (begin
          (let ((next-num 0)
                (this-num mm))
            (begin
              (while (>= this-num 10)
                (begin
                  (let ((tmp-num (euclidean/ this-num 10)))
                    (let ((this-digit (- this-num (* 10 tmp-num))))
                      (let ((nn (+ (* 10 next-num) this-digit)))
                        (begin
                          (set! next-num nn)
                          (set! this-num tmp-num)
                          ))
                      ))
                  ))

              (if (>= this-num 0)
                  (begin
                    (set! next-num (+ (* 10 next-num) this-num))
                    ))

              next-num
              )))
        (begin
          mm
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-results ncols result-htable)
  (begin
    (let ((alist-list (hash-map->list cons result-htable)))
      (let ((alist
             (map
              (lambda (tlist)
                (begin
                  (list-ref tlist 0)
                  )) alist-list)))
        (let ((blist (sort alist <))
              (max-count (length alist))
              (ncount 0))
          (begin
            (do ((ii 0 (1+ ii)))
                ((>= ii max-count))
              (begin
                (let ((bb (list-ref blist ii)))
                  (begin
                    (display
                     (format
                      #f "~a, "
                      (utils-module:commafy-number bb)))

                    (set! ncount (1+ ncount))
                    (if (>= ncount ncols)
                        (begin
                          (newline)
                          (force-output)
                          (set! ncount 0)
                          ))
                    ))
                ))

            (if (< ncount ncols)
                (begin
                  (newline)
                  ))

            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (find-prime-exponent anum pp)
  (begin
    (let ((atmp anum)
          (ak 0)
          (completed-flag #f))
      (begin
        (while (equal? completed-flag #f)
          (begin
            (if (zero? (modulo atmp pp))
                (begin
                  (set! ak (1+ ak)))
                (begin
                  (set! completed-flag #t)
                  ))

            (set! atmp (euclidean/ atmp pp))
            ))

        ak
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (slow-find-prime-exponents-list mm prime-array)
  (begin
    (let ((mm-sqrt (1+ (exact-integer-sqrt mm)))
          (exponent-list (list)))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii mm-sqrt))
          (begin
            (if (zero? (modulo mm ii))
                (begin
                  (let ((jj (euclidean/ mm ii)))
                    (begin
                      (if (>= jj ii)
                          (begin
                            (if (prime-module:is-array-prime?
                                 ii prime-array)
                                (begin
                                  (let ((ak (find-prime-exponent mm ii)))
                                    (begin
                                      (set! exponent-list
                                            (cons ak exponent-list))
                                      ))
                                  ))

                            (if (and
                                 (> jj ii)
                                 (prime-module:is-array-prime?
                                  jj prime-array))
                                (begin
                                  (let ((ak (find-prime-exponent mm jj)))
                                    (begin
                                      (set! exponent-list
                                            (cons ak exponent-list))
                                      ))
                                  ))
                            ))
                      ))
                  ))
            ))

        (reverse exponent-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax sub-find-prime-exponent-macro
  (syntax-rules ()
    ((sub-find-prime-exponent-macro
      ii mm exponent-list)
     (begin
       (if (sprp-module:sprp-prime? ii)
           (begin
             (let ((ak (find-prime-exponent mm ii)))
               (begin
                 (set!
                  exponent-list
                  (cons ak exponent-list))
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax find-prime-exponent-macro
  (syntax-rules ()
    ((find-prime-exponent-macro
      ii mm exponent-list)
     (begin
       (if (zero? (modulo mm ii))
           (begin
             (let ((jj (euclidean/ mm ii)))
               (begin
                 (if (>= jj ii)
                     (begin
                       (sub-find-prime-exponent-macro
                        ii mm exponent-list)

                       (if (> jj ii)
                           (begin
                             (sub-find-prime-exponent-macro
                              jj mm exponent-list)
                             ))
                       ))
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (find-prime-exponents-list mm)
  (begin
    (let ((mm-sqrt (1+ (exact-integer-sqrt mm)))
          (exponent-list (list)))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii mm-sqrt))
          (begin
            (find-prime-exponent-macro
             ii mm exponent-list)
            ))

        (reverse exponent-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-powerful-number? prime-exponents-list)
  (begin
    (let ((min-exp-count
           (srfi-1:reduce min 0 prime-exponents-list)))
      (begin
        (if (>= min-exp-count 2)
            (begin
              #t)
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-perfect-power? prime-exponents-list)
  (begin
    (let ((gcd-of-counts
           (srfi-1:reduce gcd 0 prime-exponents-list)))
      (begin
        (if (>= gcd-of-counts 2)
            (begin
              #t)
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (slow-main-loop start-num end-num max-prime)
  (begin
    (let ((result-count 0)
          (prime-array (prime-module:make-prime-array max-prime))
          (previous-achilles-num 0)
          (line-break-count 10)
          (display-count 0))
      (begin
        (do ((ii start-num (1+ ii)))
            ((> ii end-num))
          (begin
            (let ((exp-counts-list
                   (slow-find-prime-exponents-list
                    ii prime-array)))
              (begin
                (if (is-powerful-number? exp-counts-list)
                    (begin
                      (if (not (is-perfect-power? exp-counts-list))
                          (begin
                            (set! display-count (1+ display-count))
                            (set! result-count (1+ result-count))
                            (if (equal?
                                 (- ii previous-achilles-num)
                                 1)
                                (begin
                                  (display
                                   (ice-9-format:format
                                    #f "~%~:d  ~a"
                                    ii
                                    "(consecutive achilles number"))
                                  (display
                                   (ice-9-format:format
                                    #f ", previous=~:d)~%"
                                    previous-achilles-num))
                                  (force-output))
                                (begin
                                  (display
                                   (ice-9-format:format
                                    #f "~:d," ii))
                                  (force-output)
                                  (if (>= display-count
                                          line-break-count)
                                      (begin
                                        (newline)
                                        (set! display-count 0))
                                      (begin
                                        (display (format #f " "))
                                        ))
                                  (force-output)
                                  ))

                            (set! previous-achilles-num ii)
                            ))
                      ))
                ))
            ))

        (newline)
        (display
         (ice-9-format:format
          #f "found ~:d achilles numbers (between ~:d and ~:d)~%"
          result-count start-num end-num))
        (newline)
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop start-num end-num max-prime)
  (begin
    (let ((result-count 0)
          (previous-achilles-num 0)
          (line-break-count 10)
          (display-count 0))
      (begin
        (do ((ii start-num (1+ ii)))
            ((> ii end-num))
          (begin
            (let ((exp-counts-list
                   (find-prime-exponents-list ii)))
              (begin
                (if (is-powerful-number? exp-counts-list)
                    (begin
                      (if (not (is-perfect-power? exp-counts-list))
                          (begin
                            (set! display-count (1+ display-count))
                            (set! result-count (1+ result-count))
                            (if (equal?
                                 (- ii previous-achilles-num)
                                 1)
                                (begin
                                  (display
                                   (ice-9-format:format
                                    #f "~%~:d  ~a"
                                    ii
                                    "(consecutive achilles number"))
                                  (display
                                   (ice-9-format:format
                                    #f ", previous=~:d)~%"
                                    previous-achilles-num))
                                  (force-output))
                                (begin
                                  (display
                                   (ice-9-format:format
                                    #f "~:d," ii))
                                  (force-output)
                                  (if (>= display-count
                                          line-break-count)
                                      (begin
                                        (newline)
                                        (set! display-count 0))
                                      (begin
                                        (display (format #f " "))
                                        ))
                                  (force-output)
                                  ))

                            (set! previous-achilles-num ii)
                            ))
                      ))
                ))
            ))

        (newline)
        (display
         (ice-9-format:format
          #f "found ~:d achilles numbers (between ~:d and ~:d)~%"
          result-count start-num end-num))
        (newline)
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
