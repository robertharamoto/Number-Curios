Mon Oct  7 06:37:48 AM HST 2024
load file ./objects/utils-module-tests-1.go
load file ./objects/aliquot-module-tests-1.go
load file ./objects/timer-module-tests-1.go
load file ./objects/config-module-tests-1.go

################################################################
################################################################
###                                                          ###
###  Run unit tests (version 2024-09-12)                     ###
###                                                          ###
###  number of tests                    21                   ###
###                                                          ###
###  successful asserts                248       100.00%     ###
###  failed asserts                      0         0.00%     ###
###  ---------                   ---------     ---------     ###
###  total asserts                     248       100.00%     ###
###                                                          ###
################################################################
################################################################
(unit test version 2024-09-26)

elapsed time = 0.030 seconds : monday, october 07, 2024  06:37:48 am


sources loc = 2251 total
tests loc = 1851 total
total loc =  4102 total
