;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for aliquot-module.scm                    ###
;;;###                                                       ###
;;;###  last updated August 22, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################

;;;### ice-9 format used for advanced formatting
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((aliquot-module)
              :renamer (symbol-prefix-proc 'aliquot-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (aliquot-module-simple-assert
         sub-name test-label-index
         test-num
         shouldbe result
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) error : test-num = ~a : "
            sub-name test-label-index test-num))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (begin
        (unittest2:assert?
         (equal? shouldbe result)
         sub-name
         (string-append err-1 err-2)
         result-hash-table)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sum-of-proper-divisors-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-sum-of-proper-divisors-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 1) (list 3 1) (list 4 3)
           (list 5 1) (list 6 6) (list 7 1)
           (list 8 7) (list 9 4) (list 10 8)
           (list 11 1) (list 12 16) (list 13 1)
           (list 14 10) (list 15 9) (list 16 15)
           (list 17 1) (list 18 21) (list 19 1)
           (list 20 22) (list 21 11) (list 22 14)
           (list 23 1) (list 24 36) (list 25 6)
           (list 26 16) (list 27 13) (list 28 28)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (aliquot-module:sum-of-proper-divisors
                      test-num)))
                (begin
                  (aliquot-module-simple-assert
                   sub-name test-label-index
                   test-num
                   shouldbe-num result-num
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-recursive-inner-loop-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-recursive-inner-loop-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 (list 2 1)) (list 3 (list 3 1))
           (list 4 (list 4 3 1)) (list 5 (list 5 1))
           (list 6 (list 6 6)) (list 7 (list 7 1))
           (list 8 (list 8 7 1)) (list 9 (list 9 4 3 1))
           (list 10 (list 10 8 7 1)) (list 11 (list 11 1))
           (list 12 (list 12 16 15 9 4 3 1)) (list 13 (list 13 1))
           (list 14 (list 14 10 8 7 1)) (list 15 (list 15 9 4 3 1))
           (list 16 (list 16 15 9 4 3 1)) (list 17 (list 17 1))
           (list 18 (list 18 21 11 1)) (list 19 (list 19 1))
           (list 20 (list 20 22 14 10 8 7 1)) (list 21 (list 21 11 1))
           (list 22 (list 22 14 10 8 7 1)) (list 23 (list 23 1))
           (list 24 (list 24 36 55 17 1)) (list 25 (list 25 6 6))
           (list 26 (list 26 16 15 9 4 3 1)) (list 27 (list 27 13 1))
           (list 28 (list 28 28))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1))
                  (max-iter 20)
                  (max-num 1000000000))
              (let ((result-list
                     (aliquot-module:recursive-inner-loop
                      0 max-iter test-num max-num (list))))
                (begin
                  (aliquot-module-simple-assert
                   sub-name test-label-index
                   test-num
                   shouldbe-list result-list
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-sequence-period-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-count-sequence-period-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 4 3 1) 0)
           (list (list 6 6) 1)
           (list (list 9 4 3 1) 0)
           (list (list 11 1) 0)
           (list (list 28 28) 1)
           (list (list 220 284 220) 2)
           (list (list 95 5 6 6) 1)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-list (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (aliquot-module:count-sequence-period
                      test-list)))
                (begin
                  (aliquot-module-simple-assert
                   sub-name test-label-index
                   test-list
                   shouldbe-num result-num
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
