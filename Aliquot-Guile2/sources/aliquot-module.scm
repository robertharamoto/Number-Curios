;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  Aliquot sequences                                    ###
;;;###                                                       ###
;;;###  last updated August 22, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start aliquot-modules
(define-module (aliquot-module)
  #:export (sum-of-proper-divisors
            recursive-inner-loop
            count-sequence-period

            main-loop
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9 format used for advanced formatting
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin function definitions                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (sum-of-proper-divisors this-num)
  (begin
    (let ((max-num (1+ (exact-integer-sqrt this-num)))
          (sum 1))
      (begin
        (do ((ii 2 (+ ii 1)))
            ((>= ii max-num))
          (begin
            (if (zero? (modulo this-num ii))
                (begin
                  (set! sum (+ sum ii))

                  (let ((divisor (euclidean/ this-num ii)))
                    (begin
                      (if (and (>= divisor max-num)
                               (not (equal? divisor ii)))
                          (begin
                            (set! sum (+ sum divisor))
                            ))
                      ))
                  ))
            ))
        sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (recursive-inner-loop
         this-iter max-iter this-num max-num results-list)
  (begin
    (cond
     ((or
       (<= this-num 1)
       (> this-iter max-iter)
       (> this-num max-num))
      (begin
        (let ((final-list
               (append results-list (list this-num))))
          (begin
            final-list
            ))
        ))
     (else
      (begin
        (if (equal? (member this-num results-list) #f)
            (begin
              (let ((this-list
                     (recursive-inner-loop
                      (+ this-iter 1) max-iter
                      (sum-of-proper-divisors this-num)
                      max-num
                      (append results-list (list this-num)))))
                (begin
                  this-list
                  )))
            (begin
              (let ((final-list
                     (append results-list (list this-num))))
                (begin
                  final-list
                  ))
              ))
        )))
    ))

;;;#############################################################
;;;#############################################################
;;;### perfect numbers (6 6), (28 28), period = 1
;;;### amicable numbers (220 284 220), period = 2
;;;### sociable numbers, period >= 3
;;;### others (95 25 6 6), period = 1
;;;### no period (1), (8 7 1)
(define (count-sequence-period results-list)
  (begin
    (if (list? results-list)
        (begin
          (let ((last-element
                 (car (last-pair results-list))))
            (begin
              (cond
               ((< (length results-list) 2)
                (begin
                  0
                  ))
               ((equal? last-element 1)
                (begin
                  0
                  ))
               (else
                (begin
                  (let ((last-list
                         (member last-element results-list)))
                    (let ((this-length (length last-list)))
                      (begin
                        (if (<= this-length 1)
                            (begin
                              0)
                            (begin
                              (- this-length 1)
                              ))
                        )))
                  )))
              )))
        (begin
          0
          ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-period-count-0-macro
  (syntax-rules ()
    ((display-period-count-0-macro
      ii
      last-element max-num
      seq-length max-iter
      seq-string)
     (begin
       (if (or (>= last-element max-num)
               (>= seq-length max-iter))
           (begin
             (display
              (ice-9-format:format
               #f "(~:d) : non-terminating sequence! "
               ii))
             (display
              (ice-9-format:format
               #f "max-iter = ~:d : "
               max-iter))
             (display
              (ice-9-format:format
               #f "count = ~:d : sequence = ~a~%"
               seq-length seq-string)))
           (begin
             (display
              (ice-9-format:format
               #f "(~:d) : terminating sequence! "
               ii))
             (display
              (ice-9-format:format
               #f "count = ~:d : sequence = ~a~%"
               seq-length seq-string))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-period-count-1-macro
  (syntax-rules ()
    ((display-period-count-1-macro
      ii seq-length seq-string)
     (begin
       (if (= seq-length 2)
           (begin
             (display
              (ice-9-format:format
               #f "(~:d) : perfect number! : sequence = ~a~%"
               ii seq-string)))
           (begin
             (display
              (ice-9-format:format
               #f "(~:d) : aspiring perfect number! "
               ii))
             (display
              (ice-9-format:format
               #f "count = ~:d : sequence = ~a~%"
               seq-length seq-string))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-period-count-2-macro
  (syntax-rules ()
    ((display-period-count-2-macro
      ii seq-length seq-string)
     (begin
       (if (= seq-length 3)
           (begin
             (display
              (ice-9-format:format
               #f "(~:d) : amicable number! : sequence = ~a~%"
               ii seq-string)))
           (begin
             (display
              (ice-9-format:format
               #f "(~:d) : aspiring amicable number! "
               ii))
             (display
              (ice-9-format:format
               #f "count = ~:d : sequence = ~a~%"
               seq-length seq-string))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-period-count-gt-2-macro
  (syntax-rules ()
    ((display-period-count-gt-2-macro
      ii seq-length seq-string
      last-element first-element
      period-count max-num max-iter)
     (begin
       (cond
        ((equal? last-element first-element)
         (begin
           (display
            (ice-9-format:format
             #f "(~:d) : sociable number of period ~:d : "
             ii period-count))
           (display
            (format
             #f "sequence = ~a~%" seq-string))
           ))
        ((>= last-element max-num)
         (begin
           (display
            (ice-9-format:format
             #f "(~:d) : exploding sequence! "
             ii))
           (display
            (ice-9-format:format
             #f "count = ~:d : sequence = ~a~%"
             seq-length seq-string))
           ))
        ((>= last-element max-num)
         (begin
           (display
            (ice-9-format:format
             #f "(~:d) : exploding sequence! "
             ii))
           (display
            (ice-9-format:format
             #f "count = ~:d : sequence = ~a~%"
             seq-length seq-string))
           ))
        ((and (< seq-length max-iter)
              (< last-element max-num))
         (begin
           (display
            (ice-9-format:format
             #f "(~:d) : terminating sequence! "
             ii))
           (display
            (ice-9-format:format
             #f "count = ~:d : sequence = ~a~%"
             seq-length seq-string))
           ))
        (else
         (begin
           (display
            (ice-9-format:format
             #f "(~:d) : aspiring sociable number! "
             ii))
           (display
            (ice-9-format:format
             #f "count = ~:d : sequence = ~a~%"
             seq-length seq-string))
           )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-output ii max-iter max-num sequence-list)
  (begin
    (let ((period-count
           (count-sequence-period sequence-list))
          (seq-string
           (utils-module:sequence-list-to-string
            sequence-list))
          (seq-length (length sequence-list))
          (first-element (car sequence-list))
          (last-element
           (car (last-pair sequence-list))))
      (begin
        (cond
         ((<= period-count 0)
          (begin
            (display-period-count-0-macro
             ii last-element max-num
             seq-length max-iter
             seq-string)
            ))
         ((= period-count 1)
          (begin
            (display-period-count-1-macro
             ii seq-length seq-string)
            ))
         ((= period-count 2)
          (begin
            (display-period-count-2-macro
             ii seq-length seq-string)
            ))
         (else
          (begin
            (display-period-count-gt-2-macro
             ii seq-length seq-string
             last-element first-element
             period-count max-num max-iter)
            )))

        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### loop over integers, look for aliquot sequences
;;;### display all sequences found
(define (main-loop start-num end-num max-iter max-num)
  (begin
    (let ((already-seen-htable
           (make-hash-table 1000)))
      (begin
        (do ((ii start-num (+ ii 1)))
            ((> ii end-num))
          (begin
            (if (equal?
                 (hash-ref already-seen-htable ii #f) #f)
                (begin
                  (let ((sequence-list
                         (recursive-inner-loop
                          0 max-iter ii max-num (list))))
                    (begin
                      (display-output
                       ii max-iter max-num sequence-list)

                      (for-each
                       (lambda(this-num)
                         (begin
                           (hash-set! already-seen-htable this-num 1)
                           )) sequence-list)
                      ))
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
