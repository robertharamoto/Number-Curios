#! /bin/bash

### initialize array
for (( jj=9; jj<=20; jj++))
do
    prev_result[ ${jj} ]=0
done

PROGNAME="p587q1.scm"
prev_lines=`wc -l q1*.log | grep -i total | awk '{ print $1; }'`
this_process=`ps -e | grep -i ${PROGNAME}`
counter=0
while [ -n "${this_process}" ]
do
    counter=$(( ${counter} + 1 ))
    echo "================== (${counter}) ==========================="
    for (( jj=9; jj<=20; jj++ ))
    do
        result[ ${jj} ]=`grep -i " : k = ${jj} : " q1*.log | wc -l`
        diff=$(( ${result[ ${jj} ]} - ${prev_result[ ${jj} ]} ))
        echo "(${jj}) ${result[ ${jj} ]} : ${diff}" | sed -e ':a;s/\B[0-9]\{3\}\>/,&/;ta'

        prev_result[ ${jj} ]="${result[ ${jj} ]}"

    done

    date "+%A, %B %d, %Y  %l:%M:%S %P"

    this_log_info=`ls -lh q1-out-4.log | awk '{ print $8 " : " $5 " : " $6 " : " $7; }'`
    echo "${this_log_info}"

    this_lines=`wc -l q1*.log | grep -i total | awk '{ print $1; }'`
    diff_lines=$(( ${this_lines} - ${prev_lines} ))
    echo "number of lines = ${this_lines} : changed by ${diff_lines} lines" | sed -e ':a;s/\B[0-9]\{3\}\>/,&/;ta'
    prev_lines=${this_lines}

    echo "${this_process}"
    sleep 300
    this_process=`ps -e | grep -i ${PROGNAME}`
done



