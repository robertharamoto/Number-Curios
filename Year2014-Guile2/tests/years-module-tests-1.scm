;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for years-module.scm                      ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;###  last updated June 1, 2022                            ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((years-module)
              :renamer (symbol-prefix-proc 'years-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (assert-two-lists-equal
         test-num shouldbe-list result-list
         sub-name test-label-index result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-1
             (format
              #f "~a : (~a) error : num=~a, shouldbe=~a, "
              sub-name test-label-index test-num
              shouldbe-list))
            (err-2
             (format
              #f "lengths not equal, shouldbe=~a, result=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-1 err-2)
           result-hash-table)

          (if (equal? slen rlen)
              (begin
                (let ((err-1
                       (format
                        #f "~a : error (~a) : num=~a"
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f ", shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (for-each
                     (lambda (s-elem)
                       (begin
                         (let ((mflag (member s-elem result-list))
                               (err-3
                                (format #f ", missing item ~a" s-elem)))
                           (begin
                             (unittest2:assert?
                              (not (equal? mflag #f))
                              sub-name
                              (string-append err-1 err-2 err-3)
                              result-hash-table)
                             ))
                         )) shouldbe-list)
                    ))
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-two-list-lists-equal
         init-list shouldbe-list-list result-list-list
         sub-name test-label-index result-hash-table)
  (begin
    (let ((slen (length shouldbe-list-list))
          (rlen (length result-list-list)))
      (let ((err-1
             (format
              #f "~a : error (~a) : init-list=~a, shouldbe=~a, "
              sub-name test-label-index init-list
              shouldbe-list-list))
            (err-2
             (format
              #f "lengths not equal, shouldbe=~a, result=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-1 err-2)
           result-hash-table)

          (if (equal? slen rlen)
              (begin
                (for-each
                 (lambda (slist)
                   (begin
                     (let ((rlist
                            (member slist result-list-list)))
                       (let ((err-1
                              (format
                               #f "~a : error (~a) : init-list=~a, "
                               sub-name test-label-index init-list))
                             (err-2
                              (format
                               #f "shouldbe=~a, result=~a, "
                               shouldbe-list-list result-list-list))
                             (err-3
                              (format
                               #f "shouldbe item=~a, result=~a"
                               slist rlist)))
                         (begin
                           (unittest2:assert?
                            (not (equal? rlist #f))
                            sub-name
                            (string-append err-1 err-2 err-3)
                            result-hash-table)
                           )))
                     )) shouldbe-list-list)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factorial-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-factorial-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 1) (list 1 1)
           (list 2 2) (list 3 6)
           (list 4 24) (list 5 120)
           (list 6 720)
           (list 7 5040)
           (list 8 40320)
           (list 9 362880)
           (list 10 3628800)
           (list 11 39916800)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (years-module:factorial num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : num=~a, "
                        sub-name test-label-index num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-double-factorial-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-double-factorial-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 1) (list 2 2) (list 4 8)
           (list 6 48) (list 8 384) (list 10 3840)
           (list 1 1) (list 3 3) (list 5 15)
           (list 7 105) (list 9 945) (list 11 10395)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (years-module:double-factorial num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : num=~a, "
                        sub-name test-label-index num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-binary-operators-precedence-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-binary-operators-precedence-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list + 1) (list - 1)
           (list * 10) (list / 10)
           (list expt 100)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((oper (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (years-module:binary-operators-precedence oper)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : oper=~a, "
                        sub-name test-label-index oper))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-stack-list-to-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-stack-list-to-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 3 4 5) (list + -)
                 (list + (list - 5 4) 3))
           (list (list 2 5 10) (list - +)
                 (list - (list + 10 5) 2))
           (list (list 7 2 5 10) (list + - +)
                 (list + (list - (list + 10 5) 2) 7))
           (list (list 10 5 2) (list expt expt)
                 (list expt 2 (list expt 5 10)))
           (list (list 3 4 5) (list expt expt)
                 (list expt 5 (list expt 4 3)))
           (list (list 3 4 5 6) (list expt expt expt)
                 (list expt 6 (list expt 5 (list expt 4 3))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((number-list (list-ref alist 0))
                  (operator-list (list-ref alist 1))
                  (shouldbe-list (list-ref alist 2)))
              (let ((result-list
                     (years-module:stack-list-to-list
                      number-list operator-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : numbers=~a, operators=~a, "
                        sub-name test-label-index
                        number-list operator-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-infix-list-to-prefix-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-infix-list-to-prefix-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 10 + 5 - 2 + 1)
                 (list + (list - (list + 10 5) 2) 1))
           (list (list 10 * 5 - 2 + 1)
                 (list + (list - (list * 10 5) 2) 1))
           (list (list 10 + 5 * 2 + 1)
                 (list + (list + 10 (list * 5 2)) 1))
           (list (list 10 + 5 - 2 * 1)
                 (list - (list + 10 5) (list * 2 1)))
           (list (list 10 expt 5 * 2 + 1)
                 (list + (list * (list expt 10 5) 2) 1))
           (list (list 10 expt 5 expt 2 + 1)
                 (list + (list expt 10 (list expt 5 2)) 1))
           (list (list 1 + "(" 2 - 3 ")" + 4)
                 (list + (list + 1 (list - 2 3)) 4))
           (list (list 1 + "(" 2 - 3 + 4 ")" + 5)
                 (list + (list + 1 (list + (list - 2 3) 4)) 5))
           (list (list 1 + "(" 2 - 3 ")" / "(" 4 + 5 ")" + 6)
                 (list + (list + 1 (list / (list - 2 3)
                                         (list + 4 5))) 6))
           (list (list 1 + "(" 2 - 3 ")" / "("
                       (list sqrt 4) + 5 ")" + 6)
                 (list +
                       (list +
                             1
                             (list / (list - 2 3)
                                   (list + (list sqrt 4) 5))) 6))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((infix-list (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (years-module:infix-list-to-prefix-list
                      infix-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : infix=~a, "
                        sub-name test-label-index infix-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-add-op-to-number-jj-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-add-op-to-number-jj-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 10 + 5 - 2 + 1)
                 0 sqrt
                 (list (list sqrt 10) + 5 - 2 + 1))
           (list (list 10 + 5 - 2 + 1)
                 1 sqrt
                 (list 10 + (list sqrt 5) - 2 + 1))
           (list (list 10 + 5 - 2 + 1)
                 2 sqrt
                 (list 10 + 5 - (list sqrt 2) + 1))
           (list (list 10 + 5 - 2 + 1)
                 3 sqrt
                 (list 10 + 5 - 2 + (list sqrt 1)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((infix-list (list-ref alist 0))
                  (jj (list-ref alist 1))
                  (unary-op (list-ref alist 2))
                  (shouldbe-list (list-ref alist 3)))
              (let ((result-list
                     (years-module:add-op-to-number-jj
                      infix-list jj unary-op)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : infix=~a, jj=~a, op=~a, "
                        sub-name test-label-index infix-list
                        jj unary-op))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-eval-prefix-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-eval-prefix-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list + 2 3) 5)
           (list (list / 8 2) 4)
           (list (list / 8 (list expt 0 4)) #f)
           (list (list / 8 (list expt 0 (expt 1 2))) #f)
           (list (list sqrt 4) 2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((prefix-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (begin
                (let ((result
                       (years-module:eval-prefix-list prefix-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : prefix=~a, "
                          sub-name test-label-index prefix-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a~%"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-infix-list-to-number-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-infix-list-to-number-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2 + 0 - 1 * 4) 2014)
           (list (list 2 + 0 / 1 * 4) 2014)
           (list (list 0 + 4 - 1 * 2) 412)
           (list (list "(" 0 expt 4 ")" - 1 * 2) 412)
           (list (list "(" 2 expt 0 ")" - 1 * 4) 2014)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((infix-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (begin
                (let ((result
                       (years-module:infix-list-to-number
                        infix-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : infix=~a, "
                          sub-name test-label-index infix-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a~%"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-add-in-single-parenthesis-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-add-in-single-parenthesis-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 10 + 5 - 2 + 1)
                 (list (list "(" 10 + 5 ")" - 2 + 1)
                       (list "(" 10 + 5 - 2 ")" + 1)
                       (list "(" 10 + 5 - 2 + 1 ")")
                       (list 10 + "(" 5 - 2 ")" + 1)
                       (list 10 + "(" 5 - 2 + 1 ")")
                       (list 10 + 5 - "(" 2 + 1 ")")))
           (list (list 3 - 4 + 5 + 6)
                 (list (list "(" 3 - 4 ")" + 5 + 6)
                       (list "(" 3 - 4 + 5 ")" + 6)
                       (list "(" 3 - 4 + 5 + 6 ")")
                       (list 3 - "(" 4 + 5 ")" + 6)
                       (list 3 - "(" 4 + 5 + 6 ")")
                       (list 3 - 4 + "(" 5 + 6 ")")))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((infix-list (list-ref alist 0))
                  (in-len (length (list-ref alist 0)))
                  (shouldbe-list-list (list-ref alist 1)))
              (begin
                (let ((result-list-list
                       (years-module:add-in-single-parenthesis
                        infix-list in-len)))
                  (begin
                    (assert-two-list-lists-equal
                     infix-list shouldbe-list-list result-list-list
                     sub-name test-label-index result-hash-table)
                    ))))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-op-list-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-op-list-to-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list "(" 3 - 4 ")" + 5 + 6)
                 "(3 - 4) + 5 + 6")
           (list (list "(" 3 - 4 + 5 ")" + 6)
                 "(3 - 4 + 5) + 6")
           (list (list "(" 3 - 4 + 5 + 6 ")")
                 "(3 - 4 + 5 + 6)")
           (list (list 3 expt "(" 4 + 5 ")" + 6)
                 "3 ^ (4 + 5) + 6")
           (list (list 3 - "(" 4 expt 5 expt 6 ")")
                 "3 - (4 ^ 5 ^ 6)")
           (list (list 3 - (list sqrt 4) + "(" 5 + 6 ")")
                 "3 - (sqrt 4) + (5 + 6)")
           (list (list 3 - (list sqrt 4)
                       + (list years-module:factorial 5)
                       + (list years-module:double-factorial 6))
                 "3 - (sqrt 4) + (factorial 5) + (double-factorial 6)")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((prefix-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (years-module:op-list-to-string
                      prefix-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : prefix-list=~a, "
                        sub-name test-label-index prefix-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
