;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  years functions module                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;###  last updated May 29, 2024                            ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start years modules
(define-module (years-module)
  #:export (factorial
            double-factorial
            binary-operators-precedence
            stack-list-to-list
            infix-list-to-prefix-list
            add-op-to-number-jj
            add-in-single-parenthesis
            eval-prefix-list
            infix-list-to-number
            parenthesis-and-eval
            add-in-unary-ops-and-eval

            make-input-nums-list
            make-basic-infix-list
            cycle-over-input-nums-ops
            op-list-to-string

            main-loop
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### ice-9 rdelim for read-line/write-line
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice9-rdelim:)))

;;;### digits-module for split-digits-list functions
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;#############################################################
;;;#############################################################
(define (factorial nn)
  (begin
    (cond
     ((<= nn 1)
      (begin
        1
        ))
     ((= nn 2)
      (begin
        2
        ))
     ((= nn 3)
      (begin
        6
        ))
     ((= nn 4)
      (begin
        24
        ))
     ((= nn 5)
      (begin
        120
        ))
     ((= nn 6)
      (begin
        720
        ))
     ((= nn 7)
      (begin
        5040
        ))
     ((= nn 8)
      (begin
        40320
        ))
     ((= nn 9)
      (begin
        362880
        ))
     (else
      (begin
        (let ((result 1))
          (begin
            (do ((ii 2 (1+ ii)))
                ((> ii nn))
              (begin
                (set! result (* ii result))
                ))
            result
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (double-factorial nn)
  (define (even-dfac nn)
    (begin
      (cond
       ((< nn 2)
        (begin
          1
          ))
       ((= nn 2)
        (begin
          2
          ))
       ((= nn 4)
        (begin
          8
          ))
       ((= nn 6)
        (begin
          48
          ))
       (else
        (begin
          (let ((result 2))
            (begin
              (do ((ii 4 (+ ii 2)))
                  ((> ii nn))
                (begin
                  (set! result (* result ii))
                  ))
              result
              ))
          )))
      ))
  (define (odd-dfac nn)
    (begin
      (cond
       ((< nn 1)
        (begin
          1
          ))
       ((= nn 1)
        (begin
          1
          ))
       ((= nn 3)
        (begin
          3
          ))
       ((= nn 5)
        (begin
          15
          ))
       ((= nn 7)
        (begin
          105
          ))
       (else
        (begin
          (let ((result 1))
            (begin
              (do ((ii 3 (+ ii 2)))
                  ((> ii nn))
                (begin
                  (set! result (* result ii))
                  ))
              result
              ))
          )))
      ))
  (begin
    (if (even? nn)
        (begin
          (even-dfac nn))
        (begin
          (odd-dfac nn)
          ))
    ))

;;;#############################################################
;;;#############################################################
;;; obeys pemdas (parenthesis, exponentiation, multiplication,
;;; division, addition, subtraction)
(define (binary-operators-precedence op)
  (begin
    (cond
     ((or (equal? op +)
          (equal? op -))
      (begin
        1
        ))
     ((or (equal? op *)
          (equal? op /))
      (begin
        10
        ))
     ((equal? op expt)
      (begin
        100
        ))
     (else
      (begin
        0
        )))
    ))

;;;#############################################################
;;;#############################################################
;;; this function is called when the first operators have
;;; non increasing precedence
;;; infix=(2 - 5 + 10), number-stack=(10 5 2), operator-stack = (- +)
;;; result should be (+ (- 2 5) 10)
(define (stack-list-to-list
         number-stack operator-stack)
  (define (local-rec-function
           number-stack operator-stack
           prev-op prev-op-precedence)
    (begin
      (if (or (not (list? operator-stack))
              (<= (length operator-stack) 0))
          (begin
            (if (>= (length number-stack) 2)
                (begin
                  (list
                   prev-op
                   (cadr number-stack)
                   (car number-stack)))
                (begin
                  (car number-stack)
                  )))
          (begin
            (let ((this-op (list-ref operator-stack 0))
                  (n1 (list-ref number-stack 0))
                  (nstack-length (length number-stack)))
              (let ((this-op-precedence
                     (binary-operators-precedence this-op)))
                (begin
                  (cond
                   ((and
                     (> this-op-precedence
                        prev-op-precedence)
                     (> nstack-length 1))
                    (begin
                      (let ((n2 (list-ref number-stack 1))
                            (tail-number-stack
                             (list-tail number-stack 2))
                            (tail-operator-stack
                             (list-tail operator-stack 1)))
                        (let ((next-number-stack
                               (cons (list prev-op n2 n1)
                                     tail-number-stack)))
                          (let ((result
                                 (local-rec-function
                                  next-number-stack
                                  tail-operator-stack
                                  this-op this-op-precedence)))
                            (begin
                              result
                              ))
                          ))
                      ))
                   ((and
                     (= this-op-precedence
                        prev-op-precedence)
                     (equal? prev-op expt)
                     (> nstack-length 1))
                    (begin
                      (let ((tail-number-stack
                             (list-tail number-stack 2))
                            (n2 (list-ref number-stack 1))
                            (tail-operator-stack
                             (list-tail operator-stack 1)))
                        (let ((next-number-stack
                               (cons (list prev-op n2 n1)
                                     tail-number-stack)))
                          (let ((result
                                 (local-rec-function
                                  next-number-stack
                                  tail-operator-stack
                                  this-op this-op-precedence)))
                            (begin
                              result
                              ))
                          ))
                      ))
                   ((and
                     (= this-op-precedence prev-op-precedence)
                     (> nstack-length 1))
                    (begin
                      (let ((tail-number-stack
                             (list-tail number-stack 1))
                            (tail-operator-stack
                             (list-tail operator-stack 1)))
                        (let ((result
                               (local-rec-function
                                tail-number-stack
                                tail-operator-stack
                                this-op this-op-precedence)))
                          (begin
                            (list prev-op result n1)
                            )))
                      ))
                   ((and
                     (< this-op-precedence prev-op-precedence)
                     (> nstack-length 1))
                    (begin
                      (let ((tail-number-stack
                             (list-tail number-stack 2))
                            (n2 (list-ref number-stack 1))
                            (tail-operator-stack
                             (list-tail operator-stack 1)))
                        (let ((next-number-stack
                               (cons (list prev-op n2 n1)
                                     tail-number-stack)))
                          (let ((result
                                 (local-rec-function
                                  next-number-stack
                                  tail-operator-stack
                                  this-op this-op-precedence)))
                            (begin
                              result
                              ))
                          ))
                      )))
                  )))
            ))
      ))
  (begin
    (if (and (> (length number-stack) 1)
             (>= (length operator-stack) 1))
        (begin
          (let ((prev-op (list-ref operator-stack 0))
                (tail-operator-stack
                 (list-tail operator-stack 1)))
            (let ((prev-op-precedence
                   (binary-operators-precedence prev-op)))
              (let ((result-list
                     (local-rec-function
                      number-stack tail-operator-stack
                      prev-op prev-op-precedence)))
                (begin
                  result-list
                  ))
              )))
        (begin
          (car number-stack)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax group-previous-expression-macro
  (syntax-rules ()
    ((group-previous-expression-macro
      this-op prev-op number-stack operator-stack)
     (begin
       (let ((continue-flag #t)
             (this-precedence
              (binary-operators-precedence this-op))
             (prev-precedence
              (binary-operators-precedence prev-op)))
         (begin
           (if (<= (length operator-stack) 0)
               (begin
                 (set! continue-flag #f)
                 ))
           (while
            (and
             (< this-precedence prev-precedence)
             (equal? continue-flag #t))
            (begin
              (let ((nstack-length (length number-stack)))
                (begin
                  (if (> nstack-length 1)
                      (begin
                        (let ((num2 (list-ref number-stack 0))
                              (num1 (list-ref number-stack 1))
                              (tail-number-stack
                               (list-tail number-stack 2))
                              (tail-operator-stack
                               (list-tail operator-stack 1)))
                          (let ((next-num (list prev-op num1 num2)))
                            (begin
                              (set!
                               number-stack
                               (cons next-num tail-number-stack))
                              (set!
                               operator-stack
                               tail-operator-stack)

                              (if (<= (length operator-stack) 0)
                                  (begin
                                    (set! continue-flag #f))
                                  (begin
                                    (set!
                                     prev-op
                                     (list-ref operator-stack 0))
                                    (set!
                                     prev-precedence
                                     (binary-operators-precedence prev-op))
                                    ))
                              ))
                          ))
                      (begin
                        (set! number-stack (list))
                        ))
                  ))
              ))

           (set!
            operator-stack
            (cons this-op operator-stack))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; input-list looks like (list num1 op1 num2 op2 num3 op3 num4)
;;; or (list num1 op1 "(" num2 op2 num3 ")" op3 num4)
;;; result-list looks like (list op3 (list op2 (list op1 num1 num2) num3) num4)
;;; depending on the operators precedence or grouping symbols
(define (infix-list-to-prefix-list input-list)
  (define (local-rec-infix-process
           alist number-stack operator-stack)
    (begin
      (if (or (not (list? alist))
              (<= (length alist) 0))
          (begin
            (let ((result
                   (stack-list-to-list
                    number-stack operator-stack)))
              (begin
                result
                )))
          (begin
            (let ((this-item (list-ref alist 0))
                  (tail-list (cdr alist)))
              (begin
                (cond
                 ((or (number? this-item)
                      (list? this-item))
                  (begin
                    (set!
                     number-stack
                     (cons this-item number-stack))
                    ))
                 ((or (equal? this-item +)
                      (equal? this-item -)
                      (equal? this-item *)
                      (equal? this-item /)
                      (equal? this-item expt))
                  (begin
                    (if (>= (length operator-stack) 1)
                        (begin
                          (let ((prev-op (car operator-stack)))
                            (let ((prev-op-precedence
                                   (binary-operators-precedence prev-op))
                                  (this-item-precedence
                                   (binary-operators-precedence this-item)))
                              (begin
                                (cond
                                 ((< this-item-precedence
                                     prev-op-precedence)
                                  (begin
                                    (group-previous-expression-macro
                                     this-item prev-op
                                     number-stack operator-stack)
                                    ))
                                 ((and
                                   (equal? this-item expt)
                                   (= this-item-precedence
                                      prev-op-precedence))
                                  (begin
                                    (let ((next-operator-stack
                                           (cons this-item operator-stack)))
                                      (begin
                                        (set! operator-stack
                                              next-operator-stack)
                                        ))
                                    ))
                                 ((= this-item-precedence
                                     prev-op-precedence)
                                  (begin
                                    (let ((next-operator-stack
                                           (cons this-item operator-stack)))
                                      (begin
                                        (set! operator-stack
                                              next-operator-stack)
                                        ))
                                    ))
                                 (else
                                  (begin
                                    (let ((next-operator-stack
                                           (cons this-item operator-stack)))
                                      (begin
                                        (set! operator-stack
                                              next-operator-stack)
                                        ))
                                    )))
                                ))
                            ))
                        (begin
                          (set! operator-stack (list this-item))
                          ))
                    ))
                 ((and (string? this-item)
                       (string-ci=? this-item "("))
                  (begin
                    (let ((sub-result
                           (local-rec-infix-process
                            tail-list (list) (list)))
                          (next-input-list
                           (member ")" alist)))
                      (begin
                        (if (list? next-input-list)
                            (begin
                              (if (> (length next-input-list) 1)
                                  (begin
                                    (set!
                                     tail-list
                                     (list-tail next-input-list 1)))
                                  (begin
                                    (set!
                                     tail-list (list))
                                    ))
                              ))

                        (set!
                         number-stack
                         (cons sub-result number-stack))
                        ))
                    ))
                 ((and (string? this-item)
                       (string-ci=? this-item ")"))
                  (begin
                    (let ((sub-result
                           (stack-list-to-list
                            number-stack operator-stack)))
                      (begin
                        (set! tail-list (list))

                        sub-result
                        ))
                    ))
                 (else
                  (begin
                    (display
                     (format
                      #f "debug unhandled condition!~%"))
                    (display
                     (format
                      #f "alist=~a~%" alist))
                    (display
                     (format
                      #f "item=~a : number-stack=~a, operator-stack=~a~%"
                      this-item number-stack operator-stack))
                    (display
                     (format
                      #f "original input-list=~a~%" input-list))
                    (force-output)
                    )))

                (let ((final-list
                       (local-rec-infix-process
                        tail-list number-stack operator-stack)))
                  (begin
                    final-list
                    ))
                ))
            ))
      ))
  (begin
    (if (>= (length input-list) 3)
        (begin
          (let ((result-list
                 (local-rec-infix-process
                  input-list (list) (list))))
            (begin
              result-list
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
;;; given an infix expression, add in a unary op to the jj-th number
(define (add-op-to-number-jj
         infix-list jj unary-op-to-add)
  (begin
    (if (list? infix-list)
        (begin
          (let ((ilen (length infix-list))
                (local-ilist (list-copy infix-list))
                (num-count 0)
                (completed-flag #f))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((or (>= ii ilen)
                       (equal? completed-flag #t)))
                (begin
                  (let ((this-expr
                         (list-ref local-ilist ii)))
                    (begin
                      (if (number? this-expr)
                          (begin
                            (if (equal? num-count jj)
                                (begin
                                  (let ((sub-result-1
                                         (list unary-op-to-add this-expr)))
                                    (begin
                                      (list-set! local-ilist ii sub-result-1)
                                      (set! completed-flag #t)
                                      )))
                                (begin
                                  (set! num-count (1+ num-count))
                                  ))
                            ))
                      ))
                  ))

              local-ilist
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-each-tail-element
  (syntax-rules ()
    ((process-each-tail-element
      jj jj-len ll1 ll2 result-list)
     (begin
       (let ((jj-item (list-ref ll2 jj))
             (jjpp (1+ jj)))
         (begin
           (if (or (number? jj-item)
                   (list? jj-item)
                   (equal? jj (1- jj-len)))
               (begin
                 (if (< jjpp jj-len)
                     (begin
                       (let ((ll3 (list-head ll2 jjpp))
                             (ll4 (list-tail ll2 jjpp)))
                         (let ((next-infix-list
                                (append
                                 ll1 (list "(") ll3
                                 (list ")") ll4)))
                           (begin
                             (set!
                              result-list
                              (cons next-infix-list result-list))
                             ))
                         ))
                     (begin
                       (let ((next-infix-list
                              (append
                               ll1 (list "(") ll2 (list ")"))))
                         (begin
                           (set!
                            result-list
                            (cons next-infix-list result-list))
                           ))
                       ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-in-single-parenthesis infix-list in-len)
  (begin
    (let ((ii-max (- in-len 2))
          (result-list (list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii ii-max))
          (begin
            (let ((an-item (list-ref infix-list ii)))
              (begin
                (if (or
                     (number? an-item)
                     (list? an-item))
                    (begin
                      (let ((ll1 (list-head infix-list ii))
                            (ll2 (list-tail infix-list ii)))
                        (let ((jj-len (length ll2)))
                          (begin
                            (do ((jj 1 (1+ jj)))
                                ((>= jj jj-len))
                              (begin
                                (process-each-tail-element
                                 jj jj-len ll1 ll2 result-list)
                                ))
                            )))
                      ))
                ))
            ))
        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; eval prefix elements, one at a time, recursively
(define (eval-prefix-list prefix-list)
  (begin
    (if (or (not (list? prefix-list))
            (<= (length prefix-list) 0))
        (begin
          #f)
        (begin
          (let ((plen (length prefix-list)))
            (begin
              (cond
               ((equal? plen 1)
                (begin
                  (if (not (equal? (car prefix-list) #f))
                      (begin
                        (eval-prefix-list (car prefix-list)))
                      (begin
                        #f
                        ))
                  ))
               ((equal? plen 2)
                (begin
                  (let ((oper (list-ref prefix-list 0))
                        (arg1 (list-ref prefix-list 1)))
                    (begin
                      (if (and (not (number? arg1))
                               (not (equal? arg1 #f)))
                          (begin
                            (let ((atmp (eval-prefix-list arg1)))
                              (begin
                                (set! arg1 atmp)
                                ))
                            ))
                      (if (number? arg1)
                          (begin
                            (let ((fac-max 10)
                                  (dfac-max 15))
                              (begin
                                (if (or
                                     (and (equal? oper factorial)
                                          (<= arg1 fac-max))
                                     (and (equal? oper double-factorial)
                                          (<= arg1 dfac-max))
                                     (and (equal? oper sqrt)
                                          (>= arg1 0)))
                                    (begin
                                      (let ((result
                                             (primitive-eval (list oper arg1))))
                                        (begin
                                          result
                                          )))
                                    (begin
                                      #f
                                      ))
                                )))
                          (begin
                            #f
                            ))
                      ))
                  ))
               (else
                (begin
                  (let ((oper (list-ref prefix-list 0))
                        (arg1 (list-ref prefix-list 1))
                        (arg2 (list-ref prefix-list 2)))
                    (begin
                      (if (and (not (number? arg1))
                               (not (equal? arg1 #f)))
                          (begin
                            (let ((atmp (eval-prefix-list arg1)))
                              (begin
                                (if (number? atmp)
                                    (begin
                                      (set! arg1 atmp)
                                      ))
                                ))
                            ))
                      (if (and (not (number? arg2))
                               (not (equal? arg2 #f)))
                          (begin
                            (let ((atmp (eval-prefix-list arg2)))
                              (begin
                                (if (number? atmp)
                                    (begin
                                      (set! arg2 atmp)
                                      ))
                                ))
                            ))
                      (cond
                       ((equal? oper /)
                        (begin
                          (if (and (number? arg1)
                                   (number? arg2))
                              (begin
                                (if (not (zero? arg2))
                                    (begin
                                      (let ((result
                                             (primitive-eval
                                              (list oper arg1 arg2))))
                                        (begin
                                          result
                                          )))
                                    (begin
                                      #f
                                      )))
                              (begin
                                #f
                                ))
                          ))
                       ((equal? oper expt)
                        (begin
                          ;;; raise a number to at most the 10th power, otherwise discard
                          (let ((expt-max 10))
                            (begin
                              (if (and (integer? arg1)
                                       (integer? arg2))
                                  (begin
                                    (if (zero? arg1)
                                        (begin
                                          0)
                                        (begin
                                          (if (<= arg2 expt-max)
                                              (begin
                                                (let ((result
                                                       (primitive-eval
                                                        (list oper arg1 arg2))))
                                                  (begin
                                                    result
                                                    )))
                                              (begin
                                                #f
                                                ))
                                          )))
                                  (begin
                                    #f
                                    ))
                              ))
                          ))
                       (else
                        (begin
                          (if (and (number? arg1)
                                   (number? arg2))
                              (begin
                                (let ((result
                                       (primitive-eval
                                        (list oper arg1 arg2))))
                                  (begin
                                    result
                                    )))
                              (begin
                                #f
                                ))
                          )))
                      ))
                  )))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (infix-list-to-number infix-list)
  (begin
    (let ((rstring ""))
      (begin
        (for-each
         (lambda (in-item)
           (begin
             (if (number? in-item)
                 (begin
                   (set!
                    rstring
                    (string-append
                     rstring
                     (format #f "~a" in-item)))
                   ))
             )) infix-list)

        (if (not (equal? (string->number rstring) #f))
            (begin
              (string->number rstring))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax main-evaluate-macro
  (syntax-rules ()
    ((main-evaluate-macro
      result
      min-number max-number special-number
      acc-list memoize-htable result-hash-table)
     (begin
       (if (integer? result)
           (begin
             (if (or
                  (and (>= result min-number)
                       (<= result max-number))
                  (equal? result special-number))
                 (begin
                   (let ((curr-list
                          (hash-ref
                           result-hash-table result (list))))
                     (let ((curr-value
                            (hash-ref
                             memoize-htable curr-list #f))
                           (this-value
                            (hash-ref
                             memoize-htable acc-list
                             (infix-list-to-number acc-list))))
                       (begin
                         (if (and (equal? curr-value #f)
                                  (not (equal? curr-list (list))))
                             (begin
                               (set!
                                curr-value
                                (infix-list-to-number curr-list))
                               (hash-set!
                                memoize-htable
                                curr-list curr-value)
                               ))

                         (if (> (length curr-list) 0)
                             (begin
                               (if (and (number? this-value)
                                        (number? curr-value))
                                   (begin
                                     (let ((curr-tmp
                                            (abs
                                             (- curr-value special-number)))
                                           (this-tmp
                                            (abs
                                             (- this-value special-number))))
                                       (begin
                                         ;;; prefer those expressions with 2 0 1 4 in order
                                         (if (< this-tmp curr-tmp)
                                             (begin
                                               (hash-set!
                                                result-hash-table
                                                result acc-list)

                                               (hash-set!
                                                memoize-htable
                                                acc-list this-value)
                                               (hash-remove!
                                                memoize-htable
                                                curr-list))
                                             (begin
                                               ;;; prefer the shorter expressions
                                               (if (equal? this-tmp curr-tmp)
                                                   (begin
                                                     (let ((len1 (length curr-list))
                                                           (len2 (length acc-list)))
                                                       (begin
                                                         (if (< len2 len1)
                                                             (begin
                                                               (hash-set!
                                                                result-hash-table
                                                                result acc-list)

                                                               (hash-set!
                                                                memoize-htable
                                                                acc-list this-value)
                                                               (hash-remove!
                                                                memoize-htable curr-list)
                                                               ))
                                                         ))
                                                     ))
                                               ))
                                         ))
                                     )))
                             (begin
                               (hash-set!
                                result-hash-table result acc-list)
                               (hash-set!
                                memoize-htable acc-list this-value)
                               ))
                         )))
                   ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax do-final-eval-and-store-macro
  (syntax-rules ()
    ((do-final-eval-and-store-macro
      acc-list min-number max-number special-number
      memoize-htable result-hash-table)
     (begin
       (if (and (list? acc-list)
                (> (length acc-list) 2))
           (begin
             (let ((prefix-list
                    (infix-list-to-prefix-list acc-list)))
               (begin
                 (if (and (list? prefix-list)
                          (> (length prefix-list) 0))
                     (begin
                       (let ((result (eval-prefix-list prefix-list)))
                         (begin
                           ;;; make sure there's no dividing by zero
                           (if (number? result)
                               (begin
                                 (main-evaluate-macro
                                  result
                                  min-number max-number special-number
                                  acc-list memoize-htable result-hash-table)
                                 ))
                           ))
                       ))
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (parenthesis-and-eval
         infix-list min-number max-number special-number
         memoize-htable result-hash-table)
  (begin
    (let ((in-len (length infix-list)))
      (begin
        (if (> in-len 3)
            (begin
              (let ((a-list-list
                     (add-in-single-parenthesis
                      infix-list in-len)))
                (begin
                  (if (list? a-list-list)
                      (begin
                        (for-each
                         (lambda (alist)
                           (begin
                             (if (and (list? alist)
                                      (> (length alist) 0))
                                 (begin
                                   (do-final-eval-and-store-macro
                                    alist min-number max-number
                                    special-number
                                    memoize-htable result-hash-table)
                                   ))
                             )) a-list-list)
                        ))
                  )))
            (begin
              (do-final-eval-and-store-macro
               infix-list min-number max-number
               special-number
               memoize-htable result-hash-table)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; add in unary operators, and operate only on numbers, not expressions
;;; result-hash-table - key=number, value=(list expression1 expression2,...)
(define (add-in-unary-ops-and-eval
         starting-infix-list unary-ops-list
         min-number max-number special-number
         result-hash-table)
  (define (local-rec-process
           remaining-infix-list unary-ops-list
           acc-list memoize-htable result-hash-table)
    (begin
      (if (or (not (list? remaining-infix-list))
              (<= (length remaining-infix-list) 0))
          (begin
            ;;; add single parenthesis and evaluate with unary-ops
            (parenthesis-and-eval
             (reverse acc-list)
             min-number max-number special-number
             memoize-htable result-hash-table))
          (begin
            (let ((this-item (list-ref remaining-infix-list 0))
                  (tail-list (list-tail remaining-infix-list 1)))
              (begin
                ;;; first no unary op applied
                (local-rec-process
                 tail-list unary-ops-list
                 (cons this-item acc-list)
                 memoize-htable result-hash-table)

                ;;; next apply unary op if a value
                (if (number? this-item)
                    (begin
                      (for-each
                       (lambda (this-op)
                         (begin
                           (let ((next-item
                                  (list this-op this-item)))
                             (begin
                               (local-rec-process
                                tail-list unary-ops-list
                                (cons next-item acc-list)
                                memoize-htable result-hash-table)
                               ))
                           )) unary-ops-list)
                      (gc))
                    (begin
                      (local-rec-process
                       tail-list unary-ops-list
                       (cons this-item acc-list)
                       memoize-htable result-hash-table)
                      (gc)
                      ))
                ))
            ))
      ))
  (begin
    (if (and (list? starting-infix-list)
             (> (length starting-infix-list) 0))
        (begin
          (let ((memoize-htable (make-hash-table)))
            (begin
              (local-rec-process
               starting-infix-list unary-ops-list
               (list) memoize-htable result-hash-table)

              (hash-clear! memoize-htable)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (my-delete-first xx alist)
  (begin
    (let ((found-flag #f)
          (acc-list (list)))
      (begin
        (for-each
         (lambda (anum)
           (begin
             (if (equal? xx anum)
                 (begin
                   (if (equal? found-flag #f)
                       (begin
                         (set! found-flag #t))
                       (begin
                         (set! acc-list (cons anum acc-list))
                         )))
                 (begin
                   (set! acc-list (cons anum acc-list))
                   ))
             )) alist)
        acc-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax rec-process-2-2-macro
  (syntax-rules ()
    ((rec-process-2-2-macro
      mm-len rem-2 num-1 ll1 rlist)
     (begin
       (do ((mm 0 (1+ mm)))
           ((>= mm mm-len))
         (begin
           (let ((digit-3 (list-ref rem-2 mm))
                 (local-remain-list (list-copy rem-2)))
             (let ((rem-3
                    (my-delete-first digit-3 local-remain-list)))
               (let ((nn-len (length rem-3)))
                 (begin
                   (do ((nn 0 (1+ nn)))
                       ((>= nn nn-len))
                     (begin
                       (let ((digit-4 (list-ref rem-3 nn)))
                         (let ((num-3 (+ (* 10 digit-3) digit-4)))
                           (let ((ll1 (sort (list num-1 num-3) <)))
                             (begin
                               (if (equal? (member ll1 rlist) #f)
                                   (begin
                                     (set! rlist (cons ll1 rlist))
                                     ))
                               ))
                           ))
                       ))
                   ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax rec-process-3-1-macro
  (syntax-rules ()
    ((rec-process-3-1-macro
      mm-max rem-2 digit-1 digit-2 ll1 rlist)
     (begin
       (do ((mm 0 (1+ mm)))
           ((>= mm mm-max))
         (begin
           (let ((digit-3 (list-ref rem-2 mm))
                 (list-remain-list (list-copy rem-2)))
             (let ((rem-3
                    (my-delete-first digit-3 list-remain-list)))
               (let ((digit-4 (list-ref rem-3 0)))
                 (let ((num-1
                        (+ (* 100 digit-1)
                           (* 10 digit-2) digit-3)))
                   (let ((ll1 (list num-1 digit-4)))
                     (begin
                       (if (equal? (member ll1 rlist) #f)
                           (begin
                             (set! rlist (cons ll1 rlist))
                             ))
                       ))
                   ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-input-nums-list special-number)
  (define (local-rec-process-2-1 num-list)
    (begin
      (let ((nlen (length num-list))
            (rlist (list)))
        (begin
          (do ((ii 0 (1+ ii)))
              ((>= ii nlen))
            (begin
              (let ((digit-1 (list-ref num-list ii))
                    (local-list (list-copy num-list)))
                (let ((rem-1 (my-delete-first digit-1 local-list)))
                  (let ((jj-len (length rem-1)))
                    (begin
                      (if (> digit-1 0)
                          (begin
                            (do ((jj 0 (1+ jj)))
                                ((>= jj jj-len))
                              (begin
                                (let ((digit-2 (list-ref rem-1 jj))
                                      (local-remain-list
                                       (list-copy rem-1)))
                                  (let ((rem-2
                                         (my-delete-first
                                          digit-2 local-remain-list)))
                                    (let ((num1
                                           (+ (* 10 digit-1) digit-2)))
                                      (let ((ll1
                                             (cons num1 (sort rem-2 >))))
                                        (begin
                                          (if (equal? (member ll1 rlist) #f)
                                              (begin
                                                (set! rlist (cons ll1 rlist))
                                                ))
                                          ))
                                      )))
                                ))
                            ))
                      ))
                  ))
              ))
          rlist
          ))
      ))
  (define (local-rec-process-2-2 num-list)
    (begin
      (let ((nlen (length num-list))
            (rlist (list)))
        (let ((ii-max (1- nlen)))
          (begin
            (do ((ii 0 (1+ ii)))
                ((>= ii ii-max))
              (begin
                (let ((digit-1 (list-ref num-list ii))
                      (local-list (list-copy num-list)))
                  (let ((rem-1 (my-delete-first digit-1 local-list)))
                    (let ((jj-len (length rem-1)))
                      (begin
                        (if (> digit-1 0)
                            (begin
                              (do ((jj 0 (1+ jj)))
                                  ((>= jj jj-len))
                                (begin
                                  (let ((digit-2 (list-ref rem-1 jj))
                                        (local-remain-list
                                         (list-copy rem-1)))
                                    (let ((rem-2
                                           (my-delete-first
                                            digit-2 local-remain-list))
                                          (num-1
                                           (+ (* 10 digit-1) digit-2)))
                                      (let ((mm-len (length rem-2)))
                                        (begin
                                          (rec-process-2-2-macro
                                           mm-len rem-2 num-1 ll1 rlist)
                                          ))
                                      ))
                                  ))
                              ))
                        ))
                    ))
                ))
            rlist
            )))
      ))
  (define (local-rec-process-3-1 num-list)
    (begin
      (let ((nlen (length num-list))
            (rlist (list)))
        (begin
          (do ((ii 0 (1+ ii)))
              ((>= ii nlen))
            (begin
              (let ((digit-1 (list-ref num-list ii))
                    (local-list (list-copy num-list)))
                (let ((rem-1 (my-delete-first digit-1 local-list)))
                  (let ((jj-len (length rem-1)))
                    (begin
                      (if (> digit-1 0)
                          (begin
                            (do ((jj 0 (1+ jj)))
                                ((>= jj jj-len))
                              (begin
                                (let ((digit-2 (list-ref rem-1 jj))
                                      (local-remain-list (list-copy rem-1)))
                                  (let ((rem-2
                                         (my-delete-first
                                          digit-2 local-remain-list)))
                                    (let ((mm-max (length rem-2)))
                                      (begin
                                        (rec-process-3-1-macro
                                         mm-max rem-2
                                         digit-1 digit-2 ll1 rlist)
                                        ))
                                    ))
                                ))
                            ))
                      ))
                  ))
              ))
          rlist
          ))
      ))
  (begin
    (let ((a0
           (digits-module:split-digits-list special-number)))
      (let ((a1 (local-rec-process-2-1 a0))
            (a2 (local-rec-process-2-2 a0))
            (a3 (local-rec-process-3-1 a0)))
        (begin
          (cons a0 (append a1 a2 a3))
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; input-list looks like (list num1 op1 num2 op2 num3 op3 num4)
(define (make-basic-infix-list input-nums-list input-ops-list)
  (define (local-rec-process num-list op-list curr-list acc-list)
    (begin
      (if (or (not (list? num-list))
              (<= (length num-list) 0))
          (begin
            (cons curr-list acc-list))
          (begin
            (for-each
             (lambda (this-num)
               (begin
                 (let ((next-num-list
                        (my-delete-first this-num num-list)))
                   (let ((tmp-curr-list (cons this-num curr-list))
                         (nlen (length next-num-list)))
                     (begin
                       (if (>= nlen 1)
                           (begin
                             (for-each
                              (lambda (this-op)
                                (begin
                                  (let ((next-curr-list
                                         (cons this-op tmp-curr-list)))
                                    (let ((final-acc-list
                                           (local-rec-process
                                            next-num-list op-list
                                            next-curr-list acc-list)))
                                      (begin
                                        (set! acc-list final-acc-list)
                                        )))
                                  )) op-list))
                           (begin
                             (let ((final-acc-list
                                    (local-rec-process
                                     next-num-list op-list
                                     tmp-curr-list acc-list)))
                               (begin
                                 (set! acc-list final-acc-list)
                                 ))
                             ))
                       )))
                 )) num-list)

            acc-list
            ))
      ))
  (begin
    (let ((basic-infix-list-list
           (local-rec-process
            input-nums-list input-ops-list (list) (list))))
      (begin
        basic-infix-list-list
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; result-hash-table - key=number, value=(list expression1 expression2,...)
(define (cycle-over-input-nums-ops
         input-nums-list input-ops-list input-unary-list
         min-number max-number special-number
         result-hash-table)
  (begin
    (let ((basic-infix-list-list
           (make-basic-infix-list
            input-nums-list input-ops-list)))
      (begin
        (for-each
         (lambda (this-infix-list)
           (begin
             (add-in-unary-ops-and-eval
              this-infix-list input-unary-list
              min-number max-number special-number
              result-hash-table)
             )) basic-infix-list-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (op-list-to-string prefix-list)
  (begin
    (let ((plen (length prefix-list))
          (rstring ""))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii plen))
          (begin
            (let ((a-elem (list-ref prefix-list ii)))
              (begin
                (cond
                 ((equal? a-elem +)
                  (begin
                    (set!
                     rstring
                     (string-append
                      rstring " + "))
                    ))
                 ((equal? a-elem -)
                  (begin
                    (set!
                     rstring
                     (string-append
                      rstring " - "))
                    ))
                 ((equal? a-elem *)
                  (begin
                    (set!
                     rstring
                     (string-append
                      rstring " * "))
                    ))
                 ((equal? a-elem /)
                  (begin
                    (set!
                     rstring
                     (string-append
                      rstring " / "))
                    ))
                 ((equal? a-elem expt)
                  (begin
                    (set!
                     rstring
                     (string-append
                      rstring " ^ "))
                    ))
                 ((number? a-elem)
                  (begin
                    (set!
                     rstring
                     (string-append
                      rstring
                      (format #f "~a" a-elem)))
                    ))
                 ((list? a-elem)
                  (begin
                    (let ((oper (list-ref a-elem 0))
                          (arg1 (list-ref a-elem 1)))
                      (begin
                        (cond
                         ((equal? oper factorial)
                          (begin
                            (set!
                             rstring
                             (string-append
                              rstring
                              (format
                               #f "(factorial ~a)" arg1)))
                            ))
                         ((equal? oper double-factorial)
                          (begin
                            (set!
                             rstring
                             (string-append
                              rstring
                              (format
                               #f "(double-factorial ~a)" arg1)))
                            ))
                         ((equal? oper sqrt)
                          (begin
                            (set!
                             rstring
                             (string-append
                              rstring
                              (format
                               #f "(sqrt ~a)" arg1)))
                            ))
                         (else
                          (begin
                            (if (symbol? oper)
                                (begin
                                  (set!
                                   rstring
                                   (string-append
                                    rstring
                                    (format
                                     #f "(~a ~a)"
                                     (symbol->string oper)
                                     arg1))))
                                (begin
                                  (set!
                                   rstring
                                   (string-append
                                    rstring
                                    (format #f "~a" a-elem)))
                                  ))
                            )))
                        ))
                    ))
                 (else
                  (begin
                    (if (symbol? a-elem)
                        (begin
                          (set!
                           rstring
                           (string-append
                            rstring
                            (format
                             #f "~a"
                             (symbol->string a-elem)))))
                        (begin
                          (set!
                           rstring
                           (string-append
                            rstring
                            (format
                             #f "~a" a-elem)))
                          ))
                    )))
                ))
            ))
        rstring
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (dump-result-htable
         result-hash-table min-number max-number)
  (begin
    (do ((ii min-number (1+ ii)))
        ((> ii max-number))
      (begin
        (let ((rlist (hash-ref result-hash-table ii #f)))
          (begin
            (if (list? rlist)
                (begin
                  (display
                   (format
                    #f "~a : ~a~%"
                    ii
                    (op-list-to-string rlist))))
                (begin
                  (display
                   (format
                    #f "~a : not found~%" ii))
                  ))
            (force-output)
            ))
        ))
    (newline)
    (force-output)
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop
         min-number max-number special-number details-flag)
  (begin
    (let ((result-hash-table (make-hash-table))
          (input-nums-list-list
           (make-input-nums-list special-number))
          (binary-ops-list (list + - * / expt))
          (unary-ops-list
           (list sqrt factorial double-factorial)))
      (begin
        (for-each
         (lambda (nums-list)
           (begin
             (cycle-over-input-nums-ops
              nums-list binary-ops-list unary-ops-list
              min-number max-number special-number
              result-hash-table)
             )) input-nums-list-list)

        (dump-result-htable
         result-hash-table min-number max-number)

        (let ((result-count 0))
          (begin
            (do ((ii min-number (1+ ii)))
                ((> ii max-number))
              (begin
                (let ((rlist (hash-ref result-hash-table ii #f)))
                  (begin
                    (if (list? rlist)
                        (begin
                          (set! result-count (1+ result-count))
                          ))
                    ))
                ))

            (newline)
            (display
             (ice-9-format:format
              #f "found ~:d numbers out of ~:d, between [~:d, ~:d]~%"
              result-count (+ 1 (- max-number min-number))
              min-number max-number))
            ))

        (let ((rlist
               (hash-ref
                result-hash-table special-number #f)))
          (begin
            (if (list? rlist)
                (begin
                  (display
                   (ice-9-format:format
                    #f "~:d : ~a~%" special-number rlist))
                  (force-output))
                (begin
                  (display
                   (ice-9-format:format
                    #f "~:d : not found~%" special-number))
                  (force-output)
                  ))
            ))

        (newline)
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
