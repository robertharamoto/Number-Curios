;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for poly-primes-module.scm                ###
;;;###                                                       ###
;;;###  last updated August 21, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

(use-modules ((poly-primes-module)
              :renamer (symbol-prefix-proc 'poly-primes-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (poly-primes-assert-lists-equal
         shouldbe-list result-list sub-name
         error-message result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((err-1
             (format #f "~a : shouldbe-list=~a, result-list=~a : "
                     error-message shouldbe-list result-list))
            (err-2
             (format #f "list sizes not equal, "))
            (err-3
             (format #f "shouldbe size=~a, result size=~a"
                     shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append err-1 err-2 err-3)
           result-hash-table)

          (for-each
           (lambda (s-elem)
             (begin
               (let ((found-flag (member s-elem result-list)))
                 (let ((err-4
                        (format #f "missing element=~a"
                                s-elem)))
                   (begin
                     (unittest2:assert?
                      (not (equal? found-flag #f))
                      sub-name
                      (string-append err-1 err-4)
                      result-hash-table)
                     )))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-load-powers-of-x-array-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-load-powers-of-x-array-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 3 (list 1 3))
           (list 3 3 (list 1 3 9))
           (list 4 3 (list 1 3 9 27))
           (list 5 3 (list 1 3 9 27 81))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((ndim (list-ref alist 0))
                  (xx (list-ref alist 1))
                  (shouldbe-list (list-ref alist 2)))
              (let ((an-array (make-array 0 ndim)))
                (begin
                  (poly-primes-module:load-powers-of-x-array
                   an-array ndim xx)

                  (let ((result-list (array->list an-array))
                        (err-1
                         (format #f "~a : error (~a) : "
                                 sub-name test-label-index))
                        (err-2
                         (format #f "ndim=~a, xx=~a : "
                                 ndim xx)))
                    (begin
                      (poly-primes-assert-lists-equal
                       shouldbe-list result-list
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-eval-polynomial-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-eval-polynomial-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 0 1) (list 1 3) 2 3)
           (list (list 1 2) (list 1 3) 2 7)
           (list (list 0 0 1) (list 1 3 9) 3 9)
           (list (list 0 1 1) (list 1 3 9) 3 12)
           (list (list 1 1 1) (list 1 3 9) 3 13)
           (list (list 1 0 0 0) (list 1 2 4 8) 4 1)
           (list (list 1 1 0 0) (list 1 2 4 8) 4 3)
           (list (list 1 1 1 0) (list 1 2 4 8) 4 7)
           (list (list 1 1 1 1) (list 1 2 4 8) 4 15)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((coeff-list (list-ref alist 0))
                  (x-list (list-ref alist 1))
                  (ndim (list-ref alist 2))
                  (shouldbe (list-ref alist 3)))
              (let ((coeff-array (list->array 1 coeff-list))
                    (x-array (list->array 1 x-list)))
                (let ((result
                       (poly-primes-module:eval-polynomial
                        coeff-array x-array ndim)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "ndim=~a, coeff-list=~a, x-list=~a : "
                          ndim coeff-list x-list))
                        (err-3
                         (format #f "shouldbe=~a, result=~a"
                                 shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2 err-3)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-polynomial-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-polynomial-to-string-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1 0) 2 "1")
           (list (list 0 1) 2 "x")
           (list (list 3 1) 2 "x + 3")
           (list (list 3 2) 2 "2*x + 3")
           (list (list 2 0 0) 3 "2")
           (list (list 0 4 0) 3 "4*x")
           (list (list 0 0 1) 3 "x^2")
           (list (list 0 0 5) 3 "5*x^2")
           (list (list 0 7 1) 3 "x^2 + 7*x")
           (list (list 0 9 5) 3 "5*x^2 + 9*x")
           (list (list 1 1 1) 3 "x^2 + x + 1")
           (list (list 7 9 5) 3 "5*x^2 + 9*x + 7")
           (list (list -1 1 1) 3 "x^2 + x - 1")
           (list (list -7 9 5) 3 "5*x^2 + 9*x - 7")
           (list (list -1 -1 -1) 3 "-x^2 - x - 1")
           (list (list -88 -7 -99) 3 "-99*x^2 - 7*x - 88")
           (list (list -7 9 -5) 3 "-5*x^2 + 9*x - 7")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((coeff-list (list-ref alist 0))
                  (ndim (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((coeff-array (list->array 1 coeff-list)))
                (let ((result
                       (poly-primes-module:polynomial-to-string
                        coeff-array ndim)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "ndim=~a, coeff-list=~a : "
                          ndim coeff-list))
                        (err-3
                         (format #f "shouldbe=~s, result=~s"
                                 shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2 err-3)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-valid-polynomial-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-is-valid-polynomial-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1 0) 2 #f)
           (list (list 2 0 0) 3 #f)
           (list (list 2 0 1) 3 #t)
           (list (list 41 1 0) 3 #t)
           (list (list 41 0 -1) 3 #t)
           (list (list 41 0 0 -1) 4 #t)
           (list (list 41 0 0 0) 4 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((coeff-list (list-ref alist 0))
                  (ndim (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((coeff-array (list->array 1 coeff-list)))
                (let ((result
                       (poly-primes-module:is-valid-polynomial?
                        coeff-array ndim)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "ndim=~a, coeff-list=~a : "
                          ndim coeff-list))
                        (err-3
                         (format #f "shouldbe=~a, result=~a"
                                 shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2 err-3)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-consecutive-distinct-primes-generated-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-consecutive-distinct-primes-generated-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1 0) 2 0)
           (list (list 2 0) 2 0)
           (list (list 2 1) 2 2)
           (list (list 41 1 1) 3 40)
           (list (list 41 -1 1) 3 41)
           (list (list 29 0 2) 3 29)
           (list (list 101 0 29 0 1) 5 20)
           (list (list 37 39 3) 3 18)
           (list (list 17 1 1) 3 16)
           (list (list 59 4 4) 3 14)
           (list (list 11 0 2) 3 11)
           (list (list 17 0 1 1) 4 11)
           ))
         (prime-htable (make-hash-table 1000))
         (test-label-index 0))
     (let ((max-prime
            (prime-module:populate-prime-hash!
             prime-htable 1000)))
       (begin
         (for-each
          (lambda (alist)
            (begin
              (let ((coeff-list (list-ref alist 0))
                    (ndim (list-ref alist 1))
                    (shouldbe (list-ref alist 2)))
                (let ((coeff-array (list->array 1 coeff-list)))
                  (let ((result
                         (poly-primes-module:consecutive-distinct-primes-generated
                          coeff-array ndim max-prime prime-htable)))
                    (let ((err-1
                           (format
                            #f "~a : error (~a) : "
                            sub-name test-label-index))
                          (err-2
                           (format
                            #f "ndim=~a, coeff-list=~a : "
                            ndim coeff-list))
                          (err-3
                           (format #f "shouldbe=~a, result=~a"
                                   shouldbe result)))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe result)
                         sub-name
                         (string-append err-1 err-2 err-3)
                         result-hash-table)
                        )))
                  ))

              (set! test-label-index (1+ test-label-index))
              )) test-list)
         )))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
