#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  finding polynomials that generate primes             ###
;;;###                                                       ###
;;;###  last updated August 21, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "." "sources" "tests"))

(set! %load-compiled-path
      (append
       %load-compiled-path (list "." "objects")))

;;;#############################################################
;;;#############################################################
;;;### regex used for regex
(use-modules ((ice-9 regex)
              :renamer (symbol-prefix-proc 'ice-9-regex:)))

;;;### getopt-long used for command-line option arguments processing
(use-modules ((ice-9 getopt-long)
              :renamer (symbol-prefix-proc 'ice-9-getopt:)))

;;;### ice-9 format to automatically commafy using format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

(use-modules ((config-module)
              :renamer (symbol-prefix-proc 'config-module:)))

(use-modules ((poly-primes-module)
              :renamer (symbol-prefix-proc 'poly-primes-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin main support functions                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define (load-up-config-file
         args default-config-file
         config-htable comments-htable)
  (begin
    (let ((cf-name ""))
      (begin
        (if (and (not (equal? args #f))
                 (> (length args) 1))
            (begin
              (let ((found-flag #f)
                    (arg-fname ""))
                (begin
                  (for-each
                   (lambda (an-arg)
                     (begin
                       (if (file-exists? an-arg)
                           (begin
                             (let ((amatch
                                    (ice-9-regex:string-match
                                     ".scm" an-arg)))
                               (begin
                                 (if (equal? amatch #f)
                                     (begin
                                       (set! arg-fname an-arg)
                                       (set! found-flag #t)
                                       ))
                                 ))
                             ))
                       )) args)

                  (if (equal? found-flag #t)
                      (begin
                        (set! cf-name arg-fname)
                        (config-module:read-config-file
                         arg-fname config-htable comments-htable))
                      (begin
                        (if (file-exists? default-config-file)
                            (begin
                              (set! cf-name default-config-file)
                              (config-module:read-config-file
                               default-config-file
                               config-htable comments-htable))
                            (begin
                              (set! cf-name "")
                              (hash-clear! config-htable)
                              (hash-clear! comments-htable)
                              ))
                        ))
                  )))
            (begin
              ;;; no arguments typed
              (if (file-exists? default-config-file)
                  (begin
                    (set! cf-name default-config-file)
                    (config-module:read-config-file
                     default-config-file
                     config-htable comments-htable))
                  (begin
                    (set! cf-name "")
                    (hash-clear! config-htable)
                    (hash-clear! comments-htable)
                    ))
              ))

        cf-name
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax update-variable-macro
  (syntax-rules ()
    ((update-variable-macro
      var-name var-symbol options)
     (begin
       (let ((var-flag
              (ice-9-getopt:option-ref
               options var-symbol #f)))
         (begin
           (if (not (equal? var-flag #f))
               (begin
                 (let ((var2-flag
                        (ice-9-regex:regexp-substitute/global
                         #f "," var-flag 'pre "" 'post)))
                   (begin
                     (if (number?
                          (string->number var2-flag))
                         (begin
                           (set!
                            var-name
                            (string->number var2-flag)))
                         (begin
                           (set! var-name var2-flag)
                           ))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax update-flag-macro
  (syntax-rules ()
    ((update-flag-macro
      var-name var-symbol options)
     (begin
       (if (string? var-name)
           (begin
             (if (string-ci=? var-name "true")
                 (begin
                   (set! var-name #t))
                 (begin
                   (set! var-name #f)
                   )))
           (begin
             (set! var-name #f)
             ))

       (let ((var-flag
              (ice-9-getopt:option-ref options var-symbol #f)))
         (begin
           (if (equal? var-flag #t)
               (begin
                 (set! var-name #t)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  main code                                            ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define (main args)
  (define (local-display-version title-string)
    (begin
      (display (format #f "~a~%" title-string))
      (force-output)
      ))
  (define (local-display-help ospec title-string)
    (begin
      (display (format #f "~a~%" title-string))
      (display (format #f "options available~%"))
      (for-each
       (lambda (llist)
         (begin
           (display
            (format
             #f "  --~a, -~a~%" (car llist) (cadr (cadr llist))))
           )) ospec)
      (force-output)
      ))
  (begin
    (let ((version-string "2024-08-21"))
      (let ((title-string
             (format
              #f "Finding prime generating polynomials (version ~a)"
              version-string))
            (option-spec
             (list (list 'version '(single-char #\v) '(value #f))
                   (list 'help '(single-char #\h) '(value #f))
                   (list 'config-file '(single-char #\c) '(value #t))
                   (list 'min-coeff '(single-char #\n) '(value #t))
                   (list 'max-coeff '(single-char #\m) '(value #t))
                   (list 'restart-0-coeff '(single-char #\r) '(value #t))
                   (list 'ndim '(single-char #\d) '(value #t))
                   (list 'min-print '(single-char #\p) '(value #t))
                   (list 'max-prime '(single-char #\x) '(value #t))
                   (list 'status-minutes '(single-char #\s) '(value #t))
                   )))
        (let ((def-config-file "init0.config")
              (config-htable (make-hash-table 10))
              (comments-htable (make-hash-table 10))
              (options
               (ice-9-getopt:getopt-long args option-spec)))
          (begin
            (let ((help-flag
                   (ice-9-getopt:option-ref options 'help #f)))
              (begin
                (if (equal? help-flag #t)
                    (begin
                      (local-display-help option-spec title-string)
                      (quit)
                      ))
                ))
            (let ((version-flag
                   (ice-9-getopt:option-ref options 'version #f)))
              (begin
                (if (equal? version-flag #t)
                    (begin
                      (local-display-version title-string)
                      (quit)
                      ))
                ))

            (let ((config-file
                   (ice-9-getopt:option-ref options 'config-file #f)))
              (begin
                (if (and (not (equal? config-file #f))
                         (file-exists? config-file))
                    (begin
                      (config-module:read-config-file
                       config-file config-htable comments-htable))
                    (begin
                      (let ((result-config
                             (load-up-config-file
                              args def-config-file
                              config-htable comments-htable)))
                        (begin
                          (set! config-file result-config)
                          ))
                      ))

                ;;;### let command line options override config file
                (let ((min-coeff (hash-ref config-htable "min-coeff" -100))
                      (max-coeff (hash-ref config-htable "max-coeff" 100))
                      (restart-0-coeff
                       (hash-ref config-htable "restart-0-coeff" 2))
                      (ndim (hash-ref config-htable "ndim" 5))
                      (min-print (hash-ref config-htable "min-print" 10))
                      (max-prime (hash-ref config-htable "max-prime" 1000))
                      (status-minutes
                       (hash-ref config-htable "status-minutes" 60)))
                  (begin
                    (update-variable-macro
                     min-coeff 'min-coeff options)

                    (update-variable-macro
                     max-coeff 'max-coeff options)

                    (update-variable-macro
                     restart-0-coeff 'restart-0-coeff options)

                    (update-variable-macro
                     ndim 'ndim options)

                    (update-variable-macro
                     min-print 'min-print options)

                    (update-variable-macro
                     max-prime 'max-prime options)

                    (update-variable-macro
                     status-minutes 'status-minutes options)

                    (display (format #f "~a~%" title-string))
                    (display
                     (ice-9-format:format
                      #f "coefficients range from ~:d to ~:d, "
                      min-coeff max-coeff))
                    (display
                     (ice-9-format:format
                      #f "restart-0 from ~:d~%"
                      restart-0-coeff))
                    (display
                     (ice-9-format:format
                      #f "highest power of polynomial = ~:d : "
                      (1- ndim)))
                    (display
                     (ice-9-format:format
                      #f "min print = ~:d~%"
                      min-print))
                    (display
                     (ice-9-format:format
                      #f "max prime = ~:d : status every "
                      max-prime))
                    (display
                     (ice-9-format:format
                      #f "~:d minutes : ~a~%"
                      status-minutes
                      (timer-module:current-date-time-string)))
                    (force-output)

                    (timer-module:time-code-macro
                     (begin
                       (poly-primes-module:main-loop
                        ndim min-coeff max-coeff restart-0-coeff
                        min-print max-prime status-minutes)
                       (newline)
                       ))

                    (newline)
                    (force-output)
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
