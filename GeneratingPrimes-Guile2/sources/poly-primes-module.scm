;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  generating primes with polynomials and integer       ###
;;;###  coefficients                                         ###
;;;###                                                       ###
;;;###  last updated August 21, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start poly-primes modules

(define-module (poly-primes-module)
  #:export (load-powers-of-x-array
            eval-polynomial
            polynomial-to-string

            is-valid-polynomial?
            consecutive-distinct-primes-generated

            main-loop
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### prime-module for is-array-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;#############################################################
;;;#############################################################
(define (load-powers-of-x-array xarray ndim xx)
  (begin
    (let ((this-power 1))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii ndim))
          (begin
            (array-set! xarray this-power ii)
            (set! this-power (* this-power xx))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (eval-polynomial coeff-array xarray ndim)
  (begin
    (let ((sum 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii ndim))
          (begin
            (let ((this-term
                   (* (array-ref coeff-array ii)
                      (array-ref xarray ii))))
              (begin
                (set! sum (+ sum this-term))
                ))
            ))
        sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax pstring-coeff-macro
  (syntax-rules ()
    ((pstring-coeff-macro
      ii acoeff
      result-list nterms)
     (begin
       (cond
        ((= ii 0)
         (begin
           (if (not (= acoeff 0))
               (begin
                 (set!
                  result-list
                  (cons
                   (if (< acoeff 0)
                       (begin
                         "-")
                       (begin
                         "+"
                         ))
                   (cons
                    (ice-9-format:format
                     #f "~:d" (abs acoeff))
                    result-list)))
                 (set! nterms (1+ nterms))
                 ))
           ))
        ((= ii 1)
         (begin
           (cond
            ((or (= acoeff 1)
                 (= acoeff -1))
             (begin
               (set!
                result-list
                (cons
                 (if (< acoeff 0)
                     (begin
                       "-")
                     (begin
                       "+"
                       ))
                 (cons
                  "x"
                  result-list)))
               (set! nterms (1+ nterms))
               ))
            ((not (= acoeff 0))
             (begin
               (set!
                result-list
                (cons
                 (if (< acoeff 0)
                     (begin
                       "-")
                     (begin
                       "+"
                       ))
                 (cons
                  (ice-9-format:format
                   #f "~:d*x" (abs acoeff))
                  result-list)))
               (set! nterms (1+ nterms))
               )))
           ))
        (else
         (begin
           (cond
            ((or (= acoeff 1)
                 (= acoeff -1))
             (begin
               (set!
                result-list
                (cons
                 (if (< acoeff 0)
                     (begin
                       "-")
                     (begin
                       "+"
                       ))
                 (cons
                  (ice-9-format:format
                   #f "x^~:d" ii)
                  result-list)))
               (set! nterms (1+ nterms))
               ))
            ((not (= acoeff 0))
             (begin
               (set!
                result-list
                (cons
                 (if (< acoeff 0)
                     (begin
                       "-")
                     (begin
                       "+"
                       ))
                 (cons
                  (ice-9-format:format
                   #f "~:d*x^~:d" (abs acoeff) ii)
                  result-list)))
               (set! nterms (1+ nterms))
               )))
           )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (polynomial-to-string coeff-array ndim)
  (begin
    (let ((result-string ""))
      (begin
        (cond
         ((>= ndim 1)
          (begin
            (let ((result-list (list))
                  (nterms 0))
              (begin
                (do ((ii 0 (1+ ii)))
                    ((>= ii ndim))
                  (begin
                    (let ((acoeff (array-ref coeff-array ii)))
                      (begin
                        (pstring-coeff-macro
                         ii acoeff
                         result-list nterms)
                        ))
                    ))
                (cond
                 ((= nterms 0)
                  (begin
                    result-string
                    ))
                 ((= nterms 1)
                  (begin
                    (let ((first-sign (car result-list))
                          (second-term (cadr result-list)))
                      (begin
                        (if (equal? first-sign "-")
                            (begin
                              (format
                               #f "~a~a" first-sign second-term))
                            (begin
                              second-term
                              ))
                        ))
                    ))
                 (else
                  (begin
                    (let ((nitems (length result-list))
                          (first-sign (list-ref result-list 0))
                          (first-term (list-ref result-list 1))
                          (second-sign (list-ref result-list 2)))
                      (let ((final-list
                             (list-tail
                              (list-head result-list nitems) 3)))
                        (begin
                          (if (equal? first-sign "-")
                              (begin
                                (format
                                 #f "~a~a ~a ~a"
                                 first-sign first-term
                                 second-sign
                                 (string-join final-list " ")))
                              (begin
                                (format
                                 #f "~a ~a ~a"
                                 first-term
                                 second-sign
                                 (string-join final-list " "))
                                ))
                          )))
                    )))
                ))
            ))
         (else
          (begin
            ""
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; don't allow polynomial with just the zeroth power of x
(define (is-valid-polynomial? coeff-array ndim)
  (begin
    (let ((valid-flag #f))
      (begin
        (if (<= ndim 1)
            (begin
              (set! valid-flag #f))
            (begin
              (do ((ii 1 (1+ ii)))
                  ((or (>= ii ndim)
                       (equal? valid-flag #t)))
                (begin
                  (if (not (zero? (array-ref coeff-array ii)))
                      (begin
                        (set! valid-flag #t)
                        ))
                  ))
              ))
        valid-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; count number of distinct primes generated
;;; note: allows for duplicates
(define (consecutive-distinct-primes-generated
         coeff-array ndim max-prime prime-htable)
  (begin
    (let ((ncount 0)
          (nn 0)
          (valid-flag (is-valid-polynomial? coeff-array ndim))
          (done-flag #f))
      (begin
        (if (equal? valid-flag #t)
            (begin
              (let ((x-array (make-array 0 ndim)))
                (begin
                  (while (equal? done-flag #f)
                    (begin
                      (load-powers-of-x-array x-array ndim nn)
                      (let ((anum
                             (eval-polynomial coeff-array x-array ndim)))
                        (begin
                          (if (prime-module:lookup-is-prime?
                               anum max-prime prime-htable)
                              (begin
                                (set! ncount (1+ ncount))
                                (set! nn (1+ nn)))
                              (begin
                                (set! done-flag #t)
                                ))
                          ))
                      ))
                  ))
              ))
        ncount
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax print-results-macro
  (syntax-rules ()
    ((print-results-macro
      coeff-array ndim ncount max-prime prime-htable)
     (begin
       (let ((pstring
              (polynomial-to-string coeff-array ndim))
             (x-array (make-array 0 ndim)))
         (begin
           (display
            (ice-9-format:format
             #f "p(x) = ~a : generates ~:d primes~%"
             pstring ncount))
           (do ((nn 0 (1+ nn)))
               ((> nn ncount))
             (begin
               (load-powers-of-x-array x-array ndim nn)
               (let ((nresult
                      (eval-polynomial coeff-array x-array ndim)))
                 (begin
                   (if (prime-module:lookup-is-prime?
                        nresult max-prime prime-htable)
                       (begin
                         (display
                          (ice-9-format:format
                           #f "p(~:d) = ~:d prime"
                           nn nresult)))
                       (begin
                         (display
                          (ice-9-format:format
                           #f "p(~:d) = ~:d not prime"
                           nn nresult))
                         ))

                   (if (zero? (modulo (1+ nn) 3))
                       (begin
                         (newline))
                       (begin
                         (if (< nn ncount)
                             (begin
                               (display " ; "))
                             (begin
                               (newline)
                               ))
                         ))
                   (force-output)
                   ))
               ))

           (newline)
           (force-output)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-next-level-macro
  (syntax-rules ()
    ((process-next-level-macro
      coeff-array cc this-dim next-dim ndim
      min-coeff max-coeff min-print
      max-prime prime-htable
      start-jday status-minutes)
     (begin
       (array-set! coeff-array cc this-dim)
       (let ((next-start-jday
              (iterate-over-coefficients
               next-dim ndim coeff-array
               min-coeff max-coeff min-print
               max-prime prime-htable
               start-jday status-minutes)))
         (begin
           (set! start-jday next-start-jday)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax status-message-macro
  (syntax-rules ()
    ((status-message-macro
      status-minutes coeff-array start-jday)
     (begin
       (let ((end-jday (srfi-19:current-julian-day)))
         (let ((nminutes
                (timer-module:julian-day-difference-in-minutes
                 end-jday start-jday)))
           (begin
             (if (>= nminutes status-minutes)
                 (begin
                   (display
                    (ice-9-format:format
                     #f "current-array=~a : ~:d minutes : ~a~%"
                     (array->list coeff-array) nminutes
                     (timer-module:current-date-time-string)))
                   (force-output)
                   (set! start-jday end-jday)
                   (gc)
                   ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; iterate, if find a solution, just print it out
(define (iterate-over-coefficients
         this-dim ndim coeff-array
         min-coeff max-coeff min-print
         max-prime prime-htable
         start-jday status-minutes)
  (begin
    (if (>= this-dim ndim)
        (begin
          ;;; if we get here, then coeff-array is fully populated
          (let ((ncount
                 (consecutive-distinct-primes-generated
                  coeff-array ndim max-prime prime-htable)))
            (begin
              (if (>= ncount min-print)
                  (begin
                    (print-results-macro
                     coeff-array ndim ncount
                     max-prime prime-htable)
                    ))
              start-jday
              )))
        (begin
          (let ((next-dim (1+ this-dim)))
            (begin
              (do ((cc min-coeff (1+ cc)))
                  ((> cc max-coeff))
                (begin
                  (process-next-level-macro
                   coeff-array cc this-dim next-dim ndim
                   min-coeff max-coeff min-print
                   max-prime prime-htable
                   start-jday status-minutes)

                  (if (zero? (modulo cc 10))
                      (begin
                        (status-message-macro
                         status-minutes coeff-array start-jday)
                        ))
                  ))

              start-jday
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop
         ndim min-coeff max-coeff restart-0-coeff
         min-print max-prime status-minutes)
  (begin
    (let ((coeff-array (make-array 0 ndim))
          (prime-htable (make-hash-table max-prime))
          (start-jday (srfi-19:current-julian-day))
          (this-dim 0)
          (next-dim 1))
      (let ((max-htable-prime
             (prime-module:populate-prime-hash!
              prime-htable max-prime)))
        (begin
          (do ((cc restart-0-coeff (1+ cc)))
              ((> cc max-coeff))
            (begin
              (if (and (> cc 0)
                       (prime-module:lookup-is-prime?
                        cc max-htable-prime prime-htable))
                  (begin
                    (process-next-level-macro
                     coeff-array cc this-dim next-dim ndim
                     min-coeff max-coeff min-print
                     max-htable-prime prime-htable
                     start-jday status-minutes)
                    ))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
