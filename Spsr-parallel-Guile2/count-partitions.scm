#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  count number of partitions for a given width         ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;###  last updated June 15, 2022                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "sources" "tests" "."))

(set! %load-compiled-path
      (append %load-compiled-path
              (list ".")))

;;;### srfi-11 for let-values
(use-modules ((srfi srfi-11)
              :renamer (symbol-prefix-proc 'srfi-11:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;;### successor-of-list returns the next largest list subject to
;;;### the sum of the numbers equal to the input-target-sum
;;;### previous list goes from least significant digit to most significant
;;;### uses the algorithm discussed at
;;;### https://www.geeksforgeeks.org/generate-unique-partitions-of-an-integer/
(define (successor-of-partition-list
         previous-list)
  (begin
    (let ((remaining-value 0)
          (last-index (1- (length previous-list)))
          (max-len (length previous-list))
          (result-list (list-copy previous-list)))
      (begin
        ;;;### count the number of ones
        (while
            (and
             (>= last-index 0)
             (equal?
              (list-ref previous-list last-index) 1))
          (begin
            (let ((this-element
                   (list-ref previous-list last-index)))
              (begin
                (set!
                 remaining-value
                 (+ remaining-value
                    this-element))
                (set! last-index (1- last-index))
                ))
            ))

        (if (< last-index 0)
            (begin
              (list))
            (begin
              (list-set!
               result-list
               last-index
               (1- (list-ref result-list last-index)))
              (set! remaining-value (1+ remaining-value))

              ;;;### decrease the value at last-index by 1
              ;;;### and increase remaining-value by 1
              (let ((this-element
                     (list-ref result-list last-index)))
                (begin
                  (while (> remaining-value
                            this-element)
                    (begin
                      (let ((next-index (1+ last-index)))
                        (begin
                          (if (< next-index max-len)
                              (begin
                                (list-set!
                                 result-list
                                 next-index
                                 this-element))
                              (begin
                                (set!
                                 result-list
                                 (append result-list (list this-element)))
                                ))
                          (set!
                           remaining-value
                           (- remaining-value this-element))
                          (set!
                           last-index
                           (1+ last-index))
                          (set!
                           this-element
                           (list-ref result-list last-index))
                          ))
                      ))

                  (let ((next-index (1+ last-index)))
                    (begin
                      (if (< next-index max-len)
                          (begin
                            (list-set!
                             result-list
                             (1+ last-index)
                             remaining-value))
                          (begin
                            (set!
                             result-list
                             (append result-list (list remaining-value)))
                            ))
                      ))
                  (set!
                   last-index
                   (1+ last-index))
                  ))
              ))

        (set!
         result-list
         (list-head result-list (1+ last-index)))

        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-successor-of-partition-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-successor-of-partition-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2) (list 1 1))
           (list (list 3) (list 2 1))
           (list (list 2 1) (list 1 1 1))
           (list (list 4) (list 3 1))
           (list (list 3 1) (list 2 2))
           (list (list 2 2) (list 2 1 1))
           (list (list 2 1 1) (list 1 1 1 1))
           (list (list 5) (list 4 1))
           (list (list 4 1) (list 3 2))
           (list (list 3 2) (list 3 1 1))
           (list (list 3 1 1) (list 2 2 1))
           (list (list 2 2 1) (list 2 1 1 1))
           (list (list 2 1 1 1) (list 1 1 1 1 1))
           (list (list 1 1 1 1 1) (list))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (successor-of-partition-list
                      input-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : input=~a : "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (partition-list-contains-duplicates?
         a-partition-list)
  (begin
    (let ((dup-htable (make-hash-table 10))
          (result-flag #f)
          (alen (length a-partition-list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((or (>= ii alen)
                 (equal? result-flag #t)))
          (begin
            (let ((an-elem (list-ref a-partition-list ii)))
              (let ((dflag (hash-ref dup-htable an-elem #f)))
                (begin
                  (if (equal? dflag #f)
                      (begin
                        (hash-set! dup-htable an-elem 1))
                      (begin
                        (set! result-flag #t)
                        ))
                  )))
            ))
        result-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-partition-list-contains-duplicates-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-partition-list-contains-duplicates-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2 3 4) #f)
           (list (list 2 3 4 2) #t)
           (list (list 2 3 4 3) #t)
           (list (list 2 3 4 5 6) #f)
           (list (list 2 3 4 7 9 7) #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((partition-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (partition-list-contains-duplicates?
                      partition-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : partition-list=~a : "
                        sub-name test-label-index partition-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;;### loop over all partitions of a number
(define (count-all-partitions-row-0 max-width)
  (begin
    (let ((continue-partition-loop-flag #t)
          (examine-count 0)
          (total-count 0)
          (next-partition-list (list max-width)))
      (begin
        (while (equal? continue-partition-loop-flag #t)
          (begin
            (if (and
                 (list? next-partition-list)
                 (> (length next-partition-list) 0)
                 (equal? (member 1 next-partition-list) #f))
                (begin
                  (if (and
                       (equal? (member 2 next-partition-list) #f)
                       (equal? (member 3 next-partition-list) #f))
                      (begin
                        (let ((aflag
                               (partition-list-contains-duplicates?
                                next-partition-list))
                              (bflag
                               (< (car next-partition-list) max-width)))
                          (begin
                            (if (and (equal? aflag #f)
                                     (equal? bflag #t))
                                (begin
                                  (set! examine-count (1+ examine-count))
                                  ))
                            ))
                        ))
                  ))

            (let ((next-next-partition-list
                   (successor-of-partition-list
                    (sort next-partition-list >))))
              (begin
                ;;;### note: successor-of-partition-list requires
                ;;;### a sorted list in descending order
                (set!
                 next-partition-list
                 next-next-partition-list)

                (if (<= (length next-partition-list) 0)
                    (begin
                      (set! continue-partition-loop-flag #f))
                    (begin
                      (set! total-count (1+ total-count))
                      ))
                ))
            ))

        (list examine-count total-count)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop
         start-width end-width)
  (begin
    ;;;### cycle over different array sizes
    (do ((jj-max-width start-width (1+ jj-max-width)))
        ((> jj-max-width end-width))
      (begin
        (let ((result-list
               (count-all-partitions-row-0
                jj-max-width)))
          (let ((examine-count (list-ref result-list 0))
                (total-count (list-ref result-list 1)))
          (begin
            (display
             (ice-9-format:format
              #f "[ ~:d ] :: examine = ~:d : total = ~:d~%"
              jj-max-width examine-count total-count))
            (force-output)
            )))
        ))
    (newline)
    (force-output)
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2022-06-15"))
      (let ((title-string
             (format #f "Count partitions (version ~a)"
                     version-string)))
        (begin
          (display
           (format #f "~a~%" title-string))
          (display
           (format #f "used to estimate amount of time per width~%"))
          (newline)

          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #t))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "tests/timer-module-test-1.scm")
                 (load "tests/utils-module-test-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (display (format #f "Output:~%"))
          (newline)
          (force-output)

          (let ((start-width 2)
                (max-width 100))
            (begin
              (timer-module:time-code-macro
               (begin
                 (main-loop start-width max-width)
                 ))
              ))

          (newline)
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
