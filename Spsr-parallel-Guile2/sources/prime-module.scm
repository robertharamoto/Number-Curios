;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  prime-module - prime functions                       ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 4, 2022                                  ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start prime modules
(define-module (prime-module)
  #:export (smallest-divisor
            is-prime?
            lookup-is-prime?
            populate-prime-hash!

            make-prime-array
            binary-search
            is-array-prime?

            is-consecutive-prime?
            make-2-consecutive-prime-numbers
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###  is-prime? - predicate to find prime numbers
;;;###              takes a number, returns a bool
(define (smallest-divisor nn)
  (define (local-find-divisor nn test-divisor last-num)
    (begin
      (let ((result -1)
            (done-flag #f))
        (begin
          (if (>= test-divisor last-num)
              (begin
                (set! result nn)
                (set! done-flag #t)
                ))

          (do ((jj test-divisor (+ jj 2)))
              ((or (> jj last-num)
                   (equal? done-flag #t)))
            (begin
              (if (> jj last-num)
                  (begin
                    (set! result nn)
                    (set! done-flag #t)
                    ))
              (if (zero? (modulo nn jj))
                  (begin
                    (set! result jj)
                    (set! done-flag #t)
                    ))
              ))
          (if (< result 0)
              (begin
                (set! result nn)
                ))
          result
          ))
      ))
  (begin
    ;;; main program
    (if (zero? (modulo nn 2))
        (begin
          2)
        (begin
          (let ((last-num (+ (exact-integer-sqrt nn) 1)))
            (begin
              (local-find-divisor nn 3 last-num)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-prime? nn)
  (begin
    (cond
     ((<= nn 1)
      (begin
        #f
        ))
     ((or
       (= nn 2)
       (= nn 3)
       (= nn 5)
       (= nn 7)
       (= nn 11))
      (begin
        #t
        ))
     ((or
       (zero? (modulo nn 2))
       (zero? (modulo nn 3))
       (zero? (modulo nn 5))
       (zero? (modulo nn 7))
       (zero? (modulo nn 11)))
      (begin
        #f
        ))
     (else
      (begin
        (= nn (smallest-divisor nn))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (lookup-is-prime?
         this-num max-prime prime-htable)
  (begin
    (if (<= this-num max-prime)
        (begin
          (hash-ref prime-htable this-num #f))
        (begin
          (is-prime? this-num)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (populate-prime-hash! prime-htable max-num)
  (begin
    (let ((max-prime 2))
      (begin
        (hash-set! prime-htable 2 #t)
        (do ((ii 3 (+ ii 2)))
            ((> ii max-num))
          (begin
            (if (is-prime? ii)
                (begin
                  (hash-set! prime-htable ii #t)
                  (set! max-prime ii)
                  ))
            ))

        max-prime
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; make a list of primes less than or equal to n
;;; sieve of eratosthenes method
(define (make-prime-array max-num)
  (begin
    (let ((intermediate-array
           (make-array 0 (1+ max-num)))
          (result-list (list))
          (missing-flag -1))
      (begin
        (do ((ii 0 (1+ ii)))
            ((> ii max-num))
          (begin
            (array-set! intermediate-array ii ii)
            ))

        (do ((ii 2 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((ii-num
                   (array-ref intermediate-array ii)))
              (begin
                (if (not (equal? ii-num missing-flag))
                    (begin
                      (set! result-list (cons ii result-list))

                      (do ((jj (+ ii ii) (+ jj ii)))
                          ((> jj max-num))
                        (begin
                          (array-set!
                           intermediate-array missing-flag jj)
                          ))
                      ))
                ))
            ))
        (list->array 1 (reverse result-list))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (binary-search prime-array arr-size num)
  (begin
    (let ((lower 0)
          (mid (euclidean/ arr-size 2))
          (upper (1- arr-size))
          (result #f))
      (begin
        (while (< lower upper)
          (begin
            (set! mid (euclidean/ (+ lower upper) 2))
            (let ((a-mid
                   (array-ref prime-array mid)))
              (begin
                (if (> num a-mid)
                    (begin
                      (set! lower (1+ mid)))
                    (begin
                      (if (= num a-mid)
                          (begin
                            (set! lower mid)
                            (set! upper mid))
                          (begin
                            (set! upper mid)
                            ))
                      ))
                ))
            ))

        (let ((final-mid (euclidean/ (+ lower upper) 2)))
          (let ((a-mid (array-ref prime-array final-mid)))
            (begin
              (if (equal? num a-mid)
                  (begin
                    (set! result final-mid)
                    ))

              result
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax check-within-array-macro
  (syntax-rules ()
    ((check-within-array-macro
      nn prime-array asize max-divisor
      aprime is-prime-flag continue-loop-flag)
     (begin
       (do ((ii 0 (1+ ii)))
           ((or (>= ii asize)
                (> aprime max-divisor)
                (equal? continue-loop-flag #f)))
         (begin
           (set! aprime (array-ref prime-array ii))

           (if (zero? (modulo nn aprime))
               (begin
                 (set! is-prime-flag #f)
                 (set! continue-loop-flag #f)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-array-prime? nn prime-array)
  (begin
    (cond
     ((<= nn 1)
      (begin
        #f
        ))
     ((= nn 2)
      (begin
        #t
        ))
     ((even? nn)
      (begin
        #f
        ))
     (else
      (begin
        (let ((max-divisor
               (+ (exact-integer-sqrt nn) 1))
              (asize (car (array-dimensions prime-array))))
          (let ((max-arr-prime
                 (array-ref prime-array (1- asize))))
            (begin
              (cond
               ((<= nn max-arr-prime)
                (begin
                  (let ((b-result
                         (binary-search prime-array asize nn)))
                    (begin
                      (if (equal? b-result #f)
                          (begin
                            #f)
                          (begin
                            #t
                            ))
                      ))
                  ))
               ((<= max-divisor max-arr-prime)
                (begin
                  (let ((continue-loop-flag #t)
                        (aprime 2)
                        (is-prime-flag #t))
                    (begin
                      (check-within-array-macro
                       nn prime-array asize max-divisor
                       aprime is-prime-flag continue-loop-flag)

                      is-prime-flag
                      ))
                  ))
               (else
                (begin
                  (let ((continue-loop-flag #t)
                        (aprime 2)
                        (is-prime-flag #t))
                    (begin
                      (check-within-array-macro
                       nn prime-array asize max-divisor
                       aprime is-prime-flag continue-loop-flag)

                      (if (equal? is-prime-flag #t)
                          (begin
                            (let ((continue-loop-flag #t))
                              (begin
                                (do ((ii (+ max-arr-prime 2) (+ ii 2)))
                                    ((or (> ii max-divisor)
                                         (equal? continue-loop-flag #f)))
                                  (begin
                                    (if (zero? (modulo nn ii))
                                        (begin
                                          (set! is-prime-flag #f)
                                          (set! continue-loop-flag #f)
                                          ))
                                    ))
                                ))
                            ))

                      is-prime-flag
                      ))
                  )))
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
;;; assume prime-1 and prime-2 are primes, and prime-1 < prime-2
(define (is-consecutive-prime? prime-1 prime-2)
  (begin
    (if (= prime-1 2)
        (begin
          (if (= prime-2 3)
              (begin
                #t)
              (begin
                #f
                )))
        (begin
          (if (equal? (- prime-2 prime-1) 2)
              (begin
                #t)
              (begin
                (let ((ok-flag #t))
                  (begin
                    (do ((ii (+ prime-1 2) (+ ii 2)))
                        ((or (>= ii prime-2)
                             (equal? ok-flag #f)))
                      (begin
                        (if (is-prime? ii)
                            (begin
                              (set! ok-flag #f)
                              ))
                        ))
                    ok-flag
                    ))
                ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-2-consecutive-prime-numbers psum)
  (begin
    (let ((max-adiff (euclidean/ psum 2))
          (ok-flag #f)
          (not-found-flag #f)
          (result-list (list)))
      (begin
        (do ((adiff 1 (1+ adiff)))
            ((or (>= adiff max-adiff)
                 (equal? ok-flag #t)
                 (equal? not-found-flag #t)))
          (begin
            (let ((nn-1 (- psum adiff))
                  (nn-2 (+ psum adiff)))
              (begin
                (if (<= nn-1 1)
                    (begin
                      (set! not-found-flag #t))
                    (begin
                      (let ((p1-flag (is-prime? nn-1))
                            (p2-flag (is-prime? nn-2)))
                        (begin
                          (if (equal? p1-flag #t)
                              (begin
                                (if (equal? p2-flag #t)
                                    (begin
                                      (if (is-consecutive-prime?
                                           nn-1 nn-2)
                                          (begin
                                            (set! ok-flag #t)
                                            (set!
                                             result-list
                                             (list nn-1 nn-2)))
                                          (begin
                                            (set! not-found-flag #t)
                                            )))
                                    (begin
                                      (set! not-found-flag #t)
                                      )))
                              (begin
                                (if (equal? p2-flag #t)
                                    (begin
                                      (set! not-found-flag #t)
                                      ))
                                ))
                          ))
                      ))
                ))
            ))

        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
