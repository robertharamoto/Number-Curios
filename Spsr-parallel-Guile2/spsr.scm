#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  simple perfect squared rectangle dissection          ###
;;;###                                                       ###
;;;###  last updated September 12, 2024                      ###
;;;###                                                       ###
;;;###  updated June 21, 2022                                ###
;;;###                                                       ###
;;;###  updated April 3, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "." "sources" "tests"))

(set! %load-compiled-path
      (append
       %load-compiled-path (list "." "objects")))

;;;#############################################################
;;;#############################################################
;;;### regex used for regex
(use-modules ((ice-9 regex)
              :renamer (symbol-prefix-proc 'ice-9-regex:)))

;;;### ice-9-format for advanced formatting
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### getopt-long used for command-line option arguments processing
(use-modules ((ice-9 getopt-long)
              :renamer (symbol-prefix-proc 'ice-9-getopt:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

(use-modules ((config-module)
              :renamer (symbol-prefix-proc 'config-module:)))

(use-modules ((bouwkamp-module)
              :renamer (symbol-prefix-proc 'bouwkamp-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin main support functions                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define (load-up-config-file
         args default-config-file
         config-htable comments-htable)
  (begin
    (let ((cf-name ""))
      (begin
        (if (and (not (equal? args #f))
                 (> (length args) 1))
            (begin
              (let ((found-flag #f)
                    (arg-fname ""))
                (begin
                  (for-each
                   (lambda (an-arg)
                     (begin
                       (if (file-exists? an-arg)
                           (begin
                             (let ((amatch
                                    (ice-9-regex:string-match
                                     ".scm" an-arg)))
                               (begin
                                 (if (equal? amatch #f)
                                     (begin
                                       (set! arg-fname an-arg)
                                       (set! found-flag #t)
                                       ))
                                 ))
                             ))
                       )) args)

                  (if (equal? found-flag #t)
                      (begin
                        (set! cf-name arg-fname)
                        (config-module:read-config-file
                         arg-fname config-htable comments-htable))
                      (begin
                        (if (file-exists? default-config-file)
                            (begin
                              (set! cf-name default-config-file)
                              (config-module:read-config-file
                               default-config-file
                               config-htable comments-htable))
                            (begin
                              (set! cf-name "")
                              (hash-clear! config-htable)
                              (hash-clear! comments-htable)
                              ))
                        ))
                  )))
            (begin
              ;;; no arguments typed
              (if (file-exists? default-config-file)
                  (begin
                    (set! cf-name default-config-file)
                    (config-module:read-config-file
                     default-config-file
                     config-htable comments-htable))
                  (begin
                    (set! cf-name "")
                    (hash-clear! config-htable)
                    (hash-clear! comments-htable)
                    ))
              ))

        cf-name
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax update-variable-macro
  (syntax-rules ()
    ((update-variable-macro
      var-name var-symbol options)
     (begin
       (let ((var-flag
              (ice-9-getopt:option-ref
               options var-symbol #f)))
         (begin
           (if (not (equal? var-flag #f))
               (begin
                 (let ((var2-flag
                        (ice-9-regex:regexp-substitute/global
                         #f "," var-flag 'pre "" 'post)))
                   (begin
                     (if (number?
                          (string->number var2-flag))
                         (begin
                           (set!
                            var-name
                            (string->number var2-flag)))
                         (begin
                           (set! var-name var2-flag)
                           ))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax update-flag-macro
  (syntax-rules ()
    ((update-flag-macro
      var-name var-symbol options)
     (begin
       (if (string? var-name)
           (begin
             (if (string-ci=? var-name "true")
                 (begin
                   (set! var-name #t))
                 (begin
                   (set! var-name #f)
                   )))
           (begin
             (set! var-name #f)
             ))

       (let ((var-flag
              (ice-9-getopt:option-ref options var-symbol #f)))
         (begin
           (if (equal? var-flag #t)
               (begin
                 (set! var-name #t)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax update-variable-macro
  (syntax-rules ()
    ((update-variable-macro
      var-name var-symbol options)
     (begin
       (let ((var-flag
              (ice-9-getopt:option-ref
               options var-symbol #f)))
         (begin
           (if (not (equal? var-flag #f))
               (begin
                 (let ((var2-flag
                        (ice-9-regex:regexp-substitute/global
                         #f "," var-flag 'pre "" 'post)))
                   (begin
                     (if (number?
                          (string->number var2-flag))
                         (begin
                           (set!
                            var-name
                            (string->number var2-flag)))
                         (begin
                           (set! var-name var2-flag)
                           ))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax update-flag-macro
  (syntax-rules ()
    ((update-flag-macro
      var-name var-symbol options)
     (begin
       (if (string? var-name)
           (begin
             (if (string-ci=? var-name "true")
                 (begin
                   (set! var-name #t))
                 (begin
                   (set! var-name #f)
                   )))
           (begin
             (set! var-name #f)
             ))

       (let ((var-flag
              (ice-9-getopt:option-ref options var-symbol #f)))
         (begin
           (if (equal? var-flag #t)
               (begin
                 (set! var-name #t)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin main code                                      ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define (main args)
  (define (local-display-version title-string)
    (begin
      (display (format #f "~a~%" title-string))
      (force-output)
      ))
  (define (local-display-help ospec title-string)
    (begin
      (display (format #f "~a~%" title-string))
      (display (format #f "options available~%"))
      (for-each
       (lambda (llist)
         (begin
           (display
            (format
             #f "  --~a, -~a~%" (car llist) (cadr (cadr llist))))
           )) ospec)
      (force-output)
      ))
  (begin
    (let ((version-string "2024-09-12"))
      (let ((title-string
             (format
              #f "Simple Perfect Squared Rectangles (version ~a)"
              version-string))
            (option-spec
             (list (list 'version '(single-char #\v) '(value #f))
                   (list 'help '(single-char #\h) '(value #f))
                   (list 'config '(single-char #\c) '(value #t))
                   (list 'start-width '(single-char #\s) '(value #t))
                   (list 'end-width '(single-char #\e) '(value #t))
                   (list 'max-precompute '(single-char #\m) '(value #t))
                   (list 'nthreads '(single-char #\n) '(value #t))
                   )))
        (let ((config-htable (make-hash-table 10))
              (comments-htable (make-hash-table 10))
              (def-config-file "init0.config")
              (options (ice-9-getopt:getopt-long args option-spec))
              (nthreads 4))
          (begin
            (let ((help-flag
                   (ice-9-getopt:option-ref options 'help #f)))
              (begin
                (if (equal? help-flag #t)
                    (begin
                      (local-display-help option-spec title-string)
                      (quit)))
                ))
            (let ((version-flag
                   (ice-9-getopt:option-ref options 'version #f)))
              (begin
                (if (equal? version-flag #t)
                    (begin
                      (local-display-version title-string)
                      (quit)
                      ))
                ))

            ;;;### read in options from config file
            (let ((config-file
                   (ice-9-getopt:option-ref options 'config-file #f)))
              (begin
                (if (and (not (equal? config-file #f))
                         (file-exists? config-file))
                    (begin
                      (config-module:read-config-file
                       config-file config-htable comments-htable))
                    (begin
                      (let ((result-config
                             (load-up-config-file
                              args def-config-file
                              config-htable comments-htable)))
                        (begin
                          (set! config-file result-config)
                          ))
                      ))

                ;;;### let command line options override config file
                (let ((start-width
                       (hash-ref config-htable "start-width" 2))
                      (end-width
                       (hash-ref config-htable "end-width" 35))
                      (max-precompute
                       (hash-ref config-htable "max-precompute" 35))
                      (status-in-minutes
                       (hash-ref config-htable "status-in-minutes" 120))
                      (nthreads
                       (hash-ref config-htable "nthreads" 6)))
                  (begin
                    (update-variable-macro
                     start-width 'start-width options)

                    (update-variable-macro
                     end-width 'end-width options)

                    (update-variable-macro
                     max-precompute 'max-precompute options)

                    (update-variable-macro
                     status-in-minutes 'status-in-minutes options)

                    (update-variable-macro
                     nthreads 'nthreads options)

                    (display
                     (format
                      #f "~a~%" title-string))
                    (display
                     (format #f "config file = ~a~%" config-file))
                    (display
                     (ice-9-format:format
                      #f "rectangle width from ~:d to ~:d~%"
                      start-width end-width))
                    (display
                     (ice-9-format:format
                      #f "precompute partition max = ~:d~%"
                      max-precompute))
                    (display
                     (ice-9-format:format
                      #f "number of threads = ~:d~%"
                      nthreads))
                    (display
                     (ice-9-format:format
                      #f "status message every ~:d minutes~%"
                      status-in-minutes))
                    (display
                     (format
                      #f "~a~%"
                      (timer-module:current-date-time-string)))
                    (force-output)

                    (timer-module:time-code-macro
                     (begin
                       (bouwkamp-module:main-loop-v0
                        start-width end-width
                        max-precompute
                        nthreads
                        status-in-minutes)

                       (display (format #f "total "))
                       ))

                    (newline)
                    (force-output)
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
