;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for bouwkamp-module.scm                   ###
;;;###                                                       ###
;;;###  last updated September 10, 2024                      ###
;;;###                                                       ###
;;;###  updated March 1, 2024                                ###
;;;###    fixed run-tests.scm errors                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated April 8, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((bouwkamp-module)
              :renamer (symbol-prefix-proc 'bouwkamp-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (bouwkamp-assert-lists-equal
         sub-name test-label-index
         data shouldbe-list-list result-list-list
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list-list))
          (rlen (length result-list-list)))
      (let ((err-1
             (format
              #f "~a : error (~a) : input-list=~a : "
              sub-name test-label-index data))
            (err-2
             (format
              #f "shouldbe length = ~a, result length = ~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-1 err-2)
           result-hash-table)

          (for-each
           (lambda (s-list)
             (begin
               (let ((rflag
                      (member s-list result-list-list))
                     (err-3
                      (format
                       #f "shouldbe=~a, result=~a"
                       s-list result-list-list)))
                 (begin
                   (unittest2:assert?
                    (not (equal? rflag #f))
                    sub-name
                    (string-append err-1 err-3)
                    result-hash-table)
                   ))
               )) shouldbe-list-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-convert-bouwkamp-to-rectangle-ll-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-convert-bouwkamp-to-rectangle-ll-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1 2 3)
                 (list (list 0 0 0 0)
                       (list 0 1 1 2)
                       (list 0 3 2 5)))
           (list (list 1 2 5)
                 (list (list 0 0 0 0)
                       (list 0 1 1 2)
                       (list 0 3 4 7)))
           (list (list 5 2 1)
                 (list (list 0 0 4 4)
                       (list 0 5 1 6)
                       (list 0 7 0 7)))
           (list (list 36 19 24 33)
                 (list (list 0 0 35 35)
                       (list 0 36 18 54)
                       (list 0 55 23 78)
                       (list 0 79 32 111)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((bk-list (list-ref alist 0))
                  (shouldbe-list-list (list-ref alist 1)))
              (let ((result-list-list
                     (bouwkamp-module:convert-bouwkamp-to-rectangle-ll
                      bk-list)))
                (begin
                  (bouwkamp-assert-lists-equal
                   sub-name test-label-index
                   bk-list shouldbe-list-list
                   result-list-list
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-bouwkamp-added-to-rectangle-ll-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-bouwkamp-added-to-rectangle-ll-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 14 5)
                 (list (list 0 0 35 35)
                       (list 0 36 18 54)
                       (list 0 55 23 78)
                       (list 0 79 32 111))
                 (list (list 0 0 35 35)
                       (list 0 36 32 49)
                       (list 0 50 23 78)
                       (list 0 79 32 111)))
           (list (list 20 9)
                 (list (list 0 0 35 35)
                       (list 0 36 32 49)
                       (list 0 50 23 78)
                       (list 0 79 32 111))
                 (list (list 0 0 35 35)
                       (list 0 36 32 49)
                       (list 0 50 43 69)
                       (list 0 70 32 111)))
           (list (list 3 11)
                 (list (list 0 0 35 35)
                       (list 0 36 32 49)
                       (list 0 50 43 69)
                       (list 0 70 32 111))
                 (list (list 0 0 35 38)
                       (list 0 39 43 69)
                       (list 0 70 32 111)))
           (list (list 42)
                 (list (list 0 0 35 38)
                       (list 0 39 43 69)
                       (list 0 70 32 111))
                 (list (list 0 0 35 38)
                       (list 0 39 43 69)
                       (list 0 70 74 111)))
           (list (list 39)
                 (list (list 0 0 35 38)
                       (list 0 39 43 69)
                       (list 0 70 74 111))
                 (list (list 0 0 74 38)
                       (list 0 39 43 69)
                       (list 0 70 74 111)))
           (list (list 31)
                 (list (list 0 0 74 38)
                       (list 0 39 43 69)
                       (list 0 70 74 111))
                 (list (list 0 0 74 111)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((bk-list (list-ref alist 0))
                  (rect-list-lists (list-ref alist 1))
                  (shouldbe-list-list (list-ref alist 2)))
              (let ((result-list-list
                     (bouwkamp-module:bouwkamp-added-to-rectangle-ll
                      bk-list rect-list-lists)))
                (begin
                  (bouwkamp-assert-lists-equal
                   sub-name test-label-index
                   bk-list shouldbe-list-list
                   result-list-list
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (bouwkamp-assert-simple-equal
         sub-name test-label-index
         data shouldbe result
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : error (~a) : input-data=~a : "
            sub-name test-label-index data))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (begin
        (unittest2:assert?
         (equal? shouldbe result)
         sub-name
         (string-append err-1 err-2)
         result-hash-table)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-min-row-col-width-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-find-min-row-col-width-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 0 0 35 35)
                       (list 0 36 18 54)
                       (list 0 55 23 78)
                       (list 0 79 32 111))
                 (list 19 36 19))
           (list (list (list 0 0 35 35)
                       (list 0 36 32 49)
                       (list 0 50 23 78)
                       (list 0 79 32 111))
                 (list 24 50 29))
           (list (list (list 0 0 35 35)
                       (list 0 36 32 49)
                       (list 0 50 43 69)
                       (list 0 70 32 111))
                 (list 33 36 14))
           (list (list (list 0 0 35 38)
                       (list 0 39 43 69)
                       (list 0 70 32 111))
                 (list 33 70 42))
           (list (list (list 0 0 35 38)
                       (list 0 39 43 69)
                       (list 0 70 74 111))
                 (list 36 0 39))
           (list (list (list 0 0 74 38)
                       (list 0 39 43 69)
                       (list 0 70 74 111))
                 (list 44 39 31))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((rect-list-lists (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (bouwkamp-module:find-min-row-col-width
                      rect-list-lists)))
                (begin
                  (bouwkamp-assert-simple-equal
                   sub-name test-label-index
                   rect-list-lists
                   shouldbe-list result-list
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-bouwkamp-reverse-vector-k-to-n-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-bouwkamp-reverse-vector-k-to-n-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (vector 0 1 2) 1 (vector 0 2 1))
           (list (vector 0 1 2) 0 (vector 2 1 0))
           (list (vector 0 1 2 3) 2 (vector 0 1 3 2))
           (list (vector 0 1 2 3) 1 (vector 0 3 2 1))
           (list (vector 0 1 2 3) 0 (vector 3 2 1 0))
           (list (vector 0 1 2 3 4) 3 (vector 0 1 2 4 3))
           (list (vector 0 1 2 3 4) 2 (vector 0 1 4 3 2))
           (list (vector 0 1 2 3 4) 1 (vector 0 4 3 2 1))
           (list (vector 0 1 2 3 4) 0 (vector 4 3 2 1 0))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-vec (list-ref this-list 0))
                  (test-ii (list-ref this-list 1))
                  (shouldbe-vec (list-ref this-list 2)))
              (let ((result-vec
                     (bouwkamp-module:reverse-vector-k-to-n
                      test-vec test-ii)))
                (begin
                  (bouwkamp-assert-simple-equal
                   sub-name test-label-index
                   test-vec
                   shouldbe-vec result-vec
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-bouwkamp-next-lexicographic-permutation-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-bouwkamp-next-lexicographic-permutation-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (vector 0 1 2) (vector 0 2 1))
           (list (vector 0 2 1) (vector 1 0 2))
           (list (vector 1 0 2) (vector 1 2 0))
           (list (vector 1 2 0) (vector 2 0 1))
           (list (vector 2 0 1) (vector 2 1 0))
           (list (vector 0 1 2 3 4 5) (vector 0 1 2 3 5 4))
           (list (vector 0 1 2 3 5 4) (vector 0 1 2 4 3 5))
           (list (vector 0 1 2 4 3 5) (vector 0 1 2 4 5 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-vec (list-ref this-list 0))
                  (shouldbe-vec (list-ref this-list 1)))
              (let ((result-vec
                     (bouwkamp-module:next-lexicographic-permutation
                      test-vec)))
                (begin
                  (bouwkamp-assert-simple-equal
                   sub-name test-label-index
                   test-vec
                   shouldbe-vec result-vec
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-successor-of-partition-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-successor-of-partition-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2) (list 1 1))
           (list (list 3) (list 2 1))
           (list (list 2 1) (list 1 1 1))
           (list (list 4) (list 3 1))
           (list (list 3 1) (list 2 2))
           (list (list 2 2) (list 2 1 1))
           (list (list 2 1 1) (list 1 1 1 1))
           (list (list 5) (list 4 1))
           (list (list 4 1) (list 3 2))
           (list (list 3 2) (list 3 1 1))
           (list (list 3 1 1) (list 2 2 1))
           (list (list 2 2 1) (list 2 1 1 1))
           (list (list 2 1 1 1) (list 1 1 1 1 1))
           (list (list 1 1 1 1 1) (list))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (bouwkamp-module:successor-of-partition-list
                      input-list)))
                (begin
                  (bouwkamp-assert-lists-equal
                   sub-name test-label-index
                   input-list
                   shouldbe-list result-list
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-check-if-ok-to-use-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-check-if-ok-to-use-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2) (list 1 2 3) 6 #f)
           (list (list 3) (list 2 1) 6 #t)
           (list (list 2 1) (list 1 4 5) 10 #f)
           (list (list 4) (list 3 1) 5 #t)
           (list (list 3 1) (list 2 4) 6 #t)
           (list (list 2 2) (list 4 1 3) 8 #f)
           (list (list 2 1) (list 3 4) 7 #t)
           (list (list 4) (list 3 1) 4 #f)
           ))
         (used-htable (make-hash-table))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (hash-clear! used-htable)
            (let ((input-list (list-ref this-list 0))
                  (used-list (list-ref this-list 1))
                  (max-width (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (begin
                (for-each
                 (lambda (key)
                   (begin
                     (hash-set! used-htable key #t)
                     )) used-list)

                (let ((result
                       (bouwkamp-module:check-if-ok-to-use
                        input-list max-width used-htable)))
                  (begin
                    (bouwkamp-assert-simple-equal
                     sub-name test-label-index
                     used-list
                     shouldbe result
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-rect-list-simple-perfect-dissection-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-rect-list-simple-perfect-dissection-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 0 0 2 2))
                 (list (list 2 3 4) (list 4 5 6))
                 #t #f)
           (list (list (list 0 0 3 3))
                 (list (list 3 4 5) (list 6 7 8))
                 #f #f)
           (list (list (list 0 0 99 99))
                 (list (list 1 2 3) (list 4 5 6))
                 #t #f)
           (list (list (list 0 0 99 99))
                 (list (list 99))
                 #f #f)
           (list (list (list 0 0 99 99))
                 (list (list 1 2 3) (list 4 5 6))
                 #f #t)
           (list (list (list 0 0 32 65))
                 (reverse
                  (list (list 18 15 32) (list 7 8)
                        (list 14 4) (list 10 1) (list 9)))
                 #f #f)
           (list (list (list 0 0 33 65))
                 (reverse
                  (list (list 33 15 8 9) (list 7 1)
                        (list 10) (list 18 4) (list 14)))
                 #f #f)
           (list (list (list 0 0 47 65))
                 (reverse (list (list 22 19 24) (list 3 11 5)
                                (list 25) (list 6 23) (list 17)))
                 #f #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((rect-list-lists (list-ref this-list 0))
                  (used-partition-lists (list-ref this-list 1))
                  (init-flag (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result
                     (bouwkamp-module:is-rect-list-simple-perfect-dissection?
                      rect-list-lists used-partition-lists init-flag)))
                (begin
                  (bouwkamp-assert-simple-equal
                   sub-name test-label-index
                   rect-list-lists
                   shouldbe result
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-used-partition-lists-contains-duplicates-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-used-partition-lists-contains-duplicates-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 2 3 4) (list 4 5 6))
                 #t)
           (list (list (list 2 3 4) (list 5 6 7))
                 #f)
           (list (list (list 2 3 4) (list 5 6 7)
                       (list 8 9 10) (list 11 12 13))
                 #f)
           (list (list (list 2 3 4) (list 5 6 7)
                       (list 8 9 10) (list 10 12 13))
                 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((used-partition-lists (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (bouwkamp-module:used-partition-lists-contains-duplicates?
                      used-partition-lists)))
                (begin
                  (bouwkamp-assert-simple-equal
                   sub-name test-label-index
                   used-partition-lists
                   shouldbe result
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-partition-list-contains-duplicates-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-partition-list-contains-duplicates-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2 3 4) #f)
           (list (list 2 3 4 2) #t)
           (list (list 2 3 4 3) #t)
           (list (list 2 3 4 5 6) #f)
           (list (list 2 3 4 7 9 7) #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((partition-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (bouwkamp-module:partition-list-contains-duplicates?
                      partition-list)))
                (begin
                  (bouwkamp-assert-simple-equal
                   sub-name test-label-index
                   partition-list
                   shouldbe result
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-height-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-find-height-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 0 0 2 2)) 3)
           (list (list (list 0 0 3 3)) 4)
           (list (list (list 0 0 99 99)) 100)
           (list (list (list 0 0 88 88)) 89)
           (list (list (list 0 0 101 101) (list 0 0 102 102))
                 103)
           (list (list (list 0 0 101 101)
                       (list 0 0 102 102)
                       (list 0 0 103 103))
                 104)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((rect-list-lists (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (bouwkamp-module:find-height
                      rect-list-lists)))
                (begin
                  (bouwkamp-assert-simple-equal
                   sub-name test-label-index
                   rect-list-lists
                   shouldbe result
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-precompute-partition-list-htable-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-precompute-partition-list-htable-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 (list (list 2)))
           (list 3 (list (list 3) (list 2 1)))
           (list 4 (list (list 4) (list 3 1)))
           (list 5 (list (list 5) (list 4 1)
                         (list 3 2)))
           (list 6 (list (list 6) (list 5 1)
                         (list 4 2) (list 3 2 1)))
           ))
         (max-width 6)
         (partition-list-htable
          (make-hash-table 20))
         (test-label-index 0))
     (begin
       (bouwkamp-module:precompute-partition-list-htable
        max-width partition-list-htable)

       (for-each
        (lambda (alist)
          (begin
            (let ((this-width (list-ref alist 0))
                  (shouldbe-list-list (list-ref alist 1)))
              (let ((result-list-list
                     (hash-ref partition-list-htable
                               this-width #f)))
                (begin
                  (if (not (equal? result-list-list #f))
                      (begin
                        (bouwkamp-assert-lists-equal
                         sub-name test-label-index
                         this-width
                         shouldbe-list-list
                         result-list-list
                         result-hash-table)
                        ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
