;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for timer-module.scm                      ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated February 27, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### srfi-19 used for time/date library
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (timer-simple-test-check
         shouldbe result
         extra-string extra-data
         sub-name test-label-index
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) error : ~a=~a : "
            sub-name test-label-index
            extra-string extra-data))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (let ((error-string
             (string-append err-1 err-2)))
        (begin
          (unittest2:assert?
           (equal? shouldbe result)
           sub-name
           error-string
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-time-to-short-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-time-to-short-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (srfi-19:string->date
             "9/6/2008 04:44:44" "~m/~d/~Y ~H:~M:~S")
            "04:44:44 am")
           (list
            (srfi-19:string->date
             "9/5/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "02:44:44 pm")
           (list
            (srfi-19:string->date
             "9/4/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "02:44:44 pm")
           (list
            (srfi-19:string->date
             "9/3/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "02:44:44 pm")
           (list
            (srfi-19:string->date
             "9/2/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "02:44:44 pm")
           (list
            (srfi-19:string->date
             "9/1/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "02:44:44 pm")
           (list
            (srfi-19:string->date
             "8/31/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "02:44:44 pm")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((date (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (timer-module:time-to-short-string date)))
                (let ((string-1 "date")
                      (string-2
                       (srfi-19:date->string
                        date "~m/~d/~Y ~H:~M:~S")))
                  (begin
                    (timer-simple-test-check
                     shouldbe result
                     string-1 string-2
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-date-time-to-short-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-date-time-to-short-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (srfi-19:string->date
             "9/6/2008 04:44:44" "~m/~d/~Y ~H:~M:~S")
            "Saturday, 04:44:44 am")
           (list
            (srfi-19:string->date
             "9/5/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "Friday, 02:44:44 pm")
           (list
            (srfi-19:string->date
             "9/4/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "Thursday, 02:44:44 pm")
           (list
            (srfi-19:string->date
             "9/3/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "Wednesday, 02:44:44 pm")
           (list
            (srfi-19:string->date
             "9/2/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "Tuesday, 02:44:44 pm")
           (list
            (srfi-19:string->date
             "9/1/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "Monday, 02:44:44 pm")
           (list
            (srfi-19:string->date
             "8/31/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "Sunday, 02:44:44 pm")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((date (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (timer-module:date-time-to-short-string
                      date)))
                (let ((string-1 "date")
                      (string-2
                       (srfi-19:date->string
                        date "~m/~d/~Y ~H:~M:~S")))
                  (begin
                    (timer-simple-test-check
                     shouldbe result
                     string-1 string-2
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-date-time-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-date-time-to-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (srfi-19:string->date
             "9/6/2008 04:44:44" "~m/~d/~Y ~H:~M:~S")
            "Saturday, September 06, 2008, 04:44:44 am")
           (list
            (srfi-19:string->date
             "9/5/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "Friday, September 05, 2008, 02:44:44 pm")
           (list
            (srfi-19:string->date
             "9/4/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "Thursday, September 04, 2008, 02:44:44 pm")
           (list
            (srfi-19:string->date
             "9/3/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "Wednesday, September 03, 2008, 02:44:44 pm")
           (list
            (srfi-19:string->date
             "9/2/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "Tuesday, September 02, 2008, 02:44:44 pm")
           (list
            (srfi-19:string->date
             "9/1/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "Monday, September 01, 2008, 02:44:44 pm")
           (list
            (srfi-19:string->date
             "8/31/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "Sunday, August 31, 2008, 02:44:44 pm")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((date (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (timer-module:date-time-to-string date)))
                (let ((string-1 "date")
                      (string-2
                       (srfi-19:date->string
                        date "~m/~d/~Y ~H:~M:~S")))
                  (begin
                    (timer-simple-test-check
                     shouldbe result
                     string-1 string-2
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-julian-day-difference-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-julian-day-difference-to-string-1"
           (utils-module:get-basename (current-filename))))
         (time-zone (srfi-19:date-zone-offset (srfi-19:current-date))))
     (let ((test-list
            (list
             (list
              (srfi-19:make-date 2000000 10 10 10 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 0"
              "0.000 seconds")
             (list
              (srfi-19:make-date 20000000 10 10 10 10 10 2010 time-zone)
              (srfi-19:make-date 10000000 10 10 10 10 10 2010 time-zone)
              "label 1"
              "0.010 seconds")
             (list
              (srfi-19:make-date 1000000 11 10 10 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 2"
              "1 second")
             (list
              (srfi-19:make-date 2000000 12 10 10 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 3"
              "2.001 seconds")
             (list
              (srfi-19:make-date 2000000 10 11 10 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 4"
              "1 minute, 0.000 seconds")
             (list
              (srfi-19:make-date 2000000 11 12 10 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 5"
              "2 minutes, 1.001 seconds")
             (list
              (srfi-19:make-date 2000000 12 10 11 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 6"
              "1 hour, 2.000 seconds")
             (list
              (srfi-19:make-date 2000000 12 12 11 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 7"
              "1 hour, 2 minutes, 2.000 seconds")
             (list
              (srfi-19:make-date 2000000 12 12 12 11 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 8"
              "1 day, 2 hours, 2 minutes, 2.000 seconds")
             (list
              (srfi-19:make-date 2000000 12 12 12 11 11 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 9"
              "32 days, 2 hours, 2 minutes, 2.000 seconds")
             ))
           (test-label-index 0))
       (begin
         (for-each
          (lambda (alist)
            (begin
              (let ((end-date (list-ref alist 0))
                    (start-date (list-ref alist 1))
                    (label (list-ref alist 2))
                    (shouldbe (list-ref alist 3)))
                (let ((end-jdate
                       (srfi-19:date->julian-day end-date))
                      (start-jdate
                       (srfi-19:date->julian-day start-date)))
                  (let ((result
                         (timer-module:julian-day-difference-to-string
                          end-jdate start-jdate)))
                    (let ((string-1 "label"))
                      (begin
                        (timer-simple-test-check
                         shouldbe result
                         string-1 label
                         sub-name test-label-index
                         result-hash-table)
                        ))
                    )))

              (set! test-label-index (1+ test-label-index))
              )) test-list)
         )))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-julian-day-difference-in-minutes-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-julian-day-difference-in-minutes-1"
           (utils-module:get-basename (current-filename))))
         (time-zone
          (srfi-19:date-zone-offset
           (srfi-19:current-date))))
     (let ((test-list
            (list
             (list
              (srfi-19:make-date 2000000 10 10 10 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 0"
              "0 seconds"
              0)
             (list
              (srfi-19:make-date 20000000 10 10 10 10 10 2010 time-zone)
              (srfi-19:make-date 10000000 40 10 10 10 10 2010 time-zone)
              "label 1"
              "30 seconds"
              0)
             (list
              (srfi-19:make-date 1000000 10 11 10 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 2"
              "1.000 minute"
              1)
             (list
              (srfi-19:make-date 2000000 10 12 10 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 3"
              "2.000 minutes"
              2)
             (list
              (srfi-19:make-date 2000000 10 10 10 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 13 10 10 10 2010 time-zone)
              "label 4"
              "2 minutes, 0.99 seconds"
              2)
             (list
              (srfi-19:make-date 2000000 11 12 10 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 5"
              "2 minutes, 1.001 seconds"
              2)
             (list
              (srfi-19:make-date 2000000 12 10 11 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 6"
              "1 hour, 2.000 seconds"
              60)
             (list
              (srfi-19:make-date 2000000 12 12 11 10 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 7"
              "1 hour, 2 minutes, 2.000 seconds"
              62)
             (list
              (srfi-19:make-date 2000000 12 12 12 11 10 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 8"
              "1 day, 2 hours, 2 minutes, 2.000 seconds"
              1562)
             (list
              (srfi-19:make-date 2000000 12 12 12 11 11 2010 time-zone)
              (srfi-19:make-date 1000000 10 10 10 10 10 2010 time-zone)
              "label 9"
              "32 days, 2 hours, 2 minutes, 2.000 seconds"
              46202)
             ))
           (test-label-index 0))
       (begin
         (for-each
          (lambda (alist)
            (begin
              (let ((end-date (list-ref alist 0))
                    (start-date (list-ref alist 1))
                    (label (list-ref alist 2))
                    (shouldbe-string (list-ref alist 3))
                    (shouldbe (list-ref alist 4)))
                (let ((end-jdate
                       (srfi-19:date->julian-day end-date))
                      (start-jdate
                       (srfi-19:date->julian-day start-date)))
                  (let ((result
                         (timer-module:julian-day-difference-in-minutes
                          end-jdate start-jdate)))
                    (let ((string-1
                           (format #f "label=~a, shouldbe" label)))
                      (begin
                        (timer-simple-test-check
                         shouldbe result
                         string-1 shouldbe-string
                         sub-name test-label-index
                         result-hash-table)
                        )))
                  ))

              (set! test-label-index (1+ test-label-index))
              )) test-list)
         )))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-time-hour-integer-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-time-hour-integer-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (srfi-19:string->date
             "9/6/2008 00:44:44" "~m/~d/~Y ~H:~M:~S")
            0)
           (list
            (srfi-19:string->date
             "9/6/2008 01:44:44" "~m/~d/~Y ~H:~M:~S")
            1)
           (list
            (srfi-19:string->date
             "9/6/2008 02:44:44" "~m/~d/~Y ~H:~M:~S")
            2)
           (list
            (srfi-19:string->date
             "9/6/2008 03:44:44" "~m/~d/~Y ~H:~M:~S")
            3)
           (list
            (srfi-19:string->date
             "9/6/2008 04:44:44" "~m/~d/~Y ~H:~M:~S")
            4)
           (list
            (srfi-19:string->date
             "9/6/2008 05:44:44" "~m/~d/~Y ~H:~M:~S")
            5)
           (list
            (srfi-19:string->date
             "9/6/2008 06:44:44" "~m/~d/~Y ~H:~M:~S")
            6)
           (list
            (srfi-19:string->date
             "9/6/2008 07:44:44" "~m/~d/~Y ~H:~M:~S")
            7)
           (list
            (srfi-19:string->date
             "9/6/2008 08:44:44" "~m/~d/~Y ~H:~M:~S")
            8)
           (list
            (srfi-19:string->date
             "9/6/2008 09:44:44" "~m/~d/~Y ~H:~M:~S")
            9)
           (list
            (srfi-19:string->date
             "9/6/2008 10:44:44" "~m/~d/~Y ~H:~M:~S")
            10)
           (list
            (srfi-19:string->date
             "9/6/2008 11:44:44" "~m/~d/~Y ~H:~M:~S")
            11)
           (list
            (srfi-19:string->date
             "9/6/2008 12:44:44" "~m/~d/~Y ~H:~M:~S")
            12)
           (list
            (srfi-19:string->date
             "9/6/2008 13:44:44" "~m/~d/~Y ~H:~M:~S")
            13)
           (list
            (srfi-19:string->date
             "9/6/2008 14:44:44" "~m/~d/~Y ~H:~M:~S")
            14)
           (list
            (srfi-19:string->date
             "9/6/2008 15:44:44" "~m/~d/~Y ~H:~M:~S")
            15)
           (list
            (srfi-19:string->date
             "9/6/2008 16:44:44" "~m/~d/~Y ~H:~M:~S")
            16)
           (list
            (srfi-19:string->date
             "9/6/2008 17:44:44" "~m/~d/~Y ~H:~M:~S")
            17)
           (list
            (srfi-19:string->date
             "9/6/2008 18:44:44" "~m/~d/~Y ~H:~M:~S")
            18)
           (list
            (srfi-19:string->date
             "9/6/2008 19:44:44" "~m/~d/~Y ~H:~M:~S")
            19)
           (list
            (srfi-19:string->date
             "9/6/2008 20:44:44" "~m/~d/~Y ~H:~M:~S")
            20)
           (list
            (srfi-19:string->date
             "9/6/2008 21:44:44" "~m/~d/~Y ~H:~M:~S")
            21)
           (list
            (srfi-19:string->date
             "9/6/2008 22:44:44" "~m/~d/~Y ~H:~M:~S")
            22)
           (list
            (srfi-19:string->date
             "9/6/2008 23:44:44" "~m/~d/~Y ~H:~M:~S")
            23)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((this-date (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (timer-module:time-hour-integer
                      this-date)))
                    (let ((string-1 "date")
                          (string-2
                           (srfi-19:date->string
                            this-date "~m/~d/~Y ~H:~M:~S")))
                      (begin
                        (timer-simple-test-check
                         shouldbe result
                         string-1 string-2
                         sub-name test-label-index
                         result-hash-table)
                        ))
                    ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-date-week-day-integer-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-date-week-day-integer-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (srfi-19:string->date
             "4/11/2010 00:44:44" "~m/~d/~Y ~H:~M:~S")
            0)
           (list
            (srfi-19:string->date
             "4/12/2010 01:44:44" "~m/~d/~Y ~H:~M:~S")
            1)
           (list
            (srfi-19:string->date
             "4/13/2010 02:44:44" "~m/~d/~Y ~H:~M:~S")
            2)
           (list
            (srfi-19:string->date
             "4/14/2010 03:44:44" "~m/~d/~Y ~H:~M:~S")
            3)
           (list
            (srfi-19:string->date
             "4/15/2010 04:44:44" "~m/~d/~Y ~H:~M:~S")
            4)
           (list
            (srfi-19:string->date
             "4/16/2010 05:44:44" "~m/~d/~Y ~H:~M:~S")
            5)
           (list
            (srfi-19:string->date
             "4/17/2010 06:44:44" "~m/~d/~Y ~H:~M:~S")
            6)
           (list
            (srfi-19:string->date
             "4/18/2010 07:44:44" "~m/~d/~Y ~H:~M:~S")
            0)
           (list
            (srfi-19:string->date
             "4/19/2010 08:44:44" "~m/~d/~Y ~H:~M:~S")
            1)
           (list
            (srfi-19:string->date
             "4/20/2010 09:44:44" "~m/~d/~Y ~H:~M:~S")
            2)
           (list
            (srfi-19:string->date
             "4/21/2010 10:44:44" "~m/~d/~Y ~H:~M:~S")
            3)
           (list
            (srfi-19:string->date
             "4/22/2010 11:44:44" "~m/~d/~Y ~H:~M:~S")
            4)
           (list
            (srfi-19:string->date
             "4/23/2010 12:44:44" "~m/~d/~Y ~H:~M:~S")
            5)
           (list
            (srfi-19:string->date
             "4/24/2010 13:44:44" "~m/~d/~Y ~H:~M:~S")
            6)
           (list
            (srfi-19:string->date
             "4/25/2010 14:44:44" "~m/~d/~Y ~H:~M:~S")
            0)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((this-date (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (timer-module:date-week-day-integer this-date)))
                (let ((string-1 "date")
                      (string-2
                       (srfi-19:date->string
                        this-date "~m/~d/~Y ~H:~M:~S")))
                  (begin
                    (timer-simple-test-check
                     shouldbe result
                     string-1 string-2
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-date-week-day-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-date-week-day-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (srfi-19:string->date
             "4/11/2010 00:44:44" "~m/~d/~Y ~H:~M:~S")
            "Sunday")
           (list
            (srfi-19:string->date
             "4/12/2010 01:44:44" "~m/~d/~Y ~H:~M:~S")
            "Monday")
           (list
            (srfi-19:string->date
             "4/13/2010 02:44:44" "~m/~d/~Y ~H:~M:~S")
            "Tuesday")
           (list
            (srfi-19:string->date
             "4/14/2010 03:44:44" "~m/~d/~Y ~H:~M:~S")
            "Wednesday")
           (list
            (srfi-19:string->date
             "4/15/2010 04:44:44" "~m/~d/~Y ~H:~M:~S")
            "Thursday")
           (list
            (srfi-19:string->date
             "4/16/2010 05:44:44" "~m/~d/~Y ~H:~M:~S")
            "Friday")
           (list
            (srfi-19:string->date
             "4/17/2010 06:44:44" "~m/~d/~Y ~H:~M:~S")
            "Saturday")
           (list
            (srfi-19:string->date
             "4/18/2010 07:44:44" "~m/~d/~Y ~H:~M:~S")
            "Sunday")
           (list
            (srfi-19:string->date
             "4/19/2010 08:44:44" "~m/~d/~Y ~H:~M:~S")
            "Monday")
           (list
            (srfi-19:string->date
             "4/20/2010 09:44:44" "~m/~d/~Y ~H:~M:~S")
            "Tuesday")
           (list
            (srfi-19:string->date
             "4/21/2010 10:44:44" "~m/~d/~Y ~H:~M:~S")
            "Wednesday")
           (list
            (srfi-19:string->date
             "4/22/2010 11:44:44" "~m/~d/~Y ~H:~M:~S")
            "Thursday")
           (list
            (srfi-19:string->date
             "4/23/2010 12:44:44" "~m/~d/~Y ~H:~M:~S")
            "Friday")
           (list
            (srfi-19:string->date
             "4/24/2010 13:44:44" "~m/~d/~Y ~H:~M:~S")
            "Saturday")
           (list
            (srfi-19:string->date
             "4/25/2010 14:44:44" "~m/~d/~Y ~H:~M:~S")
            "Sunday")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((this-date (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (timer-module:date-week-day-string
                      this-date)))
                (let ((string-1 "date")
                      (string-2
                       (srfi-19:date->string
                        this-date "~m/~d/~Y ~H:~M:~S")))
                  (begin
                    (timer-simple-test-check
                     shouldbe result
                     string-1 string-2
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
