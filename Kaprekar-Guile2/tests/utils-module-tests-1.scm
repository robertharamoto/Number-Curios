;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for utils-module.scm                      ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated February 27, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (utils-simple-test-check
         shouldbe result
         extra-string extra-data
         sub-name test-label-index
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) error : ~a=~a : "
            sub-name test-label-index
            extra-string extra-data))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (let ((error-string
             (string-append err-1 err-2)))
        (begin
          (unittest2:assert?
           (equal? shouldbe result)
           sub-name
           error-string
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (utils-simple-float-test-check
         shouldbe result
         tolerance
         extra-string extra-data
         sub-name test-label-index
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) error : ~a=~a : "
            sub-name test-label-index
            extra-string extra-data))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (let ((error-string
             (string-append err-1 err-2)))
        (begin
          (unittest2:assert?
           (<= (abs (- shouldbe result))
               tolerance)
           sub-name
           error-string
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-commafy-number-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-commafy-number-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 "1")
           (list 10 "10")
           (list 100 "100")
           (list 1000 "1,000")
           (list 10000 "10,000")
           (list 1000000 "1,000,000")
           (list 10000000 "10,000,000")
           (list 1010.101 "1,010.101000")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((number (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (utils-module:commafy-number number)))
                (begin
                  (utils-simple-test-check
                   shouldbe result
                   "number" number
                   sub-name test-label-index
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;;### round floats to 2 decimal places
(unittest2:define-tests-macro
 (test-round-float-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-round-float-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1.0 2 1.00)
           (list 10.10 2 10.10)
           (list 10.101 2 10.10)
           (list 10.12345 2 10.12)
           (list 10.1544  2 10.15)
           (list 10.1549  2 10.15)
           (list 10.1550  2 10.15)
           (list 10.1551  2 10.16)
           (list 10.1552  2 10.16)
           (list -1.0 2 -1.00)
           (list -1.120 2 -1.120)
           (list -1.123 2 -1.12)
           (list -1.124 2 -1.12)
           (list -1.1244 2 -1.12)
           (list -1.1245 2 -1.12)
           (list -1.1246 2 -1.12)
           (list -1.1249 2 -1.12)
           (list -1.1250 2 -1.13)
           (list -1.1251 2 -1.13)
           (list -1.1252 2 -1.13)
           (list -1.1253 2 -1.13)
           (list -1.1254 2 -1.13)
           (list -1.1255 2 -1.13)
           (list -1.1256 2 -1.13)
           ))
         (float-tol 1.0e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((number (list-ref alist 0))
                  (ndecimals (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (utils-module:round-float number ndecimals)))
                (let ((string-1
                       (format
                        #f "number=~a, decimals"
                        number)))
                  (begin
                    (utils-simple-float-test-check
                     shouldbe result
                     float-tol
                     string-1 ndecimals
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;;### round floats to 3 decimal places
(unittest2:define-tests-macro
 (test-round-float-2 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-round-float-2"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1.0 3 1.00)
           (list 10.10 3 10.10)
           (list 10.101 3 10.101)
           (list 10.12345 3 10.123)
           (list 10.12349 3 10.123)
           (list 10.12350 3 10.124)
           (list 10.12351 3 10.124)
           (list 10.12352 3 10.124)
           (list 10.12353 3 10.124)
           (list 10.12354 3 10.124)
           (list -10.12345 3 -10.123)
           (list -10.12349 3 -10.123)
           (list -10.12350 3 -10.124)
           (list -10.12351 3 -10.124)
           (list -10.12352 3 -10.124)
           (list -10.12353 3 -10.124)
           (list -10.12354 3 -10.124)
           ))
         (float-tol 1.0e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((number (list-ref alist 0))
                  (ndecimals (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (utils-module:round-float number ndecimals)))
                (let ((string-1
                       (format
                        #f "number=~a, decimals"
                        number)))
                  (begin
                    (utils-simple-float-test-check
                     shouldbe result
                     float-tol
                     string-1 ndecimals
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sequence-list-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-sequence-list-to-string-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1 2) "[1 -> 2]")
           (list (list 1 2 3) "[1 -> 2 -> 3]")
           (list (list 1234 56789) "[1,234 -> 56,789]")
           (list (list 1 2 3 4 5) "[1 -> 2 -> 3 -> 4 -> 5]")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((tlist (list-ref alist 0))
                  (shouldbe-string (list-ref alist 1)))
              (let ((result-string
                     (utils-module:sequence-list-to-string
                      tlist)))
                (let ((string-1 "tlist"))
                  (begin
                    (utils-simple-test-check
                     shouldbe-string result-string
                     string-1 tlist
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-number-list-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-number-list-to-string-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1 2) "[1 ; 2]")
           (list (list 1 2 3) "[1 ; 2 ; 3]")
           (list (list 1234 56789) "[1,234 ; 56,789]")
           (list (list 1 2 3 4 5) "[1 ; 2 ; 3 ; 4 ; 5]")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((tlist (list-ref alist 0))
                  (shouldbe-string (list-ref alist 1)))
              (let ((result-string
                     (utils-module:number-list-to-string tlist)))
                (let ((string-1 "tlist"))
                  (begin
                    (utils-simple-test-check
                     shouldbe-string result-string
                     string-1 tlist
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-remove-commas-from-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-remove-commas-from-string-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list "123" "123")
           (list "1,234" "1234")
           (list "12,345" "12345")
           (list "123,456" "123456")
           (list "9,876,123" "9876123")
           (list "1,234,567,891" "1234567891")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((tstring (list-ref alist 0))
                  (shouldbe-string (list-ref alist 1)))
              (let ((result-string
                     (utils-module:remove-commas-from-string tstring)))
                (let ((string-1 "tstring"))
                  (begin
                    (utils-simple-test-check
                     shouldbe-string result-string
                     string-1 tstring
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-get-basename-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-get-basename-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list "/home/bob/Play/Guile2/abc" "abc")
           (list "/home/bob/Play/Guile2/cde/fgh" "fgh")
           (list "/home/bob/Play/Guile2/abc/def/ghi" "ghi")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((fname (list-ref alist 0))
                  (shouldbe-string (list-ref alist 1)))
              (let ((result-string
                     (utils-module:get-basename fname)))
                (let ((string-1 "fname"))
                  (begin
                    (utils-simple-test-check
                     shouldbe-string result-string
                     string-1 fname
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
