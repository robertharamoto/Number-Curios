;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for kaprekar-module.scm                   ###
;;;###                                                       ###
;;;###  last updated August 21, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((kaprekar-module)
              :renamer (symbol-prefix-proc 'kaprekar-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-digit-list-separate-and-check-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-digit-list-separate-and-check-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1) 0 1 (list 0 1))
           (list (list 8 1) 0 9 #f)
           (list (list 8 1) 1 9 (list 8 1))
           (list (list 8 1) 2 9 #f)
           (list (list 2 0 2 5) 2 45 (list 20 25))
           (list (list 8 8 2 0 9) 2 297 (list 88 209))
           (list (list 9 9 8 0 0 1) 0 999 #f)
           (list (list 9 9 8 0 0 1) 1 999 #f)
           (list (list 9 9 8 0 0 1) 2 999 #f)
           (list (list 9 9 8 0 0 1) 3 999 (list 998 1))
           (list (list 9 9 8 0 0 1) 4 999 #f)
           (list (list 9 9 8 0 0 1) 5 999 #f)
           (list (list 9 9 8 0 0 1) 6 999 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((digit-list (list-ref alist 0))
                  (nitems (list-ref alist 1))
                  (mm (list-ref alist 2))
                  (shouldbe (list-ref alist 3)))
              (let ((result
                     (kaprekar-module:digit-list-separate-and-check
                      digit-list nitems mm)))
                (let ((err-1
                       (format
                        #f "~a : (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "digit-list=~a, nitems=~a, mm=~a : "
                        digit-list nitems mm))
                      (err-3
                       (format #f "shouldbe=~a, result=~a"
                               shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-kaprekar-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-is-kaprekar-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 (list 0 1 0)) (list 2 #f)
           (list 3 #f) (list 4 #f) (list 9 (list 8 1 1))
           (list 10 #f) (list 40 #f)
           (list 45 (list 20 25 2)) (list 55 (list 30 25 2))
           (list 99 (list 98 1 2)) (list 297 (list 88 209 2))
           (list 703 (list 494 209 3))
           (list 999 (list 998 1 3))
           (list 2222 #f)
           (list 2223 (list 494 1729 3))
           (list 2224 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((nn (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (kaprekar-module:is-kaprekar? nn)))
                (let ((err-1
                       (format #f "~a : error (~a) : num=~a, "
                               sub-name test-label-index nn))
                      (err-2
                       (format #f "shouldbe=~a, result=~a"
                               shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
