;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for digits-module.scm                     ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (digits-simple-test-check
         shouldbe result
         extra-string extra-data
         sub-name test-label-index
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) error : ~a=~a : "
            sub-name test-label-index
            extra-string extra-data))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (let ((error-string
             (string-append err-1 err-2)))
        (begin
          (unittest2:assert?
           (equal? shouldbe result)
           sub-name
           error-string
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sum-of-digits-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-sum-of-digits-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 11 2) (list 12 3) (list 22 4) (list 33 6)
           (list 123 6) (list 456 15) (list 9876 30)
           (list 1024 7) (list 10120 4)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (digits-module:sum-of-digits test-num)))
                (let ((string-1 "number"))
                  (begin
                    (digits-simple-test-check
                     shouldbe result
                     string-1 test-num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-digit-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-calc-digit-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 (list 1))
           (list 2 (list 2))
           (list 9 (list 9))
           (list 10 (list 1 0))
           (list 11 (list 1 1))
           (list 123 (list 1 2 3))
           (list 7890 (list 7 8 9 0))
           (list 987654 (list 9 8 7 6 5 4))
           (list 1237890 (list 1 2 3 7 8 9 0))
           (list 12378900 (list 1 2 3 7 8 9 0 0))
           (list 1087115666068800124219
                 (list 1 0 8 7 1 1 5 6 6 6 0
                       6 8 8 0 0 1 2 4 2 1 9))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (digits-module:calc-digit-list test-num)))
                (let ((err-1
                       (format
                        #f "~a : (~a) error : number=~a"
                        sub-name test-label-index test-num)))
                  (begin
                    (digits-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     err-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-reverse-number-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-reverse-number-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 1)
           (list 2 2)
           (list 9 9)
           (list 10 1)
           (list 11 11)
           (list 12 21)
           (list 123 321)
           (list 7890 987)
           (list 987654 456789)
           (list 1237890 987321)
           (list 1087115666068800124219
                 9124210088606665117801)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (digits-module:reverse-number test-num)))
                (let ((string-1 "number"))
                  (begin
                    (digits-simple-test-check
                     shouldbe-num result-num
                     string-1 test-num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-split-digits-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-split-digits-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 (list 0)) (list 1 (list 1))
           (list 3 (list 3)) (list 4 (list 4))
           (list 5 (list 5)) (list 13 (list 1 3))
           (list 14 (list 1 4)) (list 15 (list 1 5))
           (list 23 (list 2 3)) (list 24 (list 2 4))
           (list 25 (list 2 5)) (list 123 (list 1 2 3))
           (list 1234 (list 1 2 3 4))
           (list 98765 (list 9 8 7 6 5))
           (list 341608987 (list 3 4 1 6 0 8 9 8 7))
           (list
            116696699999166169
            (list 1 1 6 6 9 6 6 9 9 9 9 9 1 6 6 1 6 9))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (digits-module:split-digits-list test-num)))
                (let ((err-1
                       (format
                        #f "~a : (~a) error : number=~a"
                        sub-name test-label-index test-num)))
                  (begin
                    (digits-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     err-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-digit-list-to-number-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-digit-list-to-number-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 3) 3) (list (list 4) 4)
           (list (list 5) 5) (list (list 1 3) 13)
           (list (list 1 4) 14) (list (list 1 5) 15)
           (list (list 2 3) 23) (list (list 2 4) 24)
           (list (list 2 5) 25)
           (list (list 1 2 3) 123)
           (list (list 1 2 3 4) 1234)
           (list (list 9 8 7 6 5) 98765)
           (list (list 9 8 0 0 0) 98000)
           (list (list 3 4 1 6 0 8 9 8 7) 341608987)
           (list
            (list 1 1 6 6 9 6 6 9 9 9 9 9 1 6 6 1 6 9)
            116696699999166169)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-list (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (digits-module:digit-list-to-number test-list)))
                (let ((string-1 "list"))
                  (begin
                    (digits-simple-test-check
                     shouldbe-num result-num
                     string-1 test-list
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-number-base-10-to-base-n-digit-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-number-base-10-to-base-n-digit-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 2 (list 0)) (list 1 2 (list 1))
           (list 2 2 (list 1 0)) (list 3 2 (list 1 1))
           (list 4 2 (list 1 0 0)) (list 5 2 (list 1 0 1))
           (list 6 2 (list 1 1 0)) (list 7 2 (list 1 1 1))
           (list 0 3 (list 0)) (list 1 3 (list 1))
           (list 2 3 (list 2)) (list 3 3 (list 1 0))
           (list 4 3 (list 1 1)) (list 5 3 (list 1 2))
           (list 6 3 (list 2 0)) (list 7 3 (list 2 1))
           (list 8 3 (list 2 2)) (list 9 3 (list 1 0 0))
           (list 10 3 (list 1 0 1)) (list 11 3 (list 1 0 2))
           (list 12 3 (list 1 1 0)) (list 13 3 (list 1 1 1))
           (list 14 3 (list 1 1 2)) (list 15 3 (list 1 2 0))
           (list 16 3 (list 1 2 1)) (list 17 3 (list 1 2 2))
           (list 18 3 (list 2 0 0)) (list 19 3 (list 2 0 1))
           (list 3 4 (list 3))
           (list 3 5 (list 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((anum (list-ref alist 0))
                  (base (list-ref alist 1))
                  (shouldbe-list (list-ref alist 2)))
              (let ((result-list
                     (digits-module:number-base-10-to-base-n-digit-list
                      anum base)))
                (let ((err-1
                       (format
                        #f "~a : (~a) error : anum=~a, base=~a, "
                        sub-name test-label-index anum base)))
                  (begin
                    (digits-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     err-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-base-digit-list-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-base-digit-list-to-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1 2 1) 5
                 "121(5)")
           (list (list 3 1 3) 7
                 "313(7)")
           (list (list 5 3 1 3 5) 7
                 "53,135(7)")
           (list (list 6 5 3 1 3 5 6) 7
                 "6,531,356(7)")
           (list (list 15 14 13 13 14 15) 16
                 "fed,def(16)")
           (list (list 33 35 13 13 35 33) 100
                 "33 35 d,d 35 33(100)")
           (list (list 33 35 37 37 35 33) 100
                 "33 35 37, 37 35 33(100)")
           (list (list 1 0 0 1 1 1 0 0 1) 2
                 "100,111,001(2)")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((alist (list-ref alist 0))
                  (base (list-ref alist 1))
                  (shouldbe-string (list-ref alist 2)))
              (let ((result-string
                     (digits-module:base-digit-list-to-string
                      alist base)))
                (let ((string-1
                       (format #f "alist=~a, base"
                               alist)))
                  (begin
                    (digits-simple-test-check
                     shouldbe-string result-string
                     string-1 base
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-digit-list-palindrome-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-digit-list-palindrome-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1 1) #t) (list (list 2 2) #t)
           (list (list 3 3) #t) (list (list 4 4) #t)
           (list (list 3 5) #f) (list (list 4 8) #f)
           (list (list 1 0 1) #t) (list (list 1 0 2) #f)
           (list (list 1 2 2 1) #t)
           (list (list 1 2 5 2 1) #t)
           (list (list 8 1 2 5 2 1 8) #t)
           (list (list 7 8 1 2 5 2 1 8 7) #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((tlist (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (digits-module:is-digit-list-palindrome?
                      tlist)))
                (let ((string-1 "tlist"))
                  (begin
                    (digits-simple-test-check
                     shouldbe result
                     string-1 tlist
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-get-unique-digits-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-get-unique-digits-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1 0 2) (list 1 0 2))
           (list (list 1 2 3) (list 1 2 3))
           (list (list 1 2 2) (list 1 2))
           (list (list 2 2 2) (list 2))
           (list (list 1 2 2 1) (list 1 2))
           (list (list 1 2 2 1 2) (list 1 2))
           (list (list 1 2 3 1 2) (list 1 2 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((tlist (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (digits-module:get-unique-digits-list tlist)))
                (let ((err-1
                       (format
                        #f "~a : (~a) error : tlist=~a"
                        sub-name test-label-index tlist)))
                  (begin
                    (digits-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     err-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-number-of-digits-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-number-of-digits-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 1) (list 1 1) (list 2 1)
           (list 3 1) (list 9 1) (list 10 2)
           (list 11 2) (list 12 2) (list 22 2)
           (list 33 2) (list 100 3) (list 123 3)
           (list 456 3) (list 9876 4)
           (list 9870 4) (list 1000 4)
           (list 12345 5)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (digits-module:number-of-digits test-num)))
                (let ((string-1 "number"))
                  (begin
                    (digits-simple-test-check
                     shouldbe result
                     string-1 test-num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (digits-assert-lists-equal
         shouldbe-list result-list sub-name error-message
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((slen (min shouldbe-length result-length))
            (err-1
             (format
              #f "~a : shouldbe-list=~a, result-list=~a : "
              error-message shouldbe-list result-list))
            (err-2
             (format
              #f "shouldbe size=~a, result size=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append err-1 err-2)
           result-hash-table)

          (do ((ii 0 (1+ ii)))
              ((>= ii slen))
            (begin
              (let ((s-elem (list-ref shouldbe-list ii)))
                (let ((found-flag (member s-elem result-list)))
                  (let ((err-3
                         (format
                          #f "~a : missing element(~a)=~a"
                          err-1 ii s-elem)))
                    (begin
                      (unittest2:assert?
                       (not (equal? found-flag #f))
                       sub-name
                       err-3
                       result-hash-table)
                      ))
                  ))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
