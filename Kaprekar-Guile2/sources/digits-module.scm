;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  digits-module - digit manipulation functions         ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 4, 2022                                  ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define-module (digits-module)
  #:export (sum-of-digits
            calc-digit-list
            reverse-number

            split-digits-list
            digit-list-to-number
            number-base-10-to-base-n-digit-list

            base-digit-list-to-string
            is-digit-list-palindrome?

            get-unique-digits-list
            number-of-digits
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### srfi-1 for fold functions and delete-duplicates
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;#############################################################
;;;#############################################################
;;;### sum of digits
(define (sum-of-digits this-num)
  (begin
    (let ((result-num
           (srfi-1:fold
            + 0
            (srfi-1:unfold
             (lambda (pnum)
               (begin
                 (<= pnum 0)
                 ))
             (lambda (fnum)
               (begin
                 (modulo fnum 10)
                 ))
             (lambda (gnum)
               (begin
                 (euclidean/ gnum 10)
                 ))
             this-num))))
      (begin
        result-num
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (calc-digit-list mm)
  (begin
    (let ((result-list
           (srfi-1:unfold
            (lambda (pnum)
              (begin
                (<= pnum 0)
                ))
            (lambda (fnum)
              (begin
                (modulo fnum 10)
                ))
            (lambda (gnum)
              (begin
                (euclidean/ gnum 10)
                ))
            mm)))
      (begin
        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (reverse-number mm)
  (begin
    (let ((result-num
           (srfi-1:fold
            (lambda (this-num prev-num)
              (begin
                (+ this-num (* 10 prev-num))
                ))
            0
            (srfi-1:unfold
             (lambda (pnum)
               (begin
                 (<= pnum 0)
                 ))
             (lambda (fnum)
               (begin
                 (modulo fnum 10)
                 ))
             (lambda (gnum)
               (begin
                 (euclidean/ gnum 10)
                 ))
             mm))))
      (begin
        result-num
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### make a list of digits
(define (split-digits-list this-num)
  (begin
    (if (<= this-num 0)
        (begin
          (list this-num))
        (begin
          (let ((result-list
                 (srfi-1:unfold
                  (lambda (pnum)
                    (begin
                      (<= pnum 0)
                      ))
                  (lambda (fnum)
                    (begin
                      (modulo fnum 10)
                      ))
                  (lambda (gnum)
                    (begin
                      (euclidean/ gnum 10)
                      ))
                  this-num)))
            (begin
              (reverse result-list)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
;;;### digits to number
(define (digit-list-to-number digit-list)
  (begin
    (let ((result-num
           (srfi-1:fold
            (lambda (this-num prev-num)
              (begin
                (+ this-num (* 10 prev-num))
                ))
            0
            digit-list)))
      (begin
        result-num
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### number base 10 to base n
(define (number-base-10-to-base-n-digit-list anum base)
  (begin
    (let ((result-list
           (srfi-1:unfold
            (lambda (pnum)
              (begin
                (<= pnum 0)
                ))
            (lambda (fnum)
              (begin
                (modulo fnum base)
                ))
            (lambda (gnum)
              (begin
                (euclidean/ gnum base)
                ))
            anum)))
      (begin
        (if (null? result-list)
            (begin
              (list 0))
            (begin
              (reverse result-list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### digits to number, in base bb
(define (base-digit-list-to-string digit-list bb)
  (define (local-get-digit-string adigit)
    (begin
      (cond
       ((and (< adigit 10) (>= adigit 0))
        (begin
          (format #f "~a" adigit)
          ))
       ((and (>= adigit 10) (< adigit 16))
        (begin
          (let ((alpha-list
                 (list (list 10 "a") (list 11 "b")
                       (list 12 "c") (list 13 "d")
                       (list 14 "e") (list 15 "f"))))
            (let ((alen (length alpha-list))
                  (result-string "error")
                  (done-flag #f))
              (begin
                (do ((ii 0 (1+ ii)))
                    ((or (>= ii alen)
                         (equal? done-flag #t)))
                  (begin
                    (let ((alist (list-ref alpha-list ii)))
                      (let ((a-elem (list-ref alist 0))
                            (a-string (list-ref alist 1)))
                        (begin
                          (if (equal? a-elem adigit)
                              (begin
                                (set! result-string a-string)
                                (set! done-flag #t)
                                ))
                          )))
                    ))
                result-string
                )))
          ))
       (else
        (begin
          (format #f "~a" adigit)
          )))
      ))
  (define (local-loop this-list prev-digit count string-so-far)
    (begin
      ;;; least significant digit first
      (if (or (null? this-list)
              (not (list? this-list))
              (< (length this-list) 1))
          (begin
            string-so-far)
          (begin
            (let ((this-digit (car this-list))
                  (tail-list (cdr this-list))
                  (next-count (1+ count)))
              (let ((dstring (local-get-digit-string this-digit)))
                (begin
                  (cond
                   ((equal? count 0)
                    (begin
                      (local-loop
                       tail-list this-digit
                       next-count dstring)
                      ))
                   ((zero? (modulo count 3))
                    (begin
                      (cond
                       ((< prev-digit 0)
                        (begin
                          (local-loop
                           tail-list this-digit
                           next-count dstring)
                          ))
                       ((and (>= prev-digit 0)
                             (< prev-digit 16))
                        (begin
                          (let ((next-string
                                 (format
                                  #f "~a,~a"
                                  dstring
                                  (string-trim-both string-so-far))))
                            (begin
                              (local-loop
                               tail-list this-digit
                               next-count next-string)
                              ))
                          ))
                       (else
                        (begin
                          (let ((next-string
                                 (format
                                  #f "~a, ~a"
                                  dstring string-so-far)))
                            (begin
                              (local-loop
                               tail-list this-digit
                               next-count next-string)
                              ))
                          )))
                      ))
                   (else
                    (begin
                      (cond
                       ((and (>= prev-digit 0)
                             (< prev-digit 16)
                             (>= this-digit 0)
                             (< this-digit 16))
                        (begin
                          (let ((next-string
                                 (format
                                  #f "~a~a"
                                  dstring string-so-far)))
                            (begin
                              (local-loop
                               tail-list this-digit
                               next-count next-string)
                              ))
                          ))
                       ((and (>= prev-digit 0)
                             (< prev-digit 16)
                             (>= this-digit 16))
                        (begin
                          (let ((next-string
                                 (format
                                  #f "~a ~a"
                                  dstring string-so-far)))
                            (begin
                              (local-loop
                               tail-list this-digit
                               next-count next-string)
                              ))
                          ))
                       ((and (>= prev-digit 16)
                             (>= this-digit 0))
                        (begin
                          (let ((next-string
                                 (format
                                  #f "~a ~a"
                                  dstring string-so-far)))
                            (begin
                              (local-loop
                               tail-list this-digit
                               next-count next-string)
                              ))
                          ))
                       (else
                        (begin
                          (let ((next-string
                                 (format
                                  #f "~a ~a"
                                  dstring string-so-far)))
                            (begin
                              (local-loop
                               tail-list this-digit
                               next-count next-string)
                              ))
                          )))
                      )))
                  )))
            ))
      ))
  (begin
    ;;; most significant digit first, then reverse
    ;;; so least significant digit first
    (let ((result-string
           (local-loop
            (reverse digit-list) -1 0 "")))
      (begin
        (format
         #f "~a(~a)"
         (string-trim-both result-string) bb)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-digit-list-palindrome? dlist)
  (begin
    (if (list? dlist)
        (begin
          (if (<= (length dlist) 1)
              (begin
                #t)
              (begin
                (let ((first-digit (car dlist))
                      (tail-list (cdr dlist))
                      (last-digit (car (last-pair dlist))))
                  (begin
                    (if (equal? first-digit last-digit)
                        (begin
                          (let ((tlen (length tail-list)))
                            (let ((next-list
                                   (list-head tail-list (1- tlen))))
                              (let ((bool-result
                                     (is-digit-list-palindrome?
                                      next-list)))
                                (begin
                                  bool-result
                                  ))
                              )))
                        (begin
                          #f
                          ))
                    ))
                )))
        (begin
          #t
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (get-unique-digits-list dlist)
  (begin
    (let ((result-list (list)))
      (begin
        (for-each
         (lambda (adigit)
           (begin
             (if (equal?
                  (member adigit result-list) #f)
                 (begin
                   (set!
                    result-list
                    (cons adigit result-list))
                   ))
             )) dlist)

        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (number-of-digits this-num)
  (begin
    (let ((result-num
           (srfi-1:fold
            + 0
            (srfi-1:unfold
             (lambda (pnum)
               (begin
                 (<= pnum 0)
                 ))
             (lambda (fnum)
               (begin
                 1
                 ))
             (lambda (gnum)
               (begin
                 (euclidean/ gnum 10)
                 ))
             this-num))))
      (begin
        (if (equal? this-num 0)
            (begin
              1)
            (begin
              result-num
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
