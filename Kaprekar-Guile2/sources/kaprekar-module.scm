;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  kaprekar functions module                            ###
;;;###                                                       ###
;;;###  last updated August 21, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start prime modules

(define-module (kaprekar-module)
  #:export (digit-list-separate-and-check
            count-digits
            is-kaprekar?
            prime-factors-list-to-string

            main-loop
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list functions
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;#############################################################
;;;#############################################################
(define (digit-list-separate-and-check digit-list nitems mm)
  (begin
    (let ((first-list (list-head digit-list nitems))
          (second-list (list-tail digit-list nitems)))
      (let ((first-num
             (digits-module:digit-list-to-number first-list))
            (second-num
             (digits-module:digit-list-to-number second-list)))
        (let ((sum (+ first-num second-num)))
          (begin
            (if (equal? sum mm)
                (begin
                  (if (not (zero? second-num))
                      (begin
                        (list first-num second-num))
                      (begin
                        #f
                        )))
                (begin
                  #f
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-kaprekar? nn)
  (begin
    (let ((sq-digit-list
           (digits-module:split-digits-list (* nn nn)))
          (dcount
           (1+ (digits-module:number-of-digits nn)))
          (result #f))
      (let ((sq-len (length sq-digit-list)))
        (begin
          (do ((ii-items 0 (1+ ii-items)))
              ((or (> ii-items dcount)
                   (>= ii-items sq-len)
                   (not (equal? result #f))))
            (begin
              (let ((alist
                     (digit-list-separate-and-check
                      sq-digit-list ii-items nn)))
                (begin
                  (if (list? alist)
                      (begin
                        (set! result (append alist (list ii-items)))
                        ))
                  ))
              ))

          result
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (kaprekar-number-to-string nn alist details-flag)
  (begin
    (if (and (list? alist)
             (equal? details-flag #t))
        (begin
          (let ((fstring (ice-9-format:format #f "~:d : " nn))
                (first-num (list-ref alist 0))
                (second-num (list-ref alist 1))
                (nn-sq (* nn nn)))
            (let ((f2string
                   (ice-9-format:format
                    #f "~:d^2 = ~:d : " nn nn-sq))
                  (f3string
                   (ice-9-format:format
                    #f "~:d + ~:d = ~:d"
                    first-num second-num
                    (+ first-num second-num))))
              (begin
                (string-append
                 fstring f2string f3string)
                ))
            ))
        (begin
          (ice-9-format:format #f "~:d" nn)
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop start-num end-num details-flag)
  (begin
    (let ((result-count 0))
      (begin
        (do ((ii start-num (1+ ii)))
            ((>= ii end-num))
          (begin
            (let ((result (is-kaprekar? ii)))
              (begin
                (if (list? result)
                    (begin
                      (let ((fstring
                             (kaprekar-number-to-string
                              ii result details-flag)))
                        (begin
                          (display (format #f "~a~%" fstring))
                          (force-output)
                          (set! result-count (1+ result-count))
                          ))
                      ))
                ))
            ))

        (newline)
        (display
         (ice-9-format:format
          #f "found ~:d kaprekar number pairs "
          result-count))
        (display
         (ice-9-format:format
          #f "between [~:d, ~:d]~%"
          start-num end-num))
        (newline)
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
