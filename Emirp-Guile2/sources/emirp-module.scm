;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  prime functions module                               ###
;;;###                                                       ###
;;;###  last updated August 22, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start emirp modules

(define-module (emirp-module)
  #:export (reverse-number
            display-results
            calc-next-int-stop
            main-loop
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### srfi-1 for fold functions and delete-duplicates
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;#############################################################
;;;#############################################################
(define (reverse-number mm)
  (begin
    (if (>= mm 10)
        (begin
          (let ((next-num 0)
                (this-num mm))
            (begin
              (while (>= this-num 10)
                (begin
                  (let ((tmp-num (euclidean/ this-num 10)))
                    (let ((this-digit (- this-num (* 10 tmp-num))))
                      (let ((nn (+ (* 10 next-num) this-digit)))
                        (begin
                          (set! next-num nn)
                          (set! this-num tmp-num)
                          ))
                      ))
                  ))

              (if (>= this-num 0)
                  (begin
                    (set!
                     next-num
                     (+ (* 10 next-num) this-num))
                    ))

              next-num
              )))
        (begin
          mm
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-results ncols result-htable)
  (begin
    (let ((alist-list
           (hash-map->list cons result-htable)))
      (let ((alist
             (map
              (lambda (tlist)
                (begin
                  (list-ref tlist 0)
                  )) alist-list)))
        (let ((blist (sort alist <))
              (max-count (length alist))
              (ncount 0))
          (begin
            (do ((ii 0 (1+ ii)))
                ((>= ii max-count))
              (begin
                (let ((bb (list-ref blist ii)))
                  (begin
                    (if (zero? ncount)
                        (begin
                          (if (integer? bb)
                              (begin
                                (display
                                 (ice-9-format:format
                                  #f "~:d" bb)))
                              (begin
                                (display
                                 (format #f "~a" bb))
                                )))
                        (begin
                          (if (integer? bb)
                              (begin
                                (display
                                 (ice-9-format:format
                                  #f ", ~:d" bb)))
                              (begin
                                (display
                                 (format #f ", ~a" bb))
                                ))
                          ))
                    (set! ncount (1+ ncount))
                    (if (>= ncount ncols)
                        (begin
                          (newline)
                          (force-output)
                          (set! ncount 0)
                          ))
                    ))
                ))

            (if (< ncount ncols)
                (begin
                  (newline)
                  ))

            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (calc-next-int-stop mm)
  (begin
    (if (>= mm 10)
        (begin
          (let ((next-num 1)
                (this-num mm))
            (begin
              (while (>= this-num 10)
                (begin
                  (let ((tmp-num
                         (euclidean/ this-num 10)))
                    (let ((nn (* 10 next-num)))
                      (begin
                        (set! next-num nn)
                        (set! this-num tmp-num)
                        )))
                  ))

              (if (> this-num 0)
                  (begin
                    (set! next-num (* 10 next-num))
                    ))

              next-num
              )))
        (begin
          mm
          ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-prime-macro
  (syntax-rules ()
    ((process-prime-macro
      ii result-count
      result-htable
      prime-array)
     (begin
       (if (not
            (hash-ref
             result-htable ii #f))
           (begin
             (let ((rev-num
                    (reverse-number ii)))
               (begin
                 (if (> rev-num ii)
                     (begin
                       (if (prime-module:is-array-prime?
                            rev-num prime-array)
                           (begin
                             (hash-set!
                              result-htable ii #t)
                             (hash-set!
                              result-htable rev-num #t)
                             (set!
                              result-count (+ 2 result-count))
                             ))
                       ))
                 ))
             ))
       ))
    ))


;;;#############################################################
;;;#############################################################
(define (main-loop start-num end-num ncols max-prime)
  (begin
    (let ((result-htable (make-hash-table))
          (result-count 0)
          (prime-array
           (prime-module:make-prime-array max-prime))
          (int-stop (calc-next-int-stop start-num)))
      (begin
        (if (even? start-num)
            (begin
              (set! start-num (1+ start-num))
              ))

        (do ((ii start-num (+ ii 2)))
            ((>= ii end-num))
          (begin
            (if (<= ii int-stop)
                (begin
                  (if (prime-module:is-array-prime?
                       ii prime-array)
                      (begin
                        (process-prime-macro
                         ii result-count
                         result-htable
                         prime-array)
                        )))
                (begin
                  (display-results ncols result-htable)

                  ;;; int-stop always a power of 10, so never a prime
                  (set! int-stop (calc-next-int-stop (1+ ii)))
                  (hash-clear! result-htable)
                  (gc)
                  ))
            ))

        (if (> (hash-count (const #t) result-htable) 0)
            (begin
              (display-results ncols result-htable)
              (hash-clear! result-htable)
              (gc)
              ))

        (newline)
        (display
         (ice-9-format:format
          #f "found ~:d emirps~%" result-count))
        (newline)
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
