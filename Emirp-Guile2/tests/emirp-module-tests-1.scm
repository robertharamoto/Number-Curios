;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for emirp-module.scm                      ###
;;;###                                                       ###
;;;###  last updated August 22, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((emirp-module)
              :renamer (symbol-prefix-proc 'emirp-module:)))

;;;#############################################################
;;;#############################################################
(define (emirp-module-simple-assert
         sub-name test-label-index
         test-num
         shouldbe result
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : error (~a) : num=~a, "
            sub-name test-label-index test-num))
          (err-2
           (format #f "shouldbe=~a, result=~a"
                   shouldbe result)))
      (begin
        (unittest2:assert?
         (equal? shouldbe result)
         sub-name
         (string-append err-1 err-2)
         result-hash-table)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-reverse-number-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-reverse-number-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 0) (list 1 1) (list 2 2) (list 3 3)
           (list 4 4) (list 5 5) (list 6 6) (list 7 7)
           (list 8 8) (list 9 9) (list 10 1) (list 11 11)
           (list 12 21) (list 13 31) (list 14 41) (list 15 51)
           (list 16 61) (list 17 71) (list 18 81) (list 19 91)
           (list 20 2) (list 99 99)
           (list 123 321) (list 1234 4321) (list 12345 54321)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (emirp-module:reverse-number test-num)))
                (begin
                  (emirp-module-simple-assert
                   sub-name test-label-index
                   test-num
                   shouldbe result
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-next-int-stop-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-calc-next-int-stop-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 0) (list 1 1) (list 2 2) (list 3 3)
           (list 10 100) (list 11 100) (list 12 100)
           (list 20 100) (list 99 100) (list 100 1000)
           (list 101 1000) (list 1000 10000)
           (list 10000 100000)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (emirp-module:calc-next-int-stop test-num)))
                (begin
                  (emirp-module-simple-assert
                   sub-name test-label-index
                   test-num
                   shouldbe result
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
