;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for perfect-module.scm                    ###
;;;###                                                       ###
;;;###  last updated August 21, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((perfect-module)
              :renamer (symbol-prefix-proc 'perfect-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (perfect-module-simple-assert
         sub-name test-label-index
         test-num shouldbe result
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : error (~a) : test-num = ~a, "
            sub-name test-label-index test-num))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (begin
        (unittest2:assert?
         (equal? shouldbe result)
         sub-name
         (string-append err-1 err-2)
         result-hash-table)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-prime-factors-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-prime-factors-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 (list 2)) (list 3 (list 3))
           (list 4 (list 2 2)) (list 5 (list 5))
           (list 6 (list 2 3)) (list 7 (list 7))
           (list 8 (list 2 2 2)) (list 9 (list 3 3))
           (list 10 (list 2 5)) (list 11 (list 11))
           (list 12 (list 2 2 3)) (list 13 (list 13))
           (list 14 (list 2 7)) (list 15 (list 3 5))
           (list 16 (list 2 2 2 2)) (list 17 (list 17))
           (list 18 (list 2 3 3)) (list 19 (list 19))
           (list 20 (list 2 2 5)) (list 21 (list 3 7))
           (list 22 (list 2 11)) (list 23 (list 23))
           (list 24 (list 2 2 2 3)) (list 25 (list 5 5))
           (list 26 (list 2 13)) (list 27 (list 3 3 3))
           (list 28 (list 2 2 7)) (list 29 (list 29))
           (list 30 (list 2 3 5)) (list 31 (list 31))
           (list 32 (list 2 2 2 2 2)) (list 33 (list 3 11))
           (list 34 (list 2 17)) (list 35 (list 5 7))
           (list 36 (list 2 2 3 3)) (list 37 (list 37))
           (list 38 (list 2 19)) (list 39 (list 3 13))
           (list 40 (list 2 2 2 5)) (list 41 (list 41))
           (list 42 (list 2 3 7)) (list 43 (list 43))
           (list 44 (list 2 2 11)) (list 45 (list 3 3 5))
           (list 46 (list 2 23)) (list 47 (list 47))
           (list 48 (list 2 2 2 2 3)) (list 49 (list 7 7))
           (list 50 (list 2 5 5)) (list 51 (list 3 17))
           (list 52 (list 2 2 13)) (list 53 (list 53))
           (list 54 (list 2 3 3 3)) (list 55 (list 5 11))
           (list 56 (list 2 2 2 7)) (list 57 (list 3 19))
           (list 58 (list 2 29)) (list 59 (list 59))
           (list 60 (list 2 2 3 5)) (list 61 (list 61))
           (list 62 (list 2 31)) (list 63 (list 3 3 7))
           (list 64 (list 2 2 2 2 2 2)) (list 65 (list 5 13))
           (list 66 (list 2 3 11)) (list 67 (list 67))
           (list 68 (list 2 2 17)) (list 69 (list 3 23))
           (list 70 (list 2 5 7)) (list 71 (list 71))
           (list 72 (list 2 2 2 3 3)) (list 73 (list 73))
           (list 74 (list 2 37)) (list 75 (list 3 5 5))
           (list 76 (list 2 2 19)) (list 77 (list 7 11))
           (list 78 (list 2 3 13)) (list 79 (list 79))
           (list 80 (list 2 2 2 2 5)) (list 81 (list 3 3 3 3))
           (list 82 (list 2 41)) (list 83 (list 83))
           (list 84 (list 2 2 3 7)) (list 85 (list 5 17))
           (list 86 (list 2 43)) (list 87 (list 3 29))
           (list 88 (list 2 2 2 11)) (list 89 (list 89))
           (list 90 (list 2 3 3 5)) (list 91 (list 7 13))
           (list 92 (list 2 2 23)) (list 93 (list 3 31))
           (list 94 (list 2 47)) (list 95 (list 5 19))
           (list 96 (list 2 2 2 2 2 3)) (list 97 (list 97))
           (list 98 (list 2 7 7)) (list 99 (list 3 3 11))
           (list 100 (list 2 2 5 5)) (list 125 (list 5 5 5))
           (list 504 (list 2 2 2 3 3 7))
           (list 21342 (list 2 3 3557))
           (list 71406 (list 2 3 3 3967))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (perfect-module:prime-factors-list
                      test-num)))
                (begin
                  (perfect-module-simple-assert
                   sub-name test-label-index
                   test-num
                   shouldbe-list result-list
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-compute-prime-exp-pairs-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-compute-prime-exp-pairs-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2) (list (list 2 1)))
           (list (list 3) (list (list 3 1)))
           (list (list 2 2) (list (list 2 2)))
           (list (list 5) (list (list 5 1)))
           (list (list 2 3) (list (list 2 1) (list 3 1)))
           (list (list 7) (list (list 7 1)))
           (list (list 2 2 2) (list (list 2 3)))
           (list (list 3 3) (list (list 3 2)))
           (list (list 2 5) (list (list 2 1) (list 5 1)))
           (list (list 2 2 3) (list (list 2 2) (list 3 1)))
           (list (list 2 2 2 2) (list (list 2 4)))
           (list (list 2 3 3) (list (list 2 1) (list 3 2)))
           (list (list 2 2 2 3 3) (list (list 2 3) (list 3 2)))
           (list (list 3 3 3 3 5 5 5 7 7)
                 (list (list 3 4) (list 5 3) (list 7 2)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((prime-list (list-ref alist 0))
                  (shouldbe-list-list (list-ref alist 1)))
              (let ((result-list-list
                     (perfect-module:compute-prime-exp-pairs
                      prime-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : list = ~a, "
                        sub-name test-label-index prime-list)))
                  (begin
                    (perfect-module-assert-lists-equal
                     shouldbe-list-list result-list-list
                     sub-name err-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fastexp-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-fastexp-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 1 2) (list 2 2 4) (list 2 3 8) (list 2 4 16)
           (list 3 1 3) (list 3 2 9) (list 3 3 27) (list 3 4 81)
           (list 4 1 4) (list 4 2 16) (list 4 3 64) (list 4 4 256)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-base (list-ref this-list 0))
                  (test-exp (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (perfect-module:fastexp
                      test-base test-exp)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : base=~a, exp=~a, "
                        sub-name test-label-index
                        test-base test-exp))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sum-of-proper-divisors-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-sum-of-proper-divisors-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 1) (list 3 1) (list 4 3)
           (list 5 1) (list 6 6) (list 7 1)
           (list 8 7) (list 9 4) (list 10 8)
           (list 11 1) (list 12 16) (list 13 1)
           (list 14 10) (list 15 9) (list 16 15)
           (list 17 1) (list 18 21) (list 19 1)
           (list 20 22) (list 21 11) (list 22 14)
           (list 23 1) (list 24 36) (list 25 6)
           (list 26 16) (list 27 13) (list 28 28)
           (list 29 1) (list 30 42) (list 31 1)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (perfect-module:sum-of-proper-divisors
                      test-num)))
                (begin
                  (perfect-module-simple-assert
                   sub-name test-label-index
                   test-num
                   shouldbe-num result-num
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-proper-divisors-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-proper-divisors-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 (list)) (list 2 (list 1))
           (list 3 (list 1)) (list 4 (list 1 2))
           (list 5 (list 1)) (list 6 (list 1 2 3))
           (list 7 (list 1)) (list 8 (list 1 2 4))
           (list 9 (list 1 3)) (list 10 (list 1 2 5))
           (list 11 (list 1)) (list 12 (list 1 2 3 4 6))
           (list 13 (list 1)) (list 14 (list 1 2 7))
           (list 15 (list 1 3 5)) (list 16 (list 1 2 4 8))
           (list 17 (list 1)) (list 18 (list 1 2 3 6 9))
           (list 19 (list 1)) (list 20 (list 1 2 4 5 10))
           (list 21 (list 1 3 7)) (list 22 (list 1 2 11))
           (list 23 (list 1)) (list 24 (list 1 2 3 4 6 8 12))
           (list 25 (list 1 5)) (list 26 (list 1 2 13))
           (list 27 (list 1 3 9)) (list 28 (list 1 2 4 7 14))
           (list 29 (list 1))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (perfect-module:proper-divisors-list
                      test-num)))
                (begin
                  (perfect-module-simple-assert
                   sub-name test-label-index
                   test-num
                   shouldbe-list result-list
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-perfect-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-perfect-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 #f) (list 1 #f) (list 2 #f) (list 3 #f)
           (list 4 #f) (list 5 #f) (list 6 #t) (list 7 #f)
           (list 8 #f) (list 9 #f) (list 10 #f) (list 11 #f)
           (list 12 #f) (list 13 #f) (list 14 #f) (list 15 #f)
           (list 16 #f) (list 17 #f) (list 18 #f) (list 19 #f)
           (list 20 #f) (list 21 #f) (list 22 #f) (list 23 #f)
           (list 24 #f) (list 25 #f) (list 26 #f) (list 27 #f)
           (list 28 #t) (list 496 #t) (list 8128 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (perfect-module:is-perfect?
                      test-num)))
                (begin
                  (perfect-module-simple-assert
                   sub-name test-label-index
                   test-num
                   shouldbe result
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-pn-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-calc-pn-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 6) (list 3 28)
           (list 4 120) (list 5 496)
           (list 6 2016) (list 7 8128)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (perfect-module:calc-pn test-num)))
                (begin
                  (perfect-module-simple-assert
                   sub-name test-label-index
                   test-num
                   shouldbe result
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (perfect-module-assert-lists-equal
         shouldbe-list result-list
         sub-name error-message
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((err-1
             (format
              #f "~a : shouldbe list = ~a, result = ~a : "
              error-message shouldbe-list result-list))
            (err-2
             (format
              #f "shouldbe size = ~a : result size = ~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append
            err-1 err-2)
           result-hash-table)

          (if (equal? shouldbe-length result-length)
              (begin
                (for-each
                 (lambda (list-elem)
                   (begin
                     (let ((found-flag (member list-elem result-list)))
                       (let ((err-3
                              (format
                               #f "element not found = ~a : "
                               list-elem)))
                         (begin
                           (unittest2:assert?
                            (not (equal? found-flag #f))
                            sub-name
                            (string-append err-1 err-3)
                            result-hash-table)
                           )))
                     )) shouldbe-list)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
