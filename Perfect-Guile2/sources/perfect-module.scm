;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  perfect numbers                                      ###
;;;###                                                       ###
;;;###  last updated August 21, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start perfect-modules
(define-module (perfect-module)
  #:export (prime-factors-list
            compute-prime-exp-pairs
            fastexp
            sum-of-proper-divisors
            proper-divisors-list

            is-perfect?
            calc-pn
            count-digits

            euclid-main
            brute-force-main
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### ice-9 format used for advanced formatting
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;; srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((sprp-module)
              :renamer (symbol-prefix-proc 'sprp-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin function definitions                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;; use a seive of eratosthenes method of removing prime factors of n
(define (prime-factors-list nn)
  (define (local-divide-all-factors this-num this-factor)
    (begin
      (let ((ll-num this-num)
            (acc-list (list)))
        (begin
          (while (and
                  (zero? (modulo ll-num this-factor))
                  (> ll-num 1))
            (begin
              (set! ll-num (/ ll-num this-factor))
              (set! acc-list (cons this-factor acc-list))
              ))
          (list ll-num acc-list)
          ))
      ))
  (begin
    (let ((local-nn nn)
          (kk 3)
          (result-list (list)))
      (begin
        (if (sprp-module:sprp-prime? nn)
            (begin
              (list nn))
            (begin
              (if (zero? (modulo local-nn 2))
                  (begin
                    (let ((ll-list
                           (local-divide-all-factors
                            local-nn 2)))
                      (let ((ll-remainder
                             (list-ref ll-list 0))
                            (acc-list
                             (list-ref ll-list 1)))
                        (begin
                          (set! local-nn ll-remainder)
                          (set!
                           result-list
                           (append result-list acc-list))
                          )))
                    ))

              (while (> local-nn 1)
                (begin
                  (if (zero? (modulo local-nn kk))
                      (begin
                        (let ((ll-list
                               (local-divide-all-factors
                                local-nn kk)))
                          (let ((ll-remainder (list-ref ll-list 0))
                                (acc-list (list-ref ll-list 1)))
                            (begin
                              (set! local-nn ll-remainder)
                              (set!
                               result-list
                               (append result-list acc-list))
                              )))
                        ))
                  (set! kk (1+ kk))
                  ))

              result-list
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (compute-prime-exp-pairs pfactors-list)
  (begin
    (if (null? pfactors-list)
        (begin
          (list))
        (begin
          (let ((result-list (list))
                (prev-num -1)
                (prev-count 0)
                (sorted-pfactors (sort pfactors-list <)))
            (begin
              (for-each
               (lambda (pp)
                 (begin
                   (if (equal? pp prev-num)
                       (begin
                         (set! prev-count (1+ prev-count)))
                       (begin
                         (if (not (equal? prev-num -1))
                             (begin
                               (set!
                                result-list
                                (cons
                                 (list prev-num prev-count)
                                 result-list))
                               ))
                         (set! prev-num pp)
                         (set! prev-count 1)
                         ))
                   )) sorted-pfactors)

              (if (> prev-num 0)
                  (begin
                    (reverse
                     (cons
                      (list prev-num prev-count)
                      result-list))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
;;;### fastexp - fast computation of base^exp
(define (fastexp base exp)
  (begin
    (cond
     ((<= exp 0)
      (begin
        1
        ))
     ((even? exp)
      (begin
        (let ((num (fastexp base (/ exp 2))))
          (begin
            (* num num)
            ))
        ))
     (else
      (begin
        (* base (fastexp base (- exp 1)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (sum-of-proper-divisors this-num)
  (begin
    (cond
     ((< this-num 1)
      (begin
        #f
        ))
     ((= this-num 2)
      (begin
        1
        ))
     ((= this-num 3)
      (begin
        1
        ))
     (else
      (begin
        (let ((pfactors-list
               (prime-factors-list this-num)))
          (let ((prime-exp-list
                 (compute-prime-exp-pairs pfactors-list))
                (product 1))
            (begin
              (for-each
               (lambda (apair)
                 (begin
                   (let ((pp-ii (list-ref apair 0))
                         (aa-ii (list-ref apair 1)))
                     (let ((numer (1- (fastexp pp-ii (1+ aa-ii))))
                           (denom (1- pp-ii)))
                       (begin
                         (if (not (zero? denom))
                             (begin
                               (set!
                                product
                                (* product
                                   (euclidean/ numer denom))))
                             (begin
                               (display
                                (format
                                 #f "sum-of-proper-divisors error! "))
                               (display
                                (format
                                 #f "divide by zero!~%"))
                               (display
                                (format
                                 #f "number=~a, pfactors-list=~a, "
                                 this-num pfactors-list))
                               (display
                                (format
                                 #f "prime-exp-list=~a~%"
                                 prime-exp-list))
                               (display
                                (format
                                 #f "prime-pair = ~a, "
                                 apair))
                               (display
                                (format
                                 #f "numer=~a, denom=~a~%"
                                 numer denom))
                               (quit)
                               ))
                         )))
                   )) prime-exp-list)

              (- product this-num)
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (proper-divisors-list this-num)
  (begin
    (let ((div-list (list 1))
          (max-iter
           (1+ (exact-integer-sqrt this-num))))
      (begin
        (if (<= this-num 1)
            (begin
              (list))
            (begin
              (do ((ii 2 (1+ ii)))
                  ((> ii max-iter))
                (begin
                  (if (zero? (modulo this-num ii))
                      (begin
                        (let ((other-div (euclidean/ this-num ii)))
                          (begin
                            (if (< ii other-div)
                                (begin
                                  (set!
                                   div-list
                                   (cons
                                    ii (cons other-div div-list))))
                                (begin
                                  (if (= ii other-div)
                                      (begin
                                        (set!
                                         div-list
                                         (cons ii div-list))
                                        ))
                                  ))
                            ))
                        ))
                  ))

              (sort div-list <)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### is_perfect? - predicate #t if number is perfect
;;;###   (the sum of the proper divisors = number)
(define (is-perfect? nn)
  (begin
    (if (< nn 6)
        (begin
          #f)
        (begin
          (let ((dsum (sum-of-proper-divisors nn)))
            (begin
              (= nn dsum)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
;;;### calc-pn - calculate 2^(k-1) * (2^k - 1)
(define (calc-pn pp)
  (begin
    (cond
     ((< pp 1)
      (begin
        #f
        ))
     ((= pp 1)
      (begin
        1
        ))
     (else
      (begin
        (let ((ttmp (fastexp 2 (- pp 1))))
          (let ((twopm1 (- (* ttmp 2) 1)))
            (begin
              (* ttmp twopm1)
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (count-digits a-big-num)
  (begin
    (let ((ncount 0)
          (local-num a-big-num))
      (begin
        (while (> local-num 0)
          (begin
            (set! local-num (euclidean-quotient local-num 10))
            (set! ncount (1+ ncount))
            ))
        ncount
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-not-prime-data-macro
  (syntax-rules ()
    ((display-not-prime-data-macro
      jj t2 st2)
     (begin
       (display
        (ice-9-format:format
         #f "  (2^~:d - 1) is not a prime : " jj))

       (let ((tmplist (proper-divisors-list t2)))
         (begin
           (if (and (list? tmplist)
                    (> (length tmplist) 0))
               (begin
                 (let ((stmplist
                        (utils-module:number-list-to-string
                         tmplist)))
                   (begin
                     (display
                      (ice-9-format:format
                       #f "  factors of (2^~:d - 1) = ~a = "
                       jj st2))
                     (display
                      (format #f "~a" stmplist))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (print-result jj pn debug-flag)
  (begin
    (let ((t1 (fastexp 2 (- jj 1)))
          (fsum (sum-of-proper-divisors pn))
          (spn (utils-module:commafy-number pn)))
      (let ((t2 (- (* t1 2) 1))
            (pn-digits (count-digits pn)))
        (let ((bool_prime (sprp-module:sprp-prime? t2))
              (st1 (utils-module:commafy-number t1))
              (st2 (utils-module:commafy-number t2))
              (ssof (utils-module:commafy-number fsum))
              (sdigits (utils-module:commafy-number pn-digits)))
          (begin
            (display
             (ice-9-format:format
              #f "2^~:d * (2^~:d - 1) = (~a) * (~a) = ~a, "
              (- jj 1) jj st1 st2 spn))
            (display
             (ice-9-format:format
              #f "number of digits = ~a : " sdigits))
            (display
             (ice-9-format:format
              #f "(2^~:d - 1) = ~a ~a : sum of factors = ~a : "
              jj st2
              (if bool_prime "is prime!" "not a prime.")
              ssof))
            (display
             (ice-9-format:format
              #f "~a is ~a"
              spn
              (if (equal? pn fsum)
                  "perfect!"
                  "not perfect.")
              ))

            (force-output)

            (if (equal? debug-flag #t)
                (begin
                  (let ((factor-list
                         (proper-divisors-list pn)))
                    (let ((slist
                           (utils-module:number-list-to-string
                            factor-list)))
                      (begin
                        (display
                         (format
                          #f "  factors = ~a : "
                          slist))
                        (display
                         (format
                          #f "factors sum = ~a." ssof))

                        (if (not bool_prime)
                            (begin
                              (display-not-prime-data-macro
                               jj t2 st2)
                              ))
                        )))
                  ))

            (newline)
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  main routines                                        ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define-syntax display-status-message-macro
  (syntax-rules ()
    ((display-status-message-macro
      p-count jj end start-jday status-minutes)
     (begin
       (let ((end-jday (srfi-19:current-julian-day)))
         (let ((jminutes
                (timer-module:julian-day-difference-in-minutes
                 end-jday start-jday)))
           (begin
             (if (>= jminutes status-minutes)
                 (begin
                   (display
                    (ice-9-format:format
                     #f "found ~:d perfect numbers so far : "
                     p-count))
                   (display
                    (ice-9-format:format
                     #f "~:d / ~:d : " jj end))
                   (display
                    (format
                     #f "elapsed time = ~a : ~a~%"
                     (timer-module:julian-day-difference-to-string
                      end-jday start-jday)
                     (timer-module:current-date-time-string)))
                   (force-output)
                   (set! start-jday end-jday)
                   ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (euclid-main start end status-minutes debug-flag)
  (begin
    (let ((start-jday (srfi-19:current-julian-day))
          (p-count 0)
          (counter 0)
          (counter-check 100)
          (init-0 start))
      (begin
        (if (even? init-0)
            (begin
              (set! init-0 (1+ init-0))
              ))
        (if (<= start 2)
            (begin
              (let ((pn (calc-pn 2)))
                (begin
                  (if (and
                       (not (equal? pn #f))
                       (number? pn)
                       (is-perfect? pn))
                      (begin
                        (print-result 2 pn debug-flag)
                        (set! p-count (1+ p-count))
                        ))
                  ))
              ))

        (do ((jj init-0 (+ jj 2)))
            ((> jj end))
          (begin
            (if (sprp-module:sprp-prime? jj)
                (begin
                  (let ((pn (calc-pn jj)))
                    (begin
                      (if (and (not (equal? pn #f))
                               (number? pn))
                          (begin
                            (print-result jj pn debug-flag)
                            (if (is-perfect? pn)
                                (begin
                                  (set! p-count (1+ p-count))
                                  ))
                            ))
                      ))
                  ))

            (set! counter (1+ counter))
            (if (zero? (modulo counter counter-check))
                (begin
                  (display-status-message-macro
                   p-count jj end start-jday status-minutes)
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax brute-force-found-macro
  (syntax-rules ()
    ((brute-force-found-macro
      p-count jj debug-flag)
     (begin
       (set! p-count (1+ p-count))
       (if (equal? debug-flag #f)
           (begin
             (display
              (ice-9-format:format
               #f "~:d perfect!~%" jj))
             (force-output))
           (begin
             (let ((llist (proper-divisors-list jj)))
               (begin
                 (if (and (not (null? llist))
                          (> (length llist) 0))
                     (begin
                       (let ((factor-sum
                              (srfi-1:fold + 0 llist)))
                         (begin
                           (display
                            (ice-9-format:format
                             #f "~:d perfect! : "
                             jj))
                           (display
                            (ice-9-format:format
                             #f "factors = ~a, sum = ~:d~%"
                             (utils-module:number-list-to-string
                              llist)
                             factor-sum))
                           (force-output)
                           ))
                       ))
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (brute-force-main start end status-minutes debug-flag)
  (begin
    (let ((start-jday (srfi-19:current-julian-day))
          (counter 0)
          (counter-check 100)
          (p-count 0))
      (begin
        (do ((jj start (1+ jj)))
            ((> jj end))
          (begin
            (if (not (sprp-module:sprp-prime? jj))
                (begin
                  (if (is-perfect? jj)
                      (begin
                        (brute-force-found-macro
                         p-count jj debug-flag)
                        ))
                  ))

            (set! counter (1+ counter))
            (if (zero? (modulo counter counter-check))
                (begin
                  (display-status-message-macro
                   p-count jj end start-jday status-minutes)
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
