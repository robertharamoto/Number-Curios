################################################################
################################################################
###                                                          ###
###  make file (version 2024-09-25)                          ###
###  Sunday, October 06, 2024                                ###
###                                                          ###
################################################################
################################################################
GUILD=/usr/bin/guild

################################################################
################################################################
###                                                          ###
###  define directories                                      ###
###                                                          ###
################################################################
################################################################
SOURCESDIR=sources
TESTSDIR=tests
OBJECTSDIR=objects

### flags
WARNINGS=--warn=unsupported-warning --warn=unused-variable --warn=unused-toplevel --warn=unbound-variable --warn=arity-mismatch --warn=duplicate-case-datum --warn=bad-case-datum --warn=format
TESTWARNINGS=--warn=unsupported-warning --warn=unused-variable --warn=unbound-variable  --warn=arity-mismatch --warn=duplicate-case-datum --warn=bad-case-datum --warn=format
LOADFLAGS=-L $(SOURCESDIR) -L $(TESTSDIR) -L $(OBJECTSDIR)
ENVVARS=GUILE_AUTO_COMPILE=0 GUILE_WARN_DEPRECATED="detailed"


################################################################
################################################################
###                                                          ###
###  main definitions                                        ###
###                                                          ###
################################################################
################################################################
all: run-tests


################################################################
################################################################
###                                                          ###
###  individual source files                                 ###
###                                                          ###
################################################################
################################################################
$(OBJECTSDIR)/config-module.go: $(SOURCESDIR)/config-module.scm
	$(ENVVARS) $(GUILD) compile $(WARNINGS) $(LOADFLAGS) -o $(OBJECTSDIR)/config-module.go $(SOURCESDIR)/config-module.scm

$(OBJECTSDIR)/perfect-module.go: $(SOURCESDIR)/perfect-module.scm
	$(ENVVARS) $(GUILD) compile $(WARNINGS) $(LOADFLAGS) -o $(OBJECTSDIR)/perfect-module.go $(SOURCESDIR)/perfect-module.scm

$(OBJECTSDIR)/sprp-module.go: $(SOURCESDIR)/sprp-module.scm
	$(ENVVARS) $(GUILD) compile $(WARNINGS) $(LOADFLAGS) -o $(OBJECTSDIR)/sprp-module.go $(SOURCESDIR)/sprp-module.scm

$(OBJECTSDIR)/timer-module.go: $(SOURCESDIR)/timer-module.scm
	$(ENVVARS) $(GUILD) compile $(WARNINGS) $(LOADFLAGS) -o $(OBJECTSDIR)/timer-module.go $(SOURCESDIR)/timer-module.scm

$(OBJECTSDIR)/unittest2.go: $(SOURCESDIR)/unittest2.scm
	$(ENVVARS) $(GUILD) compile $(WARNINGS) $(LOADFLAGS) -o $(OBJECTSDIR)/unittest2.go $(SOURCESDIR)/unittest2.scm

$(OBJECTSDIR)/utils-module.go: $(SOURCESDIR)/utils-module.scm
	$(ENVVARS) $(GUILD) compile $(WARNINGS) $(LOADFLAGS) -o $(OBJECTSDIR)/utils-module.go $(SOURCESDIR)/utils-module.scm



################################################################
################################################################
###                                                          ###
###  individual test files                                   ###
###                                                          ###
################################################################
################################################################
$(OBJECTSDIR)/config-module-tests-1.go: $(TESTSDIR)/config-module-tests-1.scm
	$(ENVVARS) $(GUILD) compile $(TESTWARNINGS) $(LOADFLAGS) -o $(OBJECTSDIR)/config-module-tests-1.go $(TESTSDIR)/config-module-tests-1.scm

$(OBJECTSDIR)/perfect-module-tests-1.go: $(TESTSDIR)/perfect-module-tests-1.scm
	$(ENVVARS) $(GUILD) compile $(TESTWARNINGS) $(LOADFLAGS) -o $(OBJECTSDIR)/perfect-module-tests-1.go $(TESTSDIR)/perfect-module-tests-1.scm

$(OBJECTSDIR)/sprp-module-tests-1.go: $(TESTSDIR)/sprp-module-tests-1.scm
	$(ENVVARS) $(GUILD) compile $(TESTWARNINGS) $(LOADFLAGS) -o $(OBJECTSDIR)/sprp-module-tests-1.go $(TESTSDIR)/sprp-module-tests-1.scm

$(OBJECTSDIR)/timer-module-tests-1.go: $(TESTSDIR)/timer-module-tests-1.scm
	$(ENVVARS) $(GUILD) compile $(TESTWARNINGS) $(LOADFLAGS) -o $(OBJECTSDIR)/timer-module-tests-1.go $(TESTSDIR)/timer-module-tests-1.scm

$(OBJECTSDIR)/utils-module-tests-1.go: $(TESTSDIR)/utils-module-tests-1.scm
	$(ENVVARS) $(GUILD) compile $(TESTWARNINGS) $(LOADFLAGS) -o $(OBJECTSDIR)/utils-module-tests-1.go $(TESTSDIR)/utils-module-tests-1.scm



################################################################
################################################################
###                                                          ###
###  create objects directory if needed                      ###
###                                                          ###
################################################################
################################################################
$(OBJECTSDIR):
	-mkdir $(OBJECTSDIR)


################################################################
################################################################
###                                                          ###
###  compiler dependencies                                   ###
###                                                          ###
################################################################
################################################################
all-deps: $(OBJECTSDIR) $(OBJECTSDIR)/config-module.go $(OBJECTSDIR)/perfect-module.go $(OBJECTSDIR)/sprp-module.go $(OBJECTSDIR)/timer-module.go $(OBJECTSDIR)/unittest2.go $(OBJECTSDIR)/utils-module.go $(OBJECTSDIR)/config-module-tests-1.go $(OBJECTSDIR)/perfect-module-tests-1.go $(OBJECTSDIR)/sprp-module-tests-1.go $(OBJECTSDIR)/timer-module-tests-1.go $(OBJECTSDIR)/utils-module-tests-1.go 

################################################################
################################################################
###                                                          ###
###  run tests                                               ###
###                                                          ###
################################################################
################################################################
run-tests: all-deps
	-date > out-test.txt
	-./run-tests.scm >> out-test.txt 2>&1
	@echo  >> out-test.txt 2>&1
	@echo -n   sources loc = >> out-test.txt 2>&1
	@wc -l sources/*.scm | tail -n 1 >> out-test.txt 2>&1
	@echo -n   tests loc = >> out-test.txt 2>&1
	@wc -l tests/*.scm | tail -n 1 >> out-test.txt 2>&1
	@echo -n total loc =  >> out-test.txt 2>&1
	@wc -l tests/*.scm sources/*.scm | tail -n 1 >> out-test.txt 2>&1
	-cat out-test.txt


################################################################
################################################################
###                                                          ###
###  clean up                                                ###
###                                                          ###
################################################################
################################################################
.PHONY : clean
clean:
	-rm -fv $(OBJECTSDIR)/*.go
	-rm -fv $(SOURCESDIR)/*.scm~ 
	-rm -fv $(TESTSDIR)/*.scm~
	-rm -fv *.scm~ *.sh~ Makefile~


################################################################
################################################################
###                                                          ###
###  end of make file                                        ###
###                                                          ###
################################################################
################################################################
