;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  goldbach conjecture                                  ###
;;;###                                                       ###
;;;###  last updated August 21, 2024                         ###
;;;###                                                       ###
;;;###  updated June 2, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###
;;;### start goldbach-module
(define-module (goldbach-module)
  #:export (count-odd-sums
            even-prime-sums

            main-loop
            main-plot-loop
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### ice-9 for advanced format function
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

(use-modules ((sprp-module)
              :renamer (symbol-prefix-proc 'sprp-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin function definitions                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (count-odd-sums jj)
  (begin
    (let ((result 0))
      (begin
        (if (zero? (modulo jj 4))
            (begin
              (set! result (euclidean/ jj 4)))
            (begin
              (set! result (+ 1 (euclidean/ jj 4)))
              ))

        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax check-second-term-macro
  (syntax-rules ()
    ((check-second-term-macro
      ii enum max-prime prime-htable
      end-loop-flag result-list)
     (begin
       (let ((ii-tmp (- enum ii)))
         (begin
           (if (< ii-tmp ii)
               (begin
                 (set! end-loop-flag #t))
               (begin
                 (if (< ii-tmp max-prime)
                     (begin
                       (let ((is-prime-flag
                              (hash-ref
                               prime-htable ii-tmp #f)))
                         (begin
                           (if (equal? is-prime-flag #t)
                               (begin
                                 (set!
                                  result-list
                                  (cons (list ii ii-tmp) result-list))
                                 ))
                           )))
                     (begin
                       (if (sprp-module:sprp-prime? ii-tmp)
                           (begin
                             (set!
                              result-list
                              (cons (list ii ii-tmp) result-list))
                             ))
                       ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (even-prime-sums enum max-prime prime-htable)
  (begin
    (let ((half-max (euclidean/ enum 2))
          (result-list (list))
          (end-loop-flag #f))
      (begin
        (if (= enum 4)
            (begin
              (list (list 2 2)))
            (begin
              (do ((ii 3 (+ ii 2)))
                  ((or (> ii half-max)
                       (equal? end-loop-flag #t)))
                (begin
                  (if (< ii max-prime)
                      (begin
                        (let ((is-prime-flag
                               (hash-ref prime-htable ii #f)))
                          (begin
                            (if (equal? is-prime-flag #t)
                                (begin
                                  (check-second-term-macro
                                   ii enum max-prime prime-htable
                                   end-loop-flag result-list)
                                  ))
                            )))
                      (begin
                        (if (sprp-module:sprp-prime? ii)
                            (begin
                              (check-second-term-macro
                               ii enum max-prime prime-htable
                               end-loop-flag result-list)
                              ))
                        ))
                  ))

              (reverse result-list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (num-list-list-to-string jj p-list)
  (begin
    (let ((rstring
           (format
            #f "~a "
            (utils-module:commafy-number jj))))
      (begin
        (for-each
         (lambda (a-list)
           (begin
             (let ((a1 (list-ref a-list 0))
                   (a2 (list-ref a-list 1)))
               (let ((sa1
                      (utils-module:commafy-number a1))
                     (sa2
                      (utils-module:commafy-number a2)))
                 (let ((tstring
                        (format
                         #f "~a = ~a + ~a" rstring sa1 sa2)))
                   (begin
                     (set! rstring tstring)
                     ))
                 ))
             )) p-list)
        rstring
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (print-result jj p-list pn details-flag)
  (begin
    (let ((sjj
           (utils-module:commafy-number jj))
          (spcount
           (utils-module:commafy-number (length p-list)))
          (sp-list
           (num-list-list-to-string jj p-list))
          (spn
           (utils-module:commafy-number pn))
          (pcnt
           (utils-module:round-float
            (* 100.0 (/ (length p-list) pn)) 1.0)))
      (begin
        (if (equal? details-flag #f)
            (begin
              (display
               (ice-9-format:format
                #f "~a : ~a / ~a (~4,1f%)~%"
                sjj spcount spn pcnt)))
            (begin
              (display
               (ice-9-format:format
                #f "~a : ~a / ~a (~4,1f%) : ~a~%"
                sjj spcount spn pcnt sp-list))
              ))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (print-plot-result jj p-list pn details-flag)
  (begin
    (let ((sjj
           (utils-module:commafy-number jj))
          (spcount
           (utils-module:commafy-number (length p-list)))
          (spn
           (utils-module:commafy-number pn))
          (pcnt
           (utils-module:round-float
            (* 100.0 (/ (length p-list) pn)) 1.0)))
      (begin
        (if (equal? details-flag #f)
            (begin
              (display
               (ice-9-format:format
                #f "~a | ~a | ~a | ~4,1f%~%"
                sjj spcount spn pcnt)))
            (begin
              (let ((sp-list
                     (num-list-list-to-string jj p-list)))
                (begin
                  (display
                   (ice-9-format:format
                    #f "~a | ~a | ~a | ~4,1f% | ~a~%"
                    sjj spcount spn pcnt sp-list))
                  ))
              ))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###  main routines
(define (main-loop start end max-prime details-flag)
  (begin
    (let ((jstart start)
          (prime-htable (make-hash-table)))
      (begin
        (prime-module:populate-prime-hash! prime-htable max-prime)

        (if (odd? jstart)
            (begin
              (set! jstart (1+ jstart))
              ))
        (if (< jstart 4)
            (begin
              (set! jstart 4)
              ))

        (do ((jj jstart (+ jj 2)))
            ((> jj end))
          (begin
            (let ((p-list
                   (even-prime-sums
                    jj max-prime prime-htable))
                  (pn (count-odd-sums jj)))
              (begin
                (print-result jj p-list pn details-flag)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-plot-loop start end max-prime details-flag)
  (begin
    (let ((jstart start)
          (prime-htable (make-hash-table)))
      (begin
        (prime-module:populate-prime-hash! prime-htable max-prime)

        (if (odd? jstart)
            (begin
              (set! jstart (1+ jstart))
              ))
        (if (< jstart 4)
            (begin
              (set! jstart 4)
              ))

        (do ((jj jstart (+ jj 2)))
            ((> jj end))
          (begin
            (let ((p-list
                   (even-prime-sums
                    jj max-prime prime-htable))
                  (pn (count-odd-sums jj)))
              (begin
                (print-plot-result jj p-list pn details-flag)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
