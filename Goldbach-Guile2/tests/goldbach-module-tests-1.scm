;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for goldbach-module.scm                   ###
;;;###                                                       ###
;;;###  last updated August 21, 2024                         ###
;;;###                                                       ###
;;;###  updated June 2, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

(use-modules ((goldbach-module)
              :renamer (symbol-prefix-proc 'goldbach-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-odd-sums-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-count-odd-sums-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 4 1) (list 6 2) (list 8 2) (list 10 3)
           (list 12 3) (list 14 4) (list 16 4)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (goldbach-module:count-odd-sums test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-even-prime-sums-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-even-prime-sums-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 4 (list (list 2 2)))
           (list 6 (list (list 3 3)))
           (list 8 (list (list 3 5)))
           (list 10 (list (list 3 7) (list 5 5)))
           (list 12 (list (list 5 7)))
           (list 14 (list (list 3 11) (list 7 7)))
           (list 16 (list (list 3 13) (list 5 11)))
           (list 18 (list (list 5 13) (list 7 11)))
           (list 20 (list (list 3 17) (list 7 13)))
           (list 22 (list (list 3 19) (list 5 17) (list 11 11)))
           (list 24 (list (list 5 19) (list 7 17) (list 11 13)))
           (list 26 (list (list 3 23) (list 7 19) (list 13 13)))
           (list 28 (list (list 5 23) (list 11 17)))
           (list 30 (list (list 7 23) (list 11 19) (list 13 17)))
           ))
         (prime-htable (make-hash-table 100))
         (max-prime 20)
         (test-label-index 0))
     (begin
       (prime-module:populate-prime-hash! prime-htable max-prime)
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((result-list-list
                     (goldbach-module:even-prime-sums
                      test-num max-prime prime-htable)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : num=~a, "
                          sub-name test-label-index test-num))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a : "
                          shouldbe-list-list result-list-list))
                        (err-3
                         (format
                          #f "shouldbe length=~a, result=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2 err-3)
                       result-hash-table)

                      (for-each
                       (lambda (slist)
                         (begin
                           (let ((err-4
                                  (format
                                   #f "missing element shouldbe=~a"
                                   slist))
                                 (mflag
                                  (member slist result-list-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal? mflag #f))
                                sub-name
                                (string-append err-1 err-2 err-4)
                                result-hash-table)
                               ))
                           )) shouldbe-list-list)
                      )))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
