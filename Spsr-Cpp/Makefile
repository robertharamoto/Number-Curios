###############################################################
###############################################################
###                                                         ###
###  make file (version 2024-09-25)                         ###
###  Wednesday, September 25, 2024                          ###
###                                                         ###
###############################################################
###############################################################

###############################################################
###############################################################
###                                                         ###
###  define directories                                     ###
###                                                         ###
###############################################################
###############################################################
PROGNAME=spsr
SOURCESDIR=sources
INCLUDESDIR=sources
TESTSDIR=tests
TESTINCLUDESDIR=tests
OBJECTSDIR=objects


###############################################################
###############################################################
###                                                         ###
###  main definitions                                       ###
###                                                         ###
###############################################################
###############################################################
all: objects spsr run-tests

OPTIFLAG = -O3
INCLUDE = -I. -I sources/ -Itests
WARN1FLAGS = -Wall -Wextra -Weffc++ -Wpedantic
WARN2FLAGS = -Wno-braced-scalar-init -Wno-unused-command-line-argument
WARNFLAGS = $(WARN1FLAGS) $(WARN2FLAGS)
LINKFLAGS = -L/usr/lib -lm -lgmp -lgmpxx -lrt -lpthread -lsqlite3
CFLAGS = $(WARNFLAGS) -ansi -std=c++17 $(OPTIFLAG)
TESTCXXFLAGS = -lcppunit -g $(LINKFLAGS)
CXXFLAGS = $(LINKFLAGS) $(OPTFLAG)
CXX = clang++


###############################################################
###############################################################
###                                                         ###
###  object dependencies                                    ###
###                                                         ###
###############################################################
###############################################################
MAINOBJECTS =  $(OBJECTSDIR)/argopts.o $(OBJECTSDIR)/configure.o $(OBJECTSDIR)/spsr.o $(OBJECTSDIR)/utils.o

TESTOBJECTS = $(OBJECTSDIR)/argopts_test.o $(OBJECTSDIR)/configure_test.o $(OBJECTSDIR)/spsr_test.o $(OBJECTSDIR)/utils_test.o

###############################################################
###############################################################
###                                                         ###
###  individual source file dependencies                    ###
###                                                         ###
###############################################################
###############################################################
$(OBJECTSDIR)/argopts.o : $(SOURCESDIR)/argopts.cpp $(INCLUDESDIR)/argopts.h
	$(CXX) $(SOURCESDIR)/argopts.cpp -c -o $(OBJECTSDIR)/argopts.o $(INCLUDE) $(CFLAGS) $(CXXFLAGS)

$(OBJECTSDIR)/configure.o : $(SOURCESDIR)/configure.cpp $(INCLUDESDIR)/configure.h
	$(CXX) $(SOURCESDIR)/configure.cpp -c -o $(OBJECTSDIR)/configure.o $(INCLUDE) $(CFLAGS) $(CXXFLAGS)

$(OBJECTSDIR)/main.o : $(SOURCESDIR)/main.cpp
	$(CXX) $(SOURCESDIR)/main.cpp -c -o $(OBJECTSDIR)/main.o $(INCLUDE) $(CFLAGS) $(CXXFLAGS)

$(OBJECTSDIR)/spsr.o : $(SOURCESDIR)/spsr.cpp $(INCLUDESDIR)/spsr.h
	$(CXX) $(SOURCESDIR)/spsr.cpp -c -o $(OBJECTSDIR)/spsr.o $(INCLUDE) $(CFLAGS) $(CXXFLAGS)

$(OBJECTSDIR)/utils.o : $(SOURCESDIR)/utils.cpp $(INCLUDESDIR)/utils.h
	$(CXX) $(SOURCESDIR)/utils.cpp -c -o $(OBJECTSDIR)/utils.o $(INCLUDE) $(CFLAGS) $(CXXFLAGS)


###############################################################
###############################################################
###                                                         ###
###  individual test file dependencies                      ###
###                                                         ###
###############################################################
###############################################################
$(OBJECTSDIR)/argopts_test.o : $(TESTSDIR)/argopts_test.cpp $(TESTINCLUDESDIR)/argopts_test.h
	$(CXX) $(TESTSDIR)/argopts_test.cpp -c -o $(OBJECTSDIR)/argopts_test.o $(INCLUDE) $(CFLAGS) $(TESTCXXFLAGS)

$(OBJECTSDIR)/configure_test.o : $(TESTSDIR)/configure_test.cpp $(TESTINCLUDESDIR)/configure_test.h
	$(CXX) $(TESTSDIR)/configure_test.cpp -c -o $(OBJECTSDIR)/configure_test.o $(INCLUDE) $(CFLAGS) $(TESTCXXFLAGS)

$(OBJECTSDIR)/spsr_test.o : $(TESTSDIR)/spsr_test.cpp $(TESTINCLUDESDIR)/spsr_test.h
	$(CXX) $(TESTSDIR)/spsr_test.cpp -c -o $(OBJECTSDIR)/spsr_test.o $(INCLUDE) $(CFLAGS) $(TESTCXXFLAGS)

$(OBJECTSDIR)/testmain.o : $(TESTSDIR)/testmain.cpp
	$(CXX) $(TESTSDIR)/testmain.cpp -c -o $(OBJECTSDIR)/testmain.o $(INCLUDE) $(CFLAGS) $(TESTCXXFLAGS)

$(OBJECTSDIR)/utils_test.o : $(TESTSDIR)/utils_test.cpp $(TESTINCLUDESDIR)/utils_test.h
	$(CXX) $(TESTSDIR)/utils_test.cpp -c -o $(OBJECTSDIR)/utils_test.o $(INCLUDE) $(CFLAGS) $(TESTCXXFLAGS)


###############################################################
###############################################################
###                                                         ###
###  compile dependencies                                   ###
###                                                         ###
###############################################################
###############################################################
$(OBJECTSDIR) :
	if [ ! -d "objects" ] ; then mkdir $(OBJECTSDIR) ; fi

###############################################################
###############################################################
###                                                         ###
###  main build                                             ###
###                                                         ###
###############################################################
###############################################################
$(PROGNAME) : $(OBJECTSDIR)/main.o $(MAINOBJECTS)
	@echo
	@echo generating $(PROGNAME)
	$(CXX) $(OBJECTSDIR)/main.o $(MAINOBJECTS) -o $(PROGNAME) $(CXXFLAGS) $(CFLAGS)
	@echo '==============================================================='


###############################################################
###############################################################
###                                                         ###
###  test build                                             ###
###                                                         ###
###############################################################
###############################################################
testmain :  $(OBJECTSDIR)/testmain.o $(TESTOBJECTS) $(MAINOBJECTS)
	@echo
	@echo generating testmain
	$(CXX) $(OBJECTSDIR)/testmain.o -o testmain $(TESTOBJECTS) $(MAINOBJECTS) $(TESTCXXFLAGS) $(CFLAGS)
	@echo '==============================================================='


###############################################################
###############################################################
###                                                         ###
###  run tests                                              ###
###                                                         ###
###############################################################
###############################################################
run-tests : testmain
	-./lint-all.sh > out-lint.txt 2>&1
	@date +"%A, %B %d, %Y  %r" > out-test.txt
	-./testmain >> out-test.txt 2>&1
	@echo -n 'source files loc  ' >> out-test.txt
	@wc -l sources/* | tail -n 1 >> out-test.txt
	@echo -n 'test files loc  ' >> out-test.txt
	@wc -l tests/* | tail -n 1 >> out-test.txt
	@echo -n 'sources + tests loc  ' >> out-test.txt
	@wc -l sources/* tests/* | tail -n 1 >> out-test.txt
	@date +"%A, %B %d, %Y  %r" >> out-test.txt
	-cat out-test.txt




###############################################################
###############################################################
###                                                         ###
###  clean up                                               ###
###                                                         ###
###############################################################
###############################################################
.PHONY : clean
clean:
	-rm -fv $(OBJECTSDIR)/*.o
	-rm -fv $(SOURCESDIR)/*.cc~ $(SOURCESDIR)/*.cpp~
	-rm -fv $(INCLUDESDIR)/*.h~
	-rm -fv $(TESTSDIR)/*.cc~ $(TESTSDIR)/*.cpp~
	-rm -fv $(TESTINCLUDESDIR)/*.h~
	-rm -fv spsr
	-rm -fv testmain
	-rm -fv *.scm~ *.sh~ Makefile~


###############################################################
###############################################################
###                                                         ###
###  end of file                                            ###
###                                                         ###
###############################################################
###############################################################
