#! /bin/bash

################################################################
################################################################
###                                                          ###
###  last updated May 3, 2020                                ###
###                                                          ###
################################################################
################################################################

if [ -f "$1" ]
then
    FNAME="$1"
else
    FNAME="out0.log"
fi

if [ ! -f "${FNAME}" ]
then
    echo "file '${FNAME}' not found, exiting..."
    exit 0
fi

SLEEPTIME=300
PROGNAME="spsr"

counter=0

this_process=`ps -ef | grep -i "${PROGNAME}"`
while [ -n "${this_process}" ]
do
    counter=$(( ${counter} + 1 ))

    echo "========================================================================"
    echo "========================================================================"
    echo "[${counter}] Simple Perfect Squared Rectangles"

    this_process=`ps -ef | grep -i spsr | grep -vi grep`
    this_user=`echo ${this_process} | awk '{ print $1; }'`
    this_pid=`echo ${this_process} | awk '{ print $2; }'`
    this_cpu_time=`echo ${this_process} | awk '{ print $7; }'`

    echo "${this_user} (${this_pid}) : cpu time = ${this_cpu_time}"

    echo " "
    echo -n "size = "
    ls --size --human-readable --color ${FNAME}
    echo -n "lines = "
    wc -l ${FNAME}
    tail -4 ${FNAME}

    echo " "

    date "+%A, %B %d, %Y  %r"

    sleep ${SLEEPTIME}

done

################################################################
################################################################
###                                                          ###
###  end of file                                             ###
###                                                          ###
################################################################
################################################################
