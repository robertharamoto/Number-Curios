################################################################
################################################################
###                                                          ###
###  Simple Perfect Squared Rectangles                       ###
###                                                          ###
###  written by Robert Haramoto                              ###
###                                                          ###
################################################################
################################################################

Introduction to Simple Perfect Squared Rectangles

Simple - no subset of the squares form a rectangle

Perfect - every small square has a different size

A squared rectangle is a rectangle which is tiled by squares.
A perfect squared rectangle contains no two equal tiling squares.
A simple perfect squared rectangle contains no subset of squares
that form a rectangle.  It is also called a geometrical
dissection.

This is an interesting constrained problem, where the area of
certain rectangles can be tiled by distinct squares. This is
similar to saying that the sum of squares is equal to some
product (length x width).  It's not exactly the same since it
must also obey the constraints of fitting all squares into
the rectangle.

I first came across this problem from the website:
https://www.primepuzzles.net/problems/prob_048.htm


################################################################
################################################################
Unique partitions of an integer

This method uses integer partitions with lexicographic permutations.

This version uses a partition of an integer method to find all
possible partitions of an integer to tile the rectangle.
The advantage of this method is that it only has to examine the
flat areas of the partially tiled rectangle.

This method is relatively quick (for small widths,) since for
row 0, the integer partition function (the successor function,)
can find all integer sums that equal to the width of the
rectangle, similar to the stars and bars algorithm.

The successor partition function for 5 looks like this:
(5)
(4 1)
(3 2)
(3 1 1)
(2 2 1)
(2 1 1 1)
(1 1 1 1 1)


So 33 might be 32+1, 20+10+3, or 18+15, and each possibility
is checked, along with each permutation of each possibility.

If 18+15 is the current row 0 that we are checking, then
the minimum available row is above the 15 square.  So partitions
and permutations of 15 are computed and tried.  If 3+12 is being
considered, then a procedure is done to group together the
squares 18 with the 15+3, and the 15+12.  In this way, flat
surfaces, and next minimal width, can be easily found.

Then a permutation function generates all possible arrangements
of each partition (without duplicates).

The algorithm looks at row 0, and finds all possible partitions
and permutations, then looks for the minimum row to tile next.

This method has the advantage of being a comprehensive algorithm,
so it can find all possible ways to tile a rectangle.  The
disadvantage is that it can be slow, for a rectangle of
width 33, it takes a fraction of a second, whereas for a
rectangle of width 80 it takes around 3 hours (with 4 threads).


################################################################
################################################################
Simplification

A simplification can be done for row 0, since the you can't
have any squares of size 1, 2, or 3.

To see this, suppose a 1 is on row 0, and bigger squares are
on either side of it, then no other square can sit on top
of 1, other than 1.

Suppose 2 is on row 0, then you are allowed to use the 1 square
to sit on the 2, but no other squares can sit on 2 perfectly.

Suppose 3 is on row 0, and squares 4 and 5 bracket it.  Then
3 can be tiled by 1 and 2, where the 3+1 can match the 4 square
in height, and the 3+2 squares can match the 5 square in height.
Unfortunately, the only possible way for the 4 square to be
covered (it is now 4+1), is to use a 5 square (already used),
or to use a (4+1 square which is already being used), or to
use a (2+3 squares, which are already being used).

We can simplify things even further by reversing the row 0
partition and permutation.  This is a mirror symmetry that
can cut the number of partitions and permutations in half.
For example, for the partition of (25, 17, 23) is the same
as (23, 17, 25), (for row 0).

################################################################
################################################################
Parallelism

To make use of multiple cores on the processor, it is necessary
to use c++'s futures and async instructions.  However, making
the algorithm parallel with 4 threads does not mean making the
program 4x faster, since some permutations of some partitions
take an exceptionally long time.  This means that the
3 threads stop while waiting for 4th to complete.


################################################################
################################################################
Details

To run this program, it assumes that you have installed:
/usr/bin/guile
/usr/bin/clang++ (version 14 or better)
/usr/bin/cpplint
(from debian packages)

This program tries to comply with Google's c++ style guide,
the idea being that good style can help to reduce the complexity
of c++.
https://google.github.io/styleguide/cppguide.html


guile is used for the gen-make.scm program, which makes the
make file.  This program gathers together all files in the sources
and tests directories and constructs the make file.
  ./gen-make.scm
  make

To run the program type:
./spsr --config init0.config > out0.log &

init0.config - prints out results up to width 34
init1.config - prints out results for width 65
init2.config - prints out results from 57 to 100


################################################################
################################################################
Results

Spsr of order 9
http://www.squaring.net/sq/sr/spsr/o9/order9_spsr.html
Examples found:
[ 32 x 33 ] : ((15, 18) (8, 7) (4, 14) (1, 10) (9))
  (total elapsed time for 33 = 0.04 seconds, parallel, 6 threads)

[ 64 x 66 ] : ((30 36) (16 14) (8 28) (2 20) (18))
  (total elapsed time for 66 = 3 minutes, 25 seconds, parallel, 6 threads)

This is a new result, not found on squaring.net,
(as of <2024-08-19 Mon> so here is a schematic
diagram showing that it's right:
(  30  ) (  36  )
 (16 14)
         (8  28)
     (2  20)
 ( 18 )


[ 61 x 69 ] : ((33 36) (28 5) (2 9 25) (7) (16))
  (total elapsed time for 69 = 9 minutes, 27 seconds, parallel, 6 threads)



################################################################
################################################################
Spsr of order 10
http://www.squaring.net/sq/sr/spsr/o10/order10_spsr.html
[ 55 x 57 ] : ((27, 30) (13, 11, 3) (8, 25) (2, 17) (15))
  (total elapsed time for 57 = 29 seconds, parallel, 6 threads)

[ 47 x 65 ] : ((23 17 25) (6 11) (24 5) (3 22) (19))
  (total elapsed time for 65 = 3 minutes, 9 seconds, parallel, 6 threads)


################################################################
################################################################
Spsr of order 12
http://www.squaring.net/sq/sr/spsr/o12/order12_spsr.html
[ 80 x 81 ] : ((44, 37) (7, 9, 21) (36, 13, 2) (11) (23, 1) (22))
  (total elapsed time for 81 = 4 hours, 4 minutes, parallel, 4 threads)


################################################################
################################################################
Spsr of order 14
http://www.squaring.net/sq/sr/spsr/o14/order14_spsr.html
[ 77 x 83 ] : ((43, 40) (12, 8, 7, 13) (34, 9) (1, 6) (4, 5) (25) (24))
  (total elapsed time for 83 = 6 hours, 3 minutes, parallel, 4 threads)


################################################################
################################################################
Spsr of order 16
http://www.squaring.net/sq/sr/spsr/o16/order16_spsr.html
[ 73 x 75 ] : ((41, 34) (4, 5, 9, 16) (3, 1) (6) (32, 12)
               (2, 7) (8) (23) (20))
  (total elapsed time for 75 = 58 minutes, parallel, 4 threads)

[ 74 x 79 ] : ((45, 34) (11, 23) (29, 15, 12) (3, 7, 8, 17)
               (14, 4) (10, 1) (9))
  (total elapsed time for 79 = 2 hours, 24 minutes, parallel, 4 threads)


################################################################
################################################################
Spsr of order 17
http://www.squaring.net/sq/sr/spsr/o17/o17spsr-s_01.html

[ 75 x 82 ] : ((43, 39) (4, 5, 11, 19) (32, 14, 1) (6)
               (7, 10) (18, 3) (2, 17) (15))
  (total elapsed time for 82 = 4 hours, 53 minutes, parallel, 4 threads)

[ 60 x 84 ] : ((28, 33, 23) (7, 16) (15, 13) (3, 4)
               (8, 27, 1) (5) (21) (2, 19) (17))
  (total elapsed time for 84 = 8 hours, 38 minutes, parallel, 4 threads)


################################################################
################################################################
Spsr of order 20

16 tilings of a rectangle of size 65 x 79
(These tilings were identified by discarding results that
were "mirror" permutations in the first row, or a 180
degree rotation of the rectangle.)  Here are a couple of
tiled rectangles that were found:

[ 65 x 79 ] : ((32, 25, 22) (3, 19) (17, 11) (14, 18)
               (6, 5) (24) (23) (10, 4) (7, 15) (9, 1) (8))
  (total elapsed time for 79 = 2 hours, 24 minutes, parallel, 4 threads)

[ 65 x 79 ] : ((32, 24, 23) (6, 17) (19, 5) (11) (18, 14)
               (3, 25) (22) (4, 10) (15, 7) (1, 9) (8))
  (total elapsed time for 79 = 2 hours, 24 minutes, parallel, 4 threads)


16 tilings of a rectangle of size 65 x 80
Here are a couple of tiled rectangles that were found:
[ 65 x 80 ] : ((33, 25, 22) (3, 19) (17, 11) (18, 15)
               (6, 5) (24) (23) (7, 8) (14, 4) (10, 1) (9))
  (total elapsed time for 80 = 2 hours, 45 minutes, parallel, 4 threads)

[ 65 x 80 ] : ((33, 23, 24) (17, 6) (5, 19) (11)
               (9, 10, 14) (25, 3) (8, 1) (7, 4, 22) (18) (15))
  (total elapsed time for 80 = 2 hours, 51 minutes, parallel, 4 threads)




################################################################
################################################################
Comparison

|-------+------------------------+------------------------+----------|
|       | scheme 6 threads       | c++ 6 threads          | speed-up |
| width | spsr.scm               | spsr                   |   factor |
|-------+------------------------+------------------------+----------|
|    33 | 1 second               | 0.04 seconds           |       25 |
|    57 | 7 minutes, 3 seconds   | 29 seconds             |     14.6 |
|    58 | 6 minutes, 48 seconds  | 26 seconds             |     15.7 |
|    59 | 9 minutes, 46 seconds  | 45 seconds             |       13 |
|    60 | 10 minutes, 27 seconds | 47 seconds             |     13.3 |
|    61 | 13 minutes, 28 seconds | 1 minute, 13 seconds   |       11 |
|    62 | 16 minutes, 24 seconds | 1 minute, 15 seconds   |     13.1 |
|    63 | 21 minutes, 30 seconds | 2 minutes, 8 seconds   |     10.1 |
|    64 | 27 minutes, 7 seconds  | 2 minutes, 9 seconds   |     12.6 |
|    65 | 32 minutes, 25 seconds | 3 minutes, 8 seconds   |     10.3 |
|    66 | 43 minutes, 11 seconds | 3 minutes, 30 seconds  |     12.3 |
|    67 | 1 hour, 7 minutes      | 5 minutes, 46 seconds  |     11.6 |
|    68 | 1 hour, 6 minutes      | 6 minutes, 48 seconds  |      9.7 |
|    69 | 1 hour, 38 minutes     | 9 minutes, 28 seconds  |     10.3 |
|    70 | 1 hour, 36 minutes     | 8 minutes, 6 seconds   |     11.8 |
|    71 | 2 hours, 31 minutes    | 14 minutes, 17 seconds |     10.6 |
|    72 | 3 hours, 39 minutes    | 17 minutes, 49 seconds |     12.3 |
|    73 | 3 hours, 55 minutes    | 23 minutes, 42 seconds |      9.9 |
|    74 | 4 hours, 22 minutes    | 23 minutes, 5 seconds  |     11.3 |
|    75 | 7 hours, 8 minutes     | 38 minutes, 26 seconds |     11.1 |
|    76 | 9 hours, 1 minute      | 47 minutes, 25 seconds |     11.4 |
|    77 |                        | 1 hour, 7 minutes      |          |
|    78 |                        | 1 hour, 7 minutes      |          |
|    79 |                        | 1 hour, 40 minutes     |          |
|    80 |                        | 1 hour, 46 minutes     |          |
|    81 |                        | 2 hours, 56 minutes    |          |
|    82 |                        | 2 hours, 56 minutes    |          |
|    83 |                        | 4 hours, 35 minutes    |          |
|    84 |                        | 5 hours, 24 minutes    |          |
|    85 |                        | 6 hours, 30 minutes    |          |
|    86 |                        | 6 hours, 57 minutes    |          |
|    87 |                        | 12 hours, 17 minutes   |          |
|    88 |                        | 13 hours, 55 minutes   |          |
|-------+------------------------+------------------------+----------|


################################################################
################################################################
Additional reading:

(1) Squaring.net
http://www.squaring.net/index.html
see also: http://www.squaring.net/sq/tws.html

(2) "A Computer Algorithm for Finding the Dual of a Polar Network" by Sreekar M.Shastry
https://www.math.washington.edu/~reu/papers/1997/shastry/shastry.ps
Interesting transformation of the problem to one of electrical networks.
see also: https://arxiv.org/pdf/1303.0599.pdf

(3) A.J.W. Duijvestijn, Electronic computation of squared rectangles. Dissertation
https://alexandria.tue.nl/repository/books/44157.pdf
or https://pure.tue.nl/ws/files/1854405/44157.pdf

(4) Generate unique partitions of an integer
https://www.geeksforgeeks.org/generate-unique-partitions-of-an-integer/

(5) Stars and bars algorithm
https://en.wikipedia.org/wiki/Stars_and_bars_(combinatorics)

(6) Generating permutations
https://www.topcoder.com/generating-permutations/

(7) Generate permutations of an array (c++)
https://www.geeksforgeeks.org/all-permutations-of-an-array-using-stl-in-c/

(8) C++ is defective
https://yosefk.com/c++fqa/defective.html
Difficulties working with C++.

(9) Google's C++ Style Guide
https://google.github.io/styleguide/cppguide.html
With over 100 million lines of C++ code in Google's
repository, they have developed some very good style
recommendations.



################################################################
################################################################
Notes:

Requires the Debian cpplint package to be installed.  The
makefile calls lint-all.sh which makes sure that the cpp
programs comply with the google c++ styleguide
https://google.github.io/styleguide/cppguide.html



################################################################
################################################################
History

last updated <2025-01-27 Mon> - updated documentation
updated <2024-09-27 Fri> - removed symmetric partitions
                           and permutations from
                           consideration
updated <2024-04-24 Wed> - standardized time/duration
                           use pointers not references
updated <2022-07-24 Sun> - added parallel processing -
                           using futures/async
updated <2020-05-09 Sat> - integer partition and permutation
                           method


################################################################
################################################################

################################################################
################################################################
###                                                          ###
###  end of file                                             ###
###                                                          ###
################################################################
################################################################
