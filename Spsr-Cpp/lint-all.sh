#! /bin/bash

################################################################
################################################################
###                                                          ###
###  lint-all.sh procedure                                   ###
###    run this script at the end of the week                ###
###                                                          ###
###  last updated January 22, 2025                           ###
###    changed to cpplint in debian packages                 ###
###                                                          ###
###  written by Robert Haramoto                              ###
###                                                          ###
################################################################
################################################################

###
###  last updated January 22, 2025
###    changed to cpplint in debian packages
###
###  updated April 4, 2024
###    added some date/time status
###

################################################################
################################################################
###                                                          ###
###  functions to find the time spent                        ###
###                                                          ###
################################################################
################################################################
time_spent()
{
    local LOCAL_START_TIME="$1"
    local LOCAL_END_TIME="$2"

    local START_SECS=$(echo "${LOCAL_START_TIME}" | gawk '{split($0, arr, ":"); print arr[1]*3600+arr[2]*60+arr[3];}')
    local END_SECS=$(echo "${LOCAL_END_TIME}" | gawk '{split($0, arr, ":"); print arr[1]*3600+arr[2]*60+arr[3];}')

    local total_seconds=$((${END_SECS} - ${START_SECS}))

    if [ "${total_seconds}" -lt 0 ]
    then
        local total_secs=$((${total_seconds} + 86400))
    else
        local total_secs=$((${total_seconds}))
    fi

    local hours=$((${total_secs} / 3600))
    local rem_secs=$((${total_secs} - ${hours} * 3600))
    local minutes=$((${rem_secs} / 60))
    local seconds=$((${rem_secs} - 60 * ${minutes}))

    echo "Start time = ${LOCAL_START_TIME}, end time = ${LOCAL_END_TIME}"

    if [ "${hours}" -eq 0 ]
    then
        echo "Total time elapsed = ${minutes} minutes, ${seconds} seconds"
    else
        echo "Total time elapsed = ${hours} hours, ${minutes} minutes, ${seconds} seconds"
    fi

    return
}

################################################################
################################################################
###                                                          ###
###  main code                                               ###
###                                                          ###
################################################################
################################################################
DATE_FORMAT="%A, %B %d, %Y  %r"
VERSION="2025-01-22"

echo "$(date +"${DATE_FORMAT}")"

START_TIME=$(date "+%H:%M:%S")

echo " "
cpplint sources/*.h tests/*.h

cpplint sources/*.cpp tests/*.cpp


SLINES=$(wc -l sources/*.h sources/*.cpp | \
             tail -1 | gawk -e '{ printf( $1 ) }')
TLINES=$(wc -l tests/*.h tests/*.cpp | \
             tail -1 | gawk -e '{ printf( $1 ) }')
TOTALLINES=$((${SLINES} + ${TLINES}))

echo " "
echo "source files loc = ${SLINES}"
echo "test files loc = ${TLINES}"
echo "sources + tests loc = ${TOTALLINES}"
echo " "

################################################################
################################################################

END_TIME=$(date "+%H:%M:%S")

time_spent ${START_TIME} ${END_TIME}

echo -n "completed ${0} (version ${VERSION}) : "
echo "$(date +"${DATE_FORMAT}")"

echo ""

################################################################
################################################################
###                                                          ###
###  end of file                                             ###
###                                                          ###
################################################################
################################################################
