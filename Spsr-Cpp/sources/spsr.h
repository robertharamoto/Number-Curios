#ifndef SOURCES_SPSR_H_
#define SOURCES_SPSR_H_
/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CSpsr class definition                                ###
  ###                                                        ###
  ###  last updated August 20, 2024                          ###
  ###    added symmetry checks for row 0                     ###
  ###                                                        ###
  ###  updated May 6, 2024                                   ###
  ###    changed arguments from references to pointers       ###
  ###                                                        ###
  ###  updated July 11, 2022                                 ###
  ###                                                        ###
  ###  updated April 28, 2020                                ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cstdint>
#include <ctime>
#include <vector>
#include <map>
#include <string>
#include <future>
#include <chrono>

namespace mylib {

class CSpsr {
 public:
  CSpsr();
  ~CSpsr();

struct spsr_struct {
int64_t square_size = 0L;
int64_t start_row = 0L;
int64_t start_column = 0L;
};

struct rect_struct {
int64_t start_row = 0L;
int64_t start_column = 0L;
int64_t end_row = 0L;
int64_t end_column = 0L;
};

struct output_struct {
int64_t order = 0L;
int64_t height = 0L;
int64_t width = 0L;
int64_t first_square = 0L;
std::vector<CSpsr::spsr_struct> vs_vector = {};
std::string bk_string = "";
};

// ### utility functions
static bool sort_rect_function(
    const rect_struct aa,
    const rect_struct bb);

static bool sort_spsr_function(
    const mylib::CSpsr::spsr_struct aa,
    const mylib::CSpsr::spsr_struct bb);

static bool sort_output_function(
    const output_struct aa,
    const output_struct bb);

std::string rect_struct_to_string(
    const mylib::CSpsr::rect_struct *aa) const;

std::string vector_rect_struct_to_string(
    const std::vector<mylib::CSpsr::rect_struct> *aa) const;

std::string spsr_struct_to_string(
    const mylib::CSpsr::spsr_struct *aa) const;

std::string vector_spsr_struct_to_string(
    const std::vector<mylib::CSpsr::spsr_struct> *aa) const;

bool IsPrime(const int64_t ltmp) const;

bool SuccessorOfIntegerPartition(
    int64_t part_num, std::vector<int64_t> *avec) const;

void ConvertRow0ToRectangleVector(
    const std::vector<int64_t> *input_vector,
    std::vector<rect_struct> *rectangle_vector) const;

void FindMinRowColWidthHeight(
    const std::vector<rect_struct> *rectangle_vector,
    int64_t *min_row, int64_t *min_column,
    int64_t *min_width, int64_t *min_height) const;

void AddPartitionToRectangleStruct(
    const std::vector<int64_t> *input_vector,
    const std::vector<rect_struct> *input_rectangle_vector,
    std::vector<rect_struct> *output_rectangle_vector) const;

void CombineRectangles(
    const std::vector<mylib::CSpsr::rect_struct>
    *input_rect_vector,
    std::vector<mylib::CSpsr::rect_struct>
    *output_rect_vector) const;

bool CheckIfOkToUse(const std::vector<int64_t> *input_vector,
                    std::map<int64_t, bool> *used_map) const;

bool IsRectVectorPerfectDissection(
    const std::vector<mylib::CSpsr::rect_struct>
    *rect_vector,
    const std::vector<mylib::CSpsr::spsr_struct>
    *spsr_vector) const;

std::string SpsrStructVectorToBouwkampString(
    const std::vector<struct spsr_struct> *struct_vector) const;

std::string SpsrStructVectorToStatsString(
    const std::vector<struct spsr_struct> *struct_vector) const;

void InsertIntoUsedMap(
    const std::vector<int64_t> *input_vector,
    std::map<int64_t, bool> *used_map) const;

void DeleteFromUsedMap(
    const std::vector<int64_t> *input_vector,
    std::map<int64_t, bool> *used_map) const;

void DisplayStatusMessage(
    const int64_t rectangle_width,
    const std::vector<int64_t> *partition_vector,
    const int64_t status_in_minutes,
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *start_time,
    std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *return_time) const;

void Row0MakeSpsrFromPermutation(
    const std::vector<int64_t> *permutation_vector,
    std::vector<mylib::CSpsr::spsr_struct>
    *output_spsr_vector,
    std::vector<mylib::CSpsr::rect_struct>
    *output_rect_vector) const;

void MakeSpsrFromPermutation(
    const std::vector<int64_t> *permutation_vector,
    const int64_t start_row, const int64_t start_column,
    std::vector<mylib::CSpsr::spsr_struct>
    *output_spsr_vector) const;

inline void CopyVectors(
    const std::vector<mylib::CSpsr::spsr_struct> *from_spsr_vector,
    const std::vector<mylib::CSpsr::rect_struct> *from_rect_vector,
    std::vector<mylib::CSpsr::spsr_struct> *to_spsr_vector,
    std::vector<mylib::CSpsr::rect_struct> *to_rect_vector) const;

void ProcessOtherRows(
    const int64_t rectangle_width,
    const std::vector<mylib::CSpsr::spsr_struct> *spsr_vector,
    const std::vector<mylib::CSpsr::rect_struct> *rectangle_vector,
    std::vector<mylib::CSpsr::output_struct> *output_vector,
    std::map<int64_t, bool> *used_map) const;

void ComputeNextRecursionFromPermutation(
    const int64_t rectangle_width,
    const std::vector<int64_t> *permutation_vector,
    std::vector<mylib::CSpsr::output_struct>
    *output_vector) const;

void GenerateNThreadsPermutations(
    int64_t nthreads,
    const std::vector<int64_t> *input_vector,
    std::vector<int64_t> *output_vector,
    std::vector<std::vector<int64_t> >
    *out_vector_vector,
    std::map<std::string, bool>
    *symmetric_permutation_map) const;

void SpawnFutures(
    int64_t rectangle_width,
    std::vector<int64_t> *tmp_ivector,
    std::vector<std::future<std::vector<
    mylib::CSpsr::output_struct> > > *future_out_vector) const;

void LoopOverPermutationsRow0(
    const std::vector<int64_t> *partition_vector,
    const int64_t rectangle_width,
    const int64_t status_in_minutes,
    const int64_t nthreads,
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *start_time,
    std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *return_time,
    std::vector<mylib::CSpsr::output_struct>
    *out_vector) const;

void ProcessRow0(
    const int64_t width,
    const int64_t status_in_minutes,
    const int64_t nthreads,
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *start_time,
    std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *return_time,
    std::vector<mylib::CSpsr::output_struct>
    *out_vector) const;

std::string SpsrStructVectorToHMirrorString(
    const int64_t width,
    const std::vector<mylib::CSpsr::spsr_struct>
    *spsr_vector) const;

std::string SpsrStructVectorToVMirrorString(
    const int64_t height,
    const std::vector<mylib::CSpsr::spsr_struct>
    *spsr_vector) const;

std::string SpsrStructVectorToHVMirrorString(
    const int64_t height,
    const int64_t width,
    const std::vector<mylib::CSpsr::spsr_struct>
    *spsr_vector) const;

void TrimOutputVector(
    std::vector<mylib::CSpsr::output_struct> *input_vector,
    std::vector<mylib::CSpsr::output_struct>
    *out_vector) const;

std::string OutputStructLine1ToString(
    const mylib::CSpsr::output_struct *ostruct) const;

void PrintOutputVector(
    std::vector<mylib::CSpsr::output_struct> *output_vector,
    int64_t *number_of_unique_results) const;

// ### main loop
void MainLoopPrime(
    const int64_t start_width,
    const int64_t end_width,
    const int64_t nthreads,
    const int64_t status_in_minutes) const;
};  // end of class CSpsr
}  // end of namespace mylib

#endif  // SOURCES_SPSR_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/
