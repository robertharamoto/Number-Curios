/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  main program - simple perfect squared rectangle       ###
  ###  the integer partition method, using futures           ###
  ###  and async                                             ###
  ###                                                        ###
  ###  last updated September 18, 2024                       ###
  ###    fixed updated cpplint.py errors                     ###
  ###                                                        ###
  ###  updated July 9, 2022                                  ###
  ###                                                        ###
  ###  updated April 30, 2020                                ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <stdlib.h>
#include <stdio.h>

#include <gmp.h>
#include <gmpxx.h>

#include <cstdio>
#include <cstdint>
#include <string>
#include <map>

#include "sources/utils.h"
#include "sources/argopts.h"
#include "sources/configure.h"

#include "sources/spsr.h"

// #############################################################
// #############################################################
int main(int argc, char **argv) {
  mylib::CArgOpts argopts_obj;  // process argument switches, -h, -v,...
  void run_program(
      const std::string *title_string,
      mylib::CArgOpts *argopts_obj,
      std::map<std::string, std::string> *input_map);
  void Populate_Valid_Options(int argc, char **argv,
                              std::string *version_string,
                              mylib::CArgOpts *argopts_obj);
  void Populate_Configuration_File(
      int argc, char **argv,
      const std::string *default_config_fname,
      std::string *config_fname,
      std::map<std::string, std::string> *config_string_map,
      std::map<std::string, std::string> *config_comment_map);
  std::map<std::string, std::string>
      config_string_map, config_comment_map;
  std::string version_string =
      static_cast<std::string>("2024-09-18");
  std::string title_string, program_name;
  std::string config_filename;
  std::string default_config_filename("init0.config");

  title_string =
      static_cast<std::string>("Simple perfect squared rectangles")
      + static_cast<std::string>(" (version ")
      + version_string
      + static_cast<std::string>(")");

  Populate_Valid_Options(argc, argv,
                         &version_string,
                         &argopts_obj);

  // see if we have a configuration file or database file
  Populate_Configuration_File(argc, argv,
                              &default_config_filename,
                              &config_filename,
                              &config_string_map,
                              &config_comment_map);

  // ###########################################################
  // ###########################################################
  // main program
  run_program(&title_string,
              &argopts_obj,
              &config_string_map);

  return 0;
}

// #############################################################
// #############################################################
void Populate_Valid_Options(int argc, char **argv,
                            std::string *version_string,
                            mylib::CArgOpts *argopts_obj) {
  std::string opt1, opt2, opt3, opt4, opt5;
  std::string program_name;
  bool bresult;

  program_name = static_cast<std::string>(argv[0]);
  argopts_obj->SetName(&program_name);
  argopts_obj->SetVersion(version_string);

  opt1 = static_cast<std::string>("-s");
  opt2 = static_cast<std::string>("--start-width");
  opt3 = static_cast<std::string>("start width");
  opt4 = static_cast<std::string>("1");
  opt5 = static_cast<std::string>("");
  argopts_obj->AddValidOptionWithArgument(
      &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

  opt1 = static_cast<std::string>("-e");
  opt2 = static_cast<std::string>("--end-width");
  opt3 = static_cast<std::string>("end width");
  opt4 = static_cast<std::string>("100");
  opt5 = static_cast<std::string>("");
  argopts_obj->AddValidOptionWithArgument(
      &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

  opt1 = static_cast<std::string>("-t");
  opt2 = static_cast<std::string>("--nthreads");
  opt3 = static_cast<std::string>("nthreads");
  opt4 = static_cast<std::string>("4");
  opt5 = static_cast<std::string>("");
  argopts_obj->AddValidOptionWithArgument(
      &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

  opt1 = static_cast<std::string>("-m");
  opt2 = static_cast<std::string>("--status-minutes");
  opt3 = static_cast<std::string>("status in minutes");
  opt4 = static_cast<std::string>("120");
  opt5 = static_cast<std::string>("");
  argopts_obj->AddValidOptionWithArgument(
      &opt1, &opt2, &opt3, true, true, &opt4, &opt5);

  argopts_obj->ProcessOptions(argc, (const char**)argv);

  // ###########################################################
  // ###########################################################
  // process argument switches, -h, -v,...
  opt1 = "--help";
  bresult = argopts_obj->OptionEntered(&opt1);
  if (bresult == true) {
    argopts_obj->DisplayUsage();
    ::exit(0);
  }

  opt1 = "--version";
  bresult = argopts_obj->OptionEntered(&opt1);
  if (bresult == true) {
    argopts_obj->DisplayVersion();
    ::exit(0);
  }

  return;
}

// #############################################################
// #############################################################
void Populate_Configuration_File(
    int argc, char **argv,
    const std::string *default_config_filename,
    std::string *config_filename,
    std::map<std::string, std::string>
    *config_string_map,
    std::map<std::string, std::string>
    *config_comment_map) {
  mylib::CUtils utils_obj;
  mylib::CConfigure config_obj;
  std::string fname, config_ext(".config");
  size_t found_pos;

  if (argc > 0) {
    for (int ii = 1; ii < argc; ++ii) {
      if (utils_obj.FileExists(
              static_cast<const char *>(argv[ii])) == true) {
        fname = argv[ii];
        found_pos = fname.rfind(config_ext);
        if (found_pos != std::string::npos) {
          *config_filename = fname;
        }
      }
    }
  }

  if (config_filename->size() <= 0) {
    *config_filename = *default_config_filename;
  }

  config_obj.ReadConfig(config_filename,
                        config_string_map,
                        config_comment_map);

  return;
}

// #############################################################
// #############################################################
inline int64_t argopts_or_config_option_int64(
    const std::string *option_flag_string,
    const std::string *config_flag_string,
    const std::string *default_value,
    mylib::CArgOpts *argopts_obj,
    std::map<std::string, std::string> *config_string_map) {
  mylib::CUtils utils_obj;
  std::string num_string;
  int64_t ll_result;
  bool bresult;

  bresult = argopts_obj->OptionEntered(option_flag_string);
  if (bresult == true) {
    num_string =
        argopts_obj->GetOptionArgument(option_flag_string);
  }

  if (num_string.size() <= 0) {
    num_string =
        (*config_string_map)[*config_flag_string];
    if (num_string.size() <= 0) {
      num_string = *default_value;
    }
  }

  ll_result =
      utils_obj.StringToInteger(&num_string);

  return ll_result;
}

// #############################################################
// #############################################################
inline bool argopts_or_config_option_bool(
    const std::string *option_flag_string,
    const std::string *config_flag_string,
    const std::string *default_value,
    mylib::CArgOpts *argopts_obj,
    std::map<std::string, std::string> *config_string_map) {
  mylib::CUtils utils_obj;
  std::string bool_string, tmp2_string;
  bool bresult, init_flag;

  bresult = argopts_obj->OptionEntered(option_flag_string);
  if (bresult == true) {
    bool_string =
        argopts_obj->GetOptionArgument(option_flag_string);
  }

  if (bool_string.size() <= 0) {
    bool_string =
        (*config_string_map)[*config_flag_string];
    if (bool_string.size() <= 0) {
      bool_string = *default_value;
    }
  }
  if (bool_string.size() > 0) {
    tmp2_string =
        utils_obj.LowerCase(&bool_string);
    if (tmp2_string.compare("true") == 0) {
      init_flag = true;
    } else {
      init_flag = false;
    }
  } else {
    init_flag = true;
  }

  return init_flag;
}

// #############################################################
// #############################################################
void run_program(
    const std::string *title_string,
    mylib::CArgOpts *argopts_obj,
    std::map<std::string, std::string> *config_string_map) {
  mylib::CSpsr spsr_obj;
  mylib::CUtils utils_obj;
  std::string start_width_string, end_width_string;
  std::string nthreads_string, status_in_minutes_string;
  std::string opt1, opt2, opt3;
  int64_t start_width, end_width;
  int64_t nthreads, status_in_minutes;

  opt1 = static_cast<std::string>("--start-width");
  opt2 = static_cast<std::string>("start-width");
  opt3 = static_cast<std::string>("1");
  start_width = argopts_or_config_option_int64(
      &opt1, &opt2, &opt3,
      argopts_obj, config_string_map);

  opt1 = static_cast<std::string>("--end-width");
  opt2 = static_cast<std::string>("end-width");
  opt3 = static_cast<std::string>("100");
  end_width = argopts_or_config_option_int64(
      &opt1, &opt2, &opt3,
      argopts_obj, config_string_map);

  opt1 = static_cast<std::string>("--nthreads");
  opt2 = static_cast<std::string>("nthreads");
  opt3 = static_cast<std::string>("4");
  nthreads = argopts_or_config_option_int64(
      &opt1, &opt2, &opt3,
      argopts_obj, config_string_map);

  opt1 = static_cast<std::string>("--status-minutes");
  opt2 = static_cast<std::string>("status-minutes");
  opt3 = static_cast<std::string>("120");
  status_in_minutes = argopts_or_config_option_int64(
      &opt1, &opt2, &opt3,
      argopts_obj, config_string_map);

  printf("%s\n", title_string->c_str());
  printf("start width = %ld, end width = %ld, nthreads = %ld\n",
         start_width, end_width, nthreads);
  printf("status_in_minutes = %ld : ",
         status_in_minutes);
  printf("%s\n", utils_obj.CurrentDateTime().c_str());
  fflush(stdout);

  spsr_obj.MainLoopPrime(start_width, end_width,
                         nthreads, status_in_minutes);

  return;
}
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/
