#ifndef SOURCES_CONFIGURE_H_
#define SOURCES_CONFIGURE_H_
/*

// Copyright 2024 <robert haramoto>

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  configurations file class definition                  ###
  ###                                                        ###
  ###  last updated September 18, 2024                       ###
  ###                                                        ###
  ###  last updated July 31, 2022                            ###
  ###                                                        ###
  ###  updated February 5, 2020                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <string>
#include <vector>
#include <map>

namespace mylib {

class CConfigure {
 public:
CConfigure();
~CConfigure();

// do not allow copying nor moving
CConfigure(const CConfigure&) = delete;
CConfigure& operator=(const CConfigure&) = delete;
CConfigure(CConfigure&&) = delete;
CConfigure& operator=(CConfigure&&) = delete;

void ReadConfig(const std::string *filename,
                std::map<std::string, std::string>
                *inputStringMap,
                std::map<std::string, std::string>
                *commentStringMap);

void PopulateCommentBlockVector(
    const std::string *comment_string,
    const int64_t length,
    std::vector<std::string> *output_vector) const;

void WriteRestartConfig(
    const std::string *restart_filename,
    std::map<std::string, std::string> *inputStringMap,
    std::map<std::string, std::string> *commentStringMap);

void ParseLine(const std::string *line,
               std::string *sname, std::string *svalue,
               std::string *scomment);
};  // class CConfigure

};  // namespace mylib

#endif  // SOURCES_CONFIGURE_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/
