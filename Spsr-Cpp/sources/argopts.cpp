/*

// Copyright 2024 <robert haramoto>

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  argument options class implementation                 ###
  ###                                                        ###
  ###  last updated April 25, 2024                           ###
  ###                                                        ###
  ###  updated July 16, 2022                                 ###
  ###                                                        ###
  ###  updated February 5, 2020                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/argopts.h"

#include <cctype>  // tolower
#include <cstdio>  // printf
#include <cstdint>

#include <string>
#include <map>
#include <vector>
#include <utility>  // make_pair
#include <algorithm>  // transform, find()

// #############################################################
// #############################################################

namespace mylib {

// CArgOpts constructors/destructors

// #############################################################
// #############################################################
CArgOpts::CArgOpts() :
    version_(static_cast<std::string>("2024-04-25")),
    programName_(static_cast<std::string>("CArgOpts")),
    valid_opts_map_() {
  Clear();

  SetDefaultOptions();

  return;
}

// #############################################################
// #############################################################
CArgOpts::~CArgOpts() {
  // delete all options in hash map
  Clear();

  return;
}

// #############################################################
// #############################################################
void CArgOpts::Clear() {
  valid_opts_map_.clear();
}

// #############################################################
// #############################################################
void CArgOpts::SetDefaultOptions() {
  // add default options to hash map
  std::string opt1, opt2, opt3;

  opt1 = static_cast<std::string>("-h");
  opt2 = static_cast<std::string>("--help");
  opt3 = static_cast<std::string>("display this message");
  AddValidOptionNoArgument(&opt1, &opt2, &opt3);

  opt1 = static_cast<std::string>("-v");
  opt2 = static_cast<std::string>("--version");
  opt3 = static_cast<std::string>("display program version");
  AddValidOptionNoArgument(&opt1, &opt2, &opt3);

  return;
}

// public functions

// #############################################################
// #############################################################
void CArgOpts::LowerCase(std::string *stmp) {
  std::transform(stmp->begin(), stmp->end(),
                 stmp->begin(),
                 ::tolower);
  return;
}

// #############################################################
// #############################################################
void CArgOpts::SetName(const std::string *sname) {
  std::string tmp_name = { *sname };
  size_t found;

  found = tmp_name.find("./");
  if (found == 0) {
    programName_ = tmp_name.erase(0, 2);
  } else {
    programName_ = *sname;
  }

  return;
}

// #############################################################
// #############################################################
void CArgOpts::SetVersion(const std::string *sversion) {
  version_ = *sversion;

  return;
}

// #############################################################
// #############################################################
void CArgOpts::DisplayUsage(void) {
  std::map<std::string, struct OptStruct>::const_iterator mso_cit;
  std::vector<std::string> opt_string_vector;
  std::vector<std::string>::const_iterator vs_cit;
  struct OptStruct otmp;
  std::string help_option("--help");
  std::string stmp;

  printf("Usage : %s config-file-name\n\n",
         programName_.c_str());

  mso_cit = valid_opts_map_.find(help_option);
  if (mso_cit != valid_opts_map_.end()) {
    otmp = mso_cit->second;
    printf("  %s, %s  :  %s\n",
           otmp.short_opt.c_str(),
           otmp.long_opt.c_str(),
           otmp.description.c_str());
  }

  opt_string_vector.clear();
  for (mso_cit = valid_opts_map_.cbegin();
       mso_cit != valid_opts_map_.end(); ++mso_cit) {
    opt_string_vector.push_back(mso_cit->first);
  }

  std::sort(opt_string_vector.begin(),
            opt_string_vector.end());

  for (vs_cit = opt_string_vector.cbegin();
       vs_cit != opt_string_vector.cend(); ++vs_cit) {
    otmp = valid_opts_map_[*vs_cit];
    if (otmp.long_opt != help_option) {
      if (otmp.bool_has_argument == true) {
        stmp = static_cast<std::string>("=arg");
        if (otmp.bool_is_arg_numeric == true) {
          stmp = static_cast<std::string>("=number");
        }
      } else {
        stmp = "";
      }

      printf("  %s, %s%s  :  %s\n",
             otmp.short_opt.c_str(),
             otmp.long_opt.c_str(),
             stmp.c_str(),
             otmp.description.c_str());
    }
  }

  printf("\n");

  return;
}

// #############################################################
// #############################################################
void CArgOpts::DisplayVersion(void) {
  printf("%s : version %s\n", programName_.c_str(),
         version_.c_str());

  return;
}

// #############################################################
// #############################################################
void CArgOpts::AddValidOptionNoArgument(
    std::string *short_opt,
    std::string *long_opt,
    std::string *description) {
  std::string lc_short_opt = { *short_opt };
  std::string lc_long_opt = { *long_opt };
  struct OptStruct input_struct;

  // convert to lower case
  LowerCase(&lc_short_opt);
  LowerCase(&lc_long_opt);

  input_struct.short_opt = lc_short_opt;
  input_struct.long_opt = lc_long_opt;
  input_struct.description = *description;
  input_struct.bool_has_argument = false;
  input_struct.bool_is_arg_numeric = false;
  input_struct.default_argument = "";
  input_struct.argument = "";
  input_struct.count = 0;

  valid_opts_map_[lc_long_opt] = input_struct;

  return;
}

// #############################################################
// #############################################################
void CArgOpts::AddValidOptionWithArgument(
    std::string *short_opt,
    std::string *long_opt,
    std::string *description,
    bool bool_has_argument,
    bool bool_is_arg_numeric,
    std::string *default_argument,
    std::string *argument) {
  std::string lc_short_opt = { *short_opt };
  std::string lc_long_opt = { *long_opt };
  struct OptStruct input_struct;

  // convert to lower case
  LowerCase(&lc_short_opt);
  LowerCase(&lc_long_opt);

  input_struct.short_opt = lc_short_opt;
  input_struct.long_opt = lc_long_opt;
  input_struct.description = *description;
  input_struct.bool_has_argument = bool_has_argument;
  input_struct.bool_is_arg_numeric = bool_is_arg_numeric;
  input_struct.default_argument = *default_argument;
  input_struct.argument = *argument;
  input_struct.count = 0;

  valid_opts_map_[lc_long_opt] = input_struct;

  return;
}

// #############################################################
// #############################################################
// takes --max=100 and returns opt_flag="--max", opt_arg="100"
// or --max and returns opt_flag="--max", opt_arg=""
void CArgOpts::ParseArgToOptFlag(
    const std::string *option,
    const std::string *possible1,
    const std::string *possible2,
    std::string *opt_flag,
    std::string *opt_arg) {
  const std::string equal_string = {
    static_cast<std::string>("=") };
  size_t found;

  found = option->find("=");

  if (found != std::string::npos) {
    *opt_flag = option->substr(0, found);
    if (found < option->size() - 1) {
      // handles the case "--max=100"
      *opt_arg = option->substr(found+1);
    } else {
      // handles the case "--max= 100"
      *opt_arg = *possible1;
    }

    return;
  } else {
    // no "=" found in the first option.

    // either "--max = 100" or "--max =100" or "--max 100"
    *opt_flag = *option;

    if (*possible1 == equal_string) {
      // handles the case "--max = 100"
      *opt_arg = *possible2;
    } else {
      found = possible1->find("=");
      if (found != std::string::npos) {
        // handles the case "--max =100"
        *opt_arg = possible1->substr(found + 1);
      } else {
        // handles the case "--max 100"
        *opt_arg = *possible1;
      }
    }
  }

  return;
}

// #############################################################
// #############################################################
void CArgOpts::ProcessOptions(
    const int argc, const char **argv) {
  std::string arg_string, possible1, possible2;
  std::string opt_string, opt_arg;
  std::string hash_key;
  struct OptStruct tmp_struct;
  std::map<std::string, struct OptStruct>::const_iterator pos;

  if (argc <= 1) {
    // no options to process
    return;
  }

  // assumes all hash value options in lower case
  // (lowered by addValidOption)
  for (int ii = 1; ii < argc; ++ii) {
    arg_string.assign(argv[ii]);

    LowerCase(&arg_string);

    if (ii+1 < argc) {
      possible1 = argv[ii + 1];
      LowerCase(&possible1);
    } else {
      possible1 = "";
    }

    if (ii+2 < argc) {
      possible2 = argv[ii + 2];
      LowerCase(&possible2);
    } else {
      possible2 = "";
    }

    // handles the case "--max=100" ..., and separates into
    // the option string and argument
    ParseArgToOptFlag(&arg_string, &possible1, &possible2,
                      &opt_string, &opt_arg);

    // is this option a valid option
    pos = valid_opts_map_.find(opt_string);

    if (pos != valid_opts_map_.end()) {
      hash_key = pos->first;
      tmp_struct = pos->second;

      if (tmp_struct.bool_has_argument == true) {
        if (opt_arg.size() > 0) {
          tmp_struct.argument = opt_arg;
        } else {
          tmp_struct.argument =
              tmp_struct.default_argument;
        }
      }

      tmp_struct.count++;
      valid_opts_map_[hash_key] = tmp_struct;
    } else {
      // then we could have a short option or an invalid option
      for (pos = valid_opts_map_.cbegin();
           pos != valid_opts_map_.end(); ++pos) {
        hash_key = pos->first;
        tmp_struct = pos->second;
        if (tmp_struct.short_opt == opt_string) {
          if (opt_arg.size() > 0) {
            tmp_struct.argument = opt_arg;
          } else {
            tmp_struct.argument =
                tmp_struct.default_argument;
          }

          tmp_struct.count++;
          valid_opts_map_[hash_key] = tmp_struct;
          break;
        }
      }
    }
  }

  return;
}

// #############################################################
// #############################################################
bool CArgOpts::OptionEntered(const std::string *this_opt) {
  struct OptStruct tmp_struct;
  std::string this_arg = { *this_opt };
  std::map<std::string, struct OptStruct>::const_iterator pos;

  LowerCase(&this_arg);

  tmp_struct = valid_opts_map_[this_arg];

  if (tmp_struct.count > 0) {
    // then we have a long option
    return true;
  } else if (tmp_struct.long_opt == this_arg) {
    return false;
  } else {
    // then we have a short option or an invalid option
    for (pos = valid_opts_map_.cbegin();
         pos != valid_opts_map_.cend(); ++pos) {
      tmp_struct = pos->second;
      if ((tmp_struct.short_opt == this_arg)
          && (tmp_struct.count > 0)) {
        return true;
      }
    }
  }

  return false;
}

// #############################################################
// #############################################################
std::string CArgOpts::GetOptionArgument(
    const std::string *this_opt) {
  struct OptStruct tmp_struct;
  std::string this_arg = { *this_opt };
  std::map<std::string, struct OptStruct>::const_iterator pos;

  LowerCase(&this_arg);

  pos = valid_opts_map_.find(this_arg);

  if (pos != valid_opts_map_.end()) {
    tmp_struct = pos->second;

    return tmp_struct.argument;
  } else {
    // then we have a short option or an invalid option
    for (pos = valid_opts_map_.cbegin();
         pos != valid_opts_map_.cend(); ++pos) {
      tmp_struct = pos->second;
      if (tmp_struct.short_opt == this_arg) {
        return tmp_struct.argument;
      }
    }
  }
  return "";
}

}  // end of namespace mylib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/
