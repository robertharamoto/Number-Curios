/*

// Copyright 2024 <robert haramoto>

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  configure file class implementation                   ###
  ###                                                        ###
  ###  last updated September 21, 2024                       ###
  ###                                                        ###
  ###  last updated July 16, 2022                            ###
  ###                                                        ###
  ###  updated February 5, 2020                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cstdlib>
#include <cstdio>

#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include "sources/configure.h"
#include "sources/utils.h"

namespace mylib {

// #############################################################
// #############################################################
CConfigure::CConfigure() {
}

// #############################################################
// #############################################################
CConfigure::~CConfigure() {
}

// #############################################################
// #############################################################
void CConfigure::ReadConfig(
    const std::string *filename,
    std::map<std::string, std::string> *input_string_map,
    std::map<std::string, std::string> *comment_string_map) {
    FILE *file_stream;
    const int line_size = 256;
    char *ctmp, buffer[line_size + 1];
    std::string sname, svalue, scomment, sline;

    file_stream = fopen(filename->c_str(), "r");
    if (file_stream == NULL) {
        printf("CConfigure::ReadConfig error");
        printf(": unable to open file %s for reading.\n",
               filename->c_str());
        printf("exiting...\n");
        exit(0);
    } else {
        ctmp = buffer;

        while ((!feof(file_stream))
               || (ctmp != NULL)) {
            ctmp = fgets(buffer, line_size, file_stream);
            if (ctmp != NULL) {
                sline = buffer;
                ParseLine(
                    const_cast<std::string *>(&sline),
                    &sname, &svalue, &scomment);

                if (sname.length() > 0) {
                    input_string_map->emplace(sname, svalue);
                    comment_string_map->emplace(sname, scomment);
                }
            } else {
                break;
            }
        }

        fclose(file_stream);
    }

    return;
}

// #############################################################
// #############################################################
void CConfigure::PopulateCommentBlockVector(
    const std::string *comment_string,
    const int64_t length,
    std::vector<std::string> *output_vector) const {
    mylib::CUtils utils_obj;
    const int64_t nn_sharps = { length - 5L };
    const int64_t nn_spaces = { length - 8L };
    std::string sharps_separator(nn_sharps, '#');
    std::string spaces_separator(nn_spaces, ' ');
    std::string init_string = {
        static_cast<std::string>(";;;###") };
    std::string init2_string = {
        static_cast<std::string>(";;;###  ") };
    std::string terminal_string = {
        static_cast<std::string>("###") };
    std::string blank_line;
    std::string top_line;
    std::string local_comment = { *comment_string };
    int64_t comment_size = {
        static_cast<int64_t>(comment_string->size()) };
    int64_t remaining_num = {
        length - comment_size - 10L };
    std::string remaining_spaces(remaining_num, ' ');
    std::string final_line;
    std::string updated_string = {
        static_cast<std::string>("last updated ") };
    const std::string today_string = { utils_obj.CurrentDate() };
    int64_t dlength = {
        static_cast<int64_t>(today_string.length()) };
    int64_t nn_dspaces;
    std::string dspaces, date_line;

    // create top_line
    top_line = init_string + sharps_separator;

    // create blank line
    blank_line = init_string + spaces_separator
        + terminal_string;

    // create main comment line
    if (comment_size > length) {
        local_comment =
            comment_string->substr(0, length - 10L);
        comment_size =
            static_cast<int64_t>(local_comment.size());

        remaining_num = length - comment_size - 10L;
        remaining_spaces.assign(remaining_num, ' ');
    }

    final_line = init2_string + local_comment
        + remaining_spaces + terminal_string;

    // create date line
    local_comment =
        updated_string + today_string;
    comment_size =
        static_cast<int64_t>(local_comment.length());

    if (comment_size > length) {
        local_comment =
            local_comment.substr(0, length - 10L);
        comment_size =
            static_cast<int64_t>(local_comment.size());

        remaining_num = length - comment_size - 10L;
        dspaces.assign(remaining_num, ' ');
    } else {
        nn_dspaces = length - updated_string.length()
            - dlength - 10L;
        dspaces.append(nn_dspaces, ' ');
    }

    date_line = init2_string
        + local_comment + dspaces
        + terminal_string;

    output_vector->clear();
    output_vector->push_back(top_line);
    output_vector->push_back(top_line);
    output_vector->push_back(blank_line);

    output_vector->push_back(final_line);

    output_vector->push_back(blank_line);

    output_vector->push_back(date_line);

    output_vector->push_back(blank_line);

    output_vector->push_back(top_line);
    output_vector->push_back(top_line);

    return;
}

// #############################################################
// #############################################################
void CConfigure::WriteRestartConfig(
    const std::string *restart_filename,
    std::map<std::string, std::string> *config_string_map,
    std::map<std::string, std::string> *comment_string_map) {
    FILE *file_stream;
    std::string sname, svalue, sline;
    std::string title_string = {
        static_cast<std::string>("configuration file") };
    int64_t comment_length = { 63L };
    mylib::CUtils utils_obj;
    std::vector<std::string> comment_block_vector;
    std::vector<std::string> key_vector;
    std::string this_key, this_value, this_comment;
    std::vector<std::string>::const_iterator vs_cit;
    std::map<std::string, std::string>::iterator mss_cit;

    file_stream = fopen(restart_filename->c_str(), "w");
    if (file_stream == NULL) {
        printf("CConfigure::WriteRestartConfig error");
        printf(": unable to open file %s for reading.\n",
               restart_filename->c_str());
        printf("exiting...\n");
        exit(0);
    } else {
        PopulateCommentBlockVector(
            &title_string, comment_length,
            &comment_block_vector);

        for (vs_cit = comment_block_vector.cbegin();
             vs_cit != comment_block_vector.cend(); ++vs_cit) {
            fprintf(file_stream, "%s\n", vs_cit->c_str());
        }

        key_vector.clear();
        for (mss_cit = config_string_map->begin();
             mss_cit != config_string_map->cend(); ++mss_cit) {
            this_key = mss_cit->first;
            key_vector.push_back(this_key);
        }

        std::sort(key_vector.begin(),
                  key_vector.end());

        for (vs_cit = key_vector.cbegin();
             vs_cit != key_vector.cend(); ++vs_cit) {
            this_key = *vs_cit;
            this_value = (*config_string_map)[this_key];
            this_comment = (*comment_string_map)[this_key];

            if (this_comment.size() <= 0) {
                this_comment = this_key;
            }

            fprintf(file_stream, "%s=%s\t    ;;; %s\n",
                    this_key.c_str(),
                    this_value.c_str(),
                    this_comment.c_str());
        }

        fclose(file_stream);
    }

    return;
}

/*
  ##############################################################
  ##############################################################
*/
void CConfigure::ParseLine(
    const std::string *line,
    std::string *sname, std::string *svalue,
    std::string *scomment) {
    mylib::CUtils utils_obj;
    std::string trimmed_string, comment_free_string;
    std::string comment_string, tmp_string;
    std::vector<std::string> stokens;
    bool bool_found;
    size_t slen, spos, start_strip, end_strip;

    sname->clear();
    svalue->clear();
    slen = line->length();
    if (slen <= 0) {
        return;
    }

    trimmed_string = utils_obj.StringTrim(line);
    comment_free_string =
        utils_obj.StringRemoveComment(&trimmed_string, ';');

    trimmed_string = comment_free_string;

    slen = comment_free_string.length();
    if (slen <= 0) {
        return;
    }

    utils_obj.StringTokenize(&trimmed_string,
                             '=', &stokens);
    if (stokens.size() >= 2) {
        *sname = utils_obj.StringTrim(&(stokens[0]));
        *svalue = utils_obj.StringTrim(&(stokens[1]));
    }

    // now get comment
    trimmed_string = utils_obj.StringTrim(line);
    slen = trimmed_string.length();
    start_strip = end_strip = trimmed_string.find_first_of(";");
    if (start_strip < slen) {
        bool_found = false;
        for (spos = start_strip; spos < slen; ++spos) {
            if (trimmed_string[spos] != ' '
                && trimmed_string[spos] != ';') {
                bool_found = true;
                end_strip = spos;
                break;
            }
        }

        if (bool_found == true) {
            tmp_string.clear();
            for (spos = end_strip; spos < slen;
                 ++spos) {
                tmp_string.push_back(
                    trimmed_string[spos]);
            }

            *scomment =
                utils_obj.StringTrim(&tmp_string);
        } else {
            *scomment = "";
        }
    } else {
        *scomment = "";
    }

    return;
}

}  // namespace mylib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/
