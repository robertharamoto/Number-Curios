/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  CSpsr implementation                                  ###
  ###                                                        ###
  ###  last updated August 26, 2024                          ###
  ###    added symmetry checks for row 0                     ###
  ###                                                        ###
  ###  updated May 6, 2024                                   ###
  ###    changed arguments from references to pointers       ###
  ###    fixed updated cpplint.py errors                     ###
  ###                                                        ###
  ###  updated July 20, 2022                                 ###
  ###                                                        ###
  ###  updated April 29, 2020                                ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "sources/spsr.h"

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cmath>

#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <future>
#include <chrono>

#include "sources/utils.h"

namespace mylib {

// #############################################################
// #############################################################
// constructor
CSpsr::CSpsr() {
}

// #############################################################
// #############################################################
// destructor
CSpsr::~CSpsr() {
}

// #############################################################
// #############################################################
bool CSpsr::sort_rect_function(
    const mylib::CSpsr::rect_struct aa,
    const mylib::CSpsr::rect_struct bb) {
    if (aa.start_row < bb.start_row) {
        return true;
    }

    if (aa.start_row > bb.start_row) {
        return false;
    }

// if we reach here, then aa.start_row == bb.start_row
    return (aa.start_column < bb.start_column);
}

// #############################################################
// #############################################################
bool CSpsr::sort_spsr_function(
    const mylib::CSpsr::spsr_struct aa,
    const mylib::CSpsr::spsr_struct bb) {
    if (aa.start_row < bb.start_row) {
        return true;
    }

    if (aa.start_row > bb.start_row) {
        return false;
    }

// if we reach here, then aa.start_row == bb.start_row
    return (aa.start_column < bb.start_column);
}

// #############################################################
// #############################################################
bool CSpsr::sort_output_function(
    const mylib::CSpsr::output_struct aa,
    const mylib::CSpsr::output_struct bb) {
    if (aa.order < bb.order) {
        return true;
    }

    if (aa.order > bb.order) {
        return false;
    }

// if we reach here, then aa.order == bb.order
    if (aa.height < bb.height) {
        return true;
    }

    if (aa.height > bb.height) {
        return false;
    }

// if we reach here, then aa.height == bb.height
    if (aa.first_square > bb.first_square) {
        return true;
    }

    return false;
}

// #############################################################
// #############################################################
std::string CSpsr::rect_struct_to_string(
    const mylib::CSpsr::rect_struct *aa) const {
    std::string sresult;

    sresult = static_cast<std::string>("((")
        + std::to_string(aa->start_row)
        + static_cast<std::string>(", ")
        + std::to_string(aa->start_column)
        + static_cast<std::string>("), (")
        + std::to_string(aa->end_row)
        + static_cast<std::string>(", ")
        + std::to_string(aa->end_column)
        + static_cast<std::string>("))");

    return sresult;
}

// #############################################################
// #############################################################
std::string CSpsr::vector_rect_struct_to_string(
    const std::vector<mylib::CSpsr::rect_struct> *aa) const {
    std::vector<mylib::CSpsr::rect_struct>::const_iterator vrs_cit;
    mylib::CSpsr::rect_struct tmp_rect_struct;
    std::string sresult, srect;

    sresult = static_cast<std::string>("[");
    for (vrs_cit = aa->cbegin();
         vrs_cit != aa->cend(); ++vrs_cit) {
        tmp_rect_struct = *vrs_cit;
        srect = rect_struct_to_string(&tmp_rect_struct);

        if (vrs_cit == aa->cbegin()) {
            sresult += srect;
        } else {
            sresult +=
                static_cast<std::string>(", ")
                + srect;
        }
    }

    sresult += static_cast<std::string>("]");

    return sresult;
}

// #############################################################
// #############################################################
std::string CSpsr::spsr_struct_to_string(
    const mylib::CSpsr::spsr_struct *aa) const {
    std::string sresult;

    sresult = static_cast<std::string>("(")
        + std::to_string(aa->square_size)
        + static_cast<std::string>(", ")
        + std::to_string(aa->start_row)
        + static_cast<std::string>(", ")
        + std::to_string(aa->start_column)
        + static_cast<std::string>(")");

    return sresult;
}

// #############################################################
// #############################################################
std::string CSpsr::vector_spsr_struct_to_string(
    const std::vector<mylib::CSpsr::spsr_struct> *aa) const {
    std::vector<mylib::CSpsr::spsr_struct>::const_iterator vrs_cit;
    mylib::CSpsr::spsr_struct tmp_spsr_struct;
    std::string sresult, sspsr;

    sresult = static_cast<std::string>("[");
    for (vrs_cit = aa->cbegin();
         vrs_cit != aa->cend(); ++vrs_cit) {
        tmp_spsr_struct = *vrs_cit;
        sspsr = spsr_struct_to_string(&tmp_spsr_struct);

        if (vrs_cit == aa->cbegin()) {
            sresult += sspsr;
        } else {
            sresult +=
                static_cast<std::string>(", ")
                + sspsr;
        }
    }

    sresult += static_cast<std::string>("]");

    return sresult;
}

// #############################################################
// #############################################################
// IsPrime
bool CSpsr::IsPrime(const int64_t ll_num) const {
    int64_t ll_jj;
    int64_t ll_max;

    if (ll_num < 2L) {
        return false;
    }

// handle even numbers
    if (ll_num < 2L) {
        return false;
    } else if (ll_num == 2L) {
        return true;
    } else if (ll_num % 2L == 0L) {
        return false;
    } else if (ll_num == 3L) {
        return true;
    } else if (ll_num % 3L == 0L) {
        return false;
    } else if (ll_num == 5L) {
        return true;
    } else if (ll_num % 5L == 0L) {
        return false;
    }

// loop until you hit the square root of the number ll_num
    ll_max = static_cast<int64_t>(sqrt(ll_num)) + 1L;
    for (ll_jj = 7L; ll_jj <= ll_max; ll_jj += 2L) {
        if (ll_num % ll_jj == 0) {
            return false;
        }
    }

    return true;
}

// #############################################################
// #############################################################
bool CSpsr::SuccessorOfIntegerPartition(
    int64_t num_to_partition,
    std::vector<int64_t> *avec) const {
    std::vector<int64_t>::reverse_iterator rit;
    bool return_value = true;
    int64_t remaining_value, non_one_index;
    int64_t ii, ivalue, tmp_remain;

// store the count of trailing ones
    remaining_value = 0L;
    non_one_index =
        static_cast<int64_t>(avec->size()) - 1L;
    for (rit = avec->rbegin(); rit != avec->rend(); rit++) {
        if (*rit == 1) {
            remaining_value++;
            non_one_index--;
        } else {
            break;
        }
    }

    tmp_remain = remaining_value;

// decrease the value of first non-one value, and increase
// remaining value by 1
    if (non_one_index < 0L) {
        avec->clear();
        return false;
    }

    avec->at(non_one_index)--;
    ivalue = avec->at(non_one_index);
    remaining_value++;

    if (ivalue > remaining_value) {
        if (non_one_index >= 0L) {
            for (ii = 0L; ii < remaining_value; ii++) {
                avec->pop_back();
            }

            avec->push_back(ivalue);
        }
        avec->push_back(remaining_value);
    } else {
        // get rid of all the ones
        if (tmp_remain >= num_to_partition) {
            avec->clear();
        } else {
            for (ii = 0L; ii < tmp_remain; ii++) {
                avec->pop_back();
            }
        }

        while (remaining_value > ivalue) {
            avec->push_back(ivalue);

            remaining_value = remaining_value - ivalue;
        }

        avec->push_back(remaining_value);
    }

    return return_value;
}

// #############################################################
// #############################################################
void CSpsr::ConvertRow0ToRectangleVector(
    const std::vector<int64_t> *row0_vector,
    std::vector<mylib::CSpsr::rect_struct> *rect_vector) const {
    int64_t asquare;
    int64_t start_row, start_column;
    mylib::CSpsr::rect_struct astruct;
    std::vector<int64_t>::const_iterator vi_cit;

    rect_vector->clear();

    start_row = 0L;
    start_column = 0L;
    for (vi_cit = row0_vector->cbegin();
         vi_cit != row0_vector->cend(); ++vi_cit) {
        asquare = *vi_cit;
        astruct.start_row = start_row;
        astruct.start_column = start_column;
        astruct.end_row = asquare + start_row - 1L;
        astruct.end_column = asquare + start_column - 1L;

        rect_vector->push_back(astruct);

        start_column = astruct.end_column + 1L;
    }

    return;
}

// #############################################################
// #############################################################
void CSpsr::FindMinRowColWidthHeight(
    const std::vector<mylib::CSpsr::rect_struct> *rect_vector,
    int64_t *min_row, int64_t *min_column, int64_t *min_width,
    int64_t *min_height) const {
    std::vector<mylib::CSpsr::rect_struct>::const_iterator vrs_cit;
    int64_t start_row(0L), start_col(0L), width(0L);
    int64_t end_row(0L), end_col(0L), height(0L);

    *min_row = *min_column = *min_width = *min_height = 0L;
    for (vrs_cit = rect_vector->cbegin();
         vrs_cit != rect_vector->cend(); ++vrs_cit) {
        start_row = vrs_cit->start_row;
        start_col = vrs_cit->start_column;
        end_row = vrs_cit->end_row;
        end_col = vrs_cit->end_column;

        width = end_col - start_col + 1L;
        height = end_row - start_row + 1L;

        if ((*min_row == 0L)
            || (*min_row > end_row)) {
            *min_row = end_row + 1L;
            *min_column = start_col;
            *min_width = width;
            *min_height = height;
        }
    }

    return;
}

// #############################################################
// #############################################################
// ### add a bouwkamp list to an existing rectangle list lists
// ### this bouwkamp list sits above the smallest square block
// ### example 75, 112 x 75 bouwkamp string =
// ### ((36 19 24 33) (14 5) (20 9) (3 11) (42) (39) (31))
// ### so (14 5) sits above the row 0, 19 square
// ### and (20 9) sits above the 5 and 24 squares
// ### rect list format (list start-row start-col end-row end-col)

// #############################################################
// #############################################################
// ### second, add the squares in order, left to right
// ### and decompose the rectangle into smaller sub-rectangles
void CSpsr::AddPartitionToRectangleStruct(
    const std::vector<int64_t> *input_partition_vector,
    const std::vector<mylib::CSpsr::rect_struct> *input_rect_vector,
    std::vector<mylib::CSpsr::rect_struct> *output_rect_vector)
    const {
    std::vector<mylib::CSpsr::rect_struct>::const_iterator vrs_cit;
    std::vector<int64_t>::const_iterator vin_cit;
    int64_t min_row, min_column, min_width, min_height;
    int64_t next_start_row = 0L, next_start_column = 0L;
    int64_t this_square;
    mylib::CSpsr::rect_struct astruct;

    output_rect_vector->clear();

    FindMinRowColWidthHeight(
        input_rect_vector,
        &min_row, &min_column,
        &min_width, &min_height);

    for (vrs_cit = input_rect_vector->cbegin();
         vrs_cit != input_rect_vector->cend(); ++vrs_cit) {
        if ((min_row == vrs_cit->end_row + 1L)
            && (min_column == vrs_cit->start_column)) {
            // add partition to this position
            next_start_row = -1L;
            for (vin_cit = input_partition_vector->cbegin();
                 vin_cit != input_partition_vector->cend(); ++vin_cit) {
                this_square = *vin_cit;
                if (next_start_row < 0L) {
                    next_start_row = vrs_cit->start_row;
                    next_start_column = vrs_cit->start_column;
                    astruct.start_row = next_start_row;
                    astruct.start_column = next_start_column;
                    astruct.end_row =
                        vrs_cit->end_row + this_square;
                    astruct.end_column =
                        next_start_column + this_square - 1L;
                } else {
                    astruct.start_row = next_start_row;
                    astruct.start_column = next_start_column;
                    astruct.end_row =
                        vrs_cit->end_row + this_square;
                    astruct.end_column =
                        next_start_column + this_square - 1L;
                }
                output_rect_vector->push_back(astruct);
                next_start_column = astruct.end_column + 1L;
            }
        } else {
            output_rect_vector->push_back(*vrs_cit);
        }
    }

    return;
}

// #############################################################
// #############################################################
// ### step 3, combine rectangles with the same end-rows
// ### this means the heights are the same
void CSpsr::CombineRectangles(
    const std::vector<mylib::CSpsr::rect_struct> *input_rect_vector,
    std::vector<mylib::CSpsr::rect_struct> *output_rect_vector)
    const {
    std::vector<mylib::CSpsr::rect_struct>::const_iterator vrs_cit;
    std::vector<mylib::CSpsr::rect_struct>::const_iterator begin_cit;
    mylib::CSpsr::rect_struct prev_struct;

    output_rect_vector->clear();

    begin_cit = input_rect_vector->cbegin();
    for (vrs_cit = begin_cit;
         vrs_cit != input_rect_vector->cend(); ++vrs_cit) {
        if (vrs_cit == begin_cit) {
            prev_struct.start_row = vrs_cit->start_row;
            prev_struct.start_column = vrs_cit->start_column;
            prev_struct.end_row = vrs_cit->end_row;
            prev_struct.end_column = vrs_cit->end_column;
        } else {
            if (prev_struct.end_row == vrs_cit->end_row) {
                // then we need to combine these two rectangles
                prev_struct.end_column = vrs_cit->end_column;
            } else {
                output_rect_vector->push_back(prev_struct);

                prev_struct.start_row = vrs_cit->start_row;
                prev_struct.start_column = vrs_cit->start_column;
                prev_struct.end_row = vrs_cit->end_row;
                prev_struct.end_column = vrs_cit->end_column;
            }
        }
    }

    output_rect_vector->push_back(prev_struct);

    return;
}

// #############################################################
// #############################################################
bool CSpsr::CheckIfOkToUse(
    const std::vector<int64_t> *input_vector,
    std::map<int64_t, bool> *used_map) const {
    bool uflag;
    std::vector<int64_t>::const_iterator vi_cit;
    std::vector<int64_t>::iterator vi_it;

// see if squares already being used
    for (vi_cit = input_vector->cbegin();
         vi_cit != input_vector->cend(); ++vi_cit) {
        uflag = (*used_map)[*vi_cit];
        if (uflag == true) {
            return false;
        }
    }

// see if input_vector contains duplicates
    std::vector<int64_t> tmp_vector = *input_vector;
    std::sort(tmp_vector.begin(), tmp_vector.end());
    vi_it = std::unique(tmp_vector.begin(), tmp_vector.end());

    if (vi_it == tmp_vector.cend()) {
        return true;
    }

    return false;
}

// #############################################################
// #############################################################
bool CSpsr::IsRectVectorPerfectDissection(
    const std::vector<mylib::CSpsr::rect_struct> *rect_vector,
    const std::vector<mylib::CSpsr::spsr_struct> *spsr_vector)
    const {
    int64_t ii_height, ii_width, ii_square_size;

    if ((rect_vector->size() == 1)
        && (spsr_vector->size() > 1)) {
        mylib::CSpsr::rect_struct ar_struct;
        std::vector<mylib::CSpsr::spsr_struct>::const_iterator
            ss_cit;

        ar_struct = (*rect_vector)[0];
        ii_height =
            (ar_struct.end_row - ar_struct.start_row) + 1L;
        ii_width =
            (ar_struct.end_column - ar_struct.start_column) + 1L;

        for (ss_cit = spsr_vector->cbegin();
             ss_cit != spsr_vector->cend(); ++ss_cit) {
            ii_square_size = ss_cit->square_size;
            if ((ii_square_size == ii_height)
                || (ii_square_size == ii_width)) {
                return false;
            }
        }
        return true;
    } else {
        return false;
    }
}

// #############################################################
// #############################################################
// SpsrStructVectorToBouwkampString
std::string CSpsr::SpsrStructVectorToBouwkampString(
    const std::vector<mylib::CSpsr::spsr_struct> *struct_vector)
    const {
    mylib::CUtils utils_obj;
    std::vector<mylib::CSpsr::spsr_struct> local_vector;
    int64_t ii_block_size, ii_row;
    int64_t ii_last_row;
    std::vector<mylib::CSpsr::spsr_struct>::const_iterator ss_cit;
    std::string result_string, tmp_string;

    local_vector = *struct_vector;

    std::sort(local_vector.begin(), local_vector.end(),
              CSpsr::sort_spsr_function);

    result_string = "";
    ii_last_row = -1;
    for (ss_cit = local_vector.cbegin();
         ss_cit != local_vector.cend(); ++ss_cit) {
        ii_block_size = ss_cit->square_size;
        ii_row = ss_cit->start_row;

        tmp_string = utils_obj.CommafyNum(ii_block_size);
        if (ss_cit == local_vector.cbegin()) {
            result_string += "(" + tmp_string;
        } else {
            if (ii_row == ii_last_row) {
                result_string += ", " + tmp_string;
            } else {
                result_string += ") (" + tmp_string;
            }
        }

        ii_last_row = ii_row;
    }

    if (static_cast<int64_t>(local_vector.size()) > 0L) {
        result_string += ")";
    }

    return result_string;
}

// #############################################################
// #############################################################
// SpsrStructVectorToStatsString
std::string CSpsr::SpsrStructVectorToStatsString(
    const std::vector<struct spsr_struct> *struct_vector)
    const {
    mylib::CUtils utils_obj;
    int64_t ii_block_size;
    int64_t ii_blocks, ii_prime_blocks;
    double dpcnt;
    std::vector<struct spsr_struct>::const_iterator ss_cit;
    std::string result_string;

    ii_blocks = ii_prime_blocks = 0;

    for (ss_cit = struct_vector->cbegin();
         ss_cit != struct_vector->cend(); ++ss_cit) {
        ii_block_size = ss_cit->square_size;
        ii_blocks++;

        if (IsPrime(ii_block_size) == true) {
            ii_prime_blocks++;
        }
    }

    if (static_cast<int64_t>(struct_vector->size()) > 0L) {
        const int64_t kBuff = 20;
        char buffer[kBuff];

        dpcnt = 100.0 * static_cast<double>(ii_prime_blocks) /
            static_cast<double>(ii_blocks);
        result_string = "primes/total = "
            + utils_obj.CommafyNum(ii_prime_blocks);
        result_string += " / "
            + utils_obj.CommafyNum(ii_blocks);
        snprintf(buffer, kBuff, "%.1f%%",
                 utils_obj.RoundFloat(dpcnt, 1.0));
        result_string += " = "
            + static_cast<std::string>(buffer);
    } else {
        result_string = "";
    }

    return result_string;
}

// #############################################################
// #############################################################
void CSpsr::InsertIntoUsedMap(
    const std::vector<int64_t> *input_vector,
    std::map<int64_t, bool> *used_map) const {
    std::vector<int64_t>::const_iterator vi_cit;

    for (vi_cit = input_vector->cbegin();
         vi_cit != input_vector->cend(); ++vi_cit) {
        (*used_map)[*vi_cit] = true;
    }

    return;
}

// #############################################################
// #############################################################
void CSpsr::DeleteFromUsedMap(
    const std::vector<int64_t> *input_vector,
    std::map<int64_t, bool> *used_map) const {
    std::vector<int64_t>::const_iterator vi_cit;

    for (vi_cit = input_vector->cbegin();
         vi_cit != input_vector->cend(); ++vi_cit) {
        used_map->erase(*vi_cit);
    }

    return;
}

// #############################################################
// #############################################################
void CSpsr::DisplayStatusMessage(
    const int64_t rectangle_width,
    const std::vector<int64_t> *partition_vector,
    const int64_t status_in_minutes,
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *start_time,
    std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *return_time) const {
    mylib::CUtils utils_obj;
    std::chrono::time_point<std::chrono::steady_clock,
                            std::chrono::duration<double>> end_time;
    int64_t ll_minutes, ll_sum;
    std::string elapsed_string;

    end_time = std::chrono::steady_clock::now();

    ll_minutes =
        utils_obj.ClockDurationInMinutes(&end_time, start_time);

    if (ll_minutes >= status_in_minutes) {
        elapsed_string =
            utils_obj.ClockDuration(&end_time, start_time);

        ll_sum =
            std::accumulate(partition_vector->cbegin(),
                            partition_vector->cend(), 0L);

        printf("status [ %s ] :  ",
               utils_obj.CommafyNum(rectangle_width).c_str());
        printf("partition = %s : sum = %ld : ",
               utils_obj.Int64VectorToString(partition_vector).c_str(),
               ll_sum);
        printf("%s : %s\n",
               elapsed_string.c_str(),
               utils_obj.CurrentDateTime().c_str());
        fflush(stdout);

        *return_time = end_time;

        return;
    } else {
        *return_time = *start_time;

        return;
    }
}

// #############################################################
// #############################################################
void CSpsr::Row0MakeSpsrFromPermutation(
    const std::vector<int64_t> *permutation_vector,
    std::vector<mylib::CSpsr::spsr_struct> *output_spsr_vector,
    std::vector<mylib::CSpsr::rect_struct> *output_rect_vector)
    const {
    std::vector<int64_t>::const_iterator vi_cit;
    mylib::CSpsr::spsr_struct s_struct;
    mylib::CSpsr::rect_struct r_struct;
    int64_t square_size, column;

    output_spsr_vector->clear();
    output_rect_vector->clear();

    column = 0L;
    for (vi_cit = permutation_vector->cbegin();
         vi_cit != permutation_vector->cend(); ++vi_cit) {
        square_size = *vi_cit;

        s_struct.square_size = square_size;
        s_struct.start_row = 0L;
        s_struct.start_column = column;

        output_spsr_vector->push_back(s_struct);

        r_struct.start_row = 0L;
        r_struct.start_column = column;
        r_struct.end_row = square_size - 1L;
        r_struct.end_column = column + square_size - 1L;

        output_rect_vector->push_back(r_struct);

        column = column + square_size;
    }

    return;
}

// #############################################################
// #############################################################
void CSpsr::MakeSpsrFromPermutation(
    const std::vector<int64_t> *permutation_vector,
    const int64_t start_row, const int64_t start_column,
    std::vector<mylib::CSpsr::spsr_struct> *output_spsr_vector)
    const {
    std::vector<int64_t>::const_iterator vi_cit;
    mylib::CSpsr::spsr_struct s_struct;
    int64_t square_size, column;

    column = start_column;
    for (vi_cit = permutation_vector->cbegin();
         vi_cit != permutation_vector->cend(); ++vi_cit) {
        square_size = *vi_cit;

        s_struct.square_size = square_size;
        s_struct.start_row = start_row;
        s_struct.start_column = column;

        output_spsr_vector->push_back(s_struct);

        column = column + square_size;
    }

    return;
}

// #############################################################
// #############################################################
inline void CSpsr::CopyVectors(
    const std::vector<mylib::CSpsr::spsr_struct>
    *from_spsr_vector,
    const std::vector<mylib::CSpsr::rect_struct>
    *from_rect_vector,
    std::vector<mylib::CSpsr::spsr_struct>
    *to_spsr_vector,
    std::vector<mylib::CSpsr::rect_struct>
    *to_rect_vector) const {
    to_spsr_vector->resize(from_spsr_vector->size());
    std::copy(from_spsr_vector->cbegin(), from_spsr_vector->cend(),
              to_spsr_vector->begin());

    to_rect_vector->resize(from_rect_vector->size());
    std::copy(from_rect_vector->cbegin(), from_rect_vector->cend(),
              to_rect_vector->begin());
    return;
}

// #############################################################
// #############################################################
void CSpsr::ProcessOtherRows(
    const int64_t rectangle_width,
    const std::vector<mylib::CSpsr::spsr_struct>
    *spsr_vector,
    const std::vector<mylib::CSpsr::rect_struct>
    *rectangle_vector,
    std::vector<mylib::CSpsr::output_struct>
    *output_vector,
    std::map<int64_t, bool> *used_map) const {
    std::vector<int64_t>
        local_partition_vector, local_permutation_vector;
    std::vector<mylib::CSpsr::rect_struct>
        local_rectangle_vector, old_rectangle_vector,
        new_rectangle_vector, new2_rectangle_vector;
    std::vector<mylib::CSpsr::spsr_struct>
        local_spsr_vector, old_spsr_vector;
    std::vector<mylib::CSpsr::spsr_struct>::const_iterator
        ss_cit;
    bool completed_flag(false);
    bool successor_flag, ok_to_use_flag;
    int64_t min_row, min_column, min_width, min_height;
    int64_t tmp_height, max_height;
    mylib::CSpsr::output_struct
        output_result_struct, tmp_output_struct;

    if ((static_cast<int64_t>(rectangle_vector->size()) == 1L)
        && (static_cast<int64_t>(spsr_vector->size()) > 1L)) {
        // found a perfect dissection
        int64_t order, width, height;
        mylib::CSpsr::rect_struct r_struct;
        std::string bk_string;

        order = static_cast<int64_t>(spsr_vector->size());

        r_struct = rectangle_vector->at(0);
        width = r_struct.end_column - r_struct.start_column + 1L;
        height = r_struct.end_row - r_struct.start_row + 1L;

        if (height <= width) {
            bool ok_flag = true;
            for (ss_cit = spsr_vector->cbegin();
                 ss_cit != spsr_vector->cend(); ++ss_cit) {
                if ((width == ss_cit->square_size)
                    || (height == ss_cit->square_size)) {
                    ok_flag = false;
                    break;
                }
            }

            if (ok_flag == true) {
                bk_string =
                    SpsrStructVectorToBouwkampString(spsr_vector);

                output_result_struct.order = order;
                output_result_struct.height = height;
                output_result_struct.width = width;
                output_result_struct.first_square =
                    static_cast<int64_t>(
                        (spsr_vector->at(0)).square_size);
                output_result_struct.bk_string = bk_string;
                output_result_struct.vs_vector = *spsr_vector;
            } else {
                output_result_struct =
                    { 0L, 0L, 0L, 0L,
                      {}, static_cast<std::string>(""), };
            }
        } else {
            output_result_struct =
                { 0L, 0L, 0L, 0L,
                  {}, static_cast<std::string>(""), };
        }

        if (output_result_struct.order > 0L) {
            output_vector->push_back(output_result_struct);
        }
        return;
    } else {
        FindMinRowColWidthHeight(
            rectangle_vector,
            &min_row, &min_column,
            &min_width, &min_height);

        local_partition_vector.clear();
        if (min_height <= rectangle_width) {
            CopyVectors(
                spsr_vector, rectangle_vector,
                &local_spsr_vector, &local_rectangle_vector);

            while (completed_flag == false) {
                if (static_cast<int64_t>(
                        local_partition_vector.size())
                    <= 0L) {
                    local_partition_vector.push_back(min_width);
                    successor_flag = true;
                } else {
                    successor_flag =
                        SuccessorOfIntegerPartition(
                            min_width, &local_partition_vector);
                }
                ok_to_use_flag =
                    CheckIfOkToUse(
                        &local_partition_vector, used_map);

                if ((successor_flag == true)
                    && (ok_to_use_flag == true)) {
                    tmp_height =
                        *std::max_element(local_partition_vector.cbegin(),
                                          local_partition_vector.cend());
                    max_height = tmp_height + min_row;

                    if (max_height <= rectangle_width) {
                        local_permutation_vector.resize(
                            local_partition_vector.size());
                        std::copy(local_partition_vector.cbegin(),
                                  local_partition_vector.cend(),
                                  local_permutation_vector.begin());
                        std::sort(local_permutation_vector.begin(),
                                  local_permutation_vector.end());

                        do {
                            CopyVectors(
                                &local_spsr_vector,
                                &local_rectangle_vector,
                                &old_spsr_vector,
                                &old_rectangle_vector);

                            InsertIntoUsedMap(
                                &local_permutation_vector,
                                used_map);

                            MakeSpsrFromPermutation(
                                &local_permutation_vector,
                                min_row, min_column,
                                &local_spsr_vector);

                            AddPartitionToRectangleStruct(
                                &local_permutation_vector,
                                &local_rectangle_vector,
                                &new_rectangle_vector);

                            std::sort(new_rectangle_vector.begin(),
                                      new_rectangle_vector.end(),
                                      CSpsr::sort_rect_function);

                            CombineRectangles(
                                &new_rectangle_vector,
                                &new2_rectangle_vector);

                            ProcessOtherRows(
                                rectangle_width,
                                &local_spsr_vector,
                                &new2_rectangle_vector,
                                output_vector,
                                used_map);

                            DeleteFromUsedMap(
                                &local_permutation_vector,
                                used_map);

                            CopyVectors(
                                &old_spsr_vector,
                                &old_rectangle_vector,
                                &local_spsr_vector,
                                &local_rectangle_vector);
                        } while (
                            std::next_permutation(
                                local_permutation_vector.begin(),
                                local_permutation_vector.end()));
                    }
                } else if (successor_flag == false) {
                    completed_flag = true;
                }
            }
        }
    }

    return;
}

// #############################################################
// #############################################################
void CSpsr::GenerateNThreadsPermutations(
    int64_t nthreads,
    const std::vector<int64_t> *input_vector,
    std::vector<int64_t> *last_permutation_vector,
    std::vector<std::vector<int64_t> > *out_vector_vector,
    std::map<std::string, bool> *symmetric_permutation_map)
    const {
    mylib::CUtils utils_obj;
    int64_t counter(0L);
    bool continue_loop_flag = true;
    bool next_flag = true;
    std::vector<int64_t> tmp_permutation_vector;
    std::vector<int64_t> tmp2_vector(*input_vector);
    std::vector<int64_t> tmp3_vector(*input_vector);
    std::map<std::string, bool>::iterator msb_it;
    std::string astring, rev_astring;

    out_vector_vector->push_back(tmp2_vector);
    counter++;

    astring = utils_obj.Int64VectorToString(&tmp3_vector);
    symmetric_permutation_map->emplace(astring, true);

    tmp_permutation_vector = *input_vector;

    while (continue_loop_flag == true) {
        next_flag =
            std::next_permutation(
                tmp_permutation_vector.begin(),
                tmp_permutation_vector.end());

        if (next_flag == true) {
            if (counter < nthreads) {
                tmp2_vector.clear();
                tmp2_vector = tmp_permutation_vector;
                tmp3_vector.clear();
                tmp3_vector = tmp_permutation_vector;
                std::reverse(tmp3_vector.begin(),
                             tmp3_vector.end());
                rev_astring =
                    utils_obj.Int64VectorToString(&tmp3_vector);
                msb_it =
                    symmetric_permutation_map->find(rev_astring);
                if (msb_it == symmetric_permutation_map->end()) {
                    counter += 1L;
                    out_vector_vector->push_back(tmp2_vector);
                    astring =
                        utils_obj.Int64VectorToString(
                            &tmp2_vector);
                    symmetric_permutation_map->emplace(astring, true);
                }
            } else {
                continue_loop_flag = false;
                last_permutation_vector->clear();
                *last_permutation_vector =
                    tmp_permutation_vector;
            }
        } else {
            // next_flag == false
            continue_loop_flag = false;
            last_permutation_vector->clear();
        }
    }

    return;
}

// #############################################################
// #############################################################
// ###  called once for each thread
void CSpsr::ComputeNextRecursionFromPermutation(
    const int64_t rectangle_width,
    const std::vector<int64_t> *permutation_vector,
    std::vector<mylib::CSpsr::output_struct> *output_vector)
    const {
    std::vector<mylib::CSpsr::rect_struct>
        local_rectangle_vector, old_rectangle_vector;
    std::vector<mylib::CSpsr::spsr_struct>
        local_spsr_vector, old_spsr_vector;
    std::map<int64_t, bool> used_map;

    used_map.clear();
    InsertIntoUsedMap(permutation_vector, &used_map);

// Row0MakeSpsrFromPermutation clears out spsr and rect vectors
    Row0MakeSpsrFromPermutation(
        permutation_vector, &local_spsr_vector,
        &local_rectangle_vector);

    std::sort(local_rectangle_vector.begin(),
              local_rectangle_vector.end(),
              CSpsr::sort_rect_function);

    ProcessOtherRows(
        rectangle_width,
        &local_spsr_vector,
        &local_rectangle_vector,
        output_vector,
        &used_map);

    DeleteFromUsedMap(permutation_vector, &used_map);

    return;
}

// #############################################################
// #############################################################
void CSpsr::SpawnFutures(
    int64_t rectangle_width,
    std::vector<int64_t> *tmp_ivector,
    std::vector<std::future<std::vector<
    mylib::CSpsr::output_struct> > > *future_out_vector)
    const {
    std::vector<int64_t> tmp2_ivector;
    std::vector<int64_t>::const_iterator vi_cit;

    for (vi_cit = tmp_ivector->cbegin();
         vi_cit != tmp_ivector->cend();
         ++vi_cit) {
        tmp2_ivector.push_back(*vi_cit);
    }

    future_out_vector->emplace_back(
        std::async(std::launch::async,
                   [this,
                    rectangle_width,
                    tmp2_ivector] () {
                       std::vector<
                           mylib::CSpsr::output_struct>
                           tmp_output_vector;
                       ComputeNextRecursionFromPermutation(
                           rectangle_width,
                           &tmp2_ivector,
                           &tmp_output_vector);
                       return tmp_output_vector;
                   }));
    return;
}

// #############################################################
// #############################################################
void CSpsr::LoopOverPermutationsRow0(
    const std::vector<int64_t> *input_vector,
    const int64_t rectangle_width,
    const int64_t status_in_minutes,
    const int64_t nthreads,
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *start_time,
    std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *return_time,
    std::vector<mylib::CSpsr::output_struct>
    *output_vector) const {
    mylib::CUtils utils_obj;
    std::vector<int64_t> permutation_vector;
    std::vector<int64_t> last_permutation_vector;
    std::vector<std::vector<int64_t> > perm_vector_vector;
    std::vector<mylib::CSpsr::rect_struct>
        local_rectangle_vector, old_rectangle_vector;
    std::vector<mylib::CSpsr::spsr_struct>
        local_spsr_vector, old_spsr_vector;
    bool continue_loop_flag = true;
    std::vector<int64_t> tmp_ivector;
    std::vector<int64_t>::const_iterator vi_cit;
    std::vector<std::future<std::vector<
        mylib::CSpsr::output_struct> > >
        future_out_vector;
    std::future<std::vector<
        mylib::CSpsr::output_struct> > tmp_future;
    std::vector<
        mylib::CSpsr::output_struct> tmp_out_vector;
    std::vector<
        mylib::CSpsr::output_struct>::const_iterator os_cit;
    std::chrono::time_point<
        std::chrono::steady_clock,
        std::chrono::duration<double> >
        local_time, this_time;
    std::map<std::string, bool> symmetric_permutation_map;
    std::string astring, rev_astring;
    int64_t ii, ii_last_size, ii_size;
    int64_t status_counter(0L);
    const int64_t status_check = 100L;

    status_counter = 0L;
    local_time = *start_time;

    tmp_ivector = *input_vector;
    std::sort(tmp_ivector.begin(),
              tmp_ivector.end());

    last_permutation_vector = tmp_ivector;

    astring = utils_obj.Int64VectorToString(&tmp_ivector);
    symmetric_permutation_map[astring] = true;

    while (continue_loop_flag == true) {
        perm_vector_vector.clear();
        permutation_vector = last_permutation_vector;
        ii_last_size =
            static_cast<int64_t>(last_permutation_vector.size());
        if (ii_last_size > 0L) {
            GenerateNThreadsPermutations(
                nthreads, &permutation_vector,
                &last_permutation_vector,
                &perm_vector_vector,
                &symmetric_permutation_map);

            ii_size =
                static_cast<int64_t>(perm_vector_vector.size());

            if (ii_size > 0L) {
                future_out_vector.clear();
                for (ii = 0L; ii < ii_size; ++ii) {
                    tmp_ivector.clear();
                    for (vi_cit = perm_vector_vector[ii].cbegin();
                         vi_cit != perm_vector_vector[ii].cend();
                         ++vi_cit) {
                        tmp_ivector.push_back(*vi_cit);
                    }

                    // tmp_ivector = perm_vector_vector[ii];
                    SpawnFutures(
                        rectangle_width,
                        &tmp_ivector,
                        &future_out_vector);
                }

                for (ii = 0; ii < ii_size; ++ii) {
                    tmp_out_vector = future_out_vector[ii].get();
                    for (os_cit = tmp_out_vector.cbegin();
                         os_cit != tmp_out_vector.cend();
                         ++os_cit) {
                        if (os_cit->order > 0L) {
                            output_vector->push_back(*os_cit);
                        }
                    }
                }
            } else {
                continue_loop_flag = false;
            }
        } else {
            continue_loop_flag = false;
        }

        ++status_counter;
        if (status_counter % status_check == 0L) {
            DisplayStatusMessage(
                rectangle_width, input_vector, status_in_minutes,
                &local_time, &this_time);
            local_time = this_time;
        }
    }

    symmetric_permutation_map.clear();

    *return_time = local_time;

    return;
}

// #############################################################
// #############################################################
void CSpsr::ProcessRow0(
    const int64_t rectangle_width,
    const int64_t status_in_minutes,
    const int64_t nthreads,
    const std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *start_time,
    std::chrono::time_point<std::chrono::steady_clock,
    std::chrono::duration<double>> *return_time,
    std::vector<mylib::CSpsr::output_struct> *out_vector) const {
    mylib::CUtils utils_obj;
    std::vector<int64_t> partition_vector, permutation_vector;
    std::vector<int64_t>::const_iterator
        vi_1_cit, vi_2_cit, vi_3_cit;
    std::vector<mylib::CSpsr::rect_struct>
        local_rectangle_vector, old_rectangle_vector;
    std::vector<mylib::CSpsr::spsr_struct>
        local_spsr_vector, old_spsr_vector;
    std::map<int64_t, bool> empty_used_map;
    std::chrono::time_point<std::chrono::steady_clock,
                            std::chrono::duration<double>>
        intermediate_time, local_time;
    std::map<std::string, bool> symmetric_partition_map;
    std::string astring, rev_astring;
    std::vector<int64_t> tmp2_vector;
    bool continue_loop_flag(true), successor_flag;
    bool ok_to_use_flag, seen_flag(false);

    intermediate_time = *start_time;
    partition_vector.clear();
    partition_vector.push_back(rectangle_width);

    astring = utils_obj.Int64VectorToString(
        &partition_vector);
    symmetric_partition_map[astring] = true;

    while (continue_loop_flag == true) {
        successor_flag =
            SuccessorOfIntegerPartition(
                rectangle_width, &partition_vector);

        if (successor_flag == false) {
            continue_loop_flag = false;
            break;
        } else {
            vi_1_cit =
                std::find(partition_vector.cbegin(),
                          partition_vector.cend(), 1L);

            if (vi_1_cit == partition_vector.cend()) {
                vi_2_cit =
                    std::find(partition_vector.cbegin(),
                              partition_vector.cend(), 2L);
                if (vi_2_cit == partition_vector.cend()) {
                    vi_3_cit =
                        std::find(partition_vector.cbegin(),
                                  partition_vector.cend(), 3L);

                    if (vi_3_cit == partition_vector.cend()) {
                        std::vector<int64_t>
                            input_vector(partition_vector);

                        // mainly, looking for duplicates
                        // in partition_vector
                        ok_to_use_flag =
                            CheckIfOkToUse(&partition_vector,
                                           &empty_used_map);

                        if (ok_to_use_flag == true) {
                            tmp2_vector = input_vector;
                            std::reverse(tmp2_vector.begin(),
                                         tmp2_vector.end());
                            rev_astring =
                                utils_obj.Int64VectorToString(
                                    &tmp2_vector);
                            seen_flag =
                                symmetric_partition_map[rev_astring];
                            // eliminate half the partition possibilities
                            if (seen_flag == false) {
                                astring =
                                    utils_obj.Int64VectorToString(
                                        &input_vector);
                                symmetric_partition_map[astring] = true;

                                LoopOverPermutationsRow0(
                                    &input_vector, rectangle_width,
                                    status_in_minutes, nthreads,
                                    &intermediate_time, &local_time,
                                    out_vector);
                                intermediate_time = local_time;
                            }
                        }
                    }
                }
            }
        }
    }

    symmetric_partition_map.clear();
    *return_time = intermediate_time;
    return;
}

// #############################################################
// #############################################################
std::string CSpsr::SpsrStructVectorToHMirrorString(
    const int64_t width,
    const std::vector<mylib::CSpsr::spsr_struct> *spsr_vector)
    const {
    std::vector<mylib::CSpsr::spsr_struct>::const_iterator vss_cit;
    std::vector<mylib::CSpsr::spsr_struct> mirror_vector;
    int64_t next_start_column;
    mylib::CSpsr::spsr_struct astruct, bstruct;
    std::string result_string;

    for (vss_cit = spsr_vector->cbegin();
         vss_cit != spsr_vector->cend(); ++vss_cit) {
        astruct = *vss_cit;
        // only mirror column coordinates
        next_start_column = width
            - (astruct.square_size + astruct.start_column);
        next_start_column =
            std::max(std::min(width, next_start_column), 0L);

        bstruct.square_size = astruct.square_size;
        bstruct.start_row = astruct.start_row;
        bstruct.start_column = next_start_column;

        mirror_vector.push_back(bstruct);
    }

    result_string =
        SpsrStructVectorToBouwkampString(&mirror_vector);

    return result_string;
}

// #############################################################
// #############################################################
std::string CSpsr::SpsrStructVectorToVMirrorString(
    const int64_t height,
    const std::vector<mylib::CSpsr::spsr_struct> *spsr_vector)
    const {
    std::vector<mylib::CSpsr::spsr_struct>::const_iterator
        vss_cit;
    std::vector<mylib::CSpsr::spsr_struct> mirror_vector;
    int64_t next_start_row;
    mylib::CSpsr::spsr_struct astruct, bstruct;
    std::string result_string;

    for (vss_cit = spsr_vector->cbegin();
         vss_cit != spsr_vector->cend(); ++vss_cit) {
        astruct = *vss_cit;
        // only mirror column coordinates
        next_start_row = height
            - (astruct.square_size + astruct.start_row);
        next_start_row =
            std::max(std::min(height, next_start_row), 0L);

        bstruct.square_size = astruct.square_size;
        bstruct.start_row = next_start_row;
        bstruct.start_column = astruct.start_column;

        mirror_vector.push_back(bstruct);
    }

    result_string =
        SpsrStructVectorToBouwkampString(&mirror_vector);

    return result_string;
}

// #############################################################
// #############################################################
std::string CSpsr::SpsrStructVectorToHVMirrorString(
    const int64_t height,
    const int64_t width,
    const std::vector<mylib::CSpsr::spsr_struct> *spsr_vector)
    const {
    std::vector<mylib::CSpsr::spsr_struct>::const_iterator vss_cit;
    std::vector<mylib::CSpsr::spsr_struct> mirror_vector;
    int64_t next_start_row, next_start_column;
    mylib::CSpsr::spsr_struct astruct, bstruct;
    std::string result_string;

    for (vss_cit = spsr_vector->cbegin();
         vss_cit != spsr_vector->cend(); ++vss_cit) {
        astruct = *vss_cit;
        // only mirror column coordinates
        next_start_row = height
            - (astruct.square_size + astruct.start_row);
        next_start_row =
            std::max(std::min(height, next_start_row), 0L);
        next_start_column = width
            - (astruct.square_size + astruct.start_column);
        next_start_column =
            std::max(std::min(width, next_start_column), 0L);

        bstruct.square_size = astruct.square_size;
        bstruct.start_row = next_start_row;
        bstruct.start_column = next_start_column;

        mirror_vector.push_back(bstruct);
    }

    result_string =
        SpsrStructVectorToBouwkampString(&mirror_vector);

    return result_string;
}

// #############################################################
// #############################################################
std::string CSpsr::OutputStructLine1ToString(
    const mylib::CSpsr::output_struct *ostruct) const {
    std::string rstring;
    mylib::CUtils utils_obj;

    rstring = "";
    if (ostruct->order > 0L) {
        rstring = static_cast<std::string>("order = ")
            + utils_obj.CommafyNum(ostruct->order)
            + static_cast<std::string>(", rectangle = [ ")
            + utils_obj.CommafyNum(ostruct->height)
            + static_cast<std::string>(" x ")
            + utils_obj.CommafyNum(ostruct->width)
            + static_cast<std::string>(" ]");
    }

    return rstring;
}

// #############################################################
// #############################################################
void CSpsr::TrimOutputVector(
    std::vector<mylib::CSpsr::output_struct> *input_vector,
    std::vector<mylib::CSpsr::output_struct> *output_vector)
    const {
    std::vector<mylib::CSpsr::output_struct> local_out_vector;
    std::vector<mylib::CSpsr::output_struct>::const_iterator vo_cit;
    mylib::CSpsr::output_struct out_struct;
    std::map<std::string, bool> exclude_string_map;
    std::map<std::string, bool>::iterator msb_it;
    std::string this_bk_string;
    std::string hb_string, vb_string, hvb_string;
    std::vector<CSpsr::spsr_struct> tmp_output_vector;

    output_vector->clear();

    local_out_vector = *input_vector;
    std::sort(local_out_vector.begin(), local_out_vector.end(),
              sort_output_function);

    for (vo_cit = local_out_vector.cbegin();
         vo_cit != local_out_vector.cend(); ++vo_cit) {
        out_struct = *vo_cit;
        this_bk_string = out_struct.bk_string;

        msb_it = exclude_string_map.find(this_bk_string);
        if (msb_it == exclude_string_map.end()) {
            output_vector->push_back(out_struct);

            tmp_output_vector = out_struct.vs_vector;
            hb_string =
                SpsrStructVectorToHMirrorString(out_struct.width,
                                                &tmp_output_vector);
            tmp_output_vector = out_struct.vs_vector;
            vb_string =
                SpsrStructVectorToVMirrorString(out_struct.height,
                                                &tmp_output_vector);
            tmp_output_vector = out_struct.vs_vector;
            hvb_string =
                SpsrStructVectorToHVMirrorString(out_struct.height,
                                                 out_struct.width,
                                                 &tmp_output_vector);

            exclude_string_map[hb_string] = true;
            exclude_string_map[vb_string] = true;
            exclude_string_map[hvb_string] = true;
        }
    }

    return;
}

// #############################################################
// #############################################################
void CSpsr::PrintOutputVector(
    std::vector<mylib::CSpsr::output_struct> *output_vector,
    int64_t *number_of_unique_results) const {
    std::vector<mylib::CSpsr::output_struct> trimmed_out_vector;
    std::vector<mylib::CSpsr::output_struct>::const_iterator vo_cit;
    mylib::CSpsr::output_struct ostruct;
    std::vector<mylib::CSpsr::spsr_struct> vs_vector;
    std::string rstring;
    int64_t ov_size, ii_count;
    mylib::CUtils utils_obj;

    ov_size = static_cast<int64_t>(output_vector->size());
    *number_of_unique_results = ov_size;
    if (ov_size > 0L) {
        TrimOutputVector(output_vector,
                         &trimmed_out_vector);

        std::sort(trimmed_out_vector.begin(),
                  trimmed_out_vector.end(),
                  sort_output_function);

        ii_count = 0L;
        for (vo_cit = trimmed_out_vector.cbegin();
             vo_cit != trimmed_out_vector.cend(); ++vo_cit) {
            ostruct = *vo_cit;
            rstring = OutputStructLine1ToString(&ostruct);
            ++ii_count;
            printf("(%s) : %s\n",
                   utils_obj.CommafyNum(ii_count).c_str(),
                   rstring.c_str());
            printf("bouwkamp string = (%s)\n",
                   ostruct.bk_string.c_str());
            printf("\n");
            fflush(stdout);
        }

        *number_of_unique_results =
            static_cast<int64_t>(trimmed_out_vector.size());
    }
    return;
}

// #############################################################
// #############################################################
// main loop
void CSpsr::MainLoopPrime(
    const int64_t start_width,
    const int64_t end_width,
    const int64_t nthreads,
    const int64_t status_in_minutes) const {
    mylib::CUtils utils_obj;
    std::vector<mylib::CSpsr::output_struct> output_vector;
    int64_t width, unique_results;
    std::string result_string, elapsed_time_string;
    std::chrono::time_point<std::chrono::steady_clock,
                            std::chrono::duration<double>>
        start_time, init_time, end_time, tmp_time, local_time;

    init_time = std::chrono::steady_clock::now();

    for (width = start_width; width <= end_width; ++width) {
        start_time = std::chrono::steady_clock::now();
        tmp_time = start_time;

        output_vector.clear();

        ProcessRow0(width, status_in_minutes, nthreads,
                    &tmp_time, &local_time,
                    &output_vector);

        tmp_time = local_time;

            {
                PrintOutputVector(&output_vector,
                                  &unique_results);

                end_time = std::chrono::steady_clock::now();

                elapsed_time_string =
                    utils_obj.ClockDuration(&end_time, &start_time);

                printf("[ %s ] : number found = %s : ",
                       utils_obj.CommafyNum(width).c_str(),
                       utils_obj.CommafyNum(unique_results).c_str());
                printf("elapsed time = %s : %s\n",
                       elapsed_time_string.c_str(),
                       utils_obj.CurrentDateTime().c_str());
                fflush(stdout);
            }
    }  // end of for loop

    end_time = std::chrono::steady_clock::now();
    elapsed_time_string =
        utils_obj.ClockDuration(&end_time, &init_time);
    printf("total elapsed time = %s : %s\n",
           elapsed_time_string.c_str(),
           utils_obj.CurrentDateTime().c_str());
    fflush(stdout);
    return;
}

}  // end of namespace mylib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/
