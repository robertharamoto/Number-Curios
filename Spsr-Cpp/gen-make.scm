#! /usr/bin/guile \
--no-auto-compile -e main -s
!#
;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  generate a make file based on sources and tests      ###
;;;###  directories, object files to be put into objects     ###
;;;###                                                       ###
;;;###  last updated September 26, 2024                      ###
;;;###                                                       ###
;;;###  updated July 12, 2022                                ###
;;;###                                                       ###
;;;###  updated April 21, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### regex used for regex
(use-modules ((ice-9 regex)
              :renamer (symbol-prefix-proc 'ice-9-regex:)))

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### popen - pipes functions
(use-modules ((ice-9 popen)
              :renamer (symbol-prefix-proc 'pipes-module:)))

;;;### rdelim - read-line
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'rdelim-module:)))

;;;### getopt-long used for command-line option arguments processing
(use-modules ((ice-9 getopt-long)
              :renamer (symbol-prefix-proc 'ice-9-getopt:)))

;;;### ice-9 ftw for file tree walk functions
(use-modules ((ice-9 ftw)
              :renamer (symbol-prefix-proc 'ice9-ftw:)))

;;;#############################################################
;;;#############################################################
;;;###
;;;### used to get data from system
(define (get-one-line-from-shell command-string)
  (begin
    (let ((port
           (pipes-module:open-input-pipe command-string)))
      (let ((result-string (rdelim-module:read-line port)))
        (begin
          (pipes-module:close-pipe port)
          (if (eof-object? result-string)
              (begin
                #f)
              (begin
                result-string
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
;;;###
;;;### load list of *.cpp files
(define (list-all-cc-files init-path file-string sub-string)
  (begin
    (let ((counter 0)
          (file-list (list)))
      (begin
        (ice9-ftw:nftw
         init-path
         (lambda (filename statinfo flag base level)
           (begin
             (if (equal? flag 'regular)
                 (begin
                   (if (and
                        (not
                         (equal?
                          (ice-9-regex:string-match
                           file-string filename)
                          #f))
                        (not
                         (equal?
                          (ice-9-regex:string-match
                           sub-string filename)
                          #f)))
                       (begin
                         (set!
                          file-list
                          (append
                           file-list (list (basename filename))))
                         ))
                   ))
             #t
             )))

        file-list
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###
;;;### takes the basename from .cc, .cpp, .h, or .o files
(define (fname-to-basename input-name)
  (begin
    (let ((sindex1
           (string-contains-ci input-name ".cpp"))
          (sindex2
           (string-contains-ci input-name ".cc"))
          (sindex3
           (string-contains-ci input-name ".h"))
          (sindex4
           (string-contains-ci input-name ".o")))
      (begin
        (cond
         ((not (equal? sindex1 #f))
          (begin
            (substring input-name 0 sindex1)
            ))
         ((not (equal? sindex2 #f))
          (begin
            (substring input-name 0 sindex2)
            ))
         ((not (equal? sindex3 #f))
          (begin
            (substring input-name 0 sindex3)
            ))
         ((not (equal? sindex4 #f))
          (begin
            (substring input-name 0 sindex4)
            ))
         (else
          (begin
            #f
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### produces MAINOBJECTS = $(OBJECTSDIR)/calc.o $(OBJECTSDIR)/utils.o ..., line
(define (list-to-objects-string start-string files-list)
  (begin
    (let ((line-string start-string))
      (begin
        (for-each
         (lambda (a-file)
           (begin
             (let ((base-name (fname-to-basename a-file)))
               (begin
                 (if (not (equal? base-name #f))
                     (begin
                       (set! line-string
                             (format #f "~a $(OBJECTSDIR)/~a.o"
                                     line-string base-name))
                       ))
                 ))
             )) files-list)

        line-string
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### produces MAINOBJECTS = $(OBJECTSDIR)/calc.o $(OBJECTSDIR)/utils.o ..., line
(define (remove-main-objects-string main-string objects-list)
  (begin
    (let ((result-list (list)))
      (begin
        (for-each
         (lambda (a-file)
           (begin
             (let ((a-base-name (fname-to-basename a-file)))
               (begin
                 (if (and (not (equal? a-base-name #f))
                          (not (string-ci=? main-string a-base-name)))
                     (begin
                       (set! result-list (cons a-file result-list))
                       ))
                 ))
             )) objects-list)

        (sort result-list string-ci<?)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (return-header-file-name base-name header-list)
  (begin
    (let ((hname #f))
      (begin
        (for-each
         (lambda (h-file)
           (begin
             (let ((h-base (fname-to-basename h-file)))
               (begin
                 (if (string-ci=? base-name h-base)
                     (begin
                       (set! hname h-file)
                       ))
                 ))
             )) header-list)

        hname
        ))
    ))
;;;#############################################################
;;;#############################################################
(define-syntax display-single-line-comment-string-macro
  (syntax-rules ()
    ((display-single-line-comment-string-macro
      var-comment var-length)
     (begin
       (let ((var-clen
              (string-length var-comment))
             (local-comment var-comment)
             (max-allowed-len (- var-length 10)))
         (begin
           (if (not (equal? var-clen #f))
               (begin
                 (if (> var-clen max-allowed-len)
                     (begin
                       (set! local-comment
                             (substring
                              var-comment 0 max-allowed-len))
                       (set! var-clen
                             (string-length local-comment))
                       ))

                 (let ((var-space-len
                        (- var-length var-clen 10)))
                   (begin
                     (if (> var-space-len 0)
                         (begin
                           (let ((spacer-string
                                  (make-string
                                   var-space-len #\space)))
                             (begin
                               (display
                                (format
                                 #f "###  ~a~a  ###~%"
                                 local-comment
                                 spacer-string))
                               )))
                         (begin
                           (display
                            (format
                             #f "###  ~a  ###~%"
                             local-comment))
                           ))
                     )))
               (begin
                 (let ((spacer-string
                        (make-string
                         (- var-length 10) #\space)))
                   (begin
                     (display
                      (format
                       #f "###  ~a  ###~%"
                       spacer-string))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-boxed-two-line-comment-string-macro
  (syntax-rules ()
    ((display-boxed-two-line-comment-string-macro
      separator-comment-string
      comment-line-1-string comment-line-2-string
      comment-length)
     (begin
       (let ((blank-string " "))
         (begin
           (display
            (format #f "~a~%" separator-comment-string))
           (display
            (format #f "~a~%" separator-comment-string))

           (display-single-line-comment-string-macro
            blank-string comment-length)

           (display-single-line-comment-string-macro
            comment-line-1-string comment-length)

           (display-single-line-comment-string-macro
            comment-line-2-string comment-length)

           (display-single-line-comment-string-macro
            blank-string comment-length)
           (display
            (format #f "~a~%" separator-comment-string))
           (display
            (format #f "~a~%" separator-comment-string))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-boxed-one-line-comment-string-macro
  (syntax-rules ()
    ((display-boxed-one-line-comment-string-macro
      separator-comment-string
      comment-line-string comment-length)
     (begin
       (let ((blank-string " "))
         (begin
           (display
            (format #f "~a~%" separator-comment-string))
           (display
            (format #f "~a~%" separator-comment-string))

           (display-single-line-comment-string-macro
            blank-string comment-length)

           (display-single-line-comment-string-macro
            comment-line-string comment-length)

           (display-single-line-comment-string-macro
            blank-string comment-length)

           (display
            (format #f "~a~%" separator-comment-string))
           (display
            (format #f "~a~%" separator-comment-string))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-01-define-directories-macro
  (syntax-rules ()
    ((display-01-define-directories-macro
      separator-comment-string
      comment-length
      program-name
      sources-dir
      includes-dir
      tests-dir
      test-includes-dir
      objects-dir)
     (begin
       (display-boxed-one-line-comment-string-macro
        separator-comment-string
        "define directories"
        comment-length)

       (display (format #f "PROGNAME=~a~%" program-name))
       (display (format #f "SOURCESDIR=~a~%" sources-dir))
       (display (format #f "INCLUDESDIR=~a~%" includes-dir))
       (display (format #f "TESTSDIR=~a~%" tests-dir))
       (display (format #f "TESTINCLUDESDIR=~a~%" test-includes-dir))
       (display (format #f "OBJECTSDIR=~a~%" objects-dir))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-02-cpp-flags-macro
  (syntax-rules ()
    ((display-02-cpp-flags-macro
      includes-dir test-includes-dir)
     (begin
       (display
        (format #f "OPTIFLAG = -O3~%"))
       (display
        (format #f "INCLUDE = -I. -I ~a/ -I~a~%"
                includes-dir test-includes-dir))
       (display
        (format #f "WARN1FLAGS = -Wall -Wextra -Weffc++ -Wpedantic~%"))
       (let ((tmp1 "-Wno-braced-scalar-init")
             (tmp2 "-Wno-unused-command-line-argument"))
         (begin
           (display
            (format #f "WARN2FLAGS = ~a ~a~%" tmp1 tmp2))
           ))
       (display
        (format #f "WARNFLAGS = $(WARN1FLAGS) $(WARN2FLAGS)~%"))
       (let ((tmp1 "-lgmp -lgmpxx -lrt")
             (tmp2 "-lpthread -lsqlite3"))
         (begin
           (display
            (format
             #f "LINKFLAGS = -L/usr/lib -lm ~a ~a~%"
             tmp1 tmp2))
           ))
       (display
        (format
         #f "CFLAGS = $(WARNFLAGS) -ansi -std=c++17 $(OPTIFLAG)~%"))
       (display
        (format
         #f "TESTCXXFLAGS = -lcppunit -g $(LINKFLAGS)~%"))
       (display
        (format
         #f "CXXFLAGS = $(LINKFLAGS) $(OPTFLAG)~%"))
       (display
        (format #f "CXX = clang++~%"))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-03-make-sources-list-macro
  (syntax-rules ()
    ((display-03-make-sources-list-macro
      sources-list includes-list)
     (begin
       (for-each
        (lambda (fname)
          (begin
            (let ((base-name (fname-to-basename fname)))
              (begin
                (if (not (equal? base-name #f))
                    (begin
                      (let ((hname
                             (return-header-file-name
                              base-name includes-list))
                            (top-line
                             (format
                              #f "$(OBJECTSDIR)/~a.o : $(SOURCESDIR)/~a"
                              base-name fname)))
                        (begin
                          (if (not (equal? hname #f))
                              (begin
                                (set!
                                 top-line
                                 (format #f "~a $(INCLUDESDIR)/~a"
                                         top-line hname))
                                ))
                          (display (format #f "~a~%" top-line))
                          (let ((string1
                                 (format #f "\t$(CXX) $(SOURCESDIR)/~a -c "
                                         fname))
                                (string2
                                 (format #f "-o $(OBJECTSDIR)/~a.o "
                                         base-name))
                                (string3
                                 "$(INCLUDE) $(CFLAGS) $(CXXFLAGS)"))
                            (let ((compile-string
                                   (string-append string1 string2 string3)))
                              (begin
                                (display
                                 (format #f "~a~%" compile-string))
                                (newline)
                                )))
                          ))
                      ))
                ))
            )) (sort sources-list string<?))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-04-make-tests-list-macro
  (syntax-rules ()
    ((display-04-make-tests-list-macro
      tests-list test-includes-list
      sources-list includes-list)
     (begin
       (for-each
        (lambda (fname)
          (begin
            (let ((base-name (fname-to-basename fname)))
              (begin
                (if (not (equal? base-name #f))
                    (begin
                      (let ((hname
                             (return-header-file-name
                              base-name test-includes-list))
                            (sname
                             (return-header-file-name
                              base-name sources-list))
                            (shname
                             (return-header-file-name
                              base-name includes-list))
                            (top-line
                             (format
                              #f "$(OBJECTSDIR)/~a.o : $(TESTSDIR)/~a"
                              base-name fname)))
                        (begin
                          (if (not (equal? hname #f))
                              (begin
                                (set!
                                 top-line
                                 (format
                                  #f "~a $(TESTINCLUDESDIR)/~a"
                                  top-line hname))
                                ))

                          (if (not (equal? sname #f))
                              (begin
                                (set!
                                 top-line
                                 (format #f "~a $(SOURCESDIR)/~a"
                                         top-line sname))
                                ))

                          (if (not (equal? sname #f))
                              (begin
                                (set!
                                 top-line
                                 (format #f "~a $(INCLUDESDIR)/~a"
                                         top-line shname))
                                ))
                          (display (format #f "~a~%" top-line))

                          (let ((string1
                                 (format
                                  #f "\t$(CXX) $(TESTSDIR)/~a -c "
                                  fname))
                                (string2
                                 (format #f "-o $(OBJECTSDIR)/~a.o "
                                         base-name))
                                (string3 "$(INCLUDE) $(CFLAGS) ")
                                (string4 "$(TESTCXXFLAGS)"))
                            (let ((compile-string
                                   (string-append
                                    string1 string2
                                    string3 string4)))
                              (begin
                                (display
                                 (format #f "~a~%" compile-string))
                                (newline)
                                )))
                          ))
                      ))
                ))
            )) (sort tests-list string<?))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-05-compile-dependencies-macro
  (syntax-rules ()
    ((display-05-compile-dependencies-macro
      objects-dir)
     (begin
       (display
        (format #f "$(OBJECTSDIR) :~%"))
       (display
        (format
         #f "\tif [ ! -d \"~a\" ] ; then mkdir $(OBJECTSDIR) ; fi~%"
         objects-dir))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-06-main-build-macro
  (syntax-rules ()
    ((display-06-main-build-macro
      main-name separator-echo-string)
     (begin
       (display
        (format
         #f "$(PROGNAME) : $(OBJECTSDIR)/~a.o $(MAINOBJECTS)~%"
         main-name))
       (display (format #f "\t@echo~%"))
       (display
        (format #f "\t@echo generating $(PROGNAME)~%"))

       (let ((string1
              (format #f "\t$(CXX) $(OBJECTSDIR)/~a.o "
                      main-name))
             (string2 "$(MAINOBJECTS) ")
             (string3 "-o $(PROGNAME) ")
             (string4 "$(CXXFLAGS) $(CFLAGS)"))
         (let ((compile-string
                (string-append string1 string2 string3 string4)))
           (begin
             (display
              (format #f "~a~%" compile-string))
             )))
       (display
        (format
         #f "\t@echo '~a'~%" separator-echo-string))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-07-test-build-macro
  (syntax-rules ()
    ((display-07-test-build-macro
      test-main-name separator-echo-string)
     (begin
       (let ((tmp-string "$(TESTOBJECTS) $(MAINOBJECTS)"))
         (begin
           (display
            (format #f "~a :  $(OBJECTSDIR)/~a.o ~a~%"
                    test-main-name test-main-name tmp-string))
           ))
       (display (format #f "\t@echo~%"))
       (display
        (format #f "\t@echo generating ~a~%" test-main-name))
       (let ((tmp-string "$(TESTOBJECTS) $(MAINOBJECTS)")
             (tmp2-string "$(TESTCXXFLAGS) $(CFLAGS)"))
         (begin
           (display
            (format
             #f "\t$(CXX) $(OBJECTSDIR)/~a.o -o ~a ~a ~a~%"
             test-main-name test-main-name tmp-string
             tmp2-string))
           ))
       (display
        (format
         #f "\t@echo '~a'~%" separator-echo-string))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-08-run-tests-macro
  (syntax-rules ()
    ((display-08-run-tests-macro
      test-main-name)
     (begin
       (display
        (format
         #f "run-tests : ~a~%" test-main-name))
       (display
        (format
         #f "\t-./lint-all.sh > out-lint.txt 2>&1~%"))
       (let ((out-name "out-test.txt"))
         (begin
           (display
            (format
             #f "\t@date +\"%A, %B %d, %Y  %r\" > ~a~%"
             out-name))
           (display
            (format
             #f "\t-./~a >> ~a 2>&1~%"
             test-main-name out-name))
           (display
            (format
             #f "\t@echo -n 'source files loc  ' >> ~a~%"
             out-name))
           (display
            (format
             #f "\t@wc -l sources/* | tail -n 1 >> ~a~%"
             out-name))
           (display
            (format
             #f "\t@echo -n 'test files loc  ' >> ~a~%"
             out-name))
           (display
            (format
             #f "\t@wc -l tests/* | tail -n 1 >> ~a~%"
             out-name))
           (display
            (format
             #f "\t@echo -n 'sources + tests loc  ' >> ~a~%"
             out-name))
           (display
            (format
             #f "\t@wc -l sources/* tests/* | tail -n 1 >> ~a~%"
             out-name))
           (display
            (format
             #f "\t@date +\"%A, %B %d, %Y  %r\" >> ~a~%"
             out-name))
           (display
            (format
             #f "\t-cat ~a~%" out-name))
           (newline)
           (newline)
           ))
       (force-output)
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-09-clean-procedure-macro
  (syntax-rules ()
    ((display-09-clean-procedure-macro program-name test-main-name)
     (begin
       (display (format #f ".PHONY : clean~%"))
       (display (format #f "clean:~%"))
       (display
        (format #f "\t-rm -fv $(OBJECTSDIR)/*.o~%"))
       (let ((string1 "$(SOURCESDIR)/*.cc~")
             (string2 "$(SOURCESDIR)/*.cpp~"))
         (begin
           (display
            (format #f "\t-rm -fv ~a ~a~%"
                    string1 string2))
           ))
       (display
        (format #f "\t-rm -fv $(INCLUDESDIR)/*.h~~~%"))
       (let ((string1 "$(TESTSDIR)/*.cc~")
             (string2 "$(TESTSDIR)/*.cpp~"))
         (begin
           (display
            (format #f "\t-rm -fv ~a ~a~%"
                    string1 string2))
           ))
       (display
        (format #f "\t-rm -fv $(TESTINCLUDESDIR)/*.h~~~%"))
       (display
        (format #f "\t-rm -fv ~a~%" program-name))
       (display
        (format #f "\t-rm -fv ~a~%" test-main-name))
       (display
        (format #f "\t-rm -fv *.scm~~ *.sh~~ Makefile~~~%"))
       (newline)
       ))
    ))

;;;#############################################################
;;;#############################################################
;;;### write out makefile
(define (display-makefile
         sources-dir includes-dir
         tests-dir test-includes-dir
         program-name objects-dir
         main-name test-main-name
         version-string)
  (begin
    (let ((sources-list (list-all-cc-files sources-dir sources-dir ".cpp$"))
          (includes-list (list-all-cc-files includes-dir includes-dir ".h$"))
          (tests-list (list-all-cc-files tests-dir tests-dir ".cpp$"))
          (test-includes-list (list-all-cc-files test-includes-dir
                                                 test-includes-dir ".h$"))
          (sources-deps-list (list))
          (tests-deps-list (list))
          (comment-length 63))
      (let ((separator-comment-string
             (make-string comment-length #\#))
            (separator-echo-string
             (make-string comment-length #\=))
            (blank-string " ")
            (make-title-string
             (format #f "make file (version ~a)"
                     version-string))
            (make-date-string
             (srfi-19:date->string
              (srfi-19:current-date) "~A, ~B ~d, ~Y")))
        (begin
          (display-boxed-two-line-comment-string-macro
           separator-comment-string
           make-title-string
           make-date-string
           comment-length)

          (newline)

          (display-01-define-directories-macro
           separator-comment-string
           comment-length
           program-name
           sources-dir
           includes-dir
           tests-dir
           test-includes-dir
           objects-dir)

          (newline)
          (newline)
          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "main definitions"
           comment-length)

          (display (format #f "all: ~a ~a run-tests~%"
                           objects-dir program-name))

          (newline)
          (display-02-cpp-flags-macro
           includes-dir test-includes-dir)
          (newline)

          (force-output)

          (newline)

          ;;; allow for .cc, .cpp, or .h files
          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "object dependencies"
           comment-length)

          (let ((start-line-string "MAINOBJECTS = ")
                (sslist (remove-main-objects-string main-name sources-list)))
            (let ((line-string (list-to-objects-string
                                start-line-string sslist)))
              (begin
                (display (format #f "~a~%" line-string))
                (force-output)
                )))

          (newline)
          (let ((start-line-string "TESTOBJECTS =")
                (sslist (remove-main-objects-string test-main-name tests-list)))
            (let ((line-string (list-to-objects-string
                                start-line-string sslist)))
              (begin
                (display (format #f "~a~%" line-string))
                (force-output)
                )))

          (newline)
          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "individual source file dependencies"
           comment-length)

          (display-03-make-sources-list-macro
           sources-list includes-list)

          (newline)
          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "individual test file dependencies"
           comment-length)
          (force-output)

          (display-04-make-tests-list-macro
           tests-list test-includes-list
           sources-list includes-list)

          (newline)
          (force-output)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "compile dependencies"
           comment-length)
          (force-output)
          (display-05-compile-dependencies-macro
           objects-dir)

          (newline)
          (force-output)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "main build"
           comment-length)

          (display-06-main-build-macro
           main-name separator-echo-string)

          (newline)
          (newline)
          (force-output)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "test build"
           comment-length)

          (display-07-test-build-macro
           test-main-name separator-echo-string)

          (newline)
          (newline)
          (force-output)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "run tests"
           comment-length)

          (display-08-run-tests-macro
           test-main-name)

          (newline)
          (newline)
          (force-output)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "clean up"
           comment-length)

          (display-09-clean-procedure-macro
           program-name test-main-name)

          (newline)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "end of file"
           comment-length)

          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax update-variable-macro
  (syntax-rules ()
    ((update-variable-macro
      var-name var-symbol options)
     (begin
       (let ((var-flag
              (ice-9-getopt:option-ref
               options var-symbol #f)))
         (begin
           (if (not (equal? var-flag #f))
               (begin
                 (let ((var2-flag
                        (ice-9-regex:regexp-substitute/global
                         #f "," var-flag 'pre "" 'post)))
                   (begin
                     (if (number?
                          (string->number var2-flag))
                         (begin
                           (set!
                            var-name
                            (string->number var2-flag)))
                         (begin
                           (set! var-name var-flag)
                           ))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax update-flag-macro
  (syntax-rules ()
    ((update-flag-macro
      var-name var-symbol options)
     (begin
       (if (string? var-name)
           (begin
             (if (string-ci=? var-name "true")
                 (begin
                   (set! var-name #t))
                 (begin
                   (set! var-name #f)
                   )))
           (begin
             (set! var-name #f)
             ))

       (let ((var-flag
              (ice-9-getopt:option-ref options var-symbol #f)))
         (begin
           (if (equal? var-flag #t)
               (begin
                 (set! var-name #t)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin main code                                      ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define (main args)
  (define (local-display-version title-string)
    (begin
      (display (format #f "~a~%" title-string))
      (force-output)
      ))
  (define (local-display-help ospec title-string)
    (begin
      (display (format #f "~a~%" title-string))
      (display (format #f "options available~%"))
      (for-each
       (lambda(llist)
         (display (format #f "  --~a, -~a~%" (car llist) (cadr (cadr llist)))))
       ospec)
      (force-output)
      ))
  (begin
    (let ((gen-make-version "2024-09-26")
          (makefile-name "Makefile"))
      (let ((program-name "spsr")
            (main-name "main")
            (test-main-name "testmain")
            (sources-dir "sources")
            (includes-dir "sources")
            (tests-dir "tests")
            (test-includes-dir "tests")
            (objects-dir "objects")
            (title-string
             (format #f "Generate Makefile (version ~a)"
                     gen-make-version))
            (option-spec
             (list (list 'version '(single-char #\v) '(value #f))
                   (list 'help '(single-char #\h) '(value #f))
                   (list 'sources '(single-char #\s) '(value #t))
                   (list 'includes '(single-char #\i) '(value #t))
                   (list 'tests '(single-char #\t) '(value #t))
                   (list 'test-includes '(single-char #\e) '(value #t))
                   (list 'objects '(single-char #\o) '(value #t))
                   )))
        (let ((options (ice-9-getopt:getopt-long args option-spec)))
          (begin
            ;;;### command line help option
            (let ((help-flag (ice-9-getopt:option-ref options 'help #f)))
              (begin
                (if (equal? help-flag #t)
                    (begin
                      (local-display-help option-spec title-string)
                      (quit)))
                ))
            ;;;### command line version option
            (let ((version-flag (ice-9-getopt:option-ref options 'version #f)))
              (begin
                (if (equal? version-flag #t)
                    (begin
                      (local-display-version title-string)
                      (quit)
                      ))
                ))

            ;;;### command line sources option
            (update-variable-macro
             sources-dir 'sources options)

            (update-variable-macro
             includes-dir 'includes options)

            (update-variable-macro
             tests-dir 'tests options)

            (update-variable-macro
             tests-includes-dir 'test-includes options)

            (update-variable-macro
             objects-dir 'objects options)

            (display
             (format
              #f "program name = ~a, test name = ~a~%"
              program-name test-main-name))
            (display
             (format
              #f "sources = ~a, includes = ~a~%"
              sources-dir includes-dir))
            (display
             (format
              #f "tests = ~a, test-includes = ~a, objects = ~a~%"
              tests-dir test-includes-dir objects-dir))
            (force-output)

            ;;;### once we have the input parameters, display makefile
            (with-output-to-file
                makefile-name
              (lambda ()
                (begin
                  (display-makefile
                   sources-dir includes-dir
                   tests-dir test-includes-dir
                   program-name objects-dir
                   main-name test-main-name
                   gen-make-version)
                  )))

            (let ((dstring
                   (string-downcase
                    (srfi-19:date->string
                     (srfi-19:current-date)
                     "~A, ~B ~d, ~Y  ~I:~M:~S ~p"))))
              (begin
                (display (format #f "output to ~s  :  ~a~%"
                                 makefile-name dstring))
                (force-output)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
