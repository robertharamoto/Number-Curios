/*

// Copyright 2024 robert haramoto

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  utils tests                                           ###
  ###                                                        ###
  ###  last updated August 20, 2024                          ###
  ###                                                        ###
  ###  updated July 16, 2022                                 ###
  ###                                                        ###
  ###  updated February 5, 2020                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/utils_test.h"

#include <cstdint>

#include <string>
#include <vector>
#include <set>
#include <map>

#include "sources/utils.h"

namespace test_mylib {

CPPUNIT_TEST_SUITE_REGISTRATION(TUtilsTest);

// #############################################################
// #############################################################
TUtilsTest::TUtilsTest() {
}

// #############################################################
// #############################################################
TUtilsTest::~TUtilsTest() {
}

// #############################################################
// #############################################################
void TUtilsTest::setUp() {
}

// #############################################################
// #############################################################
void TUtilsTest::tearDown() {
}

// #############################################################
// #############################################################
void TUtilsTest::TestCommafyNum1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    int64_t anum;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { 10L, "10" },
    { 1000L, "1,000" },
    { 10000L, "10,000" },
    { 100000L, "100,000" },
    { 1000000L, "1,000,000" },
    { 10000000L, "10,000,000" },
    { 100000000L, "100,000,000" },
    { 1000000000L, "1,000,000,000" }
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_int;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestCommafyNum1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_int = va_cit->anum;
    shouldbe_string = va_cit->shouldbe;

    result_string = utils_obj.CommafyNum(test_int);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestCommafyMpzNum1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    int64_t anum;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { 10L, "10" },
    { 1000L, "1,000" },
    { 10000L, "10,000" },
    { 100000L, "100,000" },
    { 1000000L, "1,000,000" },
    { 10000000L, "10,000,000" },
    { 100000000L, "100,000,000" },
    { 1000000000L, "1,000,000,000" }
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestCommafyMpzNum1:") };
  std::string error_message;
  mpz_class bn_num;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    bn_num = va_cit->anum;
    shouldbe_string = va_cit->shouldbe;

    result_string = utils_obj.CommafyMpzNum(&bn_num);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestMpzNumToSciString1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    mpz_class bnum;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { 10L, "1.0000e+01" },
    { 1000L, "1.0000e+03" },
    { 1001L, "1.0010e+03" },
    { 1010L, "1.0100e+03" },
    { 10000L, "1.0000e+04" },
    { 100000L, "1.0000e+05" },
    { 1000000L, "1.0000e+06" },
    { 10000000L, "1.0000e+07" },
    { 100000000L, "1.0000e+08" },
    { 1000000000L, "1.0000e+09" },
    { 10000000000L, "1.0000e+10" },
    { 100000000000L, "1.0000e+11" },
    { 1000000000000L, "1.0000e+12" },
    { 10000000000000L, "1.0000e+13" },
    { 100000000000000L, "1.0000e+14" },
    { 1000000000000000L, "1.0000e+15" },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  mpz_class test_bn;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestMpzNumToSciString1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_bn = va_cit->bnum;
    shouldbe_string = va_cit->shouldbe;

    result_string = utils_obj.MpzNumToSciString(&test_bn);

    error_message +=
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestMpzNumToNiceSciString1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    mpz_class bnum;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { 10L, "10" },
    { 1000L, "1 x 10^3" },
    { 1001L, "1,001" },
    { 1010L, "1,010" },
    { 1100L, "11 x 10^2" },
    { 10000L, "1 x 10^4" },
    { 100000L, "1 x 10^5" },
    { 1000000L, "1 x 10^6" },
    { 12000000L, "12 x 10^6" },
    { 123000000L, "123 x 10^6" },
    { 1234000000L, "1,234 x 10^6" },
    { 12345000000L, "12,345 x 10^6" },
    { 10000000L, "1 x 10^7" },
    { 100000000L, "1 x 10^8" },
    { 1000000000L, "1 x 10^9" },
    { 10000000000L, "1 x 10^10" },
    { 100000000000L, "1 x 10^11" },
    { 1000000000000L, "1 x 10^12" },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  mpz_class test_bn;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestMpzNumToNiceSciString1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_bn = va_cit->bnum;
    shouldbe_string = va_cit->shouldbe;

    result_string =
        utils_obj.MpzNumToNiceSciString(&test_bn);

    error_message +=
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestRoundFloat1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    double dnum, ndecimals;
    double shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { 1.1, 0.0, 1.0 }, { 1.2, 0.0, 1.0 },
    { 2.3, 0.0, 2.0 }, { 3.4, 0.0, 3.0 },
    { 3.50, 0.0, 4.0 }, { 4.51, 0.0, 5.0 },
    { 1.11, 1.0, 1.10 }, { 1.22, 1.0, 1.20 },
    { 2.34, 1.0, 2.30 }, { 3.449, 1.0, 3.40 },
    { 3.450, 1.0, 3.50 }, { 4.460, 1.0, 4.50 },
    { 1.111, 2.0, 1.110 }, { 1.222, 2.0, 1.220 },
    { 2.444, 2.0, 2.440 }, { 3.4449, 2.0, 3.440 },
    { 3.4450, 2.0, 3.450 }, { 4.556, 2.0, 4.560 },
    { 1.1114, 3.0, 1.1110 }, { 1.2224, 3.0, 1.2220 },
    { 2.33349, 3.0, 2.3330 }, { 3.44449, 3.0, 3.4440 },
    { 4.44450, 3.0, 4.4450 }, { 5.5556, 3.0, 5.5560 },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  double test_double, decimals;
  double shouldbe_double, result_double;
  double tolerance = 1.0e-12;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestRoundFloat1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_double = va_cit->dnum;
    decimals = va_cit->ndecimals;
    shouldbe_double = va_cit->shouldbe;

    result_double =
        utils_obj.RoundFloat(test_double, decimals);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + std::to_string(shouldbe_double)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_double)
        + static_cast<std::string>(", decimals = ")
        + std::to_string(decimals);

    CPPUNIT_ASSERT_MESSAGE(
        error_message,
        (fabs(shouldbe_double - result_double) < tolerance));

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestRoundFloatM1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    double dnum, ndecimals;
    double shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { -1.1, 0.0, -1.0 }, { -1.2, 0.0, -1.0 },
    { -2.3, 0.0, -2.0 }, { -3.4, 0.0, -3.0 },
    { -3.50, 0.0, -4.0 }, { -4.51, 0.0, -5.0 },
    { -1.11, 1.0, -1.10 }, { -1.22, 1.0, -1.20 },
    { -2.34, 1.0, -2.30 }, { -3.449, 1.0, -3.40 },
    { -3.450, 1.0, -3.50 }, { -4.460, 1.0, -4.50 },
    { -1.111, 2.0, -1.110 }, { -1.222, 2.0, -1.220 },
    { -2.444, 2.0, -2.440 }, { -3.4449, 2.0, -3.440 },
    { -3.4450, 2.0, -3.450 }, { -4.556, 2.0, -4.560 },
    { -1.1114, 3.0, -1.1110 }, { -1.2224, 3.0, -1.2220 },
    { -2.33349, 3.0, -2.3330 }, { -3.44449, 3.0, -3.4440 },
    { -4.44450, 3.0, -4.4450 }, { -5.5556, 3.0, -5.5560 },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  double test_double, decimals;
  double shouldbe_double, result_double;
  double tolerance = 1.0e-12;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestRoundFloatM1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_double = va_cit->dnum;
    decimals = va_cit->ndecimals;
    shouldbe_double = va_cit->shouldbe;

    result_double =
        utils_obj.RoundFloat(test_double, decimals);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + std::to_string(shouldbe_double)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_double)
        + static_cast<std::string>(", decimals = ")
        + std::to_string(decimals);

    CPPUNIT_ASSERT_MESSAGE(
        error_message,
        (fabs(shouldbe_double - result_double) < tolerance));

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestDoubleToString1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    double dnum, ndecimals;
    std::string shouldbe_string;
  };
  std::vector<atest_struct> test_vector = {
    { 1.1, 0.0, "1" }, { 1.2, 0.0, "1" },
    { 2.3, 0.0, "2" }, { 3.4, 0.0, "3" },
    { 3.50, 0.0, "4" }, { 4.51, 0.0, "5" },
    { 1.11, 1.0, "1.1" }, { 1.22, 1.0, "1.2" },
    { 2.34, 1.0, "2.3" }, { 3.449, 1.0, "3.4" },
    { 3.450, 1.0, "3.5" }, { 4.460, 1.0, "4.5" },
    { 1.111, 2.0, "1.11" }, { 1.222, 2.0, "1.22" },
    { 2.444, 2.0, "2.44" }, { 3.4449, 2.0, "3.44" },
    { 3.4450, 2.0, "3.45" }, { 4.556, 2.0, "4.56" },
    { 1.1114, 3.0, "1.111" }, { 1.2224, 3.0, "1.222" },
    { 2.33349, 3.0, "2.333" }, { 3.44449, 3.0, "3.444" },
    { 4.44450, 3.0, "4.445" }, { 5.5556, 3.0, "5.556" },
    { -1.11, 1.0, "-1.1" }, { -1.22, 1.0, "-1.2" },
    { -10.11, 1.0, "-10.1" }, { -12.22, 1.0, "-12.2" },
    { 12345.0, 2.0, "12,345.00" },
    { 12345.123, 2.0, "12,345.12" },
    { 12345678.123, 2.0, "12,345,678.12" },
    { -12345.0, 2.0, "-12,345.00" },
    { -12345.123, 2.0, "-12,345.12" },
    { -12345678.123, 2.0, "-12,345,678.12" },
    { 12345678912.0, 2.0, "1.23457e+10" },
    { 123456789123.123, 2.0, "1.23457e+11" },
    { -12345678912.0, 2.0, "-1.23457e+10" },
    { -123456789123.123, 2.0, "-1.23457e+11" },
    { 1.234e-10, 2.0, "1.234e-10" },
    { -1.234e-10, 2.0, "-1.234e-10" },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  double test_double, decimals;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestDoubleToString1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_double = va_cit->dnum;
    decimals = va_cit->ndecimals;
    shouldbe_string = va_cit->shouldbe_string;

    result_string =
        utils_obj.DoubleToString(test_double, decimals);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>(")")
        + static_cast<std::string>(", test_double = ")
        + std::to_string(test_double)
        + static_cast<std::string>(", decimals = ")
        + std::to_string(decimals)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestLowerCase1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::string tstring, shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { "String 1", "string 1" },
    { "STRING 2  ", "string 2  " },
    { "    strING   3", "    string   3" },
    { "string 4", "string 4" },
    { "", "" }
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::string test_string, shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestLowerCase1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_string = va_cit->tstring;
    shouldbe_string = va_cit->shouldbe;

    result_string = utils_obj.LowerCase(&test_string);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), test string = ")
        + test_string
        + static_cast<std::string>(", shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestStringTrim1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::string tstring, shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { "  string 1 ", "string 1" },
    { "  string 2  ", "string 2" },
    { "    string   3", "string   3" },
    { "string 4", "string 4" },
    { "", "" }
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::string test_string, shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestStringTrim1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_string = va_cit->tstring;
    shouldbe_string = va_cit->shouldbe;

    result_string = utils_obj.StringTrim(&test_string);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), test string = ")
        + test_string
        + static_cast<std::string>(", shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestStringRemoveComment1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::string tstring, shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { "  string 1 ;;; string comment ", "  string 1 " },
    { "  ;;;string 2  ", "  " },
    { "string   3 ;;;", "string   3 " },
    { "string 4", "string 4" },
    { "", "" }
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::string test_string, shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestStringRemoveComment1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_string = va_cit->tstring;
    shouldbe_string = va_cit->shouldbe;

    result_string =
        utils_obj.StringRemoveComment(&test_string, ';');

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), test string = ")
        + test_string
        + static_cast<std::string>(", shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestStringTokenize1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { "string" }, { "1" }, { ";;" }, { "string" },
    { "comment" }
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::string shouldbe_string, result_string;
  std::string test_string = "  string 1 ;; string comment ";
  std::vector<std::string> result_vector;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestStringTokenize1:") };
  std::string error_message;

  utils_obj.StringTokenize(&test_string, ' ', &result_vector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    shouldbe_string = va_cit->shouldbe;

    result_string = result_vector[test_label_index];

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestStringTokenize2() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { "  string 1 " }, { " string comment " }
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::string shouldbe_string, result_string;
  std::string test_string =
      static_cast<std::string>("  string 1 ;; string comment ");
  std::vector<std::string> result_vector;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestStringTokenize2:") };
  std::string error_message;

  utils_obj.StringTokenize(&test_string, ';', &result_vector);

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    shouldbe_string = va_cit->shouldbe;

    result_string = result_vector[test_label_index];

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestStringRemoveCommas1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::string tstring, shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { "  10,000 ", "  10000 " },
    { "10,000,000", "10000000" },
    { "123,456,789,012", "123456789012" },
    { "123", "123" },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::string test_string, shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestStringRemoveCommas1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_string = va_cit->tstring;
    shouldbe_string = va_cit->shouldbe;

    result_string = utils_obj.StringRemoveCommas(&test_string);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), test string = ")
        + test_string
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestStringToInteger1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::string tstring;
    int64_t shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { "10000", 10000L },
    { "10,000,000", 10000000L },
    { "123,456", 123456L },
    { "123", 123L },
    { "1000000000", 1000000000L },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::string test_string;
  int64_t result_long, shouldbe_long;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestStringToInteger1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_string = va_cit->tstring;
    shouldbe_long = va_cit->shouldbe;

    result_long = utils_obj.StringToInteger(&test_string);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), test string = ")
        + test_string
        + static_cast<std::string>(", shouldbe = ")
        + std::to_string(shouldbe_long)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_long);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_long, result_long);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestStringToMpzClass1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::string tstring;
    int64_t shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { "10000", 10000L },
    { "10000000", 10000000L },
    { "123,456", 123456L },
    { "123", 123L },
    { "1000000000", 1000000000L },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::string test_string;
  mpz_class bn_result, bn_shouldbe;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestStringToMpzClass1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_string = va_cit->tstring;
    bn_shouldbe = va_cit->shouldbe;

    bn_result = utils_obj.StringToMpzClass(&test_string);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), test string = ")
        + test_string
        + static_cast<std::string>(", shouldbe = ")
        + utils_obj.CommafyMpzNum(&bn_shouldbe)
        + static_cast<std::string>(", result = ")
        + utils_obj.CommafyMpzNum(&bn_result);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, bn_shouldbe, bn_result);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestIntVectorToString1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::vector<int> input_vector;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { 1, 2, 3, 4, 5 } ,
      "[1 ; 2 ; 3 ; 4 ; 5]"
    },
    { { 100, 200, 300, 400, 500 },
      "[100 ; 200 ; 300 ; 400 ; 500]"
    },
    { { 1000, 2000, 3000, 4000, 5000 } ,
      "[1,000 ; 2,000 ; 3,000 ; 4,000 ; 5,000]"
    },
    { { 10000, 20000, 30000, 40000, 50000 } ,
      "[10,000 ; 20,000 ; 30,000 ; 40,000 ; 50,000]"
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::vector<int> input_vector;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestIntVectorToString1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->input_vector;
    shouldbe_string = va_cit->shouldbe;

    result_string =
        utils_obj.IntVectorToString(&input_vector);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestInt64VectorToString1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::vector<int64_t> input_vector;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { 1, 2, 3, 4, 5 } ,
      "[1 ; 2 ; 3 ; 4 ; 5]"
    },
    { { 100, 200, 300, 400, 500 },
      "[100 ; 200 ; 300 ; 400 ; 500]"
    },
    { { 1000, 2000, 3000, 4000, 5000 } ,
      "[1,000 ; 2,000 ; 3,000 ; 4,000 ; 5,000]"
    },
    { { 10000, 20000, 30000, 40000, 50000 } ,
      "[10,000 ; 20,000 ; 30,000 ; 40,000 ; 50,000]"
    },
    { { 1000000, 2000000, 3000000, 4000000, } ,
      "[1,000,000 ; 2,000,000 ; 3,000,000 ; 4,000,000]"
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::vector<int64_t> input_vector;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestInt64VectorToString1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->input_vector;
    shouldbe_string = va_cit->shouldbe;

    result_string =
        utils_obj.Int64VectorToString(&input_vector);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestInt64SequenceVectorToString1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::vector<int64_t> input_vector;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { 1, 2, 3, 4, 5 } , "[1 -> 2 -> 3 -> 4 -> 5]" },
    { { 100, 200, 300, 400, 500 },
      "[100 -> 200 -> 300 -> 400 -> 500]" },
    { { 1000, 2000, 3000, 4000, 5000 } ,
      "[1,000 -> 2,000 -> 3,000 -> 4,000 -> 5,000]" },
    { { 10000, 20000, 30000, 40000, 50000 } ,
      "[10,000 -> 20,000 -> 30,000 -> 40,000 -> 50,000]"
    },
    { { 1000000, 2000000, 3000000 } ,
      "[1,000,000 -> 2,000,000 -> 3,000,000]"
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::vector<int64_t> input_vector;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestInt64Sequence")
    + static_cast<std::string>("VectorToString1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->input_vector;
    shouldbe_string = va_cit->shouldbe;

    result_string =
        utils_obj.Int64SequenceVectorToString(&input_vector);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestMpzVectorToString1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::vector<mpz_class> bn_vector;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { 1, 2, 3, 4, 5 } ,
      "[1 ; 2 ; 3 ; 4 ; 5]"
    },
    { { 100, 200, 300, 400, 500 },
      "[100 ; 200 ; 300 ; 400 ; 500]"
    },
    { { 1000, 2000, 3000, 4000, 5000 } ,
      "[1,000 ; 2,000 ; 3,000 ; 4,000 ; 5,000]"
    },
    { { 10000, 20000, 30000, 40000, 50000 } ,
      "[10,000 ; 20,000 ; 30,000 ; 40,000 ; 50,000]"
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::vector<mpz_class> input_vector;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestMpzVectorToString1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->bn_vector;
    shouldbe_string = va_cit->shouldbe;

    result_string = utils_obj.MpzVectorToString(&input_vector);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestMpzSequenceVectorToString1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::vector<mpz_class> bn_vector;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { 1, 2, 3, 4, 5 } ,
      "[1 -> 2 -> 3 -> 4 -> 5]"
    },
    { { 100, 200, 300, 400, 500 },
      "[100 -> 200 -> 300 -> 400 -> 500]"
    },
    { { 1000, 2000, 3000, 4000, 5000 } ,
      "[1,000 -> 2,000 -> 3,000 -> 4,000 -> 5,000]"
    },
    { { 10000, 20000, 30000, 40000, 50000 } ,
      "[10,000 -> 20,000 -> 30,000 -> 40,000 -> 50,000]"
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::vector<mpz_class> input_vector;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestMpzSequence")
    + static_cast<std::string>("VectorToString1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->bn_vector;
    shouldbe_string = va_cit->shouldbe;

    result_string =
        utils_obj.MpzSequenceVectorToString(&input_vector);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestInt64SetToString1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::set<int64_t> input_set;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { 1, 2, 3, 4, 5 } ,
      "{1 ; 2 ; 3 ; 4 ; 5}"
    },
    { { 100, 200, 300, 400, 500 },
      "{100 ; 200 ; 300 ; 400 ; 500}"
    },
    { { 1000, 2000, 3000, 4000, 5000 },
      "{1,000 ; 2,000 ; 3,000 ; 4,000 ; 5,000}"
    },
    { { 10000, 20000, 30000, 40000, 50000 },
      "{10,000 ; 20,000 ; 30,000 ; 40,000 ; 50,000}"
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::set<int64_t> input_set;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestInt64SetToString1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_set = va_cit->input_set;
    shouldbe_string = va_cit->shouldbe;

    result_string = utils_obj.Int64SetToString(&input_set);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestMpzSetToString1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::set<mpz_class> bn_set;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { 1, 2, 3, 4, 5 } ,
      "{1 ; 2 ; 3 ; 4 ; 5}"
    },
    { { 100, 200, 300, 400, 500 },
      "{100 ; 200 ; 300 ; 400 ; 500}"
    },
    { { 1000, 2000, 3000, 4000, 5000 },
      "{1,000 ; 2,000 ; 3,000 ; 4,000 ; 5,000}"
    },
    { { 10000, 20000, 30000, 40000, 50000 },
      "{10,000 ; 20,000 ; 30,000 ; 40,000 ; 50,000}"
    },
    { { 1000000, 2000000, 3000000 },
      "{1,000,000 ; 2,000,000 ; 3,000,000}"
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::set<mpz_class> input_set;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestMpzSetToString1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_set = va_cit->bn_set;
    shouldbe_string = va_cit->shouldbe;

    result_string = utils_obj.MpzSetToString(&input_set);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestMapInt64Int64ToString1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::map<int64_t, int64_t> input_map;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { { 1L, 2L } },
      "{ (1 ; 2) }"
    },
    { { { 1L, 2L }, { 2L, 3L } },
      "{ (1 ; 2), (2 ; 3) }"
    },
    { { { 1L, 2L }, { 2L, 3L }, { 3L, 5L } },
      "{ (1 ; 2), (2 ; 3), (3 ; 5) }"
    },
    { { { 1L, 2L }, { 2L, 3L }, { 3L, 5L },
        { 4L, 7L } },
      "{ (1 ; 2), (2 ; 3), (3 ; 5), (4 ; 7) }"
    },
    { { { 1L, 2L }, { 2L, 3L }, { 3L, 5L },
        { 4L, 7L }, { 5L, 11L } },
      "{ (1 ; 2), (2 ; 3), (3 ; 5), (4 ; 7), (5 ; 11) }"
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::map<int64_t, int64_t> input_map;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestMapInt64Int64ToString1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_map = va_cit->input_map;
    shouldbe_string = va_cit->shouldbe;

    result_string = utils_obj.MapInt64Int64ToString(&input_map);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestMapInt64BoolToString1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::map<int64_t, bool> input_map;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { { 1L, false }
      },
      "{ (1 ; false) }"
    },
    { { { 1L, false }, { 2L, true }
      },
      "{ (1 ; false), (2 ; true) }"
    },
    { { { 1L, false }, { 2L, true }, { 3L, true }
      },
      "{ (1 ; false), (2 ; true), (3 ; true) }"
    },
    { { { 1L, false }, { 2L, true }, { 3L, true },
        { 4L, false }
      },
      "{ (1 ; false), (2 ; true), (3 ; true), (4 ; false) }"
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::map<int64_t, bool> input_map;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestMapInt64BoolToString1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_map = va_cit->input_map;
    shouldbe_string = va_cit->shouldbe;

    result_string = utils_obj.MapInt64BoolToString(&input_map);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestMapMpzBoolToString1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::map<mpz_class, bool> input_map;
    std::string shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { { { 1L, false }
      },
      "{ (1 ; false) }"
    },
    { { { 1L, false }, { 2L, true }
      },
      "{ (1 ; false), (2 ; true) }"
    },
    { { { 1L, false }, { 2L, true }, { 3L, true }
      },
      "{ (1 ; false), (2 ; true), (3 ; true) }"
    },
    { { { 1L, false }, { 2L, true }, { 3L, true },
        { 4L, false }
      },
      "{ (1 ; false), (2 ; true), (3 ; true), (4 ; false) }"
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::map<mpz_class, bool> input_map;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestMapMpzBoolToString1:") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_map = va_cit->input_map;
    shouldbe_string = va_cit->shouldbe;

    result_string = utils_obj.MapMpzBoolToString(&input_map);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe = ")
        + shouldbe_string
        + static_cast<std::string>(", result = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestVector1EqualsVector2() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::vector<int64_t> ivec1, ivec2;
    bool shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { {1L},
      {1L},
      true,
    },
    { { 2L },
      { 2L },
      true,
    },
    { { 3L, 4L },
      { 3L, 4L},
      true,
    },
    { { 3L, 4L },
      { 2L, 4L},
      false,
    },
    { { 5L, 6L, 7L },
      { 5L, 6L, 7L },
      true,
    },
    { { 5L, 6L, 7L },
      { 5L, 6L, 8L },
      false,
    },
    { { 10L, 11L, 12L, 13L },
      { 10L, 11L, 12L, 13L },
      true,
    },
    { { 5L, 6L, 7L, 8L },
      { 5L, 6L, 8L, 9L },
      false,
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  bool bool_shouldbe, bool_result;
  std::vector<int64_t> ivec1, ivec2;
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestVector1EqualsVector2:") };
  std::string error_message, shouldbe_string, result_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    bool_shouldbe = va_cit->shouldbe;
    ivec1 = va_cit->ivec1;
    ivec2 = va_cit->ivec2;

    bool_result =
        utils_obj.Vector1EqualsVector2(&ivec1, &ivec2);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), vector 1 = ")
        + utils_obj.Int64VectorToString(&ivec1)
        + static_cast<std::string>(", vector 2 = ")
        + utils_obj.Int64VectorToString(&ivec2)
        + static_cast<std::string>(", shouldbe = ")
        + static_cast<std::string>(
            (bool_shouldbe ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (bool_result ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, bool_shouldbe, bool_result);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TUtilsTest::TestClockDurationInSeconds1() {
  mylib::CUtils utils_obj;
  struct atest_struct {
    double init_seconds;
    double shouldbe_seconds;
  };
  std::vector<atest_struct> test_vector = {
    { 10.0, 10.0 },
    { 100.0, 100.0 },
    { 200.0, 200.0 },
    { 300.0, 300.0 },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  double test_duration, shouldbe_duration, result_duration;
  std::chrono::time_point<std::chrono::steady_clock,
                          std::chrono::duration<double>>
      start_time, end_time;
  std::chrono::duration<double> dtn;
  double dtol { 1.0e-10 };
  std::string sub_name = {
    static_cast<std::string>("TUtilsTest::")
    + static_cast<std::string>("TestClockDurationInSeconds1:") };
  std::string error_message, shouldbe_string, result_string;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_duration = va_cit->init_seconds;
    shouldbe_duration = va_cit->shouldbe_seconds;

    dtn = std::chrono::duration<double>(test_duration);
    start_time = std::chrono::steady_clock::now();
    end_time = start_time;
    end_time += dtn;

    result_duration = utils_obj.ClockDurationInSeconds(
        &end_time, &start_time);

    error_message =
        sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), test duration = ")
        + std::to_string(test_duration)
        + static_cast<std::string>(", shouldbe = ")
        + std::to_string(shouldbe_duration)
        + static_cast<std::string>(", result = ")
        + std::to_string(result_duration);

    CPPUNIT_ASSERT_MESSAGE(
        error_message,
        (fabs(shouldbe_duration - result_duration) < dtol));

    ++test_label_index;
  }

  return;
}

}  // namespace test_mylib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/
