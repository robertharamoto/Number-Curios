/*

// Copyright 2024 <robert haramoto>

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  configuration file test class implementation          ###
  ###                                                        ###
  ###  last updated September 21, 2024                       ###
  ###                                                        ###
  ###  last updated March 3, 2024                            ###
  ###    fixed updated cpplint.py errors                     ###
  ###                                                        ###
  ###  updated July 16, 2022                                 ###
  ###                                                        ###
  ###  updated February 5, 2020                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/configure_test.h"

#include <string>
#include <vector>
#include <map>

#include "sources/configure.h"
#include "sources/utils.h"

namespace test_mylib {

CPPUNIT_TEST_SUITE_REGISTRATION(TConfigureTest);

// #############################################################
// #############################################################
TConfigureTest::TConfigureTest() {
}

// #############################################################
// #############################################################
TConfigureTest::~TConfigureTest() {
}

// #############################################################
// #############################################################
void TConfigureTest::setUp() {
}

// #############################################################
// #############################################################
void TConfigureTest::tearDown() {
}

// #############################################################
// #############################################################
void TConfigureTest::TestParseLine1() {
    struct atest_struct {
    std::string tstring, sname, svalue, scomment;
    };
    std::vector<atest_struct> test_vector = {
        { ";;;### comment", "", "", "",
        },
        { "", "", "", "",
        },
        { "   ", "", "", "",
        },
        { "start-num=1,000  ;;; another comment",
          "start-num", "1,000", "another comment",
        },
        { "start-num = 1,000  ;;; another comment",
          "start-num", "1,000", "another comment",
        },
        { "start-num = 1,000  ;;;",
          "start-num", "1,000", "",
        },
        { "end-num=12,345,678   ;;; comment",
          "end-num", "12,345,678", "comment",
        },
        { "outfile=tmp123.txt  ;;; ;;; comments",
          "outfile", "tmp123.txt", "comments",
        },
        { "outfile=tmp123.txt  ; ; ; ; ; ; comments",
          "outfile", "tmp123.txt", "comments",
        }
    };
    std::vector<atest_struct>::const_iterator va_cit;
    int64_t test_label_index;
    std::string test_string,
        shouldbe_sname_string,
        shouldbe_svalue_string,
        shouldbe_scomment_string;
    std::string result_sname_string,
        result_svalue_string,
        result_scomment_string;
    mylib::CConfigure config_obj;
    std::string sub_name = {
        static_cast<std::string>("TConfigureTest::")
        + static_cast<std::string>("TestParseLine1:") };
    std::string error_message, err_start;

    test_label_index = 0L;
    for (va_cit = test_vector.cbegin();
         va_cit != test_vector.cend(); ++va_cit) {
        test_string = va_cit->tstring;
        shouldbe_sname_string = va_cit->sname;
        shouldbe_svalue_string = va_cit->svalue;
        shouldbe_scomment_string = va_cit->scomment;

        config_obj.ParseLine(
            &test_string, &result_sname_string,
            &result_svalue_string, &result_scomment_string);

        err_start =
            sub_name
            + static_cast<std::string>(" (")
            + std::to_string(test_label_index)
            + static_cast<std::string>(") test string = ")
            + test_string
            + static_cast<std::string>(" : ");

        error_message =
            err_start
            + static_cast<std::string>("shouldbe name = ")
            + shouldbe_sname_string
            + static_cast<std::string>(", result = ")
            + result_sname_string;

        CPPUNIT_ASSERT_EQUAL_MESSAGE(
            error_message, shouldbe_sname_string,
            result_sname_string);

        error_message =
            err_start
            + static_cast<std::string>("shouldbe value = ")
            + shouldbe_svalue_string
            + static_cast<std::string>(", result = ")
            + result_svalue_string;

        CPPUNIT_ASSERT_EQUAL_MESSAGE(
            error_message, shouldbe_svalue_string,
            result_svalue_string);

        error_message =
            err_start
            + static_cast<std::string>("shouldbe comment = ")
            + shouldbe_scomment_string
            + static_cast<std::string>(", result = ")
            + result_scomment_string;

        CPPUNIT_ASSERT_EQUAL_MESSAGE(
            error_message, shouldbe_scomment_string,
            result_scomment_string);

        ++test_label_index;
    }

    return;
}

/*
  ##############################################################
  ##############################################################
*/
void TConfigureTest::TestReadConfig1() {
    struct atest_struct {
    std::string sname, svalue, scomment;
    };
    std::vector<atest_struct> test_vector = {
        { "start-num", "11", "" },
        { "end-num", "100,000,000", "" },
        { "bin-num", "1,000,000", "" },
        { "out-file", "test1.txt", "" },
        { "restart-file", "restart1.config", "" },
    };
    std::vector<atest_struct>::const_iterator va_cit;
    int64_t test_label_index;
    std::map<std::string, std::string>
        input_string_map,
        input_comment_map;
    std::string shouldbe_sname_string,
        shouldbe_svalue_string,
        shouldbe_scomment_string;
    std::string result_svalue_string,
        result_scomment_string;
    std::string sub_name = {
        static_cast<std::string>("TConfigureTest::")
        + static_cast<std::string>("TestReadConfig1:") };
    std::string err_1, error_message;
    mylib::CConfigure config_obj;
    std::string filename("test1.config");

    config_obj.ReadConfig(&filename,
                          &input_string_map,
                          &input_comment_map);

    test_label_index = 0L;
    for (va_cit = test_vector.cbegin();
         va_cit != test_vector.cend(); ++va_cit) {
        shouldbe_sname_string = va_cit->sname;
        shouldbe_svalue_string = va_cit->svalue;
        shouldbe_scomment_string = va_cit->scomment;

        result_svalue_string =
            input_string_map[shouldbe_sname_string];
        result_scomment_string =
            input_comment_map[shouldbe_sname_string];

        err_1 =
            sub_name
            + static_cast<std::string>(" (")
            + std::to_string(test_label_index)
            + static_cast<std::string>("), ");

        error_message =
            err_1
            + shouldbe_sname_string
            + static_cast<std::string>(", shouldbe value = ")
            + shouldbe_svalue_string
            + static_cast<std::string>(", result = ")
            + result_svalue_string;

        CPPUNIT_ASSERT_EQUAL_MESSAGE(
            error_message, shouldbe_svalue_string,
            result_svalue_string);

        error_message =
            err_1
            + shouldbe_sname_string
            + static_cast<std::string>(", shouldbe comment = ")
            + shouldbe_scomment_string
            + static_cast<std::string>(", result = ")
            + result_scomment_string;

        CPPUNIT_ASSERT_EQUAL_MESSAGE(
            error_message, shouldbe_scomment_string,
            result_scomment_string);

        ++test_label_index;
    }

    return;
}

// #############################################################
// #############################################################
// test the default values
void TConfigureTest::TestReadConfig2() {
    struct atest_struct {
    std::string sname, svalue, scomment;
    };
    std::vector<atest_struct> test_vector = {
        { "out-file", "out2222.txt", "" }
    };
    std::vector<atest_struct>::const_iterator va_cit;
    int64_t test_label_index;
    std::map<std::string, std::string>
        input_string_map,
        input_comment_map;
    std::string shouldbe_sname_string,
        shouldbe_svalue_string,
        shouldbe_scomment_string;
    std::string result_svalue_string,
        result_scomment_string;
    std::string sub_name = {
        static_cast<std::string>("TConfigureTest::")
        + static_cast<std::string>("TestReadConfig2:") };
    std::string error_message, err_1;
    mylib::CConfigure config_obj;
    std::string filename("test2.config");

    config_obj.ReadConfig(&filename,
                          &input_string_map,
                          &input_comment_map);

    test_label_index = 0L;
    for (va_cit = test_vector.cbegin();
         va_cit != test_vector.cend(); ++va_cit) {
        shouldbe_sname_string = va_cit->sname;
        shouldbe_svalue_string = va_cit->svalue;
        shouldbe_scomment_string = va_cit->scomment;

        result_svalue_string =
            input_string_map[shouldbe_sname_string];
        result_scomment_string =
            input_comment_map[shouldbe_sname_string];

        err_1 =
            sub_name
            + static_cast<std::string>(" (")
            + std::to_string(test_label_index)
            + static_cast<std::string>("), ");

        error_message =
            err_1
            + shouldbe_sname_string
            + static_cast<std::string>(", shouldbe value = ")
            + shouldbe_svalue_string
            + static_cast<std::string>(", result = ")
            + result_svalue_string;

        CPPUNIT_ASSERT_EQUAL_MESSAGE(
            error_message, shouldbe_svalue_string,
            result_svalue_string);

        error_message =
            err_1
            + shouldbe_sname_string
            + static_cast<std::string>(", shouldbe comment = ")
            + shouldbe_scomment_string
            + static_cast<std::string>(", result = ")
            + result_scomment_string;

        CPPUNIT_ASSERT_EQUAL_MESSAGE(
            error_message, shouldbe_scomment_string,
            result_scomment_string);

        ++test_label_index;
    }

    return;
}

// #############################################################
// #############################################################
void TConfigureTest::TestWriteRestartConfig1() {
    struct atest_struct {
    std::string sname, svalue, scomment;
    };
    std::vector<atest_struct> test_vector = {
        { "start-num", "3,333", "start-num" },
        { "end-num", "9,999", "end-num" },
        { "bin-num", "100", "bin-num" },
        { "cummulative-primes", "88", "cummulative-primes" },
        { "cummulative-twins", "44", "cummulative-twins" },
        { "out-file", "out-1234.txt", "out-file" },
        { "restart-file", "test3.config", "restart-file" }
    };
    std::vector<atest_struct>::const_iterator va_cit;
    int64_t test_label_index;
    std::map<std::string, std::string>
        input_string_map, input_comment_map;
    std::string shouldbe_sname_string,
        shouldbe_svalue_string,
        shouldbe_scomment_string;
    std::string result_svalue_string,
        result_scomment_string;
    std::string sub_name = {
        static_cast<std::string>("TConfigureTest::")
        + static_cast<std::string>("TestReadConfig2:") };
    std::string error_message, err_1;
    std::string restart_filename("test3.config");
    std::string output_filename("out-1234.txt");
    mylib::CConfigure config_obj;
    mylib::CUtils utils_obj;

    // put data to input maps
    for (va_cit = test_vector.cbegin();
         va_cit != test_vector.cend(); ++va_cit) {
        shouldbe_sname_string = va_cit->sname;
        shouldbe_svalue_string = va_cit->svalue;
        shouldbe_scomment_string = va_cit->scomment;

        input_string_map[shouldbe_sname_string] =
            shouldbe_svalue_string;
        input_comment_map[shouldbe_sname_string] =
            shouldbe_scomment_string;
    }

    // write data to output config file
    config_obj.WriteRestartConfig(&restart_filename,
                                  &input_string_map,
                                  &input_comment_map);

    input_string_map.clear();
    input_comment_map.clear();

    // read in data to input maps
    config_obj.ReadConfig(&restart_filename,
                          &input_string_map,
                          &input_comment_map);

    // test to see if what we put in, is what
    // we get back
    test_label_index = 0L;
    for (va_cit = test_vector.cbegin();
         va_cit != test_vector.cend(); ++va_cit) {
        shouldbe_sname_string = va_cit->sname;
        shouldbe_svalue_string = va_cit->svalue;
        shouldbe_scomment_string = va_cit->scomment;

        result_svalue_string =
            input_string_map[shouldbe_sname_string];
        result_scomment_string =
            input_comment_map[shouldbe_sname_string];

        err_1 =
            sub_name
            + static_cast<std::string>(" (")
            + std::to_string(test_label_index)
            + static_cast<std::string>("), ");

        error_message =
            err_1
            + shouldbe_sname_string
            + static_cast<std::string>(", shouldbe value = ")
            + shouldbe_svalue_string
            + static_cast<std::string>(", result = ")
            + result_svalue_string;

        CPPUNIT_ASSERT_EQUAL_MESSAGE(
            error_message, shouldbe_svalue_string,
            result_svalue_string);

        error_message =
            err_1
            + shouldbe_sname_string
            + static_cast<std::string>(", shouldbe comment = ")
            + shouldbe_scomment_string
            + static_cast<std::string>(", result = ")
            + result_scomment_string;

        CPPUNIT_ASSERT_EQUAL_MESSAGE(
            error_message, shouldbe_scomment_string,
            result_scomment_string);

        ++test_label_index;
    }

    return;
}

}  // namespace test_mylib
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/
