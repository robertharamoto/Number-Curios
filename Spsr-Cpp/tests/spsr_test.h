#ifndef TESTS_SPSR_TEST_H_
#define TESTS_SPSR_TEST_H_
/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  Spsr tests definition                                 ###
  ###                                                        ###
  ###  last updated April 25, 2024                           ###
  ###                                                        ###
  ###  updated July 19, 2022                                 ###
  ###                                                        ###
  ###  updated April 27, 2020                                ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <gmp.h>
#include <gmpxx.h>

#include <string>
#include <vector>

namespace mylib_test {

class TSpsrTest : public CPPUNIT_NS :: TestFixture {
  CPPUNIT_TEST_SUITE(TSpsrTest);

  CPPUNIT_TEST(TestIsPrime1);
  CPPUNIT_TEST(TestIsPrime2);

  CPPUNIT_TEST(TestSuccessorOfIntegerPartition1);
  CPPUNIT_TEST(TestConvertRow0ToRectangleVector1);
  CPPUNIT_TEST(TestFindMinRowColWidthHeight1);
  CPPUNIT_TEST(TestAddPartitionToRectangleStruct1);
  CPPUNIT_TEST(TestCombineRectangles1);
  CPPUNIT_TEST(TestCheckIfOkToUse1);

  CPPUNIT_TEST(TestSpsrStructVectorToBouwkampString1);
  CPPUNIT_TEST(TestSpsrStructVectorToStatsString1);

  CPPUNIT_TEST(TestRow0MakeSpsrFromPermutation1);
  CPPUNIT_TEST(TestMakeSpsrFromPermutation1);
  CPPUNIT_TEST(TestGenerateNThreadsPermutations1);

  CPPUNIT_TEST(TestSpsrStructVectorToHMirrorString1);
  CPPUNIT_TEST(TestSpsrStructVectorToVMirrorString1);
  CPPUNIT_TEST(TestSpsrStructVectorToHVMirrorString1);

  //  CPPUNIT_TEST(  );

  CPPUNIT_TEST_SUITE_END();

 public:
  void setUp();
  void tearDown();

  void TestIsPrime1();
  void TestIsPrime2();

  void TestSuccessorOfIntegerPartition1();

  void TestConvertRow0ToRectangleVector1();
  void TestFindMinRowColWidthHeight1();
  void TestAddPartitionToRectangleStruct1();
  void TestCombineRectangles1();
  void TestCheckIfOkToUse1();

  void TestSpsrStructVectorToBouwkampString1();
  void TestSpsrStructVectorToStatsString1();

  void TestRow0MakeSpsrFromPermutation1();
  void TestMakeSpsrFromPermutation1();
  void TestGenerateNThreadsPermutations1();

  void TestSpsrStructVectorToHMirrorString1();
  void TestSpsrStructVectorToVMirrorString1();
  void TestSpsrStructVectorToHVMirrorString1();

 private:
  void AssertTwoVectorsEqual(
      const std::string *err_start,
      const std::vector<int64_t> *vec1,
      const std::vector<int64_t> *vec2);
};

}  // namespace mylib_test

#endif  // TESTS_SPSR_TEST_H_

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/
