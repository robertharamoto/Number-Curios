#ifndef TESTS_CONFIGURE_TEST_H_
#define TESTS_CONFIGURE_TEST_H_
/*

// Copyright 2024 <robert haramoto>

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  configuration file test class definition              ###
  ###                                                        ###
  ###  last updated April 25, 2024                           ###
  ###                                                        ###
  ###  last updated July 31, 2022                            ###
  ###                                                        ###
  ###  updated February 5, 2020                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

namespace test_mylib {

class TConfigureTest : public CPPUNIT_NS :: TestFixture {
  CPPUNIT_TEST_SUITE(TConfigureTest);

  CPPUNIT_TEST(TestParseLine1);
  CPPUNIT_TEST(TestReadConfig1);
  CPPUNIT_TEST(TestReadConfig2);
  CPPUNIT_TEST(TestWriteRestartConfig1);

  CPPUNIT_TEST_SUITE_END();

 public:
  TConfigureTest();
  ~TConfigureTest();

  // do not allow copying nor moving
  TConfigureTest(const TConfigureTest&) = delete;
  TConfigureTest& operator=(const TConfigureTest&) = delete;
  TConfigureTest(TConfigureTest&&) = delete;
  TConfigureTest& operator=(TConfigureTest&&) = delete;

  void setUp();
  void tearDown();

  void TestParseLine1();
  void TestReadConfig1();
  void TestReadConfig2();
  void TestWriteRestartConfig1();

 private:
};  // class TConfigureTest

}  // namespace test_mylib

#endif  // TESTS_CONFIGURE_TEST_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/
