/*
  Copyright 2024 <robert haramoto>

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <https://unlicense.org/>
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  spsr tests implementation                             ###
  ###                                                        ###
  ###  last updated August 14, 2024                          ###
  ###    added symmetry checks for row 0                     ###
  ###                                                        ###
  ###  updated April 30, 2024                                ###
  ###                                                        ###
  ###  updated July 19, 2022                                 ###
  ###                                                        ###
  ###  updated April 27, 2020                                ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include "tests/spsr_test.h"

#include <gmp.h>
#include <gmpxx.h>

#include <cstdint>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include "sources/spsr.h"
#include "sources/utils.h"

namespace mylib_test {

CPPUNIT_TEST_SUITE_REGISTRATION(TSpsrTest);

// #############################################################
// #############################################################
void TSpsrTest::setUp() {
}

// #############################################################
// #############################################################
void TSpsrTest::tearDown() {
}

// #############################################################
// #############################################################
void TSpsrTest::TestIsPrime1() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    int64_t anum;
    bool shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { 2, true }, { 3, true }, { 4, false },
    { 5, true }, { 6, false }, { 7, true },
    { 8, false }, { 9, false }, { 10, false },
    { 11, true }, { 12, false }, { 13, true },
    { 14, false }, { 15, false }, { 16, false },
    { 17, true }, { 18, false }, { 19, true },
    { 20, false }, { 21, false }, { 22, false },
    { 23, true }, { 24, false }, { 25, false },
    { 26, false }, { 27, false }, { 28, false },
    { 29, true }, { 30, false }, { 31, true },
  };
  int64_t test_label_index(0L);
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t test_int;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::TestIsPrime1: ") };
  std::string error_message;

  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_int = va_cit->anum;
    shouldbe_bool = va_cit->shouldbe;

    result_bool = spsr_obj.IsPrime(test_int);

    error_message = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>(")")
        + static_cast<std::string>(", number = ")
        + std::to_string(test_int)
        + static_cast<std::string>(", shouldbe = ")
        + static_cast<std::string>(
            (shouldbe_bool ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (result_bool ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TSpsrTest::TestIsPrime2() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    int64_t anum;
    bool shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { 32, false }, { 33, false }, { 34, false },
    { 35, false }, { 36, false }, { 37, true },
    { 38, false }, { 39, false }, { 40, false },
    { 41, true }, { 42, false }, { 43, true },
    { 44, false }, { 45, false }, { 46, false },
    { 47, true }, { 48, false }, { 49, false },
    { 50, false }, { 51, false }, { 52, false },
    { 53, true }, { 54, false }, { 55, false },
    { 56, false }, { 57, false }, { 58, false },
    { 59, true }, { 60, false }, { 61, true },
  };
  int64_t test_label_index(0L);
  std::vector<atest_struct>::const_iterator va_cit;
  int test_int;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::TestIsPrime2: ") };
  std::string error_message;

  // check IsPrime()
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    test_int = va_cit->anum;
    shouldbe_bool = va_cit->shouldbe;

    result_bool = spsr_obj.IsPrime(test_int);

    error_message = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>(")")
        + static_cast<std::string>("), number = ")
        + std::to_string(test_int)
        + static_cast<std::string>(", shouldbe = ")
        + static_cast<std::string>(
            (shouldbe_bool ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (result_bool ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TSpsrTest::TestSuccessorOfIntegerPartition1() {
  mylib::CSpsr spsr_obj;
  mylib::CUtils utils_obj;
  struct atest_struct {
    int64_t part_num;
    std::vector<int64_t> part_vector;
    std::vector<int64_t> shouldbe_vector;
    bool shouldbe;
  };
  std::vector<atest_struct> test_vector = {
    { 2, { 2 }, { 1, 1 },
      true,
    },
    { 2, { 1, 1 }, { },
      false,
    },
    { 3, { 3 }, { 2, 1 },
      true,
    },
    { 3, { 2, 1 }, { 1, 1, 1 },
      true,
    },
    { 3, { 1, 1, 1 }, { },
      false,
    },
    { 4, { 4 }, { 3, 1 },
      true,
    },
    { 4, { 3, 1 }, { 2, 2 },
      true,
    },
    { 4, { 2, 2 }, { 2, 1, 1 },
      true,
    },
    { 4, { 2, 1, 1 }, { 1, 1, 1, 1 },
      true,
    },
    { 4, { 1, 1, 1, 1 }, { },
      false,
    },
    { 5, { 5 }, { 4, 1 },
      true,
    },
    { 5, { 4, 1 }, { 3, 2 },
      true,
    },
    { 5, { 3, 2 }, { 3, 1, 1 },
      true,
    },
    { 5, { 3, 1, 1 }, { 2, 2, 1 },
      true,
    },
    { 5, { 2, 2, 1 }, { 2, 1, 1, 1 },
      true,
    },
    { 5, { 2, 1, 1, 1 }, { 1, 1, 1, 1, 1 },
      true,
    },
    { 5, { 1, 1, 1, 1, 1 }, { },
      false,
    },
    { 6, { 6 }, { 5, 1 },
      true,
    },
    { 6, { 5, 1 }, { 4, 2 },
      true,
    },
    { 6, { 4, 2 }, { 4, 1, 1 },
      true,
    },
    { 6, { 4, 1, 1 }, { 3, 3 },
      true,
    },
    { 6, { 3, 3 }, { 3, 2, 1 },
      true,
    },
    { 6, { 3, 2, 1 }, { 3, 1, 1, 1 },
      true,
    },
    { 6, { 3, 1, 1, 1 }, { 2, 2, 2 },
      true,
    },
    { 6, { 2, 2, 2 }, { 2, 2, 1, 1 },
      true,
    },
    { 6, { 2, 2, 1, 1 }, { 2, 1, 1, 1, 1 },
      true,
    },
    { 6, { 2, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1, 1 },
      true,
    },
    { 6, { 1, 1, 1, 1, 1, 1 }, { },
      false,
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t part_num;
  std::vector<int64_t> input_vector, shouldbe_vector;
  std::vector<int64_t> result_vector;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestSuccessorOfInteger")
    + static_cast<std::string>("Partition1: ") };
  std::string err_start, error_message;

  // check IsPrime()
  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    part_num = va_cit->part_num;
    input_vector = result_vector = va_cit->part_vector;
    shouldbe_vector = va_cit->shouldbe_vector;
    shouldbe_bool = va_cit->shouldbe;

    result_bool =
        spsr_obj.SuccessorOfIntegerPartition(
            part_num, &result_vector);

    err_start = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>(")")
        + static_cast<std::string>(", number = ")
        + std::to_string(part_num)
        + static_cast<std::string>(", input_vector = ")
        + utils_obj.Int64VectorToString(&input_vector)
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("shouldbe vector = ")
        + utils_obj.Int64VectorToString(&shouldbe_vector)
        + static_cast<std::string>(", result vector = ")
        + utils_obj.Int64VectorToString(&result_vector);

    AssertTwoVectorsEqual(
        &error_message, &shouldbe_vector, &result_vector);

    error_message =
        err_start
        + static_cast<std::string>(", shouldbe_bool = ")
        + static_cast<std::string>(
            (shouldbe_bool ? "true" : "false"))
        + static_cast<std::string>(", result_bool = ")
        + static_cast<std::string>(
            (result_bool ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    test_label_index++;
  }

  return;
}

// #############################################################
// #############################################################
void TSpsrTest::AssertTwoVectorsEqual(
    const std::string *err_start,
    const std::vector<int64_t> *shouldbe_vector,
    const std::vector<int64_t> *result_vector) {
  mylib::CUtils utils_obj;
  std::string error_part1, error_message;
  int64_t s_size, r_size;
  int64_t jj;

  s_size = static_cast<int64_t>(shouldbe_vector->size());
  r_size = static_cast<int64_t>(shouldbe_vector->size());

  error_part1 =
      *err_start
      + static_cast<std::string>(", shouldbe_vector = ")
      + utils_obj.Int64VectorToString(shouldbe_vector)
      + static_cast<std::string>(", result_vector = ")
      + utils_obj.Int64VectorToString(result_vector);

  error_message =
      error_part1
      + static_cast<std::string>(", shouldbe size = ")
      + std::to_string(s_size)
      + static_cast<std::string>(", result size = ")
      + std::to_string(r_size);

  CPPUNIT_ASSERT_EQUAL_MESSAGE(
      error_message, s_size, r_size);

  for (jj = 0; jj < s_size; jj++) {
    error_message =
        error_part1
        + static_cast<std::string>(", (")
        + std::to_string(jj)
        + static_cast<std::string>("), shouldbe element = ")
        + std::to_string((*shouldbe_vector)[jj])
        + static_cast<std::string>(", result element = ")
        + std::to_string((*result_vector)[jj]);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, (*shouldbe_vector)[jj], (*result_vector)[jj]);
  }

  return;
}

// #############################################################
// #############################################################
void TSpsrTest::TestConvertRow0ToRectangleVector1() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    std::vector<int64_t> input_vector;
    std::vector<mylib::CSpsr::rect_struct> shouldbe_vector;
  };
  std::vector<atest_struct> test_vector = {
    { { 2, 3, 4 },
      { { 0, 0, 1, 1 },
        { 0, 2, 2, 4 },
        { 0, 5, 3, 8 }
      }
    },
    { { 9, 8, 7 },
      { { 0, 0, 8, 8 },
        { 0, 9, 7, 16 },
        { 0, 17, 6, 23 }
      }
    },
  };
  int64_t test_label_index(0L), jj, ss_size, rr_size;
  std::vector<atest_struct>::const_iterator va_cit;
  std::vector<int64_t> input_vector;
  std::vector<mylib::CSpsr::rect_struct>
      shouldbe_vector, result_vector;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestConvertRow0")
    + static_cast<std::string>("ToRectangleVector1: ") };
  std::string error_message, err_start, err_vector_string;
  mylib::CUtils utils_obj;

  // check IsPrime()
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->input_vector;
    shouldbe_vector = va_cit->shouldbe_vector;

    spsr_obj.ConvertRow0ToRectangleVector(
        &input_vector, &result_vector);

    ss_size = static_cast<int64_t>(shouldbe_vector.size());
    rr_size = static_cast<int64_t>(result_vector.size());

    err_start = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>(")")
        + static_cast<std::string>(", input_vector = ")
        + utils_obj.Int64VectorToString(&input_vector);

    error_message =
        err_start
        + static_cast<std::string>(", shouldbe_vector size = ")
        + std::to_string(ss_size)
        + static_cast<std::string>(", result_vector size = ")
        + std::to_string(rr_size);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, ss_size, rr_size);

    err_vector_string =
        static_cast<std::string>("shouldbe vector = ")
        + spsr_obj.vector_rect_struct_to_string(
            &shouldbe_vector)
        + static_cast<std::string>(", result vector = ")
        + spsr_obj.vector_rect_struct_to_string(
            &result_vector);

    for (jj = 0; jj < ss_size; jj++) {
      error_message = err_start
          + err_vector_string
          + static_cast<std::string>(", jj = ")
          + std::to_string(jj);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_vector[jj].start_row,
          result_vector[jj].start_row);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_vector[jj].start_column,
          result_vector[jj].start_column);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_vector[jj].end_row,
          result_vector[jj].end_row);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_vector[jj].end_column,
          result_vector[jj].end_column);
    }

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TSpsrTest::TestFindMinRowColWidthHeight1() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    std::vector<mylib::CSpsr::rect_struct> rect_vector;
    int64_t min_row, min_column, min_width, min_height;
  };
  std::vector<atest_struct> test_vector = {
    { { { 0, 0, 0, 0 },
        { 0, 1, 1, 2 },
        { 0, 3, 2, 5 }
      },
      1, 0, 1, 1,
    },
    { { { 0, 0, 8, 8 },
        { 0, 9, 7, 16 },
        { 0, 17, 6, 23 }
      },
      7, 17, 7, 7,
    },
    { { { 0, 0, 35, 35 },
        { 0, 36, 18, 54 },
        { 0, 55, 23, 78 },
        { 0, 79, 32, 111 },
      },
      19, 36, 19, 19,
    },
    { { { 0, 0, 35, 35 },
        { 0, 36, 33, 49 },
        { 0, 50, 23, 78 },
        { 0, 79, 32, 111 },
      },
      24, 50, 29, 24,
    },
  };
  int64_t test_label_index(0L);
  std::vector<atest_struct>::const_iterator va_cit;
  std::vector<mylib::CSpsr::rect_struct> rect_vector;
  int64_t shouldbe_min_row, shouldbe_min_column;
  int64_t shouldbe_min_width, shouldbe_min_height;
  int64_t result_min_row, result_min_column;
  int64_t result_min_width, result_min_height;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestFindMinRowColWidthHeight1: ") };
  std::string error_message, err_start;

  // check FindMinRowColWidthHeight
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    rect_vector = va_cit->rect_vector;
    shouldbe_min_row = va_cit->min_row;
    shouldbe_min_column = va_cit->min_column;
    shouldbe_min_width = va_cit->min_width;
    shouldbe_min_height = va_cit->min_height;

    spsr_obj.FindMinRowColWidthHeight(
        &rect_vector, &result_min_row, &result_min_column,
        &result_min_width, &result_min_height);

    err_start = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>(
            "), rect_vector  = ")
        + spsr_obj.vector_rect_struct_to_string(
            &rect_vector)
        + static_cast<std::string>(" : ");

    error_message =
        err_start
        + static_cast<std::string>("shouldbe min_row = ")
        + std::to_string(shouldbe_min_row)
        + static_cast<std::string>(", result min_row = ")
        + std::to_string(result_min_row);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message,
        shouldbe_min_row, result_min_row);

    error_message = err_start
        + static_cast<std::string>("shouldbe min_column = ")
        + std::to_string(shouldbe_min_column)
        + static_cast<std::string>(", result min_column = ")
        + std::to_string(result_min_column);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message,
        shouldbe_min_column, result_min_column);

    error_message = err_start
        + static_cast<std::string>("shouldbe min_width = ")
        + std::to_string(shouldbe_min_width)
        + static_cast<std::string>(", result min_width = ")
        + std::to_string(result_min_width);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message,
        shouldbe_min_width, result_min_width);

    error_message = err_start
        + static_cast<std::string>("shouldbe min_height = ")
        + std::to_string(shouldbe_min_height)
        + static_cast<std::string>(", result min_height = ")
        + std::to_string(result_min_height);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message,
        shouldbe_min_height, result_min_height);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TSpsrTest::TestAddPartitionToRectangleStruct1() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    std::vector<int64_t> input_vector;
    std::vector<mylib::CSpsr::rect_struct> input_rect_vector;
    std::vector<mylib::CSpsr::rect_struct> shouldbe_rect_vector;
  };
  std::vector<atest_struct> test_vector = {
    { { 1, 2, 3 },
      { { 0, 0, 8, 8 },
        { 0, 9, 7, 16 },
        { 0, 17, 6, 23 }
      },
      { { 0, 0, 8, 8 },
        { 0, 9, 7, 16 },
        { 0, 17, 7, 17 },
        { 0, 18, 8, 19 },
        { 0, 20, 9, 22 }
      },
    },
    { { 3, 1, 2 },
      { { 0, 0, 8, 8 },
        { 0, 9, 7, 16 },
        { 0, 17, 6, 23 }
      },
      { { 0, 0, 8, 8 },
        { 0, 9, 7, 16 },
        { 0, 17, 9, 19 },
        { 0, 20, 7, 20 },
        { 0, 21, 8, 22 }
      },
    },
    { { 5, 6, 8 },
      { { 0, 0, 35, 35 },
        { 0, 36, 18, 54 },
        { 0, 55, 23, 78 },
        { 0, 79, 32, 111 },
      },
      { { 0, 0, 35, 35 },
        { 0, 36, 23, 40 },
        { 0, 41, 24, 46 },
        { 0, 47, 26, 54 },
        { 0, 55, 23, 78 },
        { 0, 79, 32, 111 },
      },
    },
    { { 8, 5, 6 },
      { { 0, 0, 35, 35 },
        { 0, 36, 18, 54 },
        { 0, 55, 23, 78 },
        { 0, 79, 32, 111 },
      },
      { { 0, 0, 35, 35 },
        { 0, 36, 26, 43 },
        { 0, 44, 23, 48 },
        { 0, 49, 24, 54 },
        { 0, 55, 23, 78 },
        { 0, 79, 32, 111 },
      },
    },
    { { 3, 1 },
      { { 0, 0, 3, 3 },
        { 0, 4, 28, 32 }
      },
      { { 0, 0, 6, 2 },
        { 0, 3, 4, 3 },
        { 0, 4, 28, 32 },
      },
    },
    { { 1, 3 },
      { { 0, 0, 3, 3 },
        { 0, 4, 28, 32 },
      },
      { { 0, 0, 4, 0 },
        { 0, 1, 6, 3 },
        { 0, 4, 28, 32 },
      },
    },
  };
  int64_t test_label_index, jj;
  int64_t ss_size, rr_size;
  std::vector<atest_struct>::const_iterator va_cit;
  std::vector<int64_t> input_vector;
  std::vector<mylib::CSpsr::rect_struct>
      input_rect_vector, shouldbe_rect_vector, result_rect_vector;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestAddPartitionTo")
    + static_cast<std::string>("RectangleStruct1: ") };
  std::string err_start, err_start_1, err_shouldbe_vector_string;
  std::string err_result_vector_string, error_message;
  mylib::CUtils utils_obj;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->input_vector;
    input_rect_vector = va_cit->input_rect_vector;
    shouldbe_rect_vector = va_cit->shouldbe_rect_vector;

    spsr_obj.AddPartitionToRectangleStruct(
        &input_vector, &input_rect_vector,
        &result_rect_vector);

    err_start = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>(")")
        + static_cast<std::string>(", input_vector = ")
        + utils_obj.Int64VectorToString(&input_vector)
        + static_cast<std::string>(", input_rect_vector = ")
        + spsr_obj.vector_rect_struct_to_string(
            &input_rect_vector)
        + static_cast<std::string>(" : ");

    err_shouldbe_vector_string =
        static_cast<std::string>(", shouldbe_rect_vector = ")
        + spsr_obj.vector_rect_struct_to_string(
            &shouldbe_rect_vector);

    err_result_vector_string =
        static_cast<std::string>(", result_rect_vector = ")
        + spsr_obj.vector_rect_struct_to_string(
            &result_rect_vector);

    ss_size = static_cast<int64_t>(shouldbe_rect_vector.size());
    rr_size = static_cast<int64_t>(result_rect_vector.size());

    error_message = err_start
        + static_cast<std::string>(" shouldbe size = ")
        + std::to_string(ss_size)
        + static_cast<std::string>(", result size = ")
        + std::to_string(rr_size);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, ss_size, rr_size);

    err_start_1 = err_start
        + err_shouldbe_vector_string
        + err_result_vector_string;

    for (jj = 0; jj < ss_size; ++jj) {
      error_message =
          err_start_1
          + static_cast<std::string>(" : element jj = ")
          + std::to_string(jj);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_rect_vector[jj].start_row,
          result_rect_vector[jj].start_row);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_rect_vector[jj].start_column,
          result_rect_vector[jj].start_column);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_rect_vector[jj].end_row,
          result_rect_vector[jj].end_row);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_rect_vector[jj].end_column,
          result_rect_vector[jj].end_column);
    }

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TSpsrTest::TestCombineRectangles1() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    std::vector<mylib::CSpsr::rect_struct> input_rect_vector;
    std::vector<mylib::CSpsr::rect_struct> shouldbe_rect_vector;
  };
  std::vector<atest_struct> test_vector = {
    { { { 0, 0, 8, 8 },
        { 0, 9, 7, 16 },
        { 0, 17, 7, 17 },
        { 0, 18, 8, 19 },
        { 0, 20, 9, 22 }
      },
      { { 0, 0, 8, 8 },
        { 0, 9, 7, 17 },
        { 0, 18, 8, 19 },
        { 0, 20, 9, 22 }
      }
    },
    { { { 0, 0, 8, 8 },
        { 0, 9, 7, 16 },
        { 0, 17, 9, 19 },
        { 0, 20, 7, 20 },
        { 0, 21, 8, 22 }
      },
      { { 0, 0, 8, 8 },
        { 0, 9, 7, 16 },
        { 0, 17, 9, 19 },
        { 0, 20, 7, 20 },
        { 0, 21, 8, 22 }
      }
    },
    { { { 0, 0, 8, 8 },
        { 0, 9, 7, 16 },
        { 0, 17, 9, 19 },
        { 0, 20, 8, 21 },
        { 0, 22, 8, 23 }
      },
      { { 0, 0, 8, 8 },
        { 0, 9, 7, 16 },
        { 0, 17, 9, 19 },
        { 0, 20, 8, 23 },
      }
    },
    { { { 0, 0, 35, 35 },
        { 0, 36, 23, 40 },
        { 0, 41, 23, 46 },
        { 0, 47, 23, 54 },
        { 0, 55, 23, 78 },
        { 0, 79, 32, 111 }
      },
      { { 0, 0, 35, 35 },
        { 0, 36, 23, 78 },
        { 0, 79, 32, 111 }
      },
    },
    { { { 0, 0, 35, 35 },
        { 0, 36, 26, 43 },
        { 0, 44, 23, 48 },
        { 0, 49, 24, 54 },
        { 0, 55, 23, 78 },
        { 0, 79, 32, 111 }
      },
      { { 0, 0, 35, 35 },
        { 0, 36, 26, 43 },
        { 0, 44, 23, 48 },
        { 0, 49, 24, 54 },
        { 0, 55, 23, 78 },
        { 0, 79, 32, 111 }
      }
    },
    { { { 0, 0, 35, 35 },
        { 0, 36, 26, 43 },
        { 0, 44, 23, 48 },
        { 0, 49, 24, 54 },
        { 0, 55, 32, 78 },
        { 0, 79, 32, 111 }
      },
      { { 0, 0, 35, 35 },
        { 0, 36, 26, 43 },
        { 0, 44, 23, 48 },
        { 0, 49, 24, 54 },
        { 0, 55, 32, 111 }
      }
    },
  };
  int64_t test_label_index, jj;
  int64_t ss_size, rr_size;
  std::vector<atest_struct>::const_iterator va_cit;
  std::vector<mylib::CSpsr::rect_struct>
      input_rect_vector, shouldbe_rect_vector, result_rect_vector;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestCombineRectangles1: ") };
  std::string error_message;
  std::string err_start, err_shouldbe, err_result;

  test_label_index = 0L;
  // check CombineRectangles()
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_rect_vector = va_cit->input_rect_vector;
    shouldbe_rect_vector = va_cit->shouldbe_rect_vector;

    spsr_obj.CombineRectangles(
        &input_rect_vector, &result_rect_vector);

    err_start = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), input_rect_vector = ")
        + spsr_obj.vector_rect_struct_to_string(
            &input_rect_vector)
        + static_cast<std::string>(" : ");

    err_shouldbe =
        static_cast<std::string>("shouldbe_rect_vector = ")
        + spsr_obj.vector_rect_struct_to_string(
            &shouldbe_rect_vector);

    err_result =
        static_cast<std::string>(", result_rect_vector = ")
        + spsr_obj.vector_rect_struct_to_string(
            &result_rect_vector);

    ss_size = shouldbe_rect_vector.size();
    rr_size = result_rect_vector.size();

    error_message = err_start
        + err_shouldbe + err_result
        + static_cast<std::string>(", shouldbe size = ")
        + std::to_string(ss_size)
        + static_cast<std::string>(", result size = ")
        + std::to_string(rr_size);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, ss_size, rr_size);

    for (jj = 0; jj < ss_size; ++jj) {
      error_message = err_start
          + err_shouldbe + err_result
          + static_cast<std::string>(", jj = ")
          + std::to_string(jj);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_rect_vector[jj].start_row,
          result_rect_vector[jj].start_row);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_rect_vector[jj].start_column,
          result_rect_vector[jj].start_column);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_rect_vector[jj].end_row,
          result_rect_vector[jj].end_row);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_rect_vector[jj].end_column,
          result_rect_vector[jj].end_column);
    }

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TSpsrTest::TestCheckIfOkToUse1() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    std::vector<int64_t> input_vector;
    std::map<int64_t, bool> used_map;
    bool shouldbe_bool;
  };
  std::vector<atest_struct> test_vector = {
    { { 1, 2, 3 },
      { { 7, true }, { 8, true }, { 9, true },
      },
      true,
    },
    { { 1, 2, 3, 1 },
      { { 7, true }, { 8, true }, { 9, true },
      },
      false,
    },
    { { 1, 2, 3, 2 },
      { { 7, true }, { 8, true }, { 9, true },
      },
      false,
    },
    { { 2, 3, 6 },
      { { 7, true }, { 8, true }, { 9, true },
      },
      true,
    },
    { { 2, 3, 7 },
      { { 7, true }, { 8, true }, { 9, true },
      },
      false,
    },
    { { 2, 3, 8 },
      { { 7, true }, { 8, true }, { 9, true },
      },
      false,
    },
    { { 2, 3, 9 },
      { { 7, true }, { 8, true }, { 9, true },
      },
      false,
    },
    { { 2, 3, 10 },
      { { 7, true }, { 8, true }, { 9, true },
      },
      true,
    },
  };
  int64_t test_label_index;
  std::vector<atest_struct>::const_iterator va_cit;
  std::vector<int64_t> input_vector;
  std::map<int64_t, bool> used_map;
  bool shouldbe_bool, result_bool;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestCheckIfOkToUse1: ") };
  std::string error_message;
  mylib::CUtils utils_obj;

  test_label_index = 0L;
  // check CheckIfOkToUse()
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->input_vector;
    used_map = va_cit->used_map;
    shouldbe_bool = va_cit->shouldbe_bool;

    result_bool = spsr_obj.CheckIfOkToUse(
        &input_vector, &used_map);

    error_message = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>(")")
        + static_cast<std::string>(", input_vector = ")
        + utils_obj.Int64VectorToString(&input_vector)
        + static_cast<std::string>(", shouldbe = ")
        + (static_cast<std::string>(
            shouldbe_bool ? "true" : "false"))
        + static_cast<std::string>(", result = ")
        + static_cast<std::string>(
            (result_bool ? "true" : "false"));

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_bool, result_bool);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// [47, 65] : (22, 19, 24) (3, 11, 5) (25) (6, 23) (17) : count = 10
void TSpsrTest::TestSpsrStructVectorToBouwkampString1() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    std::vector<struct mylib::CSpsr::spsr_struct> input_vector;
    std::string shouldbe_string;
  };
  std::vector<atest_struct> test_vector = {
    { { { 3, 0, 0}, {4, 0, 3}, {5, 0, 7},
      },
      "(3, 4, 5)",
    },
    { { { 3, 0, 0}, {4, 0, 3}, {5, 0, 7},
        {7, 3, 0}, {8, 3, 7},
      },
      "(3, 4, 5) (7, 8)",
    },
    { { { 4, 0, 0}, {3, 0, 4}, {5, 0, 7}, {7, 3, 0},
        {8, 4, 1}, {9, 4, 9},
      },
      "(4, 3, 5) (7) (8, 9)",
    },
    { { { 22, 0, 0}, {19, 0, 22}, {24, 0, 41},
        {3, 19, 22}, {11, 19, 25}, {5, 19, 36},
        {25, 22, 0}, {6, 24, 41}, {23, 24, 47},
        {17, 30, 17},
      },
      "(22, 19, 24) (3, 11, 5) (25) (6, 23) (17)"
    },
  };
  int64_t test_label_index;
  std::string shouldbe_string, result_string;
  std::vector<struct atest_struct>::const_iterator va_cit;
  std::vector<struct mylib::CSpsr::spsr_struct> input_vector;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestSpsrStructVector")
    + static_cast<std::string>("ToBouwkampString1: ") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->input_vector;
    shouldbe_string = va_cit->shouldbe_string;

    result_string =
        spsr_obj.SpsrStructVectorToBouwkampString(&input_vector);

    error_message = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe string = ")
        + shouldbe_string
        + static_cast<std::string>(", result string = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);
  }

  return;
}

// #############################################################
// #############################################################
void TSpsrTest::TestSpsrStructVectorToStatsString1() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    std::vector<struct mylib::CSpsr::spsr_struct> input_vector;
    std::string shouldbe_string;
  };
  std::vector<atest_struct> test_vector = {
    { { { 3, 0, 0}, {4, 0, 3}, {5, 0, 7},
      },
      "primes/total = 2 / 3 = 66.7%",
    },
    { { { 3, 0, 0}, {4, 0, 3}, {5, 0, 7},
        {7, 3, 0}, {8, 3, 7},
      },
      "primes/total = 3 / 5 = 60.0%",
    },
    { { { 4, 0, 0}, {3, 0, 4}, {5, 0, 7}, {7, 3, 0},
        {8, 4, 1}, {9, 4, 9},
      },
      "primes/total = 3 / 6 = 50.0%",
    },
    { { { 22, 0, 0}, {19, 0, 22}, {24, 0, 41},
        {3, 19, 22}, {11, 19, 25}, {5, 19, 36},
        {25, 22, 0}, {6, 24, 41}, {23, 24, 47},
        {17, 30, 17},
      },
      "primes/total = 6 / 10 = 60.0%",
    },
  };
  int64_t test_label_index;
  std::vector<struct atest_struct>::const_iterator va_cit;
  std::vector<struct mylib::CSpsr::spsr_struct> input_vector;
  std::string shouldbe_string, result_string;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestSpsrStructVector")
    + static_cast<std::string>("ToStatsString1: ") };
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->input_vector;
    shouldbe_string = va_cit->shouldbe_string;

    result_string =
        spsr_obj.SpsrStructVectorToStatsString(&input_vector);

    error_message = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), shouldbe string = ")
        + shouldbe_string
        + static_cast<std::string>(", result string = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TSpsrTest::TestRow0MakeSpsrFromPermutation1() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    std::vector<int64_t> input_vector;
    std::vector<mylib::CSpsr::spsr_struct> shouldbe_spsr_vector;
    std::vector<mylib::CSpsr::rect_struct> shouldbe_rect_vector;
  };
  std::vector<atest_struct> test_vector = {
    { { 2, 3 },
      { { 2, 0, 0 }, { 3, 0, 2 },
      },
      { { 0, 0, 1, 1 }, { 0, 2, 2, 4 },
      },
    },
    { { 1, 2, 3 },
      { { 1, 0, 0 }, { 2, 0, 1 }, { 3, 0, 3 },
      },
      { { 0, 0, 0, 0 }, { 0, 1, 1, 2 }, { 0, 3, 2, 5 },
      },
    },
    { { 3, 2, 1 },
      { { 3, 0, 0 }, { 2, 0, 3 }, { 1, 0, 5 },
      },
      { { 0, 0, 2, 2 }, { 0, 3, 1, 4 }, { 0, 5, 0, 5 },
      },
    },
  };
  int64_t test_label_index, jj;
  int64_t ss_size, rr_size;
  std::vector<atest_struct>::const_iterator va_cit;
  std::vector<int64_t> input_vector;
  std::vector<mylib::CSpsr::spsr_struct>
      shouldbe_spsr_vector, result_spsr_vector;
  std::vector<mylib::CSpsr::rect_struct>
      shouldbe_rect_vector, result_rect_vector;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestRow0MakeSpsr")
    + static_cast<std::string>("FromPermutation1: ") };
  std::string error_message, err_start, err_part_1;
  mylib::CUtils utils_obj;

  // check Row0MakeSpsrFromPermutation()
  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->input_vector;
    shouldbe_spsr_vector = va_cit->shouldbe_spsr_vector;
    shouldbe_rect_vector = va_cit->shouldbe_rect_vector;

    spsr_obj.Row0MakeSpsrFromPermutation(
        &input_vector, &result_spsr_vector, &result_rect_vector);

    err_start = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), input_vector = ")
        + utils_obj.Int64VectorToString(&input_vector)
        + static_cast<std::string>(" : ");

    err_part_1 =
        err_start
        + static_cast<std::string>("shouldbe_spsr_vector ")
        + static_cast<std::string>("(square size, start r/c) = ")
        + spsr_obj.vector_spsr_struct_to_string(
            &shouldbe_spsr_vector)
        + static_cast<std::string>(", result_spsr_vector = ")
        + spsr_obj.vector_spsr_struct_to_string(
            &result_spsr_vector);

    ss_size = shouldbe_spsr_vector.size();
    rr_size = result_spsr_vector.size();
    error_message = err_part_1
        + static_cast<std::string>(
            ", shouldbe_spsr_vector size = ")
        + std::to_string(ss_size)
        + static_cast<std::string>(
            ", result_spsr_vector size = ")
        + std::to_string(rr_size);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, ss_size, rr_size);

    for (jj = 0L; jj < ss_size; ++jj) {
      error_message = err_start
          + static_cast<std::string>(
              ", (")
          + std::to_string(jj)
          + static_cast<std::string>(
              ")");

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_spsr_vector[jj].square_size,
          result_spsr_vector[jj].square_size);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_spsr_vector[jj].start_row,
          result_spsr_vector[jj].start_row);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_spsr_vector[jj].start_column,
          result_spsr_vector[jj].start_column);
    }

    err_part_1 =
        err_start
        + static_cast<std::string>(", shouldbe_rect_vector ")
        + static_cast<std::string>("(start r/c, end r/c) = ")
        + spsr_obj.vector_rect_struct_to_string(
            &shouldbe_rect_vector)
        + static_cast<std::string>(", result_spsr_vector = ")
        + spsr_obj.vector_rect_struct_to_string(
            &result_rect_vector);

    ss_size = shouldbe_rect_vector.size();
    rr_size = result_rect_vector.size();
    error_message = err_part_1
        + static_cast<std::string>(
            ", shouldbe_rect_vector size = ")
        + std::to_string(ss_size)
        + static_cast<std::string>(
            ", result_rect_vector size = ")
        + std::to_string(rr_size);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, ss_size, rr_size);

    for (jj = 0L; jj < ss_size; ++jj) {
      error_message = err_part_1
          + static_cast<std::string>(", (")
          + std::to_string(jj)
          + static_cast<std::string>(")");

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_rect_vector[jj].start_row,
          result_rect_vector[jj].start_row);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_rect_vector[jj].start_column,
          result_rect_vector[jj].start_column);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_rect_vector[jj].end_row,
          result_rect_vector[jj].end_row);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_rect_vector[jj].end_column,
          result_rect_vector[jj].end_column);
    }

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TSpsrTest::TestMakeSpsrFromPermutation1() {
  mylib::CSpsr spsr_obj;
  mylib::CUtils utils_obj;
  struct atest_struct {
    std::vector<int64_t> input_vector;
    int64_t min_row, min_column;
    std::vector<mylib::CSpsr::spsr_struct> shouldbe_spsr_vector;
  };
  std::vector<atest_struct> test_vector = {
    { { 2, 3 }, 0, 0,
      { { 2, 0, 0 }, { 3, 0, 2 }
      },
    },
    { { 1, 2, 3 }, 0, 0,
      { { 1, 0, 0 }, { 2, 0, 1 }, { 3, 0, 3 },
      },
    },
    { { 3, 2, 1 }, 0, 0,
      { { 3, 0, 0 }, { 2, 0, 3 }, { 1, 0, 5 },
      },
    },
    { { 14, 5 }, 19, 36,
      { { 14, 19, 36 }, { 5, 19, 50 },
      },
    },
    { { 20, 9 }, 24, 50,
      { { 20, 24, 50 }, { 9, 24, 70 },
      },
    }
  };
  int64_t test_label_index, jj;
  std::vector<atest_struct>::const_iterator va_cit;
  std::vector<int64_t> input_vector;
  std::vector<mylib::CSpsr::spsr_struct>
      shouldbe_spsr_vector, result_spsr_vector;
  int64_t min_row, min_column;
  int64_t ss_size, rr_size;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestMakeSpsrFromPermutation1: ") };
  std::string error_message, err_start, err_part_1;

  // check MakeSpsrFromPermutation()
  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    input_vector = va_cit->input_vector;
    min_row = va_cit->min_row;
    min_column = va_cit->min_column;
    shouldbe_spsr_vector = va_cit->shouldbe_spsr_vector;

    result_spsr_vector.clear();
    spsr_obj.MakeSpsrFromPermutation(
        &input_vector, min_row, min_column,
        &result_spsr_vector);

    err_start = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), input_vector = ")
        + utils_obj.Int64VectorToString(&input_vector)
        + static_cast<std::string>(", min_row = ")
        + std::to_string(min_row)
        + static_cast<std::string>(", min_column = ")
        + std::to_string(min_column)
        + static_cast<std::string>(" : ");

    err_part_1 =
        err_start
        + static_cast<std::string>("shouldbe_spsr_vector ")
        + static_cast<std::string>(
            "(square size, start r/c) = ")
        + spsr_obj.vector_spsr_struct_to_string(
            &shouldbe_spsr_vector)
        + static_cast<std::string>(
            "result_spsr_vector (size, r/c) = ")
        + spsr_obj.vector_spsr_struct_to_string(
            &result_spsr_vector);

    ss_size = shouldbe_spsr_vector.size();
    rr_size = result_spsr_vector.size();

    error_message = err_part_1
        + static_cast<std::string>(
            ", shouldbe size = ")
        + std::to_string(ss_size)
        + static_cast<std::string>(
            ", result size = ")
        + std::to_string(rr_size);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, ss_size, rr_size);

    for (jj = 0L; jj < ss_size; ++jj) {
      error_message =
          err_part_1
          + static_cast<std::string>(", (")
          + std::to_string(jj)
          + static_cast<std::string>(")");

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_spsr_vector[jj].square_size,
          result_spsr_vector[jj].square_size);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_spsr_vector[jj].start_row,
          result_spsr_vector[jj].start_row);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_spsr_vector[jj].start_column,
          result_spsr_vector[jj].start_column);
    }

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
void TSpsrTest::TestGenerateNThreadsPermutations1() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    int64_t nthreads;
    std::vector<int64_t> input_vector;
    std::vector<int64_t> shouldbe_vector;
    std::vector<std::vector<int64_t> > shouldbe_vector_vector;
  };
  std::vector<atest_struct> test_vector = {
    { 1,
      { 2, 3, 5 },
      { 2, 5, 3 },
      { { 2, 3, 5 }, },
    },
    { 2,
      { 2, 3 },
      { },
      { { 2, 3 }, },
    },
    { 3,
      { 22, 33, 44 },
      { 33, 44, 22 },
      { { 22, 33, 44 },
        { 22, 44, 33 },
        { 33, 22, 44 },
        },
    },
    { 3,
      { 4, 2, 3 },
      { },
      { { 4, 2, 3 },
        { 4, 3, 2 }
      },
    },
  };
  int64_t test_label_index;
  int64_t jj(0L);
  std::vector<atest_struct>::const_iterator va_cit;
  int64_t nthreads;
  int64_t shouldbe_size, result_size;
  std::vector<int64_t> input_vector,
      shouldbe_vector, result_vector;
  std::vector<int64_t> ss_vector, rr_vector;
  std::vector<std::vector<int64_t> >
      shouldbe_vector_vector, result_vector_vector;
  std::map<std::string, bool> symmetric_map;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestGenerateNThreads")
    + static_cast<std::string>("Permutations1: ") };
  std::string error_message, err_start, err_part_1;
  mylib::CUtils utils_obj;

  // check MakeSpsrFromPermutation()
  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    nthreads = va_cit->nthreads;
    input_vector = va_cit->input_vector;
    result_vector = input_vector;
    shouldbe_vector = va_cit->shouldbe_vector;
    shouldbe_vector_vector = va_cit->shouldbe_vector_vector;

    err_start = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), nthreads = ")
        + std::to_string(nthreads)
        + static_cast<std::string>(", input_vector = ")
        + utils_obj.Int64VectorToString(&input_vector)
        + static_cast<std::string>(" : ");

    result_vector_vector.clear();
    spsr_obj.GenerateNThreadsPermutations(
        nthreads, &input_vector,
        &result_vector,
        &result_vector_vector,
        &symmetric_map);

    shouldbe_size = static_cast<int64_t>(shouldbe_vector.size());
    result_size = static_cast<int64_t>(result_vector.size());

    err_part_1 =
        err_start
        + static_cast<std::string>(", shouldbe_vector = ")
        + utils_obj.Int64VectorToString(&shouldbe_vector)
        + static_cast<std::string>(", result_vector = ")
        + utils_obj.Int64VectorToString(&result_vector);

    error_message =
        err_part_1
        + static_cast<std::string>(", shouldbe size = ")
        + std::to_string(shouldbe_size)
        + static_cast<std::string>(", result size = ")
        + std::to_string(result_size);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_size, result_size);

    for (jj = 0L; jj < shouldbe_size; ++jj) {
      error_message =
        err_part_1
        + static_cast<std::string>(", (jj=")
        + std::to_string(jj)
        + static_cast<std::string>(") : shouldbe=")
        + std::to_string(shouldbe_vector[jj])
        + static_cast<std::string>(", result=")
        + std::to_string(result_vector[jj]);

      CPPUNIT_ASSERT_EQUAL_MESSAGE(
          error_message,
          shouldbe_vector[jj],
          result_vector[jj]);
    }

    shouldbe_size =
        static_cast<int64_t>(shouldbe_vector_vector.size());
    result_size =
        static_cast<int64_t>(result_vector_vector.size());

    err_part_1 =
        err_start
        + static_cast<std::string>(
            ", shouldbe_vector_vector size = ")
        + std::to_string(shouldbe_size)
        + static_cast<std::string>(
            ", result_vector_vector size = ")
        + std::to_string(result_size);

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        err_part_1, shouldbe_size, result_size);


    for (jj = 0L; jj < shouldbe_size; ++jj) {
      ss_vector = shouldbe_vector_vector[jj];
      rr_vector = result_vector_vector[jj];

      err_part_1 =
          err_start
          + static_cast<std::string>(
              ", shouldbe_vector_vector[")
          + std::to_string(jj)
          + static_cast<std::string>("], ")
          + utils_obj.Int64VectorToString(&ss_vector)
          + static_cast<std::string>(", result_vector = ")
          + utils_obj.Int64VectorToString(&rr_vector);

      AssertTwoVectorsEqual(
          &err_part_1, &ss_vector, &rr_vector);
    }

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// [47, 65] : (22, 19, 24) (3, 11, 5) (25) (6, 23) (17) : count = 10
void TSpsrTest::TestSpsrStructVectorToHMirrorString1() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    int64_t width;
    std::vector<struct mylib::CSpsr::spsr_struct> input_vector;
    std::string shouldbe_string;
  };
  std::vector<atest_struct> test_vector = {
    { 33L,
      { { 18, 0, 0}, {15, 0, 18},
        {7, 15, 18}, {8, 15, 25},
        {14, 18, 0}, {4, 18, 14},
        {10, 22, 14}, {1, 22, 24},
        {9, 23, 24}, },
      "(15, 18) (8, 7) (4, 14) (1, 10) (9)",
    },
    { 33L,
      { {15, 0, 0}, {18, 0, 15},
        {8, 15, 0}, {7, 15, 8},
        {4, 18, 15}, {14, 18, 19},
        {1, 22, 8}, {10, 22, 9},
        {9, 23, 0}, },
      "(18, 15) (7, 8) (14, 4) (10, 1) (9)",
    },
    { 33L,
      { {14, 0, 0}, {10, 0, 14}, {9, 0, 24},
        {1, 9, 24}, {8, 9, 25},
        {4, 10, 14}, {7, 10, 18},
        {18, 14, 0}, {15, 17, 18}, },
      "(9, 10, 14) (8, 1) (7, 4) (18) (15)",
    },
    { 33L,
      { {9, 0, 0}, {10, 0, 9}, {14, 0, 19},
        {8, 9, 0}, {1, 9, 8}, {7, 10, 8},
        {4, 10, 15}, {18, 14, 15}, {15, 17, 0}, },
      "(14, 10, 9) (1, 8) (4, 7) (18) (15)"
    },
  };
  int64_t test_label_index;
  std::string shouldbe_string, result_string;
  std::vector<struct atest_struct>::const_iterator va_cit;
  std::vector<struct mylib::CSpsr::spsr_struct> input_vector;
  int64_t width;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestSpsrStructVector")
    + static_cast<std::string>("ToHMirrorString1:") };
  std::string input_bouwkamp_string;
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    width = va_cit->width;
    input_vector = va_cit->input_vector;
    shouldbe_string = va_cit->shouldbe_string;
    input_bouwkamp_string =
        spsr_obj.SpsrStructVectorToBouwkampString(&input_vector);

    result_string =
        spsr_obj.SpsrStructVectorToHMirrorString(
            width, &input_vector);

    error_message = sub_name
        + static_cast<std::string>(" (")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), width = ")
        + std::to_string(width)
        + static_cast<std::string>(", input = ")
        + input_bouwkamp_string
        + static_cast<std::string>(", shouldbe string = ")
        + shouldbe_string
        + static_cast<std::string>(", result string = ")
        + result_string
        + static_cast<std::string>(" : ");

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// [47, 65] : (22, 19, 24) (3, 11, 5) (25) (6, 23) (17) : count = 10
void TSpsrTest::TestSpsrStructVectorToVMirrorString1() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    int64_t height;
    std::vector<struct mylib::CSpsr::spsr_struct> input_vector;
    std::string shouldbe_string;
  };
  std::vector<atest_struct> test_vector = {
    { 32L,
      { { 18, 0, 0}, {15, 0, 18},
        {7, 15, 18}, {8, 15, 25},
        {14, 18, 0}, {4, 18, 14},
        {10, 22, 14}, {1, 22, 24},
        {9, 23, 24},
      },
      "(14, 10, 9) (1, 8) (4, 7) (18) (15)",
    },
    { 32L,
      { {15, 0, 0}, {18, 0, 15},
        {8, 15, 0}, {7, 15, 8},
        {4, 18, 15}, {14, 18, 19},
        {1, 22, 8}, {10, 22, 9},
        {9, 23, 0},
      },
      "(9, 10, 14) (8, 1) (7, 4) (18) (15)",
    },
    { 32L,
      { {14, 0, 0}, {10, 0, 14}, {9, 0, 24},
        {1, 9, 24}, {8, 9, 25},
        {4, 10, 14}, {7, 10, 18},
        {18, 14, 0}, {15, 17, 18},
      },
      "(18, 15) (7, 8) (14, 4) (10, 1) (9)",
    },
    { 32L,
      { {9, 0, 0}, {10, 0, 9}, {14, 0, 19},
        {8, 9, 0}, {1, 9, 8}, {7, 10, 8},
        {4, 10, 15}, {18, 14, 15}, {15, 17, 0},
      },
      "(15, 18) (8, 7) (4, 14) (1, 10) (9)"
    },
  };
  int64_t test_label_index;
  std::string shouldbe_string, result_string;
  std::vector<struct atest_struct>::const_iterator va_cit;
  std::vector<struct mylib::CSpsr::spsr_struct> input_vector;
  int64_t height;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestSpsrStructVector")
    + static_cast<std::string>("ToVMirrorString1:") };
  std::string input_bouwkamp_string;
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    height = va_cit->height;
    input_vector = va_cit->input_vector;
    shouldbe_string = va_cit->shouldbe_string;
    input_bouwkamp_string =
        spsr_obj.SpsrStructVectorToBouwkampString(&input_vector);

    result_string =
        spsr_obj.SpsrStructVectorToVMirrorString(
            height, &input_vector);

    error_message = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), height = ")
        + std::to_string(height)
        + static_cast<std::string>(", input = ")
        + input_bouwkamp_string
        + static_cast<std::string>(", shouldbe string = ")
        + shouldbe_string
        + static_cast<std::string>(", result string = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

// #############################################################
// #############################################################
// [47, 65] : (22, 19, 24) (3, 11, 5) (25) (6, 23) (17) : count = 10
void TSpsrTest::TestSpsrStructVectorToHVMirrorString1() {
  mylib::CSpsr spsr_obj;
  struct atest_struct {
    int64_t height;
    int64_t width;
    std::vector<struct mylib::CSpsr::spsr_struct> input_vector;
    std::string shouldbe_string;
  };
  std::vector<atest_struct> test_vector = {
    { 32L, 33L,
      { { 18, 0, 0}, {15, 0, 18},
        {7, 15, 18}, {8, 15, 25},
        {14, 18, 0}, {4, 18, 14},
        {10, 22, 14}, {1, 22, 24},
        {9, 23, 24},
      },
      "(9, 10, 14) (8, 1) (7, 4) (18) (15)",
    },
    { 32L, 33L,
      { {15, 0, 0}, {18, 0, 15},
        {8, 15, 0}, {7, 15, 8},
        {4, 18, 15}, {14, 18, 19},
        {1, 22, 8}, {10, 22, 9},
        {9, 23, 0},
      },
      "(14, 10, 9) (1, 8) (4, 7) (18) (15)",
    },
    { 32L, 33L,
      { {14, 0, 0}, {10, 0, 14}, {9, 0, 24},
        {1, 9, 24}, {8, 9, 25},
        {4, 10, 14}, {7, 10, 18},
        {18, 14, 0}, {15, 17, 18},
      },
      "(15, 18) (8, 7) (4, 14) (1, 10) (9)",
    },
    { 32L, 33L,
      { {9, 0, 0}, {10, 0, 9}, {14, 0, 19},
        {8, 9, 0}, {1, 9, 8}, {7, 10, 8},
        {4, 10, 15}, {18, 14, 15}, {15, 17, 0},
      },
      "(18, 15) (7, 8) (14, 4) (10, 1) (9)",
    },
  };
  int64_t test_label_index;
  std::string shouldbe_string, result_string;
  std::vector<struct atest_struct>::const_iterator va_cit;
  std::vector<struct mylib::CSpsr::spsr_struct> input_vector;
  int64_t height, width;
  std::string sub_name = {
    static_cast<std::string>("TSpsrTest::")
    + static_cast<std::string>("TestSpsrStructVector")
    + static_cast<std::string>("ToHVMirrorString1:") };
  std::string input_bouwkamp_string;
  std::string error_message;

  test_label_index = 0L;
  for (va_cit = test_vector.cbegin();
       va_cit != test_vector.cend(); ++va_cit) {
    height = va_cit->height;
    width = va_cit->width;
    input_vector = va_cit->input_vector;
    shouldbe_string = va_cit->shouldbe_string;
    input_bouwkamp_string =
        spsr_obj.SpsrStructVectorToBouwkampString(&input_vector);

    result_string =
        spsr_obj.SpsrStructVectorToHVMirrorString(
            height, width, &input_vector);

    error_message = sub_name
        + static_cast<std::string>("(")
        + std::to_string(test_label_index)
        + static_cast<std::string>("), height = ")
        + std::to_string(height)
        + static_cast<std::string>(", width = ")
        + std::to_string(width)
        + static_cast<std::string>(", input = ")
        + input_bouwkamp_string
        + static_cast<std::string>(", shouldbe string = ")
        + shouldbe_string
        + static_cast<std::string>(", result string = ")
        + result_string;

    CPPUNIT_ASSERT_EQUAL_MESSAGE(
        error_message, shouldbe_string, result_string);

    ++test_label_index;
  }

  return;
}

}  // end of namespace mylib_test
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/
