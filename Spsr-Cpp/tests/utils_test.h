#ifndef TESTS_UTILS_TEST_H_
#define TESTS_UTILS_TEST_H_
/*

// Copyright 2024 robert haramoto

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###
*/

/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  utils tests                                           ###
  ###                                                        ###
  ###  last updated May 4, 2024                              ###
  ###                                                        ###
  ###  updated July 31, 2022                                 ###
  ###                                                        ###
  ###  updated February 5, 2020                              ###
  ###                                                        ###
  ###  written by Robert Haramoto                            ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <string.h>

#include "sources/utils.h"

namespace test_mylib {

class TUtilsTest : public CPPUNIT_NS :: TestFixture {
  CPPUNIT_TEST_SUITE(TUtilsTest);

  CPPUNIT_TEST(TestCommafyNum1);
  CPPUNIT_TEST(TestCommafyMpzNum1);
  CPPUNIT_TEST(TestMpzNumToSciString1);
  CPPUNIT_TEST(TestMpzNumToNiceSciString1);

  CPPUNIT_TEST(TestRoundFloat1);
  CPPUNIT_TEST(TestRoundFloatM1);

  CPPUNIT_TEST(TestDoubleToString1);

  CPPUNIT_TEST(TestLowerCase1);
  CPPUNIT_TEST(TestStringTrim1);
  CPPUNIT_TEST(TestStringRemoveComment1);
  CPPUNIT_TEST(TestStringTokenize1);
  CPPUNIT_TEST(TestStringTokenize2);
  CPPUNIT_TEST(TestStringRemoveCommas1);
  CPPUNIT_TEST(TestStringToInteger1);
  CPPUNIT_TEST(TestStringToMpzClass1);

  CPPUNIT_TEST(TestIntVectorToString1);
  CPPUNIT_TEST(TestInt64VectorToString1);
  CPPUNIT_TEST(TestInt64SequenceVectorToString1);
  CPPUNIT_TEST(TestMpzVectorToString1);
  CPPUNIT_TEST(TestMpzSequenceVectorToString1);
  CPPUNIT_TEST(TestInt64SetToString1);
  CPPUNIT_TEST(TestMpzSetToString1);
  CPPUNIT_TEST(TestMapInt64Int64ToString1);
  CPPUNIT_TEST(TestMapInt64BoolToString1);
  CPPUNIT_TEST(TestMapMpzBoolToString1);

  CPPUNIT_TEST(TestVector1EqualsVector2);
  CPPUNIT_TEST(TestClockDurationInSeconds1);

  CPPUNIT_TEST_SUITE_END();

 public:
  TUtilsTest();
  ~TUtilsTest();

  // do not allow copying nor moving
  TUtilsTest(const TUtilsTest&) = delete;
  TUtilsTest& operator=(const TUtilsTest&) = delete;
  TUtilsTest(TUtilsTest&&) = delete;
  TUtilsTest& operator=(TUtilsTest&&) = delete;

  void setUp();
  void tearDown();

  void TestCommafyNum1();
  void TestCommafyMpzNum1();
  void TestMpzNumToSciString1();
  void TestMpzNumToNiceSciString1();

  void TestRoundFloat1();
  void TestRoundFloatM1();

  void TestDoubleToString1();

  void TestLowerCase1();
  void TestStringTrim1();
  void TestStringRemoveComment1();
  void TestStringTokenize1();
  void TestStringTokenize2();
  void TestStringRemoveCommas1();
  void TestStringToInteger1();
  void TestStringToMpzClass1();

  void TestIntVectorToString1();
  void TestInt64VectorToString1();
  void TestInt64SequenceVectorToString1();
  void TestMpzVectorToString1();
  void TestMpzSequenceVectorToString1();
  void TestInt64SetToString1();
  void TestMpzSetToString1();
  void TestMapInt64Int64ToString1();
  void TestMapInt64BoolToString1();
  void TestMapMpzBoolToString1();

  void TestVector1EqualsVector2();
  void TestClockDurationInSeconds1();
};  // class TUtilsTest

}  // namespace test_mylib

#endif  // TESTS_UTILS_TEST_H_
/*
  ##############################################################
  ##############################################################
  ###                                                        ###
  ###  end of file                                           ###
  ###                                                        ###
  ##############################################################
  ##############################################################
*/
