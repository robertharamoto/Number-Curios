;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  brute force simple perfect squared rectangle         ###
;;;###  https://www.primepuzzles.net/problems/prob_048.htm   ###
;;;###                                                       ###
;;;###  last updated September 12, 2024                      ###
;;;###                                                       ###
;;;###  updated June 19, 2022                                ###
;;;###                                                       ###
;;;###  updated March 19, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start bf-spsr-modules - brute force version
(define-module (bf-spsr-module)
  #:export (make-first-bowkamp-row
            write-spsr-block-at!
            initialize-array-with-list!
            erase-spsr-block-at!
            does-spsr-block-fit?
            is-perfect-dissection?

            find-first-available-row-column
            find-first-available-max-block-size

            spsr-block-list-to-bouwkamp-string
            spsr-block-list-to-percent-string

            initialize-array-with-list!

            reverse-vector-k-to-n
            next-lexicographic-permutation

            main-loop-v0
            main-loop-v1
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### commafy function
(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;### write-config file
(use-modules ((config-module)
              :renamer (symbol-prefix-proc 'config-module:)))

;;;### is-prime?
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;#############################################################
;;;#############################################################
;;;###  simple perfect squared rectangle routines
;;;###

;;;#############################################################
;;;#############################################################
;;;### write-spr-block-at - given the current row and column,
;;;### write this block at the (row, column) coordinates
;;;### empty space is marked with a 0, filled space is marked
;;;### with the size of the block (length or width of the rectangle)
;;;### assumes that the block will fit, call does-spsr-block-fit? first
(define (write-spsr-block-at!
         rectangle-array max-rows max-columns block-size
         current-row current-column)
  (begin
    (if (and
         (<= (+ current-row block-size) max-rows)
         (<= (+ current-column block-size) max-columns))
        (begin
          (do ((ii 0 (1+ ii)))
              ((>= ii block-size))
            (begin
              (let ((this-row (+ current-row ii)))
                (begin
                  (do ((jj 0 (1+ jj)))
                      ((>= jj block-size))
                    (begin
                      (let ((this-col (+ current-column jj)))
                        (begin
                          (array-set!
                           rectangle-array block-size
                           this-row this-col)
                          ))
                      ))
                  ))
              ))
          #t)
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (erase-spsr-block-at!
         rectangle-array
         max-rows max-columns block-size
         current-row current-column)
  (begin
    (do ((ii 0 (1+ ii)))
        ((>= ii block-size))
      (begin
        (let ((this-row (+ current-row ii)))
          (begin
            (if (< this-row max-rows)
                (begin
                  (do ((jj 0 (1+ jj)))
                      ((>= jj block-size))
                    (begin
                      (let ((this-col (+ current-column jj)))
                        (begin
                          (if (< this-col max-columns)
                              (begin
                                (array-set!
                                 rectangle-array
                                 0 this-row this-col)
                                ))
                          ))
                      ))
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### does-spr-block-fit - given the current row and column,
;;;### can this block fit at the (row, column) coordinates
;;;### empty space is marked with a 0, filled space is marked
;;;### with the size of the block (length or width of the rectangle)
(define (does-spsr-block-fit?
         rectangle-array
         max-rows max-columns block-size
         current-row current-column)
  (begin
    (let ((break-flag #f))
      (begin
        (if (or
             (>=
              block-size
              (min max-rows max-columns))
             (>=
              (- (+ current-row block-size) 1)
              max-rows)
             (>=
              (- (+ current-column block-size) 1)
              max-columns))
            (begin
              #f)
            (begin
              (do ((ii 0 (1+ ii)))
                  ((or (>= ii block-size)
                       (equal? break-flag #t)))
                (begin
                  (let ((this-row (+ current-row ii)))
                    (begin
                      (do ((jj 0 (1+ jj)))
                          ((or (>= jj block-size)
                               (equal? break-flag #t)))
                        (begin
                          (let ((this-col (+ current-column jj)))
                            (let ((this-elem
                                   (array-ref
                                    rectangle-array this-row this-col)))
                              (begin
                                (if (not (zero? this-elem))
                                    (begin
                                      (set! break-flag #t)
                                      ))
                                )))
                          ))
                      ))
                  ))

              (if (equal? break-flag #t)
                  (begin
                    #f)
                  (begin
                    #t
                    ))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-perfect-dissection?
         rectangle-array max-rows max-columns)
  (begin
    (let ((break-flag #f))
      (begin
        (do ((ii 0 (1+ ii)))
            ((or (>= ii max-rows)
                 (equal? break-flag #t)))
          (begin
            (do ((jj 0 (1+ jj)))
                ((or (>= jj max-columns)
                     (equal? break-flag #t)))
              (begin
                (let ((this-elem
                       (array-ref
                        rectangle-array ii jj)))
                  (begin
                    (if (= this-elem 0)
                        (begin
                          (set! break-flag #t)
                          ))
                    ))
                ))
            ))
        (if (equal? break-flag #t)
            (begin
              #f)
            (begin
              #t
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (find-first-available-row-column
         rectangle-array max-rows max-columns
         last-row last-col)
  (begin
    (let ((this-row -1)
          (this-col -1)
          (break-flag #f))
      (begin
        (do ((ii last-row (1+ ii)))
            ((or (>= ii max-rows)
                 (equal? break-flag #t)))
          (begin
            (do ((jj 0 (1+ jj)))
                ((or (>= jj max-columns)
                     (equal? break-flag #t)))
              (begin
                (let ((this-elem
                       (array-ref rectangle-array ii jj)))
                  (begin
                    (if (= this-elem 0)
                        (begin
                          (set! this-row ii)
                          (set! this-col jj)
                          (set! break-flag #t)
                          ))
                    ))
                ))
            ))
        (list this-row this-col)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (find-first-available-max-block-size
         rectangle-array max-rows max-columns
         this-row this-col)
  (begin
    (let ((max-row-size -1)
          (max-col-size -1)
          (break-flag #f))
      (begin
        (if (and (>= this-row 0)
                 (>= this-col 0))
            (begin
              (do ((ii this-row (1+ ii)))
                  ((or (>= ii max-rows)
                       (equal? break-flag #t)))
                (begin
                  (let ((this-elem
                         (array-ref
                          rectangle-array ii this-col)))
                    (begin
                      (if (not (= this-elem 0))
                          (begin
                            (if (not (= ii this-row))
                                (begin
                                  (set!
                                   max-row-size
                                   (- ii this-row))
                                  ))
                            (set! break-flag #t)
                            ))
                      ))
                  ))
              (if (equal? break-flag #f)
                  (begin
                    (set!
                     max-row-size
                     (- max-rows this-row))
                    ))

              (set! break-flag #f)
              (do ((jj this-col (1+ jj)))
                  ((or (>= jj max-columns)
                       (equal? break-flag #t)))
                (begin
                  (let ((this-elem
                         (array-ref
                          rectangle-array this-row jj)))
                    (begin
                      (if (not (= this-elem 0))
                          (begin
                            (if (not (= jj this-col))
                                (begin
                                  (set!
                                   max-col-size (- jj this-col))
                                  ))
                            (set! break-flag #t)
                            ))
                      ))
                  ))
              (if (equal? break-flag #f)
                  (begin
                    (set!
                     max-col-size (- max-columns this-col))
                    ))
              ))
        (min max-row-size max-col-size)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-rectangle-array
         rectangle-array max-rows max-columns)
  (begin
    (do ((ii 0 (1+ ii)))
        ((>= ii max-rows))
      (begin
        (do ((jj 0 (1+ jj)))
            ((>= jj max-columns))
          (begin
            (display
             (ice-9-format:format
              #f "~4a " (array-ref rectangle-array ii jj)))
            ))
        (newline)
        ))
    (force-output)
    ))

;;;#############################################################
;;;#############################################################
(define (spsr-block-list-to-bouwkamp-string
         used-spsr-list)
  (begin
    (let ((last-row -1)
          (result-string ""))
      (begin
        (for-each
         (lambda (list-elem)
           (begin
             (let ((this-size (list-ref list-elem 0))
                   (this-row (list-ref list-elem 1)))
               (begin
                 (if (not (equal? this-row last-row))
                     (begin
                       (if (equal? last-row -1)
                           (begin
                             (set!
                              result-string
                              (string-append
                               result-string
                               (ice-9-format:format
                                #f "(~:d" this-size))))
                           (begin
                             (set!
                              result-string
                              (string-append
                               result-string
                               (ice-9-format:format
                                #f ") (~:d" this-size)))
                             )))
                     (begin
                       (set!
                        result-string
                        (string-append
                         result-string
                         (ice-9-format:format
                          #f ", ~:d" this-size)))
                       ))
                 (set! last-row this-row)
                 ))
             )) used-spsr-list)

        (if (> (length used-spsr-list) 0)
            (begin
              (set!
               result-string
               (string-append result-string ")"))
              ))

        result-string
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (spsr-block-list-to-percent-string used-spsr-list)
  (begin
    (let ((total-blocks 0)
          (total-primes 0))
      (begin
        (for-each
         (lambda (list-elem)
           (begin
             (let ((this-size (list-ref list-elem 0)))
               (begin
                 (set! total-blocks (+ total-blocks 1))
                 (if (equal?
                      (prime-module:is-prime? this-size) #t)
                     (begin
                       (set! total-primes (+ total-primes 1))
                       ))
                 ))
             )) used-spsr-list)

        (if (> total-blocks 0)
            (begin
              (ice-9-format:format
               #f "primes/total = ~:d / ~:d = ~4,1f%"
               total-primes total-blocks
               (utils-module:round-float
                (* 100.0 (/ total-primes total-blocks)) 1.0)))
            (begin
              ""
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-perfect-rectangle-dissection
         rectangle-array max-rows max-columns
         used-spsr-list
         display-square-flag)
  (begin
    (if (equal? display-square-flag #t)
        (begin
          (display-rectangle-array
           rectangle-array max-rows max-columns)
          ))

    (let ((bouwkamp-string
           (spsr-block-list-to-bouwkamp-string
            used-spsr-list))
          (percent-string
           (spsr-block-list-to-percent-string
            used-spsr-list)))
      (begin
        (display
         (ice-9-format:format
          #f "[~:d, ~:d] : ~a : ~a~%"
          max-rows max-columns
          bouwkamp-string percent-string))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (initialize-array-with-list!
         rectangle-array max-rows max-cols b-list)
  (begin
    (let ((c-row 0)
          (c-col 0))
      (begin
        (for-each
         (lambda (b-num)
           (begin
             (write-spsr-block-at!
              rectangle-array max-rows max-cols b-num
              c-row c-col)
             (set! c-col (+ c-col b-num))
             )) b-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax block-size-loop-macro
  (syntax-rules ()
    ((block-size-loop-macro
      rectangle-array max-rows max-cols
      max-block-size block-size
      this-row this-col
      last-row last-col
      used-htable used-spsr-list
      acc-list-list
      display-square-flag start-jday
      status-in-minutes)
     (begin
       (let ((bfit
              (does-spsr-block-fit?
               rectangle-array max-rows max-cols
               block-size this-row this-col)))
         (begin
           (if (equal? bfit #t)
               (begin
                 (let ((this-spsr-list
                        (list block-size this-row this-col))
                       (wflag
                        (write-spsr-block-at!
                         rectangle-array max-rows max-cols
                         block-size this-row this-col)))
                   (let ((next-used-spsr-list
                          (cons this-spsr-list used-spsr-list)))
                     (begin
                       (if (equal? wflag #t)
                           (begin
                             (hash-set!
                              used-htable block-size #t)

                             (let ((result-list-list
                                    (rec-add-squares
                                     rectangle-array max-rows max-cols
                                     max-block-size this-row this-col
                                     last-row last-col
                                     used-htable next-used-spsr-list
                                     acc-list-list
                                     display-square-flag start-jday
                                     status-in-minutes)))
                               (let ((next-acc-list-list
                                      (list-ref result-list-list 0))
                                     (next-start-jday
                                      (list-ref result-list-list 1)))
                                 (begin
                                   (set! acc-list-list next-acc-list-list)
                                   (set! start-jday next-start-jday)
                                   )))

                             (hash-set! used-htable block-size #f)

                             (erase-spsr-block-at!
                              rectangle-array max-rows max-cols
                              block-size this-row this-col)
                             ))
                       )))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax status-loop-macro
  (syntax-rules ()
    ((status-loop-macro
      max-rows max-cols
      this-row this-col
      used-spsr-list
      start-jday status-in-minutes)
     (begin
       (let ((end-jday (srfi-19:current-julian-day)))
         (let ((elapsed-mins
                (timer-module:julian-day-difference-in-minutes
                 end-jday start-jday)))
           (begin
             (if (>= elapsed-mins status-in-minutes)
                 (begin
                   (display
                    (ice-9-format:format
                     #f "status [~:d x ~:d] : "
                     max-rows max-cols))
                   (display
                    (ice-9-format:format
                     #f "current row/col = [~:d, ~:d] : "
                     this-row this-col))
                   (display
                    (format #f "used-spsr-list = ~a"
                            used-spsr-list))
                   (display
                    (format
                     #f " : elapsed time = ~a : ~a~%"
                     (timer-module:julian-day-difference-to-string
                      end-jday start-jday)
                     (timer-module:current-date-time-string)))
                   (force-output)
                   (set! start-jday end-jday)
                   ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (rec-add-squares
         rectangle-array max-rows max-cols max-block-size
         this-row this-col
         last-row last-col
         used-htable used-spsr-list
         acc-list-list
         display-square-flag start-jday
         status-in-minutes)
  (begin
    (let ((first-list
           (find-first-available-row-column
            rectangle-array max-rows max-cols
            last-row last-col)))
      (let ((next-row (list-ref first-list 0))
            (next-col (list-ref first-list 1)))
        (let ((available-block-size
               (find-first-available-max-block-size
                rectangle-array max-rows max-cols
                next-row next-col)))
          (let ((mblock
                 (min max-block-size available-block-size)))
            (begin
              (if (and (>= next-row 0) (>= next-col 0))
                  (begin
                    (do ((ii-size 1 (1+ ii-size)))
                        ((> ii-size mblock))
                      (begin
                        (let ((block-used-bool
                               (hash-ref used-htable ii-size #f)))
                          (begin
                            (if (equal? block-used-bool #f)
                                (begin
                                  (block-size-loop-macro
                                   rectangle-array max-rows max-cols
                                   max-block-size
                                   ii-size next-row next-col
                                   this-row this-col
                                   used-htable used-spsr-list
                                   acc-list-list
                                   display-square-flag start-jday
                                   status-in-minutes)

                                  (status-loop-macro
                                   max-rows max-cols
                                   next-row next-col
                                   used-spsr-list
                                   start-jday status-in-minutes)
                                  ))
                            ))
                        )))
                  (begin
                    ;;;### no available rows or columns
                    (let ((perfect-bool
                           (is-perfect-dissection?
                            rectangle-array max-rows max-cols)))
                      (begin
                        (if (equal? perfect-bool #t)
                            (begin
                              (let ((end-jday (srfi-19:current-julian-day)))
                                (let ((tdiff-string
                                       (timer-module:julian-day-difference-to-string
                                        end-jday start-jday))
                                      (next-spsr-list (reverse used-spsr-list)))
                                  (begin
                                    (set!
                                     acc-list-list
                                     (cons next-spsr-list acc-list-list))
                                    (newline)
                                    (display-perfect-rectangle-dissection
                                     rectangle-array max-rows max-cols
                                     next-spsr-list display-square-flag)
                                    (display
                                     (format
                                      #f "elapsed time = ~a : ~a~%"
                                      tdiff-string
                                      (timer-module:current-date-time-string)))
                                    (force-output)
                                    (set! start-jday end-jday)
                                    )))
                              ))
                        ))
                    (gc)
                    ))
              ))

          (list acc-list-list start-jday)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (add-squares-to-array
         rectangle-array max-rows max-cols
         used-htable used-spsr-list
         display-square-flag start-jday
         status-in-minutes)
  (begin
    (let ((max-block-size (min max-rows max-cols)))
      (let ((rr-list
             (rec-add-squares
              rectangle-array max-rows max-cols
              max-block-size
              0 0 0 0
              used-htable used-spsr-list
              (list)
              display-square-flag start-jday
              status-in-minutes)))
        (begin
          rr-list
          )))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax begin-rectangle-fitting-macro
  (syntax-rules ()
    ((begin-rectangle-fitting-macro
      rectangle-array max-rows max-cols
      max-block-size block-size
      current-list
      used-htable symmetric-row0-htable
      count
      display-square-flag start-jday
      status-in-minutes)
     (begin
       (array-fill! rectangle-array 0)
       (hash-clear! used-htable)

       (let ((used-spsr-list (list))
             (astring
              (utils-module:number-list-to-string
               (reverse current-list)))
             (current-col 0))
         (begin
           (let ((aflag
                  (hash-ref
                   symmetric-row0-htable astring #f)))
             (begin
               (if (equal? aflag #f)
                   (begin
                     (let ((bstring
                            (utils-module:number-list-to-string
                             current-list)))
                       (begin
                         (hash-set! symmetric-row0-htable
                                    bstring #t)
                         ))

                     (for-each
                      (lambda (b-num)
                        (begin
                          (hash-set! used-htable b-num #t)
                          (set!
                           used-spsr-list
                           (cons (list b-num 0 current-col)
                                 used-spsr-list))
                          (set! current-col (+ current-col b-num))
                          )) current-list)

                     (set! used-spsr-list (reverse used-spsr-list))

                     (initialize-array-with-list!
                      rectangle-array max-rows max-cols
                      (reverse current-list))

                     (let ((rr-list
                            (add-squares-to-array
                             rectangle-array max-rows max-cols
                             used-htable used-spsr-list
                             display-square-flag start-jday
                             status-in-minutes)))
                       (begin
                         (set! start-jday (list-ref rr-list 1))
                         (set! count (1+ count))
                         ))
                     ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; starting loop calculates all possible ways to add up to max-cols
;;; a starting way gives you the first row of the bouwkamp string
;;; for example if max-cols = 10, then (2 3 5) adds to 10 and is
;;; one possibility that tiles the first row of the rectanble.
(define (starting-loop
         rectangle-array max-rows max-cols
         used-htable
         display-square-flag start-jday
         status-in-minutes)
  (define (local-recursive-loop
           rectangle-array max-rows max-cols
           used-htable display-square-flag
           count start-jday status-in-minutes
           max-block-size max-dim
           seen-htable symmetric-row0-htable
           current-sum current-list)
    (begin
      (cond
       ((= current-sum max-dim)
        (begin
          (begin-rectangle-fitting-macro
           rectangle-array max-rows max-cols
           max-block-size block-size
           current-list
           used-htable symmetric-row0-htable
           count
           display-square-flag start-jday
           status-in-minutes)

          (list count start-jday)
          ))
       ((> current-sum max-dim)
        (begin
          (list count start-jday)
          ))
       (else
        (begin
          (let ((continue-loop-flag #t)
                (stat-counter 0)
                (start-ii 4))
            (begin
              (do ((ii start-ii (1+ ii)))
                  ((or (> ii max-block-size)
                       (equal? continue-loop-flag #f)))
                (begin
                  (let ((iflag
                         (hash-ref seen-htable ii #f)))
                    (begin
                      (if (equal? iflag #f)
                          (begin
                            (hash-set! seen-htable ii #t)
                            (let ((next-sum (+ current-sum ii)))
                              (begin
                                (if (<= next-sum max-dim)
                                    (begin
                                      (let ((next-list
                                             (local-recursive-loop
                                              rectangle-array
                                              max-rows max-cols
                                              used-htable
                                              display-square-flag
                                              count
                                              start-jday status-in-minutes
                                              max-block-size max-dim
                                              seen-htable
                                              symmetric-row0-htable
                                              next-sum
                                              (cons ii current-list))))
                                        (begin
                                          (set!
                                           count
                                           (list-ref next-list 0))
                                          (set!
                                           start-jday
                                           (list-ref next-list 1))

                                          (set! stat-counter (1+ stat-counter))
                                          (if (zero? (modulo stat-counter 10))
                                              (begin
                                                (status-loop-macro
                                                 max-rows max-cols
                                                 0 next-sum
                                                 current-list
                                                 start-jday status-in-minutes)
                                                ))
                                          )))
                                    (begin
                                      (set! continue-loop-flag #f)
                                      ))
                                ))

                            (hash-set! seen-htable ii #f)
                            ))
                      ))
                  ))
              ))
          (list count start-jday)
          )))
      ))
  (begin
    (let ((max-block-size (min max-rows max-cols))
          (max-dim (max max-rows max-cols))
          (seen-htable (make-hash-table 100))
          (symmetric-row0-htable (make-hash-table 10000)))
      (let ((rlist
             (local-recursive-loop
              rectangle-array max-rows max-cols
              used-htable display-square-flag
              0 start-jday status-in-minutes
              max-block-size max-dim
              seen-htable symmetric-row0-htable
              0 (list))))
        (begin
          (let ((ncount (list-ref rlist 0)))
            (begin
              (display
               (ice-9-format:format
                #f "~:d = number of ways to add up to ~:d~%"
                ncount max-cols))
              (force-output)
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop-v0
         start-row end-row start-col end-col
         display-square-flag status-in-minutes)
  (begin
    (let ((used-htable (make-hash-table 100))
          (counter 1)
          (end-row-counter 10))
      (begin
        ;;;### cycle over different array sizes
        (do ((ii-max-row start-row (1+ ii-max-row)))
            ((> ii-max-row end-row))
          (begin
            (do ((jj-max-col start-col (1+ jj-max-col)))
                ((> jj-max-col end-col))
              (begin
                (let ((start-jday (srfi-19:current-julian-day)))
                  (begin
                    (if (<= ii-max-row jj-max-col)
                        (begin
                          (hash-clear! used-htable)
                          (display
                           (ice-9-format:format
                            #f "[~:d, ~:d] "
                            ii-max-row jj-max-col))
                          (if (zero? (modulo counter end-row-counter))
                              (begin
                                (newline)
                                ))
                          (force-output)

                          (let ((rectangle-array
                                 (make-array 0 ii-max-row jj-max-col)))
                            (begin
                              (starting-loop
                               rectangle-array ii-max-row jj-max-col
                               used-htable
                               display-square-flag start-jday
                               status-in-minutes)
                              ))

                          (set! counter (1+ counter))
                          ))
                    ))
                (gc)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  start of version v1 subroutines                      ###
;;;###  first find x1^2 + x2^2 + ... + xn^2 = rows x cols    ###
;;;###  then see of some ordering can tile the rectangle     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (reverse-vector-k-to-n this-vector k)
  (begin
    (let ((vlen (vector-length this-vector))
          (result-vector (vector-copy this-vector)))
      (let ((index-diff (- vlen k)))
        (begin
          (cond
           ((< index-diff 0)
            (begin
              result-vector
              ))
           (else
            (begin
              (let ((ii1 k)
                    (ii2 (- vlen 1))
                    (half-diff (euclidean/ index-diff 2)))
                (begin
                  (do ((jj 0 (+ jj 1)))
                      ((or (>= jj half-diff)
                           (>= ii1 ii2)
                           (>= ii1 vlen)
                           (< ii2 0)
                           ))
                    (begin
                      (let ((v1 (vector-ref result-vector ii1))
                            (v2 (vector-ref result-vector ii2)))
                        (begin
                          (vector-set! result-vector ii1 v2)
                          (vector-set! result-vector ii2 v1)
                          (set! ii1 (+ ii1 1))
                          (set! ii2 (- ii2 1))
                          ))
                      ))
                  result-vector
                  ))
              )))
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; note assumes that this-vector is sorted in ascending order
(define (next-lexicographic-permutation this-vector)
  (begin
    (let ((vlen (1- (vector-length this-vector)))
          (result-vector (vector-copy this-vector))
          (kk 0)
          (aakk 0)
          (ll 0)
          (aall 0)
          (permutation-exists #f))
      (begin
        ;;; 1) find largest kk such that a[kk] < a[kk+1]
        (do ((ii 0 (1+ ii)))
            ((>= ii vlen))
          (begin
            (let ((v1 (vector-ref result-vector ii))
                  (v2 (vector-ref result-vector (1+ ii))))
              (begin
                (if (< v1 v2)
                    (begin
                      (set! permutation-exists #t)
                      (set! kk ii)
                      (set! aakk v1)
                      ))
                ))
            ))

        ;;; 2) find the largest ll such that a[kk] < a[ll]
        (if (equal? permutation-exists #t)
            (begin
              (do ((ii (+ kk 1) (1+ ii)))
                  ((> ii vlen))
                (begin
                  (let ((v1 (vector-ref result-vector ii)))
                    (begin
                      (if (< aakk v1)
                          (begin
                            (set! ll ii)
                            (set! aall v1)
                            ))
                      ))
                  ))

              ;;; 3) swap a[kk] with a[ll]
              (vector-set! result-vector kk aall)
              (vector-set! result-vector ll aakk)

              ;;; 4) reverse the sequence from (k+1) on
              (let ((final-result
                     (reverse-vector-k-to-n
                      result-vector (+ kk 1))))
                (begin
                  final-result
                  )))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### init-rectangle! - write 0s into the rectangular array
(define (init-rectangle! rectangle-array max-rows max-columns)
  (begin
    (do ((ii 0 (1+ ii)))
        ((>= ii max-rows))
      (begin
        (do ((jj 0 (1+ jj)))
            ((>= jj max-columns))
          (begin
            (array-set! rectangle-array 0 ii jj)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax examine-block-do-loop-macro
  (syntax-rules ()
    ((examine-block-do-loop-macro
      rectangle-array max-rows max-cols
      ii used-block-vector
      this-row this-col
      this-spsr-list
      used-htable
      doesnt-fit-flag)
     (begin
       (let ((this-block
              (vector-ref used-block-vector ii)))
         (let ((bfit
                (does-spsr-block-fit?
                 rectangle-array max-rows max-cols
                 this-block this-row this-col)))
           (begin
             (if (equal? bfit #t)
                 (begin
                   (let ((an-spsr-element
                          (list this-block this-row this-col)))
                     (begin
                       (write-spsr-block-at!
                        rectangle-array max-rows max-cols
                        this-block this-row this-col)

                       (let ((next-used-spsr-list
                              (cons an-spsr-element this-spsr-list)))
                         (begin
                           (hash-set! used-htable this-block #t)

                           (let ((rr-list
                                  (find-first-available-row-column
                                   rectangle-array max-rows max-cols
                                   this-row this-col)))
                             (let ((next-row (list-ref rr-list 0))
                                   (next-col (list-ref rr-list 1)))
                               (begin
                                 (set! this-row next-row)
                                 (set! this-col next-col)
                                 (set! this-spsr-list next-used-spsr-list)
                                 )))
                           ))
                       )))
                 (begin
                   (set! doesnt-fit-flag #t)
                   ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax examine-block-found-spsr-macro
  (syntax-rules ()
    ((examine-block-found-spsr-macro
      rectangle-array max-rows max-cols
      this-row this-col
      used-spsr-list
      used-htable display-square-flag
      perfect-bool)
     (begin
       (if (equal? perfect-bool #t)
           (begin
             (let ((next-spsr-list (reverse used-spsr-list)))
               (begin
                 (newline)
                 (display-perfect-rectangle-dissection
                  rectangle-array max-rows max-cols
                  next-spsr-list display-square-flag)
                 (display
                  (format
                   #f "~a~%"
                   (timer-module:current-date-time-string)))
                 (force-output)
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;;###  examine-block-configuration-v1 - see if current      ###
;;;###  ordered block configuration fits into rectangle      ###
;;;###  does it fit, in order                                ###
(define (examine-block-configuration-v1
         rectangle-array max-rows max-cols
         used-block-list
         used-htable
         count display-square-flag
         start-jday status-in-minutes)
  (begin
    (let ((examine-done-flag #f)
          (block-list-length (length used-block-list))
          (used-block-vector
           (list->vector (sort used-block-list <)))
          (this-spsr-list (list))
          (this-row 0)
          (this-col 0)
          (tmp-status-counter 0))
      (begin
        (while
         (equal? examine-done-flag #f)
         (begin
           (init-rectangle! rectangle-array max-rows max-cols)

           (let ((doesnt-fit-flag #f))
             (begin
               (do ((ii 0 (1+ ii)))
                   ((or (>= ii block-list-length)
                        (equal? examine-done-flag #t)))
                 (begin
                   (examine-block-do-loop-macro
                    rectangle-array max-rows max-cols
                    ii used-block-vector
                    this-row this-col
                    this-spsr-list
                    used-htable
                    doesnt-fit-flag)
                   ))

               (let ((perfect-bool
                      (is-perfect-dissection?
                       rectangle-array max-rows max-cols)))
                 (begin
                   (if (equal? perfect-bool #t)
                       (begin
                         (examine-block-found-spsr-macro
                          rectangle-array max-rows max-cols
                          this-row this-col
                          this-spsr-list
                          used-htable display-square-flag
                          perfect-bool)
                         ))
                   ))

               (set! tmp-status-counter (1+ tmp-status-counter))
               (if (zero? (modulo tmp-status-counter 100))
                   (begin
                     (let ((local-block-list
                            (vector->list used-block-vector)))
                       (begin
                         (status-loop-macro
                          max-rows max-cols
                          this-row this-col
                          local-block-list
                          start-jday status-in-minutes)
                         ))
                     ))

               (let ((next-block-vector
                      (next-lexicographic-permutation
                       used-block-vector)))
                 (begin
                   (if (equal? next-block-vector #f)
                       (begin
                         (set! examine-done-flag #t))
                       (begin
                         (set! used-block-vector
                               (vector-copy next-block-vector))
                         ))
                   ))
               ))
           (gc)
           ))
        (list count start-jday)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###  add-squares-to-array-v1 - given a valid list of      ###
;;;###  squares, try all possible arrangements of squares    ###
(define (add-squares-to-array-v1
         rectangle-array max-rows max-cols
         valid-list-length valid-list
         used-htable
         count display-square-flag
         start-jday status-in-minutes)
  (define (local-recursive-loop
           rectangle-array max-rows max-cols
           valid-list-length valid-list
           used-block-list
           used-htable
           count display-square-flag
           start-jday status-in-minutes)
    (begin
      (if (<= valid-list-length 0)
          (begin
            (let ((rr-list
                   (examine-block-configuration-v1
                    rectangle-array max-rows max-cols
                    used-block-list
                    used-htable
                    count display-square-flag
                    start-jday status-in-minutes)))
              (begin
                (set! count (list-ref rr-list 0))
                (set! start-jday (list-ref rr-list 1))
                )))
          (begin
            (let ((next-list-length (1- valid-list-length))
                  (local-vlist (list-copy valid-list)))
              (begin
                (for-each
                 (lambda (a-block)
                   (begin
                     (if (not (member a-block used-block-list))
                         (begin
                           (let ((next-valid-list
                                  (delete a-block local-vlist))
                                 (next-used-block-list
                                  (cons a-block used-block-list)))
                             (begin
                               (hash-set! used-htable a-block #t)

                               (let ((rr-list
                                      (local-recursive-loop
                                       rectangle-array
                                       max-rows max-cols
                                       next-list-length
                                       next-valid-list
                                       next-used-block-list
                                       used-htable
                                       count
                                       display-square-flag
                                       start-jday
                                       status-in-minutes)))
                                 (begin
                                   (set! count (list-ref rr-list 0))
                                   (set! start-jday (list-ref rr-list 1))
                                   ))

                               (hash-remove! used-htable a-block)
                               ))
                           ))
                     )) valid-list)
                ))
            ))
      (list count start-jday)
      ))
  (begin
    (let ((used-block-list (list)))
      (begin
        (hash-clear! used-htable)

        (let ((rr-list
               (local-recursive-loop
                rectangle-array
                max-rows max-cols
                valid-list-length
                valid-list
                used-block-list
                used-htable
                count
                display-square-flag
                start-jday
                status-in-minutes)))
          (begin
            rr-list
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax begin-rectangle-fitting-v1-macro
  (syntax-rules ()
    ((begin-rectangle-fitting-v1-macro
      rectangle-array max-rows max-cols
      valid-list
      used-htable symmetric-row0-htable
      count
      display-square-flag start-jday
      status-in-minutes)
     (begin
       (array-fill! rectangle-array 0)
       (hash-clear! used-htable)

       (let ((astring
              (utils-module:number-list-to-string
               (reverse valid-list))))
         (begin
           (let ((aflag
                  (hash-ref
                   symmetric-row0-htable astring #f)))
             (begin
               (if (equal? aflag #f)
                   (begin
                     (let ((bstring
                            (utils-module:number-list-to-string
                             valid-list)))
                       (begin
                         (hash-set! symmetric-row0-htable
                                    bstring #t)
                         ))

                     (let ((rr-list
                            (add-squares-to-array-v1
                             rectangle-array max-rows max-cols
                             (length valid-list) valid-list
                             used-htable
                             count display-square-flag
                             start-jday status-in-minutes)))
                       (begin
                         (set! count (list-ref rr-list 0))
                         (set! start-jday (list-ref rr-list 1))
                         ))

                     (set! count (1+ count))
                     ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; starting-loop-v1 calculates all possible ways to add up to max-cols
;;; times max-rows, a recursive loop checks to see if the sum
;;; of squares is equal to max-rows * max-cols, and if so, then
;;; check to see if a certain configuration can tile the rectangle
(define (starting-loop-v1
         rectangle-array max-rows max-cols
         used-htable display-square-flag start-jday
         status-in-minutes)
  (define (local-recursive-loop
           rectangle-array max-rows max-cols
           used-htable
           min-block-size
           max-block-size max-dim
           seen-htable symmetric-row0-htable
           target-sum-squares current-sum-squares
           current-list
           count display-square-flag
           start-jday status-in-minutes)
    (begin
      (cond
       ((> current-sum-squares target-sum-squares)
        (begin
          (gc)
          (list count start-jday)
          ))
       ((= current-sum-squares target-sum-squares)
        (begin
          (begin-rectangle-fitting-v1-macro
           rectangle-array max-rows max-cols
           current-list
           used-htable symmetric-row0-htable
           count display-square-flag
           start-jday status-in-minutes)

          (gc)
          (list count start-jday)
          ))
       (else
        (begin
          (let ((continue-loop-flag #t))
            (begin
              (do ((ii min-block-size (1+ ii)))
                  ((or (>= ii max-block-size)
                       (equal? continue-loop-flag #f)))
                (begin
                  (let ((iflag
                         (hash-ref seen-htable ii #f)))
                    (begin
                      (if (equal? iflag #f)
                          (begin
                            (hash-set! seen-htable ii #t)
                            (let ((next-sum
                                   (+ current-sum-squares (* ii ii))))
                              (begin
                                (if (<= next-sum target-sum-squares)
                                    (begin
                                      (let ((next-list
                                             (local-recursive-loop
                                              rectangle-array
                                              max-rows max-cols
                                              used-htable
                                              ii
                                              max-block-size max-dim
                                              seen-htable
                                              symmetric-row0-htable
                                              target-sum-squares
                                              next-sum
                                              (cons ii current-list)
                                              count
                                              display-square-flag
                                              start-jday
                                              status-in-minutes)))
                                        (begin
                                          (set!
                                           count
                                           (list-ref next-list 0))
                                          (set!
                                           start-jday
                                           (list-ref next-list 1))
                                          )))
                                    (begin
                                      (set! continue-loop-flag #f)
                                      ))
                                ))

                            (hash-set! seen-htable ii #f)
                            (gc)
                            ))
                      ))
                  ))
              ))
          (list count start-jday)
          )))
      ))
  (begin
    (let ((max-block-size (min max-rows max-cols))
          (min-block-size 1)
          (max-dim (max max-rows max-cols))
          (target-sum-squares (* max-rows max-cols))
          (seen-htable (make-hash-table 100))
          (symmetric-row0-htable (make-hash-table 10000)))
      (begin
        (init-rectangle! rectangle-array max-rows max-cols)

        (let ((rlist
               (local-recursive-loop
                rectangle-array max-rows max-cols
                used-htable
                min-block-size
                max-block-size max-dim
                seen-htable symmetric-row0-htable
                target-sum-squares 0
                (list)
                0 display-square-flag
                start-jday status-in-minutes)))
          (begin
            (let ((ncount (list-ref rlist 0)))
              (begin
                (display
                 (ice-9-format:format
                  #f "(~:d x ~:d) : number of simple perfect "
                  max-rows max-cols))
                (display
                 (ice-9-format:format
                  #f "squared rectangles found = ~:d~%"
                  ncount))
                (force-output)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop-v1
         start-row end-row start-col end-col
         display-square-flag status-in-minutes)
  (begin
    (let ((used-htable (make-hash-table 100))
          (counter 1)
          (end-row-counter 10))
      (begin
        ;;;### cycle over different array sizes
        (do ((ii-max-row start-row (1+ ii-max-row)))
            ((> ii-max-row end-row))
          (begin
            (do ((jj-max-col start-col (1+ jj-max-col)))
                ((> jj-max-col end-col))
              (begin
                (let ((start-jday (srfi-19:current-julian-day)))
                  (begin
                    (if (<= ii-max-row jj-max-col)
                        (begin
                          (hash-clear! used-htable)
                          (display
                           (ice-9-format:format
                            #f "[~:d, ~:d] "
                            ii-max-row jj-max-col))
                          (force-output)

                          (if (zero?
                               (modulo
                                counter end-row-counter))
                              (begin
                                (newline)
                                ))
                          (force-output)

                          (let ((rectangle-array
                                 (make-array
                                  0 ii-max-row jj-max-col)))
                            (begin
                              (starting-loop-v1
                               rectangle-array
                               ii-max-row jj-max-col
                               used-htable
                               display-square-flag
                               start-jday
                               status-in-minutes)
                              ))

                          (set! counter (1+ counter))
                          ))
                    ))
                (gc)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
