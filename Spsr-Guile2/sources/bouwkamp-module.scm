;;;### this is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  simple perfect squared rectangle                     ###
;;;###  integer partition method                             ###
;;;###                                                       ###
;;;###  last updated September 12, 2024                      ###
;;;###                                                       ###
;;;###  updated March 2, 2024                                ###
;;;###    fixed run-tests.scm errors                         ###
;;;###                                                       ###
;;;###  updated June 19, 2022                                ###
;;;###                                                       ###
;;;###  updated April 9, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start en-spsr-modules - electrical network version
(define-module (bouwkamp-module)
  #:export (convert-bouwkamp-to-rectangle-ll
            bouwkamp-added-to-rectangle-ll
            find-min-row-col-width

            reverse-vector-k-to-n
            next-lexicographic-permutation
            successor-of-partition-list

            check-if-ok-to-use
            is-rect-list-simple-perfect-dissection?
            used-partition-lists-contains-duplicates?

            partition-list-contains-duplicates?
            find-height

            precompute-partition-list-htable
            main-loop-v0
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### srfi-1 for fold functions
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### commafy function
(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;### is-prime?
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;#############################################################
;;;#############################################################
;;;###  simple perfect squared rectangle routines

;;;#############################################################
;;;#############################################################
(define-syntax added-to-rect-ll-01-macro
  (syntax-rules ()
    ((added-to-rect-ll-01-macro
      rect-list-lists
      prev-height
      min-start-row min-start-col
      min-end-row min-end-col)
     (begin
       (for-each
        (lambda (a-rect-list)
          (begin
            (let ((start-row (list-ref a-rect-list 0))
                  (start-col (list-ref a-rect-list 1))
                  (end-row (list-ref a-rect-list 2))
                  (end-col (list-ref a-rect-list 3)))
              (let ((height (- end-row start-row)))
                (begin
                  (if (or (< prev-height 0)
                          (< height prev-height))
                      (begin
                        (set! prev-height height)
                        (set! min-start-row start-row)
                        (set! min-start-col start-col)
                        (set! min-end-row end-row)
                        (set! min-end-col end-col)
                        ))
                  )))
            )) rect-list-lists)
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax added-to-rect-ll-02-macro
  (syntax-rules ()
    ((added-to-rect-ll-02-macro
      bk-list rect-list-lists
      min-start-row min-start-col
      min-end-row min-end-col
      new-rect-list-lists)
     (begin
       (for-each
        (lambda (a-rect-list)
          (begin
            (let ((start-row (list-ref a-rect-list 0))
                  (start-col (list-ref a-rect-list 1))
                  (end-row (list-ref a-rect-list 2))
                  (end-col (list-ref a-rect-list 3))
                  (next-start-row -1)
                  (next-start-col -1)
                  (next-end-row -1)
                  (next-end-col -1))
              (begin
                (if (and (= start-row min-start-row)
                         (= start-col min-start-col)
                         (= end-row min-end-row)
                         (= end-col min-end-col))
                    (begin
                      (let ((next-list-lists (list)))
                        (begin
                          (set! next-start-row start-row)
                          (set! next-start-col start-col)
                          (set! next-end-row end-row)
                          (set! next-end-col end-col)

                          (for-each
                           (lambda (bk-square)
                             (begin
                               (let ((bk-start-row next-start-row)
                                     (bk-start-col next-start-col))
                                 (let ((bk-end-row
                                        (+ next-end-row bk-square))
                                       (bk-end-col
                                        (1-
                                         (+ next-start-col bk-square))))
                                   (let ((alist
                                          (list
                                           bk-start-row  bk-start-col
                                           bk-end-row bk-end-col)))
                                     (begin
                                       (set!
                                        next-list-lists
                                        (cons alist next-list-lists))
                                       (set! next-start-col (1+ bk-end-col))
                                       ))
                                   ))
                               )) bk-list)
                          (set!
                           new-rect-list-lists
                           (append (reverse next-list-lists)
                                   new-rect-list-lists))
                          )))
                    (begin
                      (set!
                       new-rect-list-lists
                       (append (list a-rect-list) new-rect-list-lists))
                      ))
                ))
            )) rect-list-lists)
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax added-to-rect-ll-03-macro
  (syntax-rules ()
    ((added-to-rect-ll-03-macro
      sorted-list-lists
      prev-rect-list
      final-list-lists)
     (begin
       (for-each
        (lambda (a-rect-list)
          (begin
            (if (equal? prev-rect-list (list))
                (begin
                  (set! prev-rect-list a-rect-list))
                (begin
                  (let ((prev-end-row (list-ref prev-rect-list 2))
                        (prev-end-col (list-ref prev-rect-list 3))
                        (current-start-col (list-ref a-rect-list 1))
                        (current-end-row (list-ref a-rect-list 2)))
                    (begin
                      (if (and (equal?
                                prev-end-row current-end-row)
                               (equal?
                                (- current-start-col prev-end-col)
                                1))
                          (begin
                            (let ((srow
                                   (list-ref prev-rect-list 0))
                                  (scol
                                   (list-ref prev-rect-list 1))
                                  (erow prev-end-row)
                                  (ecol
                                   (list-ref a-rect-list 3)))
                              (let ((next-rect-list
                                     (list srow scol
                                           erow ecol)))
                                (begin
                                  (set!
                                   prev-rect-list next-rect-list)
                                  ))))
                          (begin
                            (set!
                             final-list-lists
                             (append (list prev-rect-list)
                                     final-list-lists))
                            (set! prev-rect-list a-rect-list)
                            ))
                      ))
                  ))
            )) sorted-list-lists)
       (let ((pflag (member prev-rect-list final-list-lists)))
         (begin
           (if (equal? pflag #f)
               (begin
                 (set!
                  final-list-lists
                  (append (list prev-rect-list) final-list-lists))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;;### add a bouwkamp list to an existing rectangle list lists
;;;### this bouwkamp list sits above the smallest square block
;;;### example 75, 112 x 75 bouwkamp string =
;;;### ((36 19 24 33) (14 5) (20 9) (3 11) (42) (39) (31))
;;;### so (14 5) sits above the row 0, 19 square
;;;### and (20 9) sits above the 5 and 24 squares
;;;### rect list format (list start-row start-col end-row end-col)
(define (bouwkamp-added-to-rectangle-ll bk-list rect-list-lists)
  (begin
    (let ((new-rect-list-lists (list))
          (min-start-row -1)
          (min-start-col -1)
          (min-end-row -1)
          (min-end-col -1)
          (prev-height -1))
      (begin
        ;;;### first, find the smallest place where this bk-list goes
        (added-to-rect-ll-01-macro
         rect-list-lists
         prev-height
         min-start-row min-start-col
         min-end-row min-end-col)

        ;;;### second, add the squares in order, left to right
        ;;;### and decompose the rectangle into smaller sub-rectangles
        (set! new-rect-list-lists (list))
        (added-to-rect-ll-02-macro
         bk-list rect-list-lists
         min-start-row min-start-col
         min-end-row min-end-col
         new-rect-list-lists)

        ;;;### step 3, combine rectangles with the same end-rows
        ;;;### this means the heights are the same
        (let ((final-list-lists (list))
              (prev-rect-list (list))
              (sorted-list-lists
               (sort
                new-rect-list-lists
                (lambda (aa bb)
                  (begin
                    (< (list-ref aa 1) (list-ref bb 1))
                    ))
                )))
          (begin
            (added-to-rect-ll-03-macro
             sorted-list-lists
             prev-rect-list
             final-list-lists)

            (let ((sorted-final-list-lists
                   (sort
                    final-list-lists
                    (lambda (aa bb)
                      (begin
                        (< (list-ref aa 1) (list-ref bb 1))
                        ))
                    )))
              (begin
                sorted-final-list-lists
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### convert a row 0 bouwkamp string to a rectangle list lists
;;;### rect list format (list start-row start-col end-row end-col)
(define (convert-bouwkamp-to-rectangle-ll bk-list)
  (begin
    (if (equal? bk-list (list))
        (begin
          (list))
        (begin
          (let ((rect-list-lists (list))
                (start-row 0)
                (start-col 0)
                (end-row 0)
                (end-col -1))
            (begin
              (for-each
               (lambda (a-square)
                 (begin
                   (set! end-row (1- a-square))
                   (set! start-col (1+ end-col))
                   (set! end-col (+ end-col a-square))
                   (let ((next-list
                          (list start-row start-col
                                end-row end-col)))
                     (begin
                       (set!
                        rect-list-lists
                        (append rect-list-lists (list next-list)))
                       ))
                   )) bk-list)

              (let ((sorted-list-lists
                     (sort
                      rect-list-lists
                      (lambda (aa bb)
                        (begin
                          (< (list-ref aa 1) (list-ref bb 1))
                          ))
                      )))
                (begin
                  sorted-list-lists
                  ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
;;;### bouwkamp string list of lists means subsequent lists
;;;### sits above the minimum square block
;;;### for a bouwkamp list of (36 19 24 33)
;;;### given the rect-list-lists
;;;### (list (list 0 0 35 35) (list 0 36 18 54)
;;;###       (list 0 55 23 78) (list 0 79 32 111))
;;;### returns (list 19 36 19)
(define (find-min-row-col-width rect-list-lists)
  (begin
    (let ((min-row -1)
          (min-col -1)
          (min-height -1)
          (min-width -1))
      (begin
        (for-each
         (lambda (a-rect-list)
           (begin
             (let ((start-row (list-ref a-rect-list 0))
                   (start-col (list-ref a-rect-list 1))
                   (end-row (list-ref a-rect-list 2))
                   (end-col (list-ref a-rect-list 3)))
               (let ((this-height
                      (1+ (- end-row start-row)))
                     (this-width
                      (1+ (- end-col start-col))))
                 (begin
                   (if (< min-row 0)
                       (begin
                         (set! min-row (1+ end-row))
                         (set! min-col start-col)
                         (set! min-height this-height)
                         (set! min-width this-width))
                       (begin
                         (if (< this-height min-height)
                             (begin
                               (set! min-row (1+ end-row))
                               (set! min-col start-col)
                               (set! min-height this-height)
                               (set! min-width this-width)
                               ))
                         ))
                   )))
             )) rect-list-lists)

        (list min-row min-col min-width)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (reverse-vector-k-to-n this-vector k)
  (begin
    (let ((vlen (vector-length this-vector))
          (result-vector (vector-copy this-vector)))
      (let ((index-diff (- vlen k)))
        (begin
          (cond
           ((< index-diff 0)
            (begin
              result-vector
              ))
           (else
            (begin
              (let ((ii1 k)
                    (ii2 (- vlen 1))
                    (half-diff (euclidean/ index-diff 2)))
                (begin
                  (do ((jj 0 (+ jj 1)))
                      ((or (>= jj half-diff)
                           (>= ii1 ii2)
                           (>= ii1 vlen)
                           (< ii2 0)
                           ))
                    (begin
                      (let ((v1 (vector-ref result-vector ii1))
                            (v2 (vector-ref result-vector ii2)))
                        (begin
                          (vector-set! result-vector ii1 v2)
                          (vector-set! result-vector ii2 v1)
                          (set! ii1 (+ ii1 1))
                          (set! ii2 (- ii2 1))
                          ))
                      ))
                  result-vector
                  ))
              )))
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; note assumes that this-vector is sorted in ascending order
(define (next-lexicographic-permutation this-vector)
  (begin
    (let ((vlen (1- (vector-length this-vector)))
          (result-vector (vector-copy this-vector))
          (kk 0)
          (aakk 0)
          (ll 0)
          (aall 0)
          (permutation-exists #f))
      (begin
        ;;; 1) find largest kk such that a[kk] < a[kk+1]
        (do ((ii 0 (1+ ii)))
            ((>= ii vlen))
          (begin
            (let ((v1 (vector-ref result-vector ii))
                  (v2 (vector-ref result-vector (1+ ii))))
              (begin
                (if (< v1 v2)
                    (begin
                      (set! permutation-exists #t)
                      (set! kk ii)
                      (set! aakk v1)
                      ))
                ))
            ))

        ;;; 2) find the largest ll such that a[kk] < a[ll]
        (if (equal? permutation-exists #t)
            (begin
              (do ((ii (+ kk 1) (1+ ii)))
                  ((> ii vlen))
                (begin
                  (let ((v1 (vector-ref result-vector ii)))
                    (begin
                      (if (< aakk v1)
                          (begin
                            (set! ll ii)
                            (set! aall v1)
                            ))
                      ))
                  ))

              ;;; 3) swap a[kk] with a[ll]
              (vector-set! result-vector kk aall)
              (vector-set! result-vector ll aakk)

              ;;; 4) reverse the sequence from (k+1) on
              (let ((final-result
                     (reverse-vector-k-to-n
                      result-vector (+ kk 1))))
                (begin
                  final-result
                  )))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### successor-of-list returns the next largest list subject to
;;;### the sum of the numbers equal to the input-target-sum
;;;### previous list goes from least significant digit to most significant
;;;### uses the algorithm discussed at
;;;### https://www.geeksforgeeks.org/generate-unique-partitions-of-an-integer/
(define (successor-of-partition-list
         previous-list)
  (begin
    (let ((remaining-value 0)
          (last-index (1- (length previous-list)))
          (max-len (length previous-list))
          (result-list (list-copy previous-list)))
      (begin
        ;;;### count the number of ones
        (while
         (and
          (>= last-index 0)
          (equal?
           (list-ref previous-list last-index) 1))
         (begin
           (let ((this-element
                  (list-ref previous-list last-index)))
             (begin
               (set!
                remaining-value
                (+ remaining-value
                   this-element))
               (set! last-index (1- last-index))
               ))
           ))

        (if (< last-index 0)
            (begin
              (list))
            (begin
              (list-set!
               result-list
               last-index
               (1- (list-ref result-list last-index)))
              (set! remaining-value (1+ remaining-value))

              ;;;### decrease the value at last-index by 1
              ;;;### and increase remaining-value by 1
              (let ((this-element
                     (list-ref result-list last-index)))
                (begin
                  (while
                   (> remaining-value this-element)
                   (begin
                     (let ((next-index (1+ last-index)))
                       (begin
                         (if (< next-index max-len)
                             (begin
                               (list-set!
                                result-list
                                next-index
                                this-element))
                             (begin
                               (set!
                                result-list
                                (append
                                 result-list
                                 (list this-element)))
                               ))
                         (set!
                          remaining-value
                          (- remaining-value this-element))
                         (set!
                          last-index (1+ last-index))
                         (set!
                          this-element
                          (list-ref result-list last-index))
                         ))
                     ))

                  (let ((next-index (1+ last-index)))
                    (begin
                      (if (< next-index max-len)
                          (begin
                            (list-set!
                             result-list (1+ last-index)
                             remaining-value))
                          (begin
                            (set!
                             result-list
                             (append
                              result-list
                              (list remaining-value)))
                            ))
                      ))
                  (set!
                   last-index (1+ last-index))
                  ))
              ))

        (set!
         result-list
         (list-head result-list (1+ last-index)))

        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### rect-list-lists elements are of the form
;;;### (list start-row start-col end-row end-col)
(define (is-rect-list-simple-perfect-dissection?
         rect-list-lists used-partition-lists init-flag)
  (begin
    (if (and
         (= (length rect-list-lists) 1)
         (> (length used-partition-lists) 1)
         (equal? init-flag #f))
        (begin
          (let ((local-rect-list
                 (list-ref rect-list-lists 0))
                (local-base-partition
                 (list-ref
                  (reverse used-partition-lists) 0)))
            (let ((r-height
                   (- (list-ref local-rect-list 2)
                      (list-ref local-rect-list 0)))
                  (mflag #t))
              (begin
                (for-each
                 (lambda (an-elem)
                   (begin
                     (if (>= an-elem r-height)
                         (begin
                           (set! mflag #f)
                           ))
                     )) local-base-partition)
                mflag
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (partition-list-contains-duplicates?
         a-partition-list)
  (begin
    (let ((dup-htable (make-hash-table 10))
          (result-flag #f)
          (alen (length a-partition-list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((or (>= ii alen)
                 (equal? result-flag #t)))
          (begin
            (let ((an-elem
                   (list-ref a-partition-list ii)))
              (let ((dflag
                     (hash-ref dup-htable an-elem #f)))
                (begin
                  (if (equal? dflag #f)
                      (begin
                        (hash-set! dup-htable an-elem 1))
                      (begin
                        (set! result-flag #t)
                        ))
                  )))
            ))
        result-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (used-partition-lists-contains-duplicates?
         used-partition-lists)
  (begin
    (let ((aflag #f)
          (tmp-htable (make-hash-table)))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (for-each
              (lambda (anum)
                (begin
                  (let ((count
                         (hash-ref tmp-htable anum 0)))
                    (begin
                      (hash-set!
                       tmp-htable
                       anum (1+ count))
                      (if (>= count 1)
                          (begin
                            (set! aflag #t)
                            ))
                      ))
                  )) alist)
             )) used-partition-lists)

        aflag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (populate-used-hash-table used-htable alist)
  (begin
    (for-each
     (lambda (anum)
       (begin
         (hash-set! used-htable anum #t)
         )) alist)
    ))

;;;#############################################################
;;;#############################################################
(define (undo-used-hash-table used-htable alist)
  (begin
    (for-each
     (lambda (anum)
       (begin
         (hash-remove! used-htable anum)
         )) alist)
    ))

;;;#############################################################
;;;#############################################################
(define (check-if-ok-to-use
         partition-list max-width used-htable)
  (begin
    (if (or (equal? partition-list (list))
            (<= (car partition-list) 0)
            (>= (car partition-list) max-width))
        (begin
          #f)
        (begin
          (let ((part-ok-flag #t)
                (count-htable (make-hash-table 20)))
            (begin
              (for-each
               (lambda (anum)
                 (begin
                   (hash-set!
                    count-htable
                    anum
                    (1+
                     (hash-ref count-htable anum 0)))
                   (let ((aflag
                          (hash-ref used-htable anum #f)))
                     (begin
                       (if (equal? aflag #t)
                           (begin
                             (set! part-ok-flag #f)
                             ))
                       ))
                   )) partition-list)

              (if (equal? part-ok-flag #f)
                  (begin
                    part-ok-flag)
                  (begin
                    (let ((no-dup-flag #t))
                      (begin
                        (hash-for-each
                         (lambda (key value)
                           (begin
                             (if (>= value 2)
                                 (begin
                                   (set! no-dup-flag #f)
                                   ))
                             )) count-htable)

                        no-dup-flag
                        ))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (find-height rect-list-lists)
  (begin
    (let ((max-height -1))
      (begin
        (for-each
         (lambda (a-rect-list)
           (begin
             (let ((start-row (list-ref a-rect-list 0))
                   (end-row (list-ref a-rect-list 2)))
               (let ((this-height
                      (1+ (- end-row start-row))))
                 (begin
                   (if (> this-height max-height)
                       (begin
                         (set! max-height this-height)
                         ))
                   )))
             )) rect-list-lists)
        max-height
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### rect-list-lists = (list (list start-row start-col end-row end-col) ...
(define-syntax end-of-bouwkamp-processing-macro
  (syntax-rules ()
    ((end-of-bouwkamp-processing-macro
      used-partition-lists
      rect-list-lists
      acc-string-list)
     (begin
       (let ((this-rectangle-list (car rect-list-lists))
             (bouwkamp-list-lists (reverse used-partition-lists))
             (last-list (car used-partition-lists))
             (order 0))
         (let ((start-row
                (list-ref this-rectangle-list 0))
               (start-col
                (list-ref this-rectangle-list 1))
               (end-row
                (list-ref this-rectangle-list 2))
               (end-col
                (list-ref this-rectangle-list 3)))
           (let ((height (1+ (- end-row start-row)))
                 (width (1+ (- end-col start-col))))
             (let ((str-1
                    (format #f "[~a x ~a] :"
                            (utils-module:commafy-number height)
                            (utils-module:commafy-number width)))
                   (str-2 "(")
                   (string-line-1 "")
                   (string-line-2 ""))
               (begin
                 (for-each
                  (lambda (a-list)
                    (begin
                      (let ((list-string
                             (string-join
                              (map
                               (lambda (anum)
                                 (begin
                                   (set! order (1+ order))
                                   (utils-module:commafy-number anum)
                                   )) a-list)
                              " ")))
                        (begin
                          (if (equal? a-list last-list)
                              (begin
                                (set!
                                 str-2
                                 (string-append
                                  str-2 (format #f "(~a)" list-string))))
                              (begin
                                (set!
                                 str-2
                                 (string-append
                                  str-2 (format #f "(~a) " list-string)))
                                ))
                          ))
                      )) bouwkamp-list-lists)
                 (set! str-2 (string-append str-2 (format #f ")")))

                 (set! string-line-1
                       (format #f "~a ~a" str-1 str-2))

                 (set! string-line-2
                       (format #f "order = ~a"
                               (utils-module:commafy-number order)))

                 (set!
                  acc-string-list
                  (cons (list string-line-1 string-line-2)
                        acc-string-list))
                 ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax fitting-status-loop-macro
  (syntax-rules ()
    ((fitting-status-loop-macro
      min-width
      next-partition-list
      init-jday status-in-minutes)
     (begin
       (let ((final-jday (srfi-19:current-julian-day)))
         (let ((elapsed-mins
                (timer-module:julian-day-difference-in-minutes
                 final-jday init-jday)))
           (begin
             (if (>= elapsed-mins status-in-minutes)
                 (begin
                   (display
                    (ice-9-format:format
                     #f "status [ ~:d ] : "
                     min-width))
                   (display
                    (ice-9-format:format
                     #f "next-partition-list = ~a : "
                     next-partition-list))
                   (display
                    (format
                     #f "elapsed time = ~a : ~a~%"
                     (timer-module:julian-day-difference-to-string
                      final-jday init-jday)
                     (timer-module:current-date-time-string)))
                   (force-output)
                   (set! init-jday final-jday)
                   (gc)
                   ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (try-all-permutations-other-rows
         partition-list
         rect-list-lists
         used-partition-lists
         used-htable
         min-width
         max-width partition-list-htable
         acc-string-list)
  (begin
    (let ((sorted-partition-list
           (sort partition-list <))
          (continue-permutation-loop-flag #t))
      (let ((permutation-vector
             (list->vector sorted-partition-list)))
        (begin
          (while
           (equal? continue-permutation-loop-flag #t)
           (begin
             (let ((new-rect-list-lists
                    (bouwkamp-added-to-rectangle-ll
                     sorted-partition-list rect-list-lists))
                   (empty-list (list)))
               (begin
                 (populate-used-hash-table
                  used-htable sorted-partition-list)
                 (let ((next-acc-string-list
                        (try-all-partitions-other-rows
                         sorted-partition-list
                         new-rect-list-lists
                         (append
                          (list sorted-partition-list)
                          used-partition-lists)
                         used-htable
                         max-width partition-list-htable
                         empty-list)))
                   (begin
                     (if (> (length next-acc-string-list) 0)
                         (begin
                           (set! acc-string-list
                                 (append
                                  next-acc-string-list
                                  acc-string-list))
                           ))
                     (undo-used-hash-table
                      used-htable sorted-partition-list)
                     ))
                 ))

             (let ((avec
                    (next-lexicographic-permutation
                     permutation-vector)))
               (begin
                 ;;;### note: next-lexicographic-permutation requires
                 ;;;### a sorted list in ascending order
                 (if (and (not (equal? avec #f))
                          (vector? avec))
                     (begin
                       (let ((next-permutation-list
                              (vector->list avec)))
                         (begin
                           (set!
                            sorted-partition-list
                            next-permutation-list)
                           )))
                     (begin
                       (set! continue-permutation-loop-flag #f)
                       ))

                 (set! permutation-vector avec)
                 ))
             ))

          acc-string-list
          )))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax other-rows-partition-macro
  (syntax-rules ()
    ((other-rows-partition-macro
      next-partition-list
      rect-list-lists
      used-partition-lists
      used-htable
      min-width
      max-width partition-list-htable
      continue-partition-loop-flag
      acc-string-list)
     (begin
       (let ((aflag
              (partition-list-contains-duplicates?
               next-partition-list)))
         (begin
           (if (and (equal? aflag #f)
                    (check-if-ok-to-use
                     next-partition-list
                     max-width used-htable))
               (begin
                 (populate-used-hash-table
                  used-htable next-partition-list)

                 (let ((next-acc-string-list
                        (try-all-permutations-other-rows
                         next-partition-list
                         rect-list-lists
                         used-partition-lists
                         used-htable
                         min-width
                         max-width partition-list-htable
                         acc-string-list)))
                   (begin
                     (set! acc-string-list next-acc-string-list)

                     (undo-used-hash-table
                      used-htable next-partition-list)
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;;### loop over all partitions of a number
(define (try-all-partitions-other-rows
         partition-list
         rect-list-lists
         used-partition-lists
         used-htable
         max-width partition-list-htable
         acc-string-list)
  (begin
    (if (is-rect-list-simple-perfect-dissection?
         rect-list-lists used-partition-lists #f)
        (begin
          (let ((aflag
                 (used-partition-lists-contains-duplicates?
                  used-partition-lists)))
            (begin
              (if (equal? aflag #f)
                  (begin
                    ;;;### found a simple perfect squared rectangle
                    (let ((this-acc-string-list (list)))
                      (begin
                        (end-of-bouwkamp-processing-macro
                         used-partition-lists rect-list-lists
                         this-acc-string-list)

                        (if (> (length this-acc-string-list) 0)
                            (begin
                              (set!
                               acc-string-list
                               (append
                                this-acc-string-list
                                acc-string-list))
                              ))
                        ))
                    ))

              acc-string-list
              )))
        (begin
          (let ((rr-list
                 (find-min-row-col-width
                  rect-list-lists)))
            (let ((min-width (list-ref rr-list 2))
                  (max-height (find-height rect-list-lists)))
              (let ((continue-partition-loop-flag #t)
                    (all-partitions-list
                     (hash-ref partition-list-htable
                               min-width (list))))
                (let ((all-length
                       (length all-partitions-list)))
                  (begin
                    (if (<= all-length 0)
                        (begin
                          (let ((tmp-list
                                 (calc-single-partition-list-lists
                                  min-width)))
                            (begin
                              (set! all-partitions-list tmp-list)
                              (set! all-length (length tmp-list))
                              ))
                          ))

                    (if (or
                         (<= all-length 0)
                         (> max-height max-width)
                         (>= min-width max-width))
                        (begin
                          (set! continue-partition-loop-flag #f)
                          ))

                    (do ((ii 0 (1+ ii)))
                        ((or
                          (>= ii all-length)
                          (equal? continue-partition-loop-flag #f)))
                      (begin
                        (let ((next-partition-list
                               (list-ref all-partitions-list ii)))
                          (begin
                            (other-rows-partition-macro
                             next-partition-list
                             rect-list-lists
                             used-partition-lists
                             used-htable
                             min-width
                             max-width partition-list-htable
                             continue-partition-loop-flag
                             acc-string-list)
                            ))
                        ))
                    )))
              ))
          ))

    acc-string-list
    ))

;;;#############################################################
;;;#############################################################
(define-syntax try-1-rows-0-permutations-macro
  (syntax-rules ()
    ((try-1-rows-0-permutations-macro
      sorted-partition-list
      used-htable
      max-width partition-list-htable
      acc-string-list)
     (begin
       (let ((rect-list-lists
              (convert-bouwkamp-to-rectangle-ll
               sorted-partition-list))
             (empty-list (list)))
         (begin
           (populate-used-hash-table
            used-htable sorted-partition-list)
           (let ((next-acc-string-list
                  (try-all-partitions-other-rows
                   sorted-partition-list
                   rect-list-lists
                   (list sorted-partition-list)
                   used-htable
                   max-width partition-list-htable
                   empty-list)))
             (begin
               (if (> (length next-acc-string-list) 0)
                   (begin
                     (set!
                      acc-string-list
                      (append
                       next-acc-string-list
                       acc-string-list))
                     ))
               (undo-used-hash-table
                used-htable sorted-partition-list)
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax set-symmetric-hash-table-macro
  (syntax-rules ()
    ((set-symmetric-hash-table-macro
      this-list symmetric-htable)
     (begin
       (let ((this-string
              (utils-module:number-list-to-string
               this-list)))
         (begin
           (hash-set!
            symmetric-htable
            this-string #t)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (try-all-permutations-row-0
         partition-list
         used-htable
         max-width partition-list-htable
         acc-string-list)
  (begin
    (let ((sorted-partition-list
           (sort partition-list <))
          (symmetric-permutations-htable
           (make-hash-table 1000))
          (continue-permutation-loop-flag #t))
      (let ((permutation-vector
             (list->vector sorted-partition-list)))
        (begin
          (set-symmetric-hash-table-macro
           sorted-partition-list
           symmetric-permutations-htable)

          (while
           (equal? continue-permutation-loop-flag #t)
           (begin
             (if (> (length sorted-partition-list) 1)
                 (begin
                   (let ((rev-astring
                          (utils-module:number-list-to-string
                           (reverse sorted-partition-list))))
                     (let ((bool-flag
                            (hash-ref
                             symmetric-permutations-htable
                             rev-astring #f)))
                       (begin
                         (if (equal? bool-flag #f)
                             (begin
                               (try-1-rows-0-permutations-macro
                                sorted-partition-list
                                used-htable
                                max-width partition-list-htable
                                acc-string-list)

                               (set-symmetric-hash-table-macro
                                sorted-partition-list
                                symmetric-permutations-htable)
                               ))
                         )))
                   ))

             (let ((avec
                    (next-lexicographic-permutation
                     permutation-vector)))
               (begin
                 ;;;### note: next-lexicographic-permutation requires
                 ;;;### a sorted list in ascending order
                 (if (and (not (equal? avec #f))
                          (vector? avec))
                     (begin
                       (let ((next-permutation-list
                              (vector->list avec)))
                         (begin
                           (set!
                            sorted-partition-list
                            next-permutation-list)
                           )))
                     (begin
                       (set! continue-permutation-loop-flag #f)
                       ))

                 (set! permutation-vector avec)
                 ))
             ))

          (hash-clear! symmetric-permutations-htable)
          (gc)
          acc-string-list
          )))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax try-rows-0-partition-macro
  (syntax-rules ()
    ((try-rows-0-partition-macro
      this-partition-list
      used-htable
      symmetric-partition-htable
      max-width partition-list-htable
      acc-string-list-list)
     (begin
       (let ((tmp-acc-string-list (list))
             (rev-astring
              (utils-module:number-list-to-string
               (reverse this-partition-list))))
         (let ((aflag
                (hash-ref
                 symmetric-partition-htable rev-astring #f)))
           (begin
             (if (equal? aflag #f)
                 (begin
                   ;;; this partition hasn't been seen
                   (hash-clear! used-htable)
                   (populate-used-hash-table
                    used-htable this-partition-list)

                   (let ((acc-string-list
                          (try-all-permutations-row-0
                           this-partition-list
                           used-htable
                           max-width partition-list-htable
                           tmp-acc-string-list)))
                     (begin
                       (if (and
                            (list? acc-string-list)
                            (> (length acc-string-list) 0))
                           (begin
                             (set!
                              acc-string-list-list
                              (append
                               acc-string-list
                               acc-string-list-list))
                             ))

                       (set-symmetric-hash-table-macro
                        this-partition-list
                        symmetric-partition-htable)
                       ))
                   ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;;### loop over all partitions of a number
(define (try-all-partitions-row-0
         max-width partition-list-htable
         init-jday status-in-minutes)
  (begin
    (let ((acc-string-list-list (list))
          (symmetric-partition-htable
           (make-hash-table 1000))
          (list-of-partitions
           (hash-ref partition-list-htable max-width (list)))
          (used-htable (make-hash-table 100)))
      (begin
        (if (or (not (list? list-of-partitions))
                (<= (length list-of-partitions) 0))
            (begin
              (let ((tmp-list
                     (calc-single-partition-list-lists
                      max-width)))
                (begin
                  (set! list-of-partitions tmp-list)
                  ))
              ))

        (if (list? list-of-partitions)
            (begin
              (for-each
               (lambda (next-partition-list)
                 (begin
                   ;;;### note: successor-of-partition-list requires
                   ;;;### a sorted list in descending order
                   (if (and
                        (list? next-partition-list)
                        (> (length next-partition-list) 0))
                       (begin
                         (if (and
                              (equal?
                               (member 1 next-partition-list) #f)
                              (equal?
                               (member 2 next-partition-list) #f)
                              (equal?
                               (member 3 next-partition-list) #f))
                             (begin
                               ;;; we have a valid partition-list
                               (try-rows-0-partition-macro
                                next-partition-list
                                used-htable
                                symmetric-partition-htable
                                max-width partition-list-htable
                                acc-string-list-list)
                               ))
                         ))

                   (fitting-status-loop-macro
                    max-width
                    next-partition-list
                    init-jday status-in-minutes)
                   )) list-of-partitions)

              (hash-clear! symmetric-partition-htable)
              (gc)

              (for-each
               (lambda (as-list)
                 (begin
                   (if (and
                        (list? as-list)
                        (> (length as-list) 1))
                       (begin
                         (display
                          (ice-9-format:format
                           #f "~a : ~a~%"
                           (list-ref as-list 0)
                           (list-ref as-list 1)))
                         ))
                   )) acc-string-list-list)

              init-jday
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; starting loop calculates all possible ways to add up to max-cols
;;; a starting way gives you the first row of the bouwkamp string
;;; for example if max-cols = 10, then (2 3 5) adds to 10 and is
;;; one possibility that tiles the first row of the rectanble.
(define (starting-loop
         max-width partition-list-htable
         begin-jday status-in-minutes)
  (begin
    (let ((result-jday #f)
          (init-loop-jday (srfi-19:current-julian-day)))
      (begin
        (try-all-partitions-row-0
         max-width partition-list-htable
         begin-jday status-in-minutes)

        (let ((final-loop-jday (srfi-19:current-julian-day)))
          (begin
            (display
             (ice-9-format:format
              #f "[~:d] elapsed time = ~a : ~a~%"
              max-width
              (timer-module:julian-day-difference-to-string
               final-loop-jday init-loop-jday)
              (timer-module:current-date-time-string)))
            (force-output)
            (set! result-jday final-loop-jday)
            ))

        result-jday
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (calc-single-partition-list-lists
         current-width)
  (begin
    (let ((acc-list-list (list (list current-width)))
          (this-partition-list (list current-width))
          (continue-flag #t))
      (begin
        (while
         (equal? continue-flag #t)
         (begin
           (let ((next-partition-list
                  (successor-of-partition-list
                   this-partition-list)))
             (begin
               (if (and
                    (list? next-partition-list)
                    (> (length next-partition-list) 0))
                   (begin
                     (let ((dup-flag
                            (partition-list-contains-duplicates?
                             next-partition-list)))
                       (begin
                         (if (equal? dup-flag #f)
                             (begin
                               ;;; we have a valid partition-list
                               (set!
                                acc-list-list
                                (cons next-partition-list
                                      acc-list-list)
                                )))
                         (set! this-partition-list
                               next-partition-list)
                         )))
                   (begin
                     (set! continue-flag #f)
                     (set! this-partition-list
                           next-partition-list)
                     ))
               ))
           ))

        acc-list-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (precompute-partition-list-htable
         max-width partition-list-htable)
  (begin
    (hash-clear! partition-list-htable)

    (do ((ii 1 (1+ ii)))
        ((> ii max-width))
      (begin
        (let ((acc-list-list
               (calc-single-partition-list-lists
                ii)))
          (begin
            (if (> (length acc-list-list) 0)
                (begin
                  (hash-set! partition-list-htable
                             ii acc-list-list)
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop-v0
         start-width end-width
         max-precompute
         status-in-minutes)
  (begin
    (let ((partition-list-htable
           (make-hash-table 100000)))
      (begin
        (timer-module:time-code-macro
         (begin
           (precompute-partition-list-htable
            max-precompute partition-list-htable)
           (display
            (format
             #f "completed precompute-partition-list-htable~%"))
           ))
        (newline)
        (force-output)

        (let ((init-jday (srfi-19:current-julian-day)))
          (begin
            ;;;### cycle over different array sizes
            (do ((ii-width start-width (1+ ii-width)))
                ((> ii-width end-width))
              (begin
                (let ((next-init-jday
                       (starting-loop
                        ii-width
                        partition-list-htable
                        init-jday status-in-minutes)))
                  (begin
                    (set! init-jday next-init-jday)
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
