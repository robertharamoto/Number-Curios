;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for bf-spsr-module.scm                    ###
;;;###                                                       ###
;;;###  last updated August 11, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 19, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((bf-spsr-module)
              :renamer (symbol-prefix-proc 'bf-spsr-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (bf-assert-2d-arrays-equal
         shouldbe-array result-array sub-name error-message
         result-hash-table)
  (begin
    ;;;### first assert dimensions are the same
    (let ((shouldbe-shape (array-shape shouldbe-array))
          (result-shape (array-shape result-array)))
      (let ((row-bounds (list-ref shouldbe-shape 0))
            (col-bounds (list-ref shouldbe-shape 1))
            (dim-error
             (format
              #f "~a : array shape shouldbe = ~a : result = ~a"
              error-message shouldbe-shape result-shape)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-shape result-shape)
           sub-name
           dim-error
           result-hash-table)

          ;;;### assert individual elements are the same
          (do ((ii (list-ref row-bounds 0) (+ ii 1)))
              ((>= ii (list-ref row-bounds 1)))
            (begin
              (do ((jj (list-ref col-bounds 0) (+ jj 1)))
                  ((>= jj (list-ref col-bounds 1)))
                (begin
                  (let ((shouldbe-elem
                         (array-ref shouldbe-array ii jj))
                        (result-elem
                         (array-ref result-array ii jj)))
                    (let ((elem-1-error
                           (format
                            #f "~a : ii = ~a, jj = ~a : "
                            error-message ii jj))
                          (elem-2-error
                           (format
                            #f "array element shouldbe = ~a, result = ~a"
                            shouldbe-elem result-elem)))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe-elem result-elem)
                         sub-name
                         (string-append elem-1-error elem-2-error)
                         result-hash-table)
                        )))
                  ))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (bf-make-error-string
         sub-name test-label-index
         test-block-size
         test-xx test-yy
         test-max-row test-max-col
         shouldbe-list result-array)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) : error : block-size = ~a, "
            sub-name test-label-index test-block-size))
          (err-2
           (format
            #f "row = ~a, col = ~a, "
            test-xx test-yy))
          (err-3
           (format
            #f "max-row = ~a, max-col = ~a : "
            test-max-row test-max-col))
          (err-4
           (format
            #f "shouldbe-list = ~a, result = ~a"
            shouldbe-list result-array)))
      (begin
        (string-append
         err-1 err-2 err-3 err-4)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-write-spsr-block-at-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-write-spsr-block-at-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            3
            0 0 5 5
            (list (list 3 3 3 0 0)
                  (list 3 3 3 0 0)
                  (list 3 3 3 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)))
           (list
            3
            0 1 5 5
            (list (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)))
           (list
            3
            0 2 5 5
            (list (list 0 0 3 3 3)
                  (list 0 0 3 3 3)
                  (list 0 0 3 3 3)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)))
           (list
            3
            1 1 5 5
            (list (list 0 0 0 0 0)
                  (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 0 0 0 0)))
           (list
            4
            1 1 5 5
            (list (list 0 0 0 0 0)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)))
           (list
            3
            2 2 5 5
            (list (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 3 3 3)
                  (list 0 0 3 3 3)
                  (list 0 0 3 3 3)))
           (list
            3
            2 2 5 6
            (list (list 0 0 0 0 0 0)
                  (list 0 0 0 0 0 0)
                  (list 0 0 3 3 3 0)
                  (list 0 0 3 3 3 0)
                  (list 0 0 3 3 3 0)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-block-size (list-ref alist 0))
                  (test-xx (list-ref alist 1))
                  (test-yy (list-ref alist 2))
                  (test-max-row (list-ref alist 3))
                  (test-max-col (list-ref alist 4))
                  (shouldbe-list (list-ref alist 5)))
              (let ((result-array
                     (make-array 0 test-max-row test-max-col))
                    (shouldbe-array
                     (list->array (list 0 0) shouldbe-list)))
                (begin
                  (bf-spsr-module:write-spsr-block-at!
                   result-array test-max-row test-max-col
                   test-block-size test-xx test-yy)

                  (let ((err-string
                         (bf-make-error-string
                          sub-name test-label-index
                          test-block-size
                          test-xx test-yy
                          test-max-row test-max-col
                          shouldbe-list result-array)))
                    (begin
                      (bf-assert-2d-arrays-equal
                       shouldbe-array result-array sub-name
                       err-string
                       result-hash-table)
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-erase-spsr-block-at-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-erase-spsr-block-at-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            3
            0 0 5 5
            (list (list 3 3 3 0 0)
                  (list 3 3 3 0 0)
                  (list 3 3 3 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0))
            (list (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)))
           (list
            3
            0 1 5 5
            (list (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0))
            (list (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)))
           (list
            3
            0 2 5 5
            (list (list 0 0 3 3 3)
                  (list 0 0 3 3 3)
                  (list 0 0 3 3 3)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0))
            (list (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)))
           (list
            3
            1 1 5 5
            (list (list 0 0 0 0 0)
                  (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 0 0 0 0))
            (list (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)))
           (list
            4
            1 1 5 5
            (list (list 0 0 0 0 0)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4))
            (list (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)))
           (list
            3
            2 2 5 5
            (list (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 3 3 3)
                  (list 0 0 3 3 3)
                  (list 0 0 3 3 3))
            (list (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)))
           (list
            3
            2 2 5 5
            (list (list 2 2 0 0 0)
                  (list 2 2 0 0 0)
                  (list 0 0 3 3 3)
                  (list 0 0 3 3 3)
                  (list 0 0 3 3 3))
            (list (list 2 2 0 0 0)
                  (list 2 2 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)))
           (list
            2
            0 0 5 5
            (list (list 2 2 0 0 0)
                  (list 2 2 0 0 0)
                  (list 0 0 3 3 3)
                  (list 0 0 3 3 3)
                  (list 0 0 3 3 3))
            (list (list 0 0 0 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 3 3 3)
                  (list 0 0 3 3 3)
                  (list 0 0 3 3 3)))
           (list
            2
            0 0 5 6
            (list (list 2 2 0 0 0 1)
                  (list 2 2 0 0 0 0)
                  (list 0 0 3 3 3 0)
                  (list 0 0 3 3 3 0)
                  (list 0 0 3 3 3 0))
            (list (list 0 0 0 0 0 1)
                  (list 0 0 0 0 0 0)
                  (list 0 0 3 3 3 0)
                  (list 0 0 3 3 3 0)
                  (list 0 0 3 3 3 0)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-block-size (list-ref alist 0))
                  (test-xx (list-ref alist 1))
                  (test-yy (list-ref alist 2))
                  (test-max-row (list-ref alist 3))
                  (test-max-col (list-ref alist 4))
                  (square-list (list-ref alist 5))
                  (shouldbe-list (list-ref alist 6)))
              (let ((square-array
                     (list->array (list 0 0) square-list))
                    (shouldbe-array
                     (list->array (list 0 0) shouldbe-list)))
                (begin
                  (bf-spsr-module:erase-spsr-block-at!
                   square-array test-max-row test-max-col
                   test-block-size test-xx test-yy)

                  (let ((err-string
                         (bf-make-error-string
                          sub-name test-label-index
                          test-block-size
                          test-xx test-yy
                          test-max-row test-max-col
                          shouldbe-list square-array)))
                    (begin
                      (bf-assert-2d-arrays-equal
                       shouldbe-array square-array sub-name
                       err-string
                       result-hash-table)
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (bf-assert-simple-equal
         sub-name test-label-index
         input-data
         shouldbe result
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) : error : "
            sub-name test-label-index))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (begin
        (unittest2:assert?
         (equal? shouldbe result)
         sub-name
         (string-append err-1 err-2)
         result-hash-table)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-does-spsr-block-fit-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-does-spsr-block-fit-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            3
            0 0 5 5
            (list (list 3 3 3 0 0)
                  (list 3 3 3 0 0)
                  (list 3 3 3 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0))
            #f)
           (list
            2
            3 0 5 5
            (list (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0))
            #t)
           (list
            2
            3 1 5 5
            (list (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0))
            #t)
           (list
            2
            3 2 5 5
            (list (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0))
            #t)
           (list
            2
            3 3 5 5
            (list (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0))
            #t)
           (list
            2
            3 4 5 5
            (list (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0))
            #f)
           (list
            3
            0 2 5 5
            (list (list 0 0 3 3 3)
                  (list 0 0 3 3 3)
                  (list 0 0 3 3 3)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0))
            #f)
           (list
            3
            0 0 5 5
            (list (list 0 0 0 0 0)
                  (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 3 3 3 0)
                  (list 0 0 0 0 0))
            #f)
           (list
            2
            0 0 5 5
            (list (list 0 0 0 0 0)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4))
            #f)
           (list
            1
            0 0 5 5
            (list (list 0 0 0 0 0)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4))
            #t)
           (list
            1
            0 1 5 5
            (list (list 0 0 0 0 0)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4))
            #t)
           (list
            1
            0 4 5 5
            (list (list 0 0 0 0 0)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4))
            #t)
           (list
            1
            1 0 5 5
            (list (list 0 0 0 0 0)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4))
            #t)
           (list
            1
            4 0 5 5
            (list (list 0 0 0 0 0)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4))
            #t)
           (list
            1
            4 0 5 6
            (list (list 0 0 0 0 0 0)
                  (list 0 4 4 4 4 0)
                  (list 0 4 4 4 4 0)
                  (list 0 4 4 4 4 0)
                  (list 0 4 4 4 4 0))
            #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-block-size (list-ref alist 0))
                  (test-xx (list-ref alist 1))
                  (test-yy (list-ref alist 2))
                  (test-max-row (list-ref alist 3))
                  (test-max-col (list-ref alist 4))
                  (square-list (list-ref alist 5))
                  (shouldbe-bool (list-ref alist 6)))
              (let ((square-array
                     (list->array (list 0 0) square-list)))
                (let ((result-bool
                       (bf-spsr-module:does-spsr-block-fit?
                        square-array test-max-row test-max-col
                        test-block-size test-xx test-yy)))
                  (begin
                    (bf-assert-simple-equal
                     sub-name test-label-index
                     square-list
                     shouldbe-bool result-bool
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-perfect-dissection-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-perfect-dissection-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            5 5
            (list (list 3 3 3 2 2)
                  (list 3 3 3 2 2)
                  (list 3 3 3 4 4)
                  (list 5 5 5 4 4)
                  (list 5 5 5 7 7))
            #t)
           (list
            5 5
            (list (list 3 3 3 2 2)
                  (list 3 3 3 2 2)
                  (list 3 3 3 4 4)
                  (list 5 5 0 4 4)
                  (list 5 5 5 7 7))
            #f)
           (list
            5 6
            (list (list 3 3 3 2 2 1)
                  (list 3 3 3 2 2 1)
                  (list 3 3 3 4 4 1)
                  (list 5 5 0 4 4 1)
                  (list 5 5 5 7 7 1))
            #f)
           (list
            5 6
            (list (list 3 3 3 2 2 1)
                  (list 3 3 3 2 2 1)
                  (list 3 3 3 4 4 1)
                  (list 5 5 5 4 4 1)
                  (list 5 5 5 7 7 1))
            #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-max-row (list-ref alist 0))
                  (test-max-col (list-ref alist 1))
                  (square-list (list-ref alist 2))
                  (shouldbe-bool (list-ref alist 3)))
              (let ((square-array
                     (list->array (list 0 0) square-list)))
                (let ((result-bool
                       (bf-spsr-module:is-perfect-dissection?
                        square-array test-max-row test-max-col)))
                  (begin
                    (bf-assert-simple-equal
                     sub-name test-label-index
                     square-list
                     shouldbe-bool result-bool
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-first-available-row-column-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-find-first-available-row-column-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            5 5
            (list (list 3 3 3 2 2)
                  (list 3 3 3 2 2)
                  (list 3 3 3 4 4)
                  (list 5 5 5 4 4)
                  (list 5 5 5 7 7))
            (list -1 -1))
           (list
            5 5
            (list (list 3 3 3 2 2)
                  (list 3 3 3 2 2)
                  (list 3 3 3 4 4)
                  (list 5 5 0 4 4)
                  (list 5 5 5 7 7))
            (list 3 2))
           (list
            5 5
            (list (list 3 3 3 2 2)
                  (list 3 3 3 0 2)
                  (list 3 3 3 4 4)
                  (list 5 5 4 4 4)
                  (list 5 5 5 7 7))
            (list 1 3))
           (list
            5 5
            (list (list 3 3 3 2 2)
                  (list 3 3 3 2 2)
                  (list 3 3 3 4 4)
                  (list 5 5 4 4 4)
                  (list 5 5 5 0 7))
            (list 4 3))
           (list
            5 5
            (list (list 3 3 0 2 2)
                  (list 3 3 3 2 2)
                  (list 3 3 3 4 4)
                  (list 5 5 4 4 4)
                  (list 5 5 5 0 7))
            (list 0 2))
           (list
            5 6
            (list (list 3 3 3 2 2 1)
                  (list 3 3 3 2 2 0)
                  (list 3 3 3 4 4 0)
                  (list 5 5 4 4 4 0)
                  (list 5 5 5 0 7 0))
            (list 1 5))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((max-rows (list-ref alist 0))
                  (max-columns (list-ref alist 1))
                  (square-list (list-ref alist 2))
                  (shouldbe-list (list-ref alist 3)))
              (let ((square-array
                     (list->array (list 0 0) square-list)))
                (let ((result-list
                       (bf-spsr-module:find-first-available-row-column
                        square-array max-rows max-columns 0 0)))
                  (begin
                    (bf-assert-simple-equal
                     sub-name test-label-index
                     square-list
                     shouldbe-list result-list
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-first-available-max-block-size-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-find-first-available-max-block-size-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            5 5
            (list (list 3 3 3 2 2)
                  (list 3 3 3 2 2)
                  (list 3 3 3 4 4)
                  (list 5 5 5 4 4)
                  (list 5 5 5 7 7))
            4 4 -1)
           (list
            5 5
            (list (list 3 3 3 2 2)
                  (list 3 3 3 2 2)
                  (list 3 3 3 4 4)
                  (list 5 5 0 4 4)
                  (list 5 5 5 7 7))
            3 2 1)
           (list
            5 5
            (list (list 3 3 3 2 2)
                  (list 3 3 3 0 0)
                  (list 3 3 3 0 0)
                  (list 5 5 4 4 4)
                  (list 5 5 5 7 7))
            1 3 2)
           (list
            5 5
            (list (list 3 3 3 2 2)
                  (list 3 3 3 2 2)
                  (list 3 3 3 4 4)
                  (list 5 5 0 0 0)
                  (list 5 5 0 0 0))
            3 2 2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((max-rows (list-ref alist 0))
                  (max-columns (list-ref alist 1))
                  (square-list (list-ref alist 2))
                  (this-row (list-ref alist 3))
                  (this-col (list-ref alist 4))
                  (shouldbe (list-ref alist 5)))
              (let ((square-array
                     (list->array (list 0 0) square-list)))
                (let ((result
                       (bf-spsr-module:find-first-available-max-block-size
                        square-array max-rows max-columns
                        this-row this-col)))
                  (begin
                    (bf-assert-simple-equal
                     sub-name test-label-index
                     square-list
                     shouldbe result
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-spsr-block-list-to-bouwkamp-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-spsr-block-list-to-bouwkamp-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (list
             (list 3 0 0)
             (list 2 0 3)
             (list 1 0 5))
            "(3, 2, 1)")
           (list
            (list
             (list 3 0 0)
             (list 2 0 3)
             (list 1 0 5)
             (list 4 1 0)
             (list 5 1 5))
            "(3, 2, 1) (4, 5)")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((bouwkamp-list (list-ref alist 0))
                  (shouldbe-string (list-ref alist 1)))
              (let ((result-string
                     (bf-spsr-module:spsr-block-list-to-bouwkamp-string
                      bouwkamp-list)))
                (begin
                  (bf-assert-simple-equal
                   sub-name test-label-index
                   bouwkamp-list
                   shouldbe-string result-string
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-spsr-block-list-to-percent-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-spsr-block-list-to-percent-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (list
             (list 3 0 0)
             (list 2 0 3)
             (list 1 0 5))
            "primes/total = 2 / 3 = 66.7%")
           (list
            (list
             (list 3 0 0)
             (list 2 0 3)
             (list 1 0 5)
             (list 4 1 0)
             (list 5 1 5))
            "primes/total = 3 / 5 = 60.0%")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((bouwkamp-list (list-ref alist 0))
                  (shouldbe-string (list-ref alist 1)))
              (let ((result-string
                     (bf-spsr-module:spsr-block-list-to-percent-string
                      bouwkamp-list)))
                (begin
                  (bf-assert-simple-equal
                   sub-name test-label-index
                   bouwkamp-list
                   shouldbe-string result-string
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-initialize-array-with-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-initialize-array-with-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            5 5 (list 3 2)
            (list (list 3 3 3 2 2)
                  (list 3 3 3 2 2)
                  (list 3 3 3 0 0)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)))
           (list
            5 5 (list 1 4)
            (list (list 1 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 4 4 4 4)
                  (list 0 0 0 0 0)))
           (list
            5 5 (list 2 3)
            (list (list 2 2 3 3 3)
                  (list 2 2 3 3 3)
                  (list 0 0 3 3 3)
                  (list 0 0 0 0 0)
                  (list 0 0 0 0 0)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((max-row (list-ref alist 0))
                  (max-col (list-ref alist 1))
                  (test-list (list-ref alist 2))
                  (shouldbe-list (list-ref alist 3)))
              (let ((result-array
                     (make-array 0 max-row max-col))
                    (shouldbe-array
                     (list->array (list 0 0) shouldbe-list)))
                (begin
                  (bf-spsr-module:initialize-array-with-list!
                   result-array max-row max-col test-list)

                  (let ((err-1
                         (format
                          #f "~a : (~a) : error : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "max-row = ~a, max-col = ~a, "
                          max-row max-col))
                        (err-3
                         (format
                          #f "test-list = ~a : "
                          test-list))
                        (err-4
                         (format
                          #f "shouldbe-list = ~a, result = ~a"
                          shouldbe-list result-array)))
                    (begin
                      (bf-assert-2d-arrays-equal
                       shouldbe-array result-array sub-name
                       (string-append
                        err-1 err-2 err-3 err-4)
                       result-hash-table)
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-reverse-vector-k-to-n-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-reverse-vector-k-to-n-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (vector 0 1 2) 1 (vector 0 2 1))
           (list (vector 0 1 2) 0 (vector 2 1 0))
           (list (vector 0 1 2 3) 2 (vector 0 1 3 2))
           (list (vector 0 1 2 3) 1 (vector 0 3 2 1))
           (list (vector 0 1 2 3) 0 (vector 3 2 1 0))
           (list (vector 0 1 2 3 4) 3 (vector 0 1 2 4 3))
           (list (vector 0 1 2 3 4) 2 (vector 0 1 4 3 2))
           (list (vector 0 1 2 3 4) 1 (vector 0 4 3 2 1))
           (list (vector 0 1 2 3 4) 0 (vector 4 3 2 1 0))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-vec (list-ref this-list 0))
                  (test-ii (list-ref this-list 1))
                  (shouldbe-vec (list-ref this-list 2)))
              (let ((result-vec
                     (bf-spsr-module:reverse-vector-k-to-n
                      test-vec test-ii)))
                (begin
                  (bf-assert-simple-equal
                   sub-name test-label-index
                   test-vec
                   shouldbe-vec result-vec
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-next-lexicographic-permutation-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-next-lexicographic-permutation-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (vector 0 1 2) (vector 0 2 1))
           (list (vector 0 2 1) (vector 1 0 2))
           (list (vector 1 0 2) (vector 1 2 0))
           (list (vector 1 2 0) (vector 2 0 1))
           (list (vector 2 0 1) (vector 2 1 0))
           (list (vector 0 1 2 3 4 5) (vector 0 1 2 3 5 4))
           (list (vector 0 1 2 3 5 4) (vector 0 1 2 4 3 5))
           (list (vector 0 1 2 4 3 5) (vector 0 1 2 4 5 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-vec (list-ref this-list 0))
                  (shouldbe-vec (list-ref this-list 1)))
              (let ((result-vec
                     (bf-spsr-module:next-lexicographic-permutation
                      test-vec)))
                (begin
                  (bf-assert-simple-equal
                   sub-name test-label-index
                   test-vec
                   shouldbe-vec result-vec
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
