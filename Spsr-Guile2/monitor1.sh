#! /bin/bash

PROG="psqr.scm"
SLEEPSECS=600
OUTLOG=out1.log

this_process=`ps -e | egrep -i "${PROG}"`

while [ -n "${this_process}" ]
do
    echo "###########################################################"
    tail -n 2 "${OUTLOG}"
    echo " "
    ls -lrth "${OUTLOG}"
    echo " "
    ps -eo pid,nice,%cpu,rss,%mem,cputime,args --sort=nice | egrep -i "${PROG}" | grep -vi "emacs" | grep -vi "grep"
    date
    uptime
    sleep ${SLEEPSECS}
    this_process=`ps -e | egrep -i "${PROG}"`
done

date

