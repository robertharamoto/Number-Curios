#! /bin/bash

################################################################
################################################################
###                                                          ###
###  start.sh                                                ###
###                                                          ###
###  last updated September 12, 2024                         ###
###                                                          ###
################################################################
################################################################

################################################################
################################################################
function run_main_program()
{
    local LOCAL_CONFIG_FILE="$1"
    local LOCAL_OUT_LOG="$2"

    nice --adjustment=10 ./spsr-v1.scm \
         --config "${LOCAL_CONFIG_FILE}" > tmp

    sdiff -s tmp "${LOCAL_OUT_LOG}"

    cat tmp

    mv tmp "${LOCAL_OUT_LOG}"
}

################################################################
################################################################
function run_main_bf_program()
{
    local LOCAL_CONFIG_FILE="$1"
    local LOCAL_OUT_LOG="$2"

    nice --adjustment=10 ./brute-spsr.scm \
         --config "${LOCAL_CONFIG_FILE}" > tmp

    sdiff -s tmp "${LOCAL_OUT_LOG}"

    cat tmp

    mv tmp "${LOCAL_OUT_LOG}"
}

################################################################
################################################################
DATE_FORMAT="%A, %B %d, %Y  %r"

echo "starting brute-spsr.scm"
echo "$(date +"${DATE_FORMAT}")"

run_main_bf_program "brute-init0.config" "out-brute-2024-32x33.log"

run_main_bf_program "brute-init1.config" "out-brute-2024-47x65.log"


echo "starting spsr-v1.scm"
echo "$(date +"${DATE_FORMAT}")"

run_main_program "init0.config" "out-2024-32x33.log"

run_main_program "init1.config" "out-2024-47x65.log"

echo "$(date +"${DATE_FORMAT}")"
nice --adjustment=10 ./spsr-v1.scm tmp-init0.config > tmp1 &


################################################################
################################################################

################################################################
################################################################
###                                                          ###
###  end of file                                             ###
###                                                          ###
################################################################
################################################################
