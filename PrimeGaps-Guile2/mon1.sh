#! /bin/bash

MAXNUM=10

for ((ii=0;ii<=${MAXNUM};ii+=1))
do
    ls -lrth *.txt
    tail out20.txt
    echo " "
    echo -n "loop ${ii} :: "
    date
    echo " "
    if [ "${ii}" -lt "${MAXNUM}" ]
    then
	sleep 600
    fi
done
