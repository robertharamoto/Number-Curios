;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for prime-gaps-module.scm                 ###
;;;###                                                       ###
;;;###  last updated October 7, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((prime-gaps-module)
              :renamer (symbol-prefix-proc 'prime-gaps-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;          1         2         3         4         5         6
;;; 123456789012345678901234567890123456789012345678901234567890
;;;  pp p p   p p   p p   p     p p     p   p p   p     p     p
(unittest2:define-tests-macro
 (test-process-bin-sprp-1 result-htable)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-process-bin-sprp-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list -1 0 10 100 6
                 (list 0 10 7 4)
                 (list (list 2 2) (list 4 0)))
           (list 7 10 10 200 6
                 (list 10 20 19 4)
                 (list (list 2 2) (list 4 2)))
           (list 19 20 10 300 6
                 (list 20 30 29 2)
                 (list (list 2 0) (list 4 1) (list 6 1)))
           (list 29 30 10 400 6
                 (list 30 40 37 2)
                 (list (list 2 1) (list 4 0) (list 6 1)))
           (list 37 40 10 500 4
                 (list 40 50 47 3)
                 (list (list 2 1) (list 4 2) (list 6 0)))
           (list 47 50 10 1000 4
                 (list 50 60 59 2)
                 (list (list 2 0) (list 4 0) (list 6 2)))
           ))
         (bin-pgaps-htable (make-hash-table))
         (cumulative-pgaps-htable (make-hash-table))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((last-prime (list-ref alist 0))
                  (start-num (list-ref alist 1))
                  (bin-num (list-ref alist 2))
                  (end-num (list-ref alist 3))
                  (max-gap (list-ref alist 4))
                  (shouldbe-list (list-ref alist 5))
                  (shouldbe-htable-list-list (list-ref alist 6)))
              (begin
                (hash-clear! bin-pgaps-htable)
                (hash-clear! cumulative-pgaps-htable)

                (let ((result-list
                       (prime-gaps-module:process-bin-sprp
                        last-prime start-num bin-num end-num max-gap
                        bin-pgaps-htable cumulative-pgaps-htable)))
                  (begin
                    (let ((result-max (list-ref result-list 1)))
                      (let ((err-1
                             (format
                              #f  "~a : (~a) : "
                              sub-name test-label-index))
                            (err-2
                             (format
                              #f  "error for bin (~a, ~a, ~a, ~a), "
                              start-num bin-num end-num max-gap))
                            (err-3
                             (format
                              #f  "max = ~a " result-max)))
                        (begin
                          (prime-gaps-assert-lists-equal
                           shouldbe-list result-list
                           sub-name
                           (string-append err-1 err-2 err-3)
                           result-htable)

                          (let ((err-4
                                 (string-append err-1 err-2 err-3)))
                            (begin
                              (for-each
                               (lambda (alist)
                                 (begin
                                   (let ((agap (list-ref alist 0))
                                         (svalue (list-ref alist 1)))
                                     (let ((bvalue
                                            (hash-ref
                                             bin-pgaps-htable agap 0))
                                           (cvalue
                                            (hash-ref
                                             cumulative-pgaps-htable agap 0)))
                                       (let ((err-5
                                              (format
                                               #f "~a, agap = ~a, "
                                               err-4 agap))
                                             (err-6
                                              (format
                                               #f "svalue = ~a, "
                                               svalue))
                                             (err-7
                                              (format
                                               #f "bvalue = ~a, "
                                               bvalue))
                                             (err-8
                                              (format
                                               #f "cvalue = ~a"
                                               cvalue)))
                                         (begin
                                           (unittest2:assert?
                                            (equal? svalue bvalue)
                                            sub-name
                                            (string-append
                                             err-5 err-6 err-7)
                                            result-htable)

                                           (unittest2:assert?
                                            (equal? svalue cvalue)
                                            sub-name
                                            (string-append
                                             err-5 err-6 err-8)
                                            result-htable)
                                           ))
                                       ))
                                   )) shouldbe-htable-list-list)
                              ))
                          )))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; line definition
;;; = start bin num, end bin num, last prime, num primes, percent primes,
;;; max-gap, { gap, number in bin gap, cumulative number for gap = 2, 4, 6,
;;; ..., max-gap, } then final-gap, number in final gap, cumulative number
;;; in final gap
(unittest2:define-tests-macro
 (test-parse-results-line-1 result-htable)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-parse-results-line-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (string-append
             " 3 | 10 | 7 | 4 | 40% | 4 | 2 | 2 | "
             "2 | 4 | 0 | 0 | 6 | 0 | 0")
            7
            (list (list 2 2) (list 4 0) (list 6 0)))
           (list
            (string-append
             "10 | 20 | 19 | 4 | 40% | 4 | 2 | 2 | "
             "2 | 4 | 2 | 2 | 6 | 0 | 0")
            19
            (list (list 2 2) (list 4 2) (list 6 0)))
           (list
            (string-append
             "20 | 30 | 29 | 2 | 20% | 4 | 2 | 0 | "
             "0 | 4 | 1 | 1 | 6 | 1 | 1")
             29
             (list (list 2 0) (list 4 1) (list 6 1)))
           ))
         (cumulative-pgaps-htable (make-hash-table))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((line-string (list-ref alist 0))
                  (shouldbe-last-prime (list-ref alist 1))
                  (shouldbe-htable-list-list (list-ref alist 2)))
              (begin
                (hash-clear! cumulative-pgaps-htable)

                (let ((result-last-prime
                       (prime-gaps-module:parse-results-line
                        line-string cumulative-pgaps-htable)))
                  (let ((err-1
                         (format
                          #f  "~a : (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f  "error for line-string = ~s, "
                          line-string))
                        (err-3
                         (format
                          #f  "shouldbe = ~a, result = ~a"
                          shouldbe-last-prime
                          result-last-prime)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-last-prime
                               result-last-prime)
                       sub-name
                       (string-append
                        err-1 err-2 err-3)
                       result-htable)

                      (for-each
                       (lambda (alist)
                         (begin
                           (let ((agap (list-ref alist 0))
                                 (svalue (list-ref alist 1)))
                             (let ((cvalue
                                    (hash-ref
                                     cumulative-pgaps-htable agap 0)))
                               (let ((err-4
                                      (format
                                       #f "agap = ~a, "
                                       agap))
                                     (err-5
                                      (format
                                       #f "svalue = ~a, "
                                       svalue))
                                     (err-6
                                      (format
                                       #f "cvalue = ~a"
                                       cvalue)))
                                 (begin
                                   (unittest2:assert?
                                    (equal? svalue cvalue)
                                    sub-name
                                    (string-append
                                     err-1 err-2
                                     err-4 err-5 err-6)
                                    result-htable)
                                   ))
                               ))
                           )) shouldbe-htable-list-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (prime-gaps-assert-lists-equal
         shouldbe-list result-list sub-name
         error-message result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((err-1
             (format
              #f "~a : shouldbe-list=~a, result-list=~a : "
              error-message shouldbe-list result-list))
            (err-2
             (format
              #f "list sizes not equal, "))
            (err-3
             (format
              #f "shouldbe size=~a, result size=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append
            err-1 err-2 err-3)
           result-hash-table)

          (for-each
           (lambda (s-elem)
             (begin
               (let ((found-flag (member s-elem result-list)))
                 (let ((err-4
                        (format
                         #f "missing element=~a"
                         s-elem)))
                   (begin
                     (unittest2:assert?
                      (not (equal? found-flag #f))
                      sub-name
                      (string-append
                       err-1 err-4)
                      result-hash-table)
                     )))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
