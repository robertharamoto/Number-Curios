#! /bin/bash

for ii in "*.scm~" "*.sh~" "*.org~"
do
    echo "  ### looking for files ${ii} ###"
    find . -iname "${ii}" -print -delete
done

date

