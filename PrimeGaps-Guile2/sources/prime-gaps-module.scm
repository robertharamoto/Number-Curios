;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  Puzzle Module for prime gaps                         ###
;;;###                                                       ###
;;;###  last updated October 7, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start prime-gaps-module
(define-module (prime-gaps-module)
  #:export (process-bin-sprp
            parse-results-line

            main-loop
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### read/write functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### timer-module for current-date-time-string
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### utils-module for commafy-number function
(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;### sprp-module for sprp-prime? function
(use-modules ((sprp-module)
              :renamer (symbol-prefix-proc 'sprp-module:)))

;;;#############################################################
;;;#############################################################
;;;          1         2         3         4         5         6
;;; 123456789012345678901234567890123456789012345678901234567890
;;;  pp p p   p p   p p   p     p p     p   p p   p     p     p
(define (process-bin-sprp
         last-prime start-num bin-num end-num max-gap
         bin-pgaps-htable cumulative-pgaps-htable)
  (begin
    (let ((begin-num (+ last-prime 2))
          (prime-count 0)
          (final-gap (+ max-gap 2))
          (prev-prime last-prime)
          (ll-tmp
           (inexact->exact (truncate (/ start-num bin-num)))))
      (let ((ll-max (* (+ ll-tmp 1) bin-num)))
        (begin
          (if (< last-prime 0)
              (begin
                (if (even? start-num)
                    (begin
                      (set! begin-num (- start-num 1))
                      ))

                (if (<= begin-num 2)
                    (begin
                      (set! begin-num 3)
                      (set! prime-count 1))
                    (begin
                      (if (sprp-module:sprp-prime? begin-num)
                          (begin
                            (set! prev-prime begin-num)
                            (set! begin-num (+ begin-num 2))
                            ))
                      ))
                ))

          (if (> ll-max end-num)
              (begin
                (set! ll-max end-num)
                ))

          (do ((jj begin-num (+ jj 2)))
              ((> jj ll-max))
            (begin
              (if (sprp-module:sprp-prime? jj)
                  (begin
                    (set! prime-count (+ prime-count 1))
                    (if (> prev-prime 0)
                        (begin
                          (let ((gap (- jj prev-prime))
                                (index-gap -1))
                            (begin
                              (if (< gap final-gap)
                                  (begin
                                    (set! index-gap gap))
                                  (begin
                                    (set! index-gap final-gap)
                                    ))

                              (let ((gcount
                                     (hash-ref
                                      bin-pgaps-htable
                                      index-gap 0))
                                    (cumulative-gcount
                                     (hash-ref
                                      cumulative-pgaps-htable
                                      index-gap 0)))
                                (begin
                                  (hash-set!
                                   bin-pgaps-htable
                                   index-gap (1+ gcount))
                                  (hash-set!
                                   cumulative-pgaps-htable
                                   index-gap (1+ cumulative-gcount))
                                  ))
                              ))
                          ))

                    (set! prev-prime jj)
                    ))
              ))

          (list start-num ll-max prev-prime prime-count)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (display-results
         k bin-num end-num last-prime
         nprimes max-gap bin-pgaps-htable
         cumulative-pgaps-htable)
  (begin
    (display
     (ice-9-format:format
      #f "~:d | ~:d | ~:d | ~:d | ~4,1f% | ~:d |"
      k end-num last-prime nprimes
      (utils-module:round-float
       (* 100.0 (/ nprimes bin-num)) 1.0)
      max-gap))

    (do ((pgap 2 (+ pgap 2)))
        ((> pgap max-gap))
      (begin
        (let ((bin-num
               (hash-ref bin-pgaps-htable pgap 0))
              (cumulative-num
               (hash-ref cumulative-pgaps-htable pgap 0)))
          (begin
            (display
             (ice-9-format:format
              #f " ~:d | ~:d | ~:d |"
              pgap bin-num cumulative-num))
            ))
        ))

    (let ((final-gap (+ max-gap 2)))
      (let ((bin-num
             (hash-ref bin-pgaps-htable final-gap 0))
            (cumulative-num
             (hash-ref cumulative-pgaps-htable final-gap 0)))
        (begin
          (display
           (ice-9-format:format
            #f " ~:d | ~:d | ~:d"
            final-gap bin-num cumulative-num))
          )))

    (newline)
    (force-output)
    ))


;;;#############################################################
;;;#############################################################
;;; line definition
;;; = start bin num, end bin num, last prime, num primes, percent primes,
;;; max-gap, { gap, number in bin gap, cumulative number for gap = 2, 4, 6,
;;; ..., max-gap, } then final-gap, number in final gap, cumulative number
;;; in final gap
(define (parse-results-line
         line cumulative-pgaps-htable)
  (begin
    (let ((alist1 (string-split line #\|)))
      (let ((alist2 (map string-trim-both alist1)))
        (let ((alist3
               (map
                (lambda (astr)
                  (begin
                    (let ((no-commas-str (string-delete #\, astr)))
                      (let ((anum (string->number no-commas-str)))
                        (begin
                          (if (number? anum)
                              (begin
                                anum)
                              (begin
                                no-commas-str
                                ))
                          )))
                    )) alist2)))
          (begin
            ;;; fill cumulative bin numbers
            (hash-clear! cumulative-pgaps-htable)
            (if (number? (list-ref alist3 5))
                (begin
                  (let ((max-gap (list-ref alist3 5)))
                    (let ((max-index
                           (+ 6 (* 3 (euclidean/ max-gap 2))))
                          (start-index 6))
                      (begin
                        (do ((jj-index start-index (+ jj-index 3)))
                            ((> jj-index max-index))
                          (begin
                            (let ((gap (list-ref alist3 jj-index))
                                  (cumulative-count
                                   (list-ref
                                    alist3 (+ jj-index 2))))
                              (begin
                                (hash-set!
                                 cumulative-pgaps-htable
                                 gap cumulative-count)
                                ))
                            ))

                        (let ((last-prime (list-ref alist3 2)))
                          (begin
                            last-prime
                            ))
                        ))
                    ))
                (begin
                  -1
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (load-up-previous-results
         input-filename cumulative-pgaps-htable)
  (begin
    (if (file-exists? input-filename)
        (begin
          (let ((prev-port (open-input-file input-filename))
                (last-line ""))
            (begin
              (do ((line
                    (ice-9-rdelim:read-line prev-port)
                    (ice-9-rdelim:read-line prev-port)))
                  ((eof-object? line))
                (begin
                  (if (not (eof-object? line))
                      (begin
                        (set! last-line line)
                        ))
                  ))
              (let ((last-prime
                     (parse-results-line
                      last-line cumulative-pgaps-htable)))
                (begin
                  last-prime
                  ))
              )))
        (begin
          (hash-clear! cumulative-pgaps-htable)
          -1
          ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax status-message-macro
  (syntax-rules ()
    ((status-message-macro
      nn end-num bin-counter status-num)
     (begin
       (if (zero? (modulo bin-counter status-num))
           (begin
             (display
              (ice-9-format:format
               #f "status so far ~:d / ~:d : " nn end-num))
             (display
              (format
               #f "~a~%"
               (timer-module:current-date-time-string)))
             (force-output)
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;;          1         2         3         4         5         6
;;; 123456789012345678901234567890123456789012345678901234567890
;;;  pp p p   p p   p p   p     p p     p   p p   p     p     p
(define (count-twin-primes-in-bins-sprp
         start-num bin-num end-num last-prime
         max-gap status-num
         cumulative-pgaps-htable)
  (define (local-main-loop
           start-num bin-num end-num last-prime max-gap
           status-num cumulative-pgaps-htable)
    (begin
      (let ((jj start-num)
            (total-primes 0)
            (previous-prime last-prime)
            (bin-counter 0)
            (bin-pgaps-htable (make-hash-table)))
        (begin
          (while (< jj end-num)
            (begin
              (if (< previous-prime jj)
                  (begin
                    (hash-clear! bin-pgaps-htable)
                    (set! bin-counter (1+ bin-counter))

                    (let ((result-list
                           (process-bin-sprp
                            previous-prime jj bin-num end-num max-gap
                            bin-pgaps-htable cumulative-pgaps-htable)))
                      (let ((new-jj (list-ref result-list 0))
                            (ll-max (list-ref result-list 1))
                            (prev-prime (list-ref result-list 2))
                            (prime-count (list-ref result-list 3)))
                        (begin
                          (set! jj ll-max)
                          (set! total-primes
                                (+ total-primes prime-count))
                          (set! previous-prime prev-prime)

                          (display-results
                           new-jj bin-num ll-max
                           prev-prime prime-count
                           max-gap bin-pgaps-htable
                           cumulative-pgaps-htable)
                          ))
                      ))
                  (begin
                    (set! jj (+ jj bin-num))
                    ))

              (set! bin-counter (1+ bin-counter))
              ))
          ))
      ))
  (begin
    ;;; main code
    ;;; print header line
    (display
     (format
      #f "start-num | end-num | last-prime | nprimes | "))
    (display
     (format
      #f "pcnt | max-gap | "))

    (do ((pgap 2 (+ pgap 2)))
        ((> pgap max-gap))
      (begin
        (display
         (format
          #f "gap=~a | bin-~a-count | cumulative-~a-count | "
          pgap pgap pgap))
        ))

    (let ((final-gap (+ max-gap 2)))
      (begin
        (display
         (format #f "gap=~a+ | bin-~a+-count | cumulative-~a+-count"
                 final-gap final-gap final-gap))
        ))
    (newline)
    (force-output)


    (let ((begin-num start-num))
      (begin
        (if (even? start-num)
            (begin
              (set! begin-num (1+ begin-num))
              ))

        (local-main-loop
         begin-num bin-num end-num
         last-prime max-gap status-num
         cumulative-pgaps-htable)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop
         start-num bin-num end-num max-gap status-num)
  (begin
    (let ((cumulative-pgaps-htable (make-hash-table))
          (last-prime -1))
      (begin
        (count-twin-primes-in-bins-sprp
         start-num bin-num end-num last-prime
         max-gap status-num
         cumulative-pgaps-htable)
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
