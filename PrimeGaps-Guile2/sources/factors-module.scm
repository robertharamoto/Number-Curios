;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  factors-module - factors and divisors functions      ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated August 25, 2022                              ###
;;;###                                                       ###
;;;###  updated February 27, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define-module (factors-module)
  #:export (number-of-divisors
            unique-divisors
            sum-of-divisors

            remove-factors-of-k
            find-max-prime-divisor
            find-min-prime-divisor
            find-min-prime-divisor-max-minutes-list

            prime-factors-list
            make-prime-factors-hash-table
            number-to-prime-factors-to-string

            group-prime-exp-pairs
            exp-pairs-list-to-string

            integer-exp
            sum-of-divisors-v1
            proper-sum-of-divisors-v1
            radical-v1
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### srfi-11 for let-values
(use-modules ((srfi srfi-11)
              :renamer (symbol-prefix-proc 'srfi-11:)))

;;;### srfi-19 for julian date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### sprp-module for sprp-prime?
(use-modules ((sprp-module)
              :renamer (symbol-prefix-proc 'sprp-module:)))

;;;### timer-module for julian-day-difference-in-minutes function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;#############################################################
;;;#############################################################
(define (number-of-divisors input-number)
  (begin
    (let ((div-count 0))
      (begin
        (cond
         ((<= input-number 0)
          (begin
            0
            ))
         ((= input-number 1)
          (begin
            1
            ))
         ((= input-number 2)
          (begin
            2
            ))
         ((= input-number 3)
          (begin
            2
            ))
         (else
          (begin
            (let ((ll-max
                   (1+ (exact-integer-sqrt input-number))))
              (begin
                (do ((ii 1 (1+ ii)))
                    ((> ii ll-max))
                  (begin
                    (srfi-11:let-values
                     (((ll-div ll-remainder)
                       (euclidean/ input-number ii)))
                     (begin
                       (if (zero? ll-remainder)
                           (begin
                             (if (<= ii ll-div)
                                 (begin
                                   (if (equal? ii ll-div)
                                       (begin
                                         (set!
                                          div-count
                                          (1+ div-count)))
                                       (begin
                                         (set!
                                          div-count
                                          (+ div-count 2))
                                         ))
                                   ))
                              ))
                          ))
                    ))

                div-count
                ))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (unique-divisors input-number)
  (begin
    (let ((div-list (list)))
      (begin
        (cond
         ((<= input-number 0)
          (begin
            #f
            ))
         ((= input-number 1)
          (begin
            (list 1)
            ))
         ((= input-number 2)
          (begin
            (list 1 2)
            ))
         ((= input-number 3)
          (begin
            (list 1 3)
            ))
         ((= input-number 4)
          (begin
            (list 1 2 4)
            ))
         ((= input-number 5)
          (begin
            (list 1 5)
            ))
         (else
          (begin
            (let ((ll-max
                   (1+ (exact-integer-sqrt
                        input-number))))
              (begin
                (do ((ii 1 (1+ ii)))
                    ((> ii ll-max))
                  (begin
                    (srfi-11:let-values
                     (((ll-div ll-remainder)
                       (euclidean/ input-number ii)))
                     (begin
                       (if (zero? ll-remainder)
                           (begin
                             (if (equal?
                                  (member ii div-list) #f)
                                 (begin
                                   (set!
                                    div-list (cons ii div-list))
                                   ))
                             (if (equal?
                                  (member ll-div div-list) #f)
                                 (begin
                                   (set!
                                    div-list (cons ll-div div-list))
                                   ))
                             ))
                       ))
                    ))

                (sort div-list <)
                ))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sum-of-divisors input-number)
  (begin
    (cond
     ((<= input-number 0)
      (begin
        #f
        ))
     ((= input-number 1)
      (begin
        1
        ))
     (else
      (begin
        (let ((pr-list
               (prime-factors-list input-number)))
          (let ((pr-htable
                 (make-prime-factors-hash-table pr-list)))
            (let ((result
                   (sum-of-divisors-v1 pr-htable)))
              (begin
                result
                ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (remove-factors-of-k nn kk)
  (begin
    (let ((result nn))
      (begin
        (while (zero? (modulo result kk))
          (begin
            (set! result (euclidean/ result kk))
            ))
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; modified version of the seive of eratosthenes
(define (find-max-prime-divisor ss)
  (begin
    (cond
     ((<= ss 1)
      (begin
        #f
        ))
     ((= ss 2)
      (begin
        2
        ))
     ((= ss 3)
      (begin
        3
        ))
     ((equal? (sprp-module:sprp-prime? ss) #t)
      (begin
        ss
        ))
     (else
      (begin
        (let ((this-num ss)
              (ll-max (1+ (exact-integer-sqrt ss)))
              (max-factor 1)
              (done-flag #f))
          (begin
            (if (even? this-num)
                (begin
                  (set! max-factor 2)
                  (let ((ll-next (remove-factors-of-k this-num 2)))
                    (begin
                      (if (equal?
                           (sprp-module:sprp-prime? ll-next) #t)
                          (begin
                            (set! max-factor ll-next)
                            (set! done-flag #t))
                          (begin
                            (set! this-num ll-next)
                            ))
                      ))
                  ))

            (let ((jj 3))
              (begin
                (while
                 (and (> this-num 1)
                      (equal? done-flag #f))
                 (begin
                   (let ((ll-remainder
                          (euclidean-remainder this-num jj)))
                     (begin
                       (if (zero? ll-remainder)
                           (begin
                             (if (> jj max-factor)
                                 (begin
                                   (set! max-factor jj)
                                   ))

                             (let ((ll-next-div
                                    (remove-factors-of-k
                                     this-num jj)))
                               (begin
                                 (if (equal?
                                      (sprp-module:sprp-prime?
                                       ll-next-div)
                                      #t)
                                     (begin
                                       (if (> ll-next-div max-factor)
                                           (begin
                                             (set! max-factor ll-next-div)
                                             ))

                                       (set! done-flag #t)
                                       ))

                                 (set! this-num ll-next-div)
                                 ))
                             ))

                       (set! jj (+ jj 2))
                       (if (> jj ll-max)
                           (begin
                             (set! done-flag #t)
                             ))
                       ))
                   ))
                ))

            max-factor
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (find-min-prime-divisor ss)
  (begin
    (cond
     ((<= ss 1)
      (begin
        #f
        ))
     ((even? ss)
      (begin
        2
        ))
     ((equal? (sprp-module:sprp-prime? ss) #t)
      (begin
        ss
        ))
     ((zero? (modulo ss 3))
      (begin
        3
        ))
     (else
      (begin
        (let ((smallest-div -1)
              (max-num (1+ (exact-integer-sqrt ss))))
          (begin
            (do ((jj 3 (+ jj 2)))
                ((or (> jj max-num)
                     (> smallest-div 0)))
              (begin
                (srfi-11:let-values
                 (((ll-div ll-remainder)
                   (euclidean/ ss jj)))
                 (begin
                   (if (zero? ll-remainder)
                       (begin
                         (set! smallest-div jj)
                         (if (< ll-div jj)
                             (begin
                               (set! smallest-div ll-div)
                               ))
                         ))
                   ))
                ))

            (if (> smallest-div 0)
                (begin
                  smallest-div)
                (begin
                  ss
                  ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (find-min-prime-divisor-max-minutes-list
         ss max-minutes)
  (begin
    (cond
     ((<= ss 1)
      (begin
        #f
        ))
     ((even? ss)
      (begin
        2
        ))
     ((equal? (sprp-module:sprp-prime? ss) #t)
      (begin
        ss
        ))
     ((zero? (modulo ss 3))
      (begin
        3
        ))
     (else
      (begin
        (let ((smallest-div -1)
              (error-flag 0)
              (start-jday (srfi-19:current-julian-day))
              (max-num (1+ (exact-integer-sqrt ss)))
              (check-count 10000)
              (counter 0))
          (begin
            (do ((jj 3 (+ jj 2)))
                ((or (> jj max-num)
                     (> smallest-div 0)))
              (begin
                (srfi-11:let-values
                 (((ll-div ll-remainder)
                   (euclidean/ ss jj)))
                 (begin
                   (if (zero? ll-remainder)
                       (begin
                         (set! smallest-div jj)
                         (if (< ll-div jj)
                             (begin
                               (set! smallest-div ll-div)
                               ))
                         ))
                   ))

                (set! counter (1+ counter))
                (if (zero? (modulo counter check-count))
                    (begin
                      (let ((end-jday (srfi-19:current-julian-day)))
                        (let ((elapsed-mins
                               (timer-module:julian-day-difference-in-minutes
                                end-jday start-jday)))
                          (begin
                            (if (>= elapsed-mins max-minutes)
                                (begin
                                  (display
                                   (ice-9-format:format
                                    #f "(~:d minutes) time expired...~%"
                                    elapsed-mins))
                                  (force-output)
                                  (set! smallest-div -1)
                                  (set! error-flag -1)
                                  ))
                            )))
                      ))
                ))

            (if (> smallest-div 0)
                (begin
                  smallest-div)
                (begin
                  (if (>= error-flag 0)
                      (begin
                        ss)
                      (begin
                        error-flag
                        ))
                  ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax prime-factors-helper-macro
  (syntax-rules ()
    ((prime-factors-helper-macro
      local-nn kk result-list
      local-divide-all-factors)
     (begin
       (if (zero? (modulo local-nn kk))
           (begin
             (let ((ll-list
                    (local-divide-all-factors local-nn kk)))
               (let ((ll-remainder (list-ref ll-list 0))
                     (acc-list (list-ref ll-list 1)))
                 (begin
                   (if (equal?
                        (sprp-module:sprp-prime? ll-remainder)
                        #t)
                       (begin
                         (set! local-nn 1)
                         (set!
                          acc-list
                          (append acc-list (list ll-remainder))))
                       (begin
                         (set! local-nn ll-remainder)
                         ))

                   (set!
                    result-list
                    (append result-list acc-list))
                   )))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; use a seive of eratosthenes type of method of removing prime factors of n
(define (prime-factors-list nn)
  (define (local-divide-all-factors this-num this-factor)
    (begin
      (let ((ll-num this-num)
            (acc-list (list)))
        (begin
          (while (and
                  (zero? (modulo ll-num this-factor))
                  (> ll-num 1))
            (begin
              (set! ll-num (/ ll-num this-factor))
              (set! acc-list (cons this-factor acc-list))
              ))
          (list ll-num acc-list)
          ))
      ))
  (begin
    (let ((local-nn nn)
          (max-kk (1+ (exact-integer-sqrt nn)))
          (result-list (list)))
      (begin
        (prime-factors-helper-macro
         local-nn 2 result-list
         local-divide-all-factors)

        (let ((kk 3)
              (done-flag #f))
          (begin
            (while
             (and (> local-nn 1)
                  (equal? done-flag #f))
             (begin
               (prime-factors-helper-macro
                local-nn kk result-list
                local-divide-all-factors)

               (set! kk (+ kk 2))
               (if (> kk max-kk)
                   (begin
                     (if (> local-nn 1)
                         (begin
                           (set! result-list
                                 (cons local-nn result-list))
                           ))
                     (set! done-flag #t)
                     (break #f)
                     ))
               ))
            ))

        (if (< (length result-list) 1)
            (begin
              (list nn))
            (begin
              (sort result-list <)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-prime-factors-hash-table prime-list)
  (begin
    (let ((pr-htable (make-hash-table 10)))
      (begin
        (for-each
         (lambda (anum)
           (begin
             (let ((hcount
                    (hash-ref pr-htable anum 0)))
               (begin
                 (hash-set! pr-htable anum (1+ hcount))
                 ))
             )) prime-list)

        pr-htable
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (htable-to-string pr-htable)
  (begin
    (let ((str-list (list))
          (key-list
           (sort
            (hash-map->list
             (lambda (key value) key)
             pr-htable)
            <)))
      (begin
        (for-each
         (lambda (aprime)
           (begin
             (let ((aexp (hash-ref pr-htable aprime)))
               (begin
                 (if (= aexp 1)
                     (begin
                       (set!
                        str-list
                        (cons
                         (ice-9-format:format
                          #f "~:d" aprime)
                         str-list)))
                     (begin
                       (set!
                        str-list
                        (cons
                         (ice-9-format:format
                          #f "~:d^~:d" aprime aexp)
                         str-list))
                       ))
                 ))
             )) key-list)

        str-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (number-to-prime-factors-to-string anum)
  (begin
    (let ((pr-list (prime-factors-list anum)))
      (let ((pr-htable
             (make-prime-factors-hash-table pr-list)))
        (let ((a-string-list
               (htable-to-string pr-htable)))
          (let ((result-string
                 (string-join
                  (reverse a-string-list)
                  " * ")))
            (begin
              result-string
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (group-prime-exp-pairs pfactors-list)
  (begin
    (if (null? pfactors-list)
        (begin
          (list))
        (begin
          (let ((result-list (list))
                (prev-num -1)
                (prev-count 0)
                (llen (length pfactors-list)))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((>= ii llen))
                (begin
                  (let ((pp (list-ref pfactors-list ii)))
                    (begin
                      (if (equal? pp prev-num)
                          (begin
                            (set! prev-count (1+ prev-count)))
                          (begin
                            (if (not (equal? prev-num -1))
                                (begin
                                  (set!
                                   result-list
                                   (cons
                                    (list prev-num prev-count)
                                    result-list))
                                  ))
                            (set! prev-num pp)
                            (set! prev-count 1)
                            ))
                      ))
                  ))
              (if (> prev-num 0)
                  (begin
                    (reverse
                     (cons
                      (list prev-num prev-count)
                      result-list))
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (exp-pairs-list-to-string exp-pairs-list)
  (begin
    (if (null? exp-pairs-list)
        (begin
          "1")
        (begin
          (let ((rstring "")
                (spairs-list
                 (sort
                  exp-pairs-list
                  (lambda (a b)
                    (begin
                      (< (car a) (car b))
                      ))
                  )))
            (begin
              (for-each
               (lambda (apair)
                 (begin
                   (let ((abase (car apair))
                         (aexp (cadr apair)))
                     (let ((astr
                            (ice-9-format:format
                             #f "~:d^~:d" abase aexp)))
                       (begin
                         (if (equal? aexp 1)
                             (begin
                               (set!
                                astr
                                (ice-9-format:format
                                 #f "~:d" abase))
                               ))

                         (if (string-ci=? rstring "")
                             (begin
                               (set! rstring astr))
                             (begin
                               (set!
                                rstring
                                (string-append rstring " * " astr))
                               ))
                         )))
                   )) spairs-list)
              rstring
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (integer-exp bb ee)
  (begin
    (let ((result 1)
          (base bb)
          (exponent ee))
      (begin
        (while (> exponent 0)
          (begin
            (if (odd? exponent)
                (begin
                  (set! result (* result base))
                  ))

            (set! exponent (euclidean/ exponent 2))
            (set! base (* base base))
            ))

        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sum-of-divisors-v1 prime-htable)
  (begin
    (let ((product 1))
      (begin
        (hash-for-each
         (lambda (aprime acount)
           (begin
             (let ((numer
                    (1- (integer-exp aprime (1+ acount))))
                   (denom
                    (1- aprime)))
               (begin
                 (set!
                  product
                  (* product (euclidean/ numer denom)))
                 ))
             )) prime-htable)

        product
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (proper-sum-of-divisors-v1 nn prime-htable)
  (begin
    (if (> nn 1)
        (begin
          (let ((sum-all-div
                 (sum-of-divisors-v1 prime-htable)))
            (begin
              (- sum-all-div nn)
              )))
        (begin
          0
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (radical-v1 prime-htable)
  (begin
    (let ((product 1))
      (begin
        (hash-for-each
         (lambda (aprime acount)
           (begin
             (set! product (* product aprime))
             )) prime-htable)

        product
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
