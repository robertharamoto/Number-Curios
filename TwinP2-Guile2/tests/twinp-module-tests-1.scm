;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for twinp-module.scm                      ###
;;;###                                                       ###
;;;###  last updated October 6, 2024                         ###
;;;###                                                       ###
;;;###  updated June 2, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 18, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

(use-modules ((twinp-module)
              :renamer (symbol-prefix-proc 'twinp-module:)))

;;;#############################################################
;;;#############################################################
(define (twinp-assert-function
         sub-name test-label-index
         start-num bin-num end-num
         shouldbe-prime-sum result-prime-sum
         shouldbe-twin-prime-sum
         result-twin-prime-sum
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f  "~a : (~a)"
            sub-name test-label-index))
          (err-2
           (format
            #f  " : error for number (~a, ~a, ~a)"
            start-num bin-num end-num))
          (err-3
           (format
            #f  "shouldbe prime count = ~a, result = ~a"
            shouldbe-prime-sum result-prime-sum))
          (err-4
           (format
            #f  "shouldbe twin prime count = ~a"
            shouldbe-twin-prime-sum))
          (err-5
           (format
            #f  ", result = ~a"
            result-twin-prime-sum)))
      (begin
        (unittest2:assert?
         (equal? shouldbe-prime-sum result-prime-sum)
         sub-name
         (string-append
          err-1 err-2 err-3)
         result-hash-table)

        (unittest2:assert?
         (equal?
          shouldbe-twin-prime-sum
          result-twin-prime-sum)
         sub-name
         (string-append
          err-1 err-2 err-4 err-5)
         result-hash-table)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;          1         2         3         4         5         6
;;; 123456789012345678901234567890123456789012345678901234567890
;;;  pp p p   p p   p p   p     p p     p   p p   p     p     p
(unittest2:define-tests-macro
 (test-process-bin-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-process-bin-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 10 100 4 2)
           (list 10 10 200 4 2)
           (list 20 10 300 2 0)
           (list 30 10 400 2 1)
           (list 40 10 500 3 1)
           (list 50 10 1000 2 0)
           ))
         (prime-array (prime-module:make-prime-array 10))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((start-num (list-ref alist 0))
                  (bin-num (list-ref alist 1))
                  (end-num (list-ref alist 2))
                  (shouldbe-prime-sum (list-ref alist 3))
                  (shouldbe-twin-prime-sum (list-ref alist 4)))
              (let ((result-list
                     (twinp-module:process-bin
                      start-num bin-num end-num prime-array)))
                (let ((result-prime-sum (list-ref result-list 2))
                      (result-twin-prime-sum (list-ref result-list 3)))
                  (begin
                    (twinp-assert-function
                     sub-name test-label-index
                     start-num bin-num end-num
                     shouldbe-prime-sum result-prime-sum
                     shouldbe-twin-prime-sum
                     result-twin-prime-sum
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;;          1         2         3         4         5         6
;;; 123456789012345678901234567890123456789012345678901234567890
;;;  pp p p   p p   p p   p     p p     p   p p   p     p     p
(unittest2:define-tests-macro
 (test-process-bin-sprp-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-process-bin-sprp-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 10 100 4 2)
           (list 10 10 200 4 2)
           (list 20 10 300 2 0)
           (list 30 10 400 2 1)
           (list 40 10 500 3 1)
           (list 50 10 1000 2 0)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((start-num (list-ref alist 0))
                  (bin-num (list-ref alist 1))
                  (end-num (list-ref alist 2))
                  (shouldbe-prime-sum (list-ref alist 3))
                  (shouldbe-twin-prime-sum (list-ref alist 4)))
              (let ((result-list
                     (twinp-module:process-bin-sprp
                      start-num bin-num end-num)))
                (let ((result-prime-sum (list-ref result-list 2))
                      (result-twin-prime-sum (list-ref result-list 3)))
                  (begin
                    (twinp-assert-function
                     sub-name test-label-index
                     start-num bin-num end-num
                     shouldbe-prime-sum result-prime-sum
                     shouldbe-twin-prime-sum
                     result-twin-prime-sum
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
