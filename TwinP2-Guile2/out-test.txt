Mon Oct  7 06:20:19 AM HST 2024
load file ./objects/utils-module-tests-1.go
load file ./objects/twinp-module-tests-1.go
load file ./objects/prime-module-tests-1.go
load file ./objects/timer-module-tests-1.go
load file ./objects/config-module-tests-1.go
load file ./objects/sprp-module-tests-1.go

################################################################
################################################################
###                                                          ###
###  Run unit tests (version 2024-09-12)                     ###
###                                                          ###
###  number of tests                    36                   ###
###                                                          ###
###  successful asserts              1,065       100.00%     ###
###  failed asserts                      0         0.00%     ###
###  ---------                   ---------     ---------     ###
###  total asserts                   1,065       100.00%     ###
###                                                          ###
################################################################
################################################################
(unit test version 2024-09-26)

elapsed time = 0.047 seconds : monday, october 07, 2024  06:20:19 am


sources loc =  3235 total
tests loc =  2982 total
total loc =  6217 total
