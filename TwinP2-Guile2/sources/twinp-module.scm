;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  Twin primes conjecture - there are                   ###
;;;###  infinitely many twin primes p and p + 2              ###
;;;###  prints out number of primes cummulative,             ###
;;;###  and number of twin primes cummulative                ###
;;;###                                                       ###
;;;###  last updated October 7, 2024                         ###
;;;###                                                       ###
;;;###  updated June 2, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 18, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start twinp modules
(define-module (twinp-module)
  #:export (process-bin
            process-bin-sprp

            run-twins
            run-twins-sprp
            ))

;;;#############################################################
;;;#############################################################
;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9-format for advanced formatting
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### utils for commafy functions
(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;### timer module for date/time functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### prime module for is-array-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### sprp module for sprp-prime? function
(use-modules ((sprp-module)
              :renamer (symbol-prefix-proc 'sprp-module:)))

;;;#############################################################
;;;#############################################################
;;;          1         2         3         4         5         6
;;; 123456789012345678901234567890123456789012345678901234567890
;;;  pp p p   p p   p p   p     p p     p   p p   p     p     p
(define (process-bin start-num bin-num end-num prime-array)
  (begin
    (let ((begin-num (- start-num 2))
          (prime-count 0)
          (twin-prime-count 0)
          (ll-tmp
           (inexact->exact
            (truncate (/ start-num bin-num)))))
      (let ((ll-max (* (+ ll-tmp 1) bin-num))
            (last-prime -1))
        (begin
          (if (even? start-num)
              (begin
                (set! begin-num (- start-num 1))
                ))

          (if (<= begin-num 2)
              (begin
                (set! begin-num 3)
                (set! prime-count 1))
              (begin
                (if (prime-module:is-array-prime?
                     begin-num prime-array)
                    (begin
                      (set! last-prime begin-num)
                      (set! begin-num (+ begin-num 2))
                      ))
                ))

          (if (> ll-max end-num)
              (begin
                (set! ll-max end-num)
                ))

          (do ((jj begin-num (+ jj 2)))
              ((> jj ll-max))
            (begin
              (if (prime-module:is-array-prime?
                   jj prime-array)
                  (begin
                    (set! prime-count (1+ prime-count))
                    (if (equal? (- jj last-prime) 2)
                        (begin
                          (set!
                           twin-prime-count
                           (1+ twin-prime-count))
                          ))
                    (set! last-prime jj)
                    ))
              ))

          (list start-num ll-max prime-count twin-prime-count)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;;          1         2         3         4         5         6
;;; 123456789012345678901234567890123456789012345678901234567890
;;;  pp p p   p p   p p   p     p p     p   p p   p     p     p
(define (old-count-twin-primes-in-bins
         start-num bin-num end-num max-prime out-filename)
  (define (local-display-results
           k bin-num end-num nprimes ntwprimes
           pi-primes pi-twprimes out-file-handle)
    (begin
      (if (> nprimes 0)
          (begin
            (let ((pcnt
                   (utils-module:round-float
                    (* 100.0 (/ nprimes bin-num)) 1.0))
                  (tpcnt
                   (utils-module:round-float
                    (* 100.0 (/ ntwprimes nprimes)) 1.0)))
              (begin
                (display
                 (ice-9-format:format
                  #f "~:d | ~:d | ~:d | ~4,1f% | ~:d | ~4,1f% | ~:d | ~:d~%"
                  k end-num nprimes
                  pcnt
                  ntwprimes
                  tpcnt
                  pi-primes pi-twprimes)
                 out-file-handle)
                (force-output out-file-handle)
                ))
            ))
      ))
  (define (local-main-loop
           start-num bin-num end-num prime-array outfh)
    (begin
      (let ((jj start-num)
            (total-primes 0)
            (total-twin-primes 0)
            (start-jday (srfi-19:current-julian-day)))
        (begin
          (while
              (< jj end-num)
            (begin
              (let ((result-list
                     (process-bin
                      jj bin-num end-num prime-array)))
                (let ((new-jj (list-ref result-list 0))
                      (ll-max (list-ref result-list 1))
                      (prime-sum (list-ref result-list 2))
                      (twin-prime-sum (list-ref result-list 3))
                      (end-jday (srfi-19:current-julian-day)))
                  (begin
                    (set! jj ll-max)
                    (set!
                     total-primes
                     (+ total-primes prime-sum))
                    (set!
                     total-twin-primes
                     (+ total-twin-primes twin-prime-sum))

                    (local-display-results
                     new-jj bin-num ll-max prime-sum
                     twin-prime-sum total-primes total-twin-primes
                     outfh)

                    (display
                     (ice-9-format:format
                      #f "completed ~:d / ~:d : total twin primes = ~:d : ~a : ~a~%"
                      ll-max end-num total-twin-primes
                      (timer-module:julian-day-difference-to-string
                       end-jday start-jday)
                      (timer-module:current-date-time-string)))
                    (set! start-jday end-jday)
                    (force-output)
                    )))
              ))
          ))
      ))
  (begin
    ;;; main code
    (let ((outfh (open-file out-filename "w"))
          (prime-array
           (prime-module:make-prime-array max-prime))
          (begin-num start-num))
      (begin
        (display
         (format #f "start-num | end-num | primes | pcnt | twin primes | pcnt ")
         outfh)
        (display
         (format #f "| cummulative primes | cummulative twin primes~%")
         outfh)
        (force-output outfh)

        (if (even? start-num)
            (begin
              (set! begin-num (1+ begin-num))
              ))

        (local-main-loop
         begin-num bin-num end-num prime-array outfh)
        (close outfh)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;          1         2         3         4         5         6
;;; 123456789012345678901234567890123456789012345678901234567890
;;;  pp p p   p p   p p   p     p p     p   p p   p     p     p
(define (count-twin-primes-in-bins
         start-num bin-num end-num max-prime)
  (define (local-display-results
           k bin-num end-num nprimes ntwprimes
           pi-primes pi-twprimes)
    (begin
      (if (> nprimes 0)
          (begin
            (let ((pcnt
                   (utils-module:round-float
                    (* 100.0 (/ nprimes bin-num)) 1.0))
                  (tpcnt
                   (utils-module:round-float
                    (* 100.0 (/ ntwprimes nprimes)) 1.0)))
              (begin
                (display
                 (ice-9-format:format
                  #f "~:d | ~:d | ~:d | ~4,1f% | ~:d | ~4,1f% | ~:d | ~:d~%"
                  k end-num nprimes
                  pcnt
                  ntwprimes
                  tpcnt
                  pi-primes pi-twprimes))
                (force-output)
                ))
            ))
      ))
  (define (local-main-loop
           start-num bin-num end-num prime-array)
    (begin
      (let ((jj start-num)
            (total-primes 0)
            (total-twin-primes 0))
        (begin
          (while
              (< jj end-num)
            (begin
              (let ((result-list
                     (process-bin
                      jj bin-num end-num prime-array)))
                (let ((new-jj (list-ref result-list 0))
                      (ll-max (list-ref result-list 1))
                      (prime-sum (list-ref result-list 2))
                      (twin-prime-sum (list-ref result-list 3)))
                  (begin
                    (set! jj ll-max)
                    (set!
                     total-primes
                     (+ total-primes prime-sum))
                    (set!
                     total-twin-primes
                     (+ total-twin-primes twin-prime-sum))

                    (local-display-results
                     new-jj bin-num ll-max prime-sum
                     twin-prime-sum total-primes total-twin-primes)
                    )))
              ))
          ))
      ))
  (begin
    ;;; main code
    (let ((prime-array
           (prime-module:make-prime-array max-prime))
          (begin-num start-num))
      (begin
        (display
         (format
          #f "start-num | end-num | primes | pcnt | "))
        (display
         (format
          #f "twin primes | pcnt | cummulative primes | "))
        (display
         (format
          #f "cummulative twin primes~%"))
        (force-output)

        (if (even? start-num)
            (begin
              (set! begin-num (1+ begin-num))
              ))

        (local-main-loop
         begin-num bin-num end-num prime-array)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;          1         2         3         4         5         6
;;; 123456789012345678901234567890123456789012345678901234567890
;;;  pp p p   p p   p p   p     p p     p   p p   p     p     p

;;;#############################################################
;;;#############################################################
(define (run-twins
         start-num bin-num end-num max-prime)
  (begin
    (let ((s-snum
           (utils-module:commafy-number start-num))
          (s-enum
           (utils-module:commafy-number end-num)))
      (begin
        (display
         (format
          #f "starting ~a to ~a at ~a~%"
          s-snum s-enum (timer-module:current-date-time-string)))
        (force-output)

        (count-twin-primes-in-bins
         start-num bin-num end-num max-prime)

        (newline)
        (display
         (format
          #f "completed ~a to ~a at ~a~%"
          s-snum s-enum
          (timer-module:current-date-time-string)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;          1         2         3         4         5         6
;;; 123456789012345678901234567890123456789012345678901234567890
;;;  pp p p   p p   p p   p     p p     p   p p   p     p     p
(define (process-bin-sprp start-num bin-num end-num)
  (begin
    (let ((begin-num (- start-num 2))
          (prime-count 0)
          (twin-prime-count 0)
          (ll-tmp
           (inexact->exact
            (truncate (/ start-num bin-num)))))
      (let ((ll-max (* (+ ll-tmp 1) bin-num))
            (last-prime -1))
        (begin
          (if (even? start-num)
              (begin
                (set! begin-num (- start-num 1))
                ))

          (if (<= begin-num 2)
              (begin
                (set! begin-num 3)
                (set! prime-count 1))
              (begin
                (if (sprp-module:sprp-prime? begin-num)
                    (begin
                      (set! last-prime begin-num)
                      (set! begin-num (+ begin-num 2))
                      ))
                ))

          (if (> ll-max end-num)
              (begin
                (set! ll-max end-num)
                ))

          (do ((jj begin-num (+ jj 2)))
              ((> jj ll-max))
            (begin
              (if (sprp-module:sprp-prime? jj)
                  (begin
                    (set!
                     prime-count (1+ prime-count))
                    (if (equal? (- jj last-prime) 2)
                        (begin
                          (set!
                           twin-prime-count
                           (1+ twin-prime-count))
                          ))
                    (set! last-prime jj)
                    ))
              ))

          (list start-num ll-max prime-count twin-prime-count)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;;          1         2         3         4         5         6
;;; 123456789012345678901234567890123456789012345678901234567890
;;;  pp p p   p p   p p   p     p p     p   p p   p     p     p
(define (count-twin-primes-in-bins-sprp
         start-num bin-num end-num max-prime)
  (define (local-display-results
           k bin-num end-num nprimes ntwprimes
           pi-primes pi-twprimes)
    (begin
      (if (> nprimes 0)
          (begin
            (let ((pcnt
                   (utils-module:round-float
                    (* 100.0 (/ nprimes bin-num)) 1.0))
                  (tpcnt
                   (utils-module:round-float
                    (* 100.0 (/ ntwprimes nprimes)) 1.0)))
              (begin
                (display
                 (ice-9-format:format
                  #f "~:d | ~:d | ~:d | ~4,1f% | "
                  k end-num nprimes pcnt))
                (display
                 (ice-9-format:format
                  #f "~:d | ~4,1f% | ~:d | ~:d~%"
                  ntwprimes tpcnt
                  pi-primes pi-twprimes))
                (force-output)
                ))
            ))
      ))
  (define (local-main-loop
           start-num bin-num end-num)
    (begin
      (let ((jj start-num)
            (total-primes 0)
            (total-twin-primes 0))
        (begin
          (while (< jj end-num)
            (begin
              (let ((result-list
                     (process-bin-sprp jj bin-num end-num)))
                (let ((new-jj (list-ref result-list 0))
                      (ll-max (list-ref result-list 1))
                      (prime-sum (list-ref result-list 2))
                      (twin-prime-sum (list-ref result-list 3)))
                  (begin
                    (set! jj ll-max)
                    (set!
                     total-primes
                     (+ total-primes prime-sum))
                    (set!
                     total-twin-primes
                     (+ total-twin-primes twin-prime-sum))

                    (local-display-results
                     new-jj bin-num ll-max prime-sum
                     twin-prime-sum total-primes total-twin-primes)
                    )))
              ))
          ))
      ))
  ;;; main code
  (begin
    (let ((begin-num start-num))
      (begin
        (display
         (format #f "start-num | end-num | primes | "))
        (display
         (format #f "pcnt | twin primes | pcnt | "))
        (display
         (format
          #f "cummulative primes | cummulative twin primes~%"))
        (force-output)

        (if (even? start-num)
            (begin
              (set! begin-num (1+ begin-num))
              ))

        (local-main-loop begin-num bin-num end-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (run-twins-sprp
         start-num bin-num end-num
         max-prime)
  (begin
    (let ((s-snum (utils-module:commafy-number start-num))
          (s-enum (utils-module:commafy-number end-num)))
      (begin
        (display
         (format
          #f "starting ~a to ~a at ~a~%"
          s-snum s-enum
          (timer-module:current-date-time-string)))
        (force-output)

        (count-twin-primes-in-bins-sprp
         start-num bin-num end-num max-prime)

        (newline)
        (display
         (format
          #f "completed ~a to ~a at ~a~%"
          s-snum s-enum
          (timer-module:current-date-time-string)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
