################################################################
################################################################
###                                                          ###
###  Erap - Ruth-Aaron pairs                                 ###
###                                                          ###
###  written by Robert Haramoto                              ###
###                                                          ###
################################################################
################################################################

Rap - Ruth-Aaron pairs - consecutive numbers with the sums of
the prime factors are equal.

Unique rap - Ruth-Aaron pairs - consecutive numbers with the sums of
the unique prime factors are equal.

Erap - pairs of consecutive numbers with the sums
of the prime factors are also consecutive.

Here are some Rap pairs, when only distinct prime factors
are counted:
(5, 6), (24, 25), (49, 50), (77, 78), (104, 105), (153, 154), (369,
370), (492, 493), ...

Here are some Rap pairs, when repeated prime factors are counted:
(5, 6), (8, 9), (15, 16), (77, 78), (125, 126), ...

Here are some erap number pairs:
(2, 3), (3, 4), (4, 5), (9, 10), (20, 21), (24, 25), (98, 99), (170, 171), (1104, 1105), (1274, 1275), (2079, 2080), (2255, 2256), (3438, 3439), (4233, 4234), (4345, 4346), (4716, 4717), (5368, 5369), (7105, 7106), and (7625, 7626)


################################################################
################################################################
Discussion:

(1) to run this program initially, type
      ./gen-make
      make
      ./erap.scm --config init0.config > e-out0.log &
      ./rap.scm --config init0.config > out0.log &
      ./unique-rap.scm --config init0.config > out0.log &
      make clean


(2) cat out0.log


################################################################
################################################################
Notes:

(1) start-num/end-num : end of range of starting sequence numbers
to consider

(2) max-prime : max prime used to generate prime array, (used
to speed is-prime? calculations)

(3) details-flag : true or false, display the factors/sums of
the consecutive pairs

(4) for help on the various options allowed, type
      ./erap.scm --help
      ./rap.scm --help
      ./unique-rap.scm --help

(5) for more on rap/erap, see
https://en.wikipedia.org/wiki/Ruth%E2%80%93Aaron_pair
http://www.numbersaplenty.com/set/eRAP/

(6) Ruth-Aaron sequence at:
https://oeis.org/A006145


################################################################
################################################################

Assumes that guile 3.0 is installed in /usr/bin.

Uses the unlicense public domain license.
See the UNLICENSE file or https://unlicense.org/



################################################################
################################################################
History

last updated <2024-09-27 Fri>
updated <2022-06-01 Wed>
updated <2020-02-16 Sun>

################################################################
################################################################
###                                                          ###
###  end of file                                             ###
###                                                          ###
################################################################
################################################################
