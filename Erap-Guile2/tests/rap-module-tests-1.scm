;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <http://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for rap-module.scm                        ###
;;;###                                                       ###
;;;###  last updated August 22, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

(use-modules ((rap-module)
              :renamer (symbol-prefix-proc 'rap-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (rap-module-assert-lists-equal
         sub-name test-label-index
         test-num
         shouldbe-list result-list
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list))
          (rlen (length result-list)))
      (let ((err-1
             (format
              #f "~a : error (~a) : num=~a : "
              sub-name test-label-index test-num))
            (err-2
             (format
              #f "shouldbe=~a, result=~a"
              shouldbe-list result-list))
            (err-3
             (format
              #f ", shouldbe length=~a, result=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append err-1 err-2 err-3)
           result-hash-table)

          (for-each
           (lambda (s-elem)
             (begin
               (let ((mflag (member s-elem result-list))
                     (err-4
                      (format
                       #f ", missing element ~a" s-elem)))
                 (begin
                   (unittest2:assert?
                    (not (equal? mflag #f))
                    sub-name
                    (string-append err-1 err-2 err-4)
                    result-hash-table)
                   ))
               )) shouldbe-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-single-prime-divisor-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-calc-single-prime-divisor-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 2 (list 2))
           (list 4 2 (list 2 2))
           (list 8 2 (list 2 2 2))
           (list 16 2 (list 2 2 2 2))
           (list 3 3 (list 3))
           (list 9 3 (list 3 3))
           (list 27 3 (list 3 3 3))
           (list 81 3 (list 3 3 3 3))
           (list 5 5 (list 5))
           (list 25 5 (list 5 5))
           (list 125 5 (list 5 5 5))
           (list 625 5 (list 5 5 5 5))
           (list 8 3 (list))
           (list 27 5 (list))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (pp (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list
                     (rap-module:calc-single-prime-divisor-list
                      test-num pp)))
                (begin
                  (rap-module-assert-lists-equal
                   sub-name test-label-index
                   test-num
                   shouldbe-list result-list
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-prime-divisors-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-find-prime-divisors-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 (list 2))
           (list 4 (list 2 2))
           (list 8 (list 2 2 2))
           (list 16 (list 2 2 2 2))
           (list 48 (list 2 2 2 2 3))
           (list 3 (list 3))
           (list 9 (list 3 3))
           (list 27 (list 3 3 3))
           (list 81 (list 3 3 3 3))
           (list 162 (list 2 3 3 3 3))
           (list 5 (list 5))
           (list 25 (list 5 5))
           (list 125 (list 5 5 5))
           (list 625 (list 5 5 5 5))
           (list 1250 (list 2 5 5 5 5))
           (list 2500 (list 2 2 5 5 5 5))
           ))
         (prime-array
          (prime-module:make-prime-array 20))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (rap-module:find-prime-divisors-list
                      test-num prime-array)))
                (begin
                  (rap-module-assert-lists-equal
                   sub-name test-label-index
                   test-num
                   shouldbe-list result-list
                   result-hash-table)
                  )))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-prime-factors-list-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-prime-factors-list-to-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2) "2")
           (list (list 2 2) "2^2")
           (list (list 2 2 2) "2^3")
           (list (list 2 2 2 2) "2^4")
           (list (list 2 2 2 2 3) "2^4 x 3")
           (list (list 2 3 5) "2 x 3 x 5")
           (list (list 2 3 3 5) "2 x 3^2 x 5")
           (list (list 2 3 5 5) "2 x 3 x 5^2")
           (list (list 2 3 3 5 5) "2 x 3^2 x 5^2")
           (list (list 2 2 2 3 3 5 5) "2^3 x 3^2 x 5^2")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (rap-module:prime-factors-list-to-string
                      input-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
