;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  ruth aaron pairs functions module                    ###
;;;###                                                       ###
;;;###  last updated August 22, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start prime modules

(define-module (rap-module)
  #:export (calc-single-prime-divisor-list
            find-prime-divisors-list
            prime-factors-list-to-string

            erap-main-loop
            rap-main-loop
            unique-rap-main-loop
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### srfi-1 for fold functions and delete-duplicates
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;#############################################################
;;;#############################################################
(define (calc-single-prime-divisor-list anum pp)
  (begin
    (let ((atmp anum)
          (pp-list (list))
          (completed-flag #f))
      (begin
        (while (equal? completed-flag #f)
          (begin
            (if (zero? (modulo atmp pp))
                (begin
                  (set! pp-list (cons pp pp-list)))
                (begin
                  (set! completed-flag #t)
                  ))

            (set! atmp (euclidean/ atmp pp))
            ))

        pp-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (find-prime-divisors-list mm prime-array)
  (begin
    (let ((mm-sqrt (1+ (exact-integer-sqrt mm)))
          (exponent-list (list)))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii mm-sqrt))
          (begin
            (if (zero? (modulo mm ii))
                (begin
                  (let ((jj (euclidean/ mm ii)))
                    (begin
                      (if (>= jj ii)
                          (begin
                            (if (prime-module:is-array-prime?
                                 ii prime-array)
                                (begin
                                  (let ((alist
                                         (calc-single-prime-divisor-list
                                          mm ii)))
                                    (begin
                                      (set!
                                       exponent-list
                                       (append exponent-list alist))
                                      ))
                                  ))

                            (if (and
                                 (> jj ii)
                                 (prime-module:is-array-prime?
                                  jj prime-array))
                                (begin
                                  (let ((alist
                                         (calc-single-prime-divisor-list
                                          mm jj)))
                                    (begin
                                      (set!
                                       exponent-list
                                       (append exponent-list alist))
                                      ))
                                  ))
                            ))
                      ))
                  ))
            ))

        exponent-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax set-string-macro
  (syntax-rules ()
    ((set-string-macro
      fstring prev prev-count)
     (begin
       (if (> prev-count 1)
           (begin
             (let ((tmp-string
                    (ice-9-format:format
                     #f "~:d^~:d" prev prev-count)))
               (begin
                 (if (string-ci=? fstring "")
                     (begin
                       (set!
                        fstring
                        (format
                         #f "~a" tmp-string)))
                     (begin
                       (set!
                        fstring
                        (format
                         #f "~a x ~a" fstring tmp-string))
                       ))
                 )))
           (begin
             (let ((tmp-string
                    (ice-9-format:format
                     #f "~:d" prev)))
               (begin
                 (if (string-ci=? fstring "")
                     (begin
                       (set!
                        fstring
                        (format
                         #f "~a" tmp-string)))
                     (begin
                       (set!
                        fstring
                        (format
                         #f "~a x ~a" fstring tmp-string))
                       ))
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (prime-factors-list-to-string all-factor-list)
  (begin
    (if (list? all-factor-list)
        (begin
          (let ((fstring "")
                (prev (car all-factor-list))
                (prev-count 1)
                (tail-list (cdr all-factor-list)))
            (begin
              (while (and
                      (list? tail-list)
                      (> (length tail-list) 0))
                (begin
                  (let ((this-elem (car tail-list))
                        (next-tail-list (cdr tail-list)))
                    (begin
                      (if (equal? this-elem prev)
                          (begin
                            (set! prev-count (1+ prev-count)))
                          (begin
                            (set-string-macro
                             fstring prev prev-count)
                            (set! prev-count 1)
                            ))

                      (set! prev this-elem)
                      (set! tail-list next-tail-list)
                      ))
                  ))

              (set-string-macro
               fstring prev prev-count)

              fstring
              )))
        (begin
          ""
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-erap-results
         nn-1 all-factor-list-1
         nn-2 all-factor-list-2 details-flag)
  (begin
    (let ((mult-string-1
           (prime-factors-list-to-string
            all-factor-list-1))
          (mult-string-2
           (prime-factors-list-to-string
            all-factor-list-2))
          (sum-string-1
           (string-join
            (map
             (lambda (anum)
               (begin
                 (ice-9-format:format #f "~:d" anum)
                 )) all-factor-list-1)
            " + "))
          (sum-string-2
           (string-join
            (map
             (lambda (anum)
               (begin
                 (ice-9-format:format #f "~:d" anum)
                 )) all-factor-list-2)
            " + "))
          (string-sum-1
           (ice-9-format:format
            #f "~:d"
            (srfi-1:fold + 0 all-factor-list-1)))
          (string-sum-2
           (ice-9-format:format
            #f "~:d"
            (srfi-1:fold + 0 all-factor-list-2)))
          (nn-string-1
           (ice-9-format:format #f "~:d" nn-1))
          (nn-string-2
           (ice-9-format:format #f "~:d" nn-2)))
      (begin
        ;;; line 1
        (display
         (format
          #f "erap: (~a, ~a)~%"
          nn-string-1 nn-string-2))

        (if (equal? details-flag #t)
            (begin
              ;;; line 2
              (display
               (format
                #f "    ~a = ~a  :  ~a = ~a~%"
                nn-string-1 mult-string-1
                sum-string-1 string-sum-1))

              ;;; line 3
              (display
               (format
                #f "    ~a = ~a  :  ~a = ~a~%"
                nn-string-2 mult-string-2
                sum-string-2 string-sum-2))
              ))

        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-rap-results
         nn-1 all-factor-list-1
         nn-2 all-factor-list-2 details-flag)
  (begin
    (let ((mult-string-1
           (prime-factors-list-to-string
            all-factor-list-1))
          (mult-string-2
           (prime-factors-list-to-string
            all-factor-list-2))
          (sum-string-1
           (string-join
            (map
             (lambda (anum)
               (begin
                 (ice-9-format:format #f "~:d" anum)
                 )) all-factor-list-1)
            " + "))
          (sum-string-2
           (string-join
            (map
             (lambda (anum)
               (begin
                 (ice-9-format:format #f "~:d" anum)
                 )) all-factor-list-2)
            " + "))
          (string-sum-1
           (ice-9-format:format
            #f "~:d"
            (srfi-1:fold + 0 all-factor-list-1)))
          (string-sum-2
           (ice-9-format:format
            #f "~:d"
            (srfi-1:fold + 0 all-factor-list-2)))
          (nn-string-1
           (ice-9-format:format
            #f "~:d" nn-1))
          (nn-string-2
           (ice-9-format:format
            #f "~:d" nn-2)))
      (begin
        ;;; line 1
        (display
         (format
          #f "rap: (~a, ~a)~%"
          nn-string-1 nn-string-2))

        (if (equal? details-flag #t)
            (begin
              ;;; line 2
              (display
               (format
                #f "    ~a = ~a  :  ~a = ~a~%"
                nn-string-1 mult-string-1
                sum-string-1 string-sum-1))

              ;;; line 3
              (display
               (format
                #f "    ~a = ~a  :  ~a = ~a~%"
                nn-string-2 mult-string-2
                sum-string-2 string-sum-2))
              ))

        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-unique-rap-results
         nn-1 all-factor-list-1
         nn-2 all-factor-list-2 details-flag)
  (begin
    (let ((mult-string-1
           (prime-factors-list-to-string
            all-factor-list-1))
          (mult-string-2
           (prime-factors-list-to-string
            all-factor-list-2))
          (unique-factors-1
           (srfi-1:delete-duplicates all-factor-list-1))
          (unique-factors-2
           (srfi-1:delete-duplicates all-factor-list-2))
          (nn-string-1
           (ice-9-format:format #f "~:d" nn-1))
          (nn-string-2
           (ice-9-format:format #f "~:d" nn-2)))
      (let ((sum-string-1
             (string-join
              (map
               (lambda (anum)
                 (begin
                   (ice-9-format:format #f "~:d" anum)
                   )) unique-factors-1)
              " + "))
            (sum-string-2
             (string-join
              (map
               (lambda (anum)
                 (begin
                   (ice-9-format:format #f "~:d" anum)
                   )) unique-factors-2)
              " + "))
            (string-sum-1
             (ice-9-format:format
              #f "~:d"
              (srfi-1:fold + 0 unique-factors-1)))
            (string-sum-2
             (ice-9-format:format
              #f "~:d"
              (srfi-1:fold + 0 unique-factors-2))))
        (begin
          ;;; line 1
          (display
           (format
            #f "unique rap: (~a, ~a)~%"
            nn-string-1 nn-string-2))

          (if (equal? details-flag #t)
              (begin
                ;;; line 2
                (display
                 (format
                  #f "    ~a = ~a  :  ~a = ~a~%"
                  nn-string-1 mult-string-1
                  sum-string-1 string-sum-1))

                ;;; line 3
                (display
                 (format
                  #f "    ~a = ~a  :  ~a = ~a~%"
                  nn-string-2 mult-string-2
                  sum-string-2 string-sum-2))
                ))

          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (erap-main-loop
         start-num end-num max-prime details-flag)
  (begin
    (let ((result-count 0)
          (prime-array
           (prime-module:make-prime-array max-prime)))
      (let ((prev-num start-num)
            (prev-factors-list
             (find-prime-divisors-list
              start-num prime-array)))
        (let ((prev-sum
               (srfi-1:fold + 0 prev-factors-list)))
          (begin
            (do ((ii (1+ start-num) (1+ ii)))
                ((>= ii end-num))
              (begin
                (let ((ii-factors-list
                       (find-prime-divisors-list
                        ii prime-array)))
                  (let ((ii-sum
                         (srfi-1:fold + 0 ii-factors-list)))
                    (begin
                      (if (equal? (1+ prev-sum) ii-sum)
                          (begin
                            (display-erap-results
                             prev-num prev-factors-list
                             ii ii-factors-list details-flag)

                            (set! result-count (1+ result-count))
                            ))

                      (set! prev-num ii)
                      (set! prev-factors-list ii-factors-list)
                      (set! prev-sum ii-sum)
                      )))
                ))

            (newline)
            (display
             (ice-9-format:format
              #f "found ~:d erap number pairs~%"
              result-count))
            (newline)
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (rap-main-loop
         start-num end-num max-prime details-flag)
  (begin
    (let ((result-count 0)
          (prime-array
           (prime-module:make-prime-array max-prime)))
      (let ((prev-num start-num)
            (prev-factors-list
             (find-prime-divisors-list
              start-num prime-array)))
        (let ((prev-sum
               (srfi-1:fold + 0 prev-factors-list)))
          (begin
            (do ((ii (1+ start-num) (1+ ii)))
                ((>= ii end-num))
              (begin
                (let ((ii-factors-list
                       (find-prime-divisors-list
                        ii prime-array)))
                  (let ((ii-sum
                         (srfi-1:fold + 0 ii-factors-list)))
                    (begin
                      (if (equal? prev-sum ii-sum)
                          (begin
                            (display-rap-results
                             prev-num prev-factors-list
                             ii ii-factors-list details-flag)

                            (set! result-count (1+ result-count))
                            ))

                      (set! prev-num ii)
                      (set! prev-factors-list ii-factors-list)
                      (set! prev-sum ii-sum)
                      )))
                ))

            (newline)
            (display
             (ice-9-format:format
              #f "found ~:d rap number pairs~%"
              result-count))
            (newline)
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (unique-rap-main-loop
         start-num end-num max-prime details-flag)
  (begin
    (let ((result-count 0)
          (prime-array
           (prime-module:make-prime-array max-prime)))
      (let ((prev-num start-num)
            (prev-factors-list
             (find-prime-divisors-list
              start-num prime-array)))
        (let ((prev-sum
               (srfi-1:fold
                + 0
                (srfi-1:delete-duplicates prev-factors-list))))
          (begin
            (do ((ii (1+ start-num) (1+ ii)))
                ((>= ii end-num))
              (begin
                (let ((ii-factors-list
                       (find-prime-divisors-list
                        ii prime-array)))
                  (let ((ii-sum
                         (srfi-1:fold
                          + 0
                          (srfi-1:delete-duplicates
                           ii-factors-list))))
                    (begin
                      (if (equal? prev-sum ii-sum)
                          (begin
                            (display-unique-rap-results
                             prev-num prev-factors-list
                             ii ii-factors-list details-flag)

                            (set! result-count (1+ result-count))
                            ))

                      (set! prev-num ii)
                      (set! prev-factors-list ii-factors-list)
                      (set! prev-sum ii-sum)
                      )))
                ))

            (newline)
            (display
             (ice-9-format:format
              #f "found ~:d unique prime factors rap number pairs~%"
              result-count))
            (newline)
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
