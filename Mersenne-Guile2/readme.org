################################################################
################################################################
###                                                          ###
###  Mersenne primes                                         ###
###                                                          ###
###  written by Robert Haramoto                              ###
###                                                          ###
################################################################
################################################################

Mersenne prime - numbers of the form 2^n - 1

I recently watched a 2016 numberphile video which talks
about a recent discovery of the biggest prime number:

How they found the World's Biggest Prime Number
https://www.youtube.com/watch?v=lEvXcTYqtKU

In the video, they talk about how the Mersenne numbers
are prime only if M(p) = 2^p - 1, but it's not guaranteed
that if p is a prime that M(p) will be a prime, (it's a
necessary but not sufficient condition).

I thought it would be a neat idea to write display the
Mersenne primes up to M(67), and show which Mersenne numbers
are prime and which are composites.


################################################################
################################################################
Discussion:

(1) to run this program initially, type
      ./gen-make
      make
      ./mersenne.scm --config init0.config > out0.log &
      make clean


(2) cat out0.log
starting mersenne prime numbers
config file = init0.config
start = 2, end = 20
saturday, january 19, 2019  09:01:59 am
2^(2) - 1 = 3 prime!  number of digits = 1
2^(3) - 1 = 7 prime!  number of digits = 1
2^(5) - 1 = 31 prime!  number of digits = 2
2^(7) - 1 = 127 prime!  number of digits = 3
2^(11) - 1 = 2,047 composite!  number of digits = 4 : [23 ; 89]
2^(13) - 1 = 8,191 prime!  number of digits = 4
2^(17) - 1 = 131,071 prime!  number of digits = 6
2^(19) - 1 = 524,287 prime!  number of digits = 6

number of primes between 2 and 20 is 8
number of mersenne primes = 7
number of mersenne composites = 1
total = 8
elapsed time = 0.001 seconds : saturday, january 19, 2019  09:01:59 am

################################################################
################################################################
Notes:

(1) start-num/end-num : end of range of starting numbers p
to consider

(2) for help on the various options allowed, type
    ./mersenne.scm --help

(3) for more on Mersenne numbers, see
https://en.wikipedia.org/wiki/Mersenne_prime
https://oeis.org/wiki/Mersenne_primes
https://oeis.org/wiki/Mersenne_numbers
https://en.wikipedia.org/wiki/List_of_Mersenne_primes_and_perfect_numbers

(4) sequences can be described at:
https://oeis.org/A000668

(5) great internet mersenne prime search
https://www.mersenne.org/


################################################################
################################################################

Assumes that guile 3.0 is installed in /usr/bin

Uses the unlicense public domain license.
See the UNLICENSE file or https://unlicense.org/


################################################################
################################################################
History

last updated <2024-09-27 Fri>
updated <2022-06-02 Thu>
updated <2020-02-13 Thu>

################################################################
################################################################
###                                                          ###
###  end of file                                             ###
###                                                          ###
################################################################
################################################################
