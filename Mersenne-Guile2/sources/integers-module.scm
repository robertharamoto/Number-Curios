;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  integers-module - integer functions                  ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 4, 2022                                  ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define-module (integers-module)
  #:export (is-square?

            calc-triangular-root
            is-triangular?

            calc-mersenne-prime
            is-perfect?
            calc-perfect-number-euclidean
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### srfi-11 for let-values
(use-modules ((srfi srfi-11)
              :renamer (symbol-prefix-proc 'srfi-11:)))

;;;### sprp-module for sprp-prime?
(use-modules ((sprp-module)
              :renamer (symbol-prefix-proc 'sprp-module:)))

;;;### factors-module for sum-of-divisors
(use-modules ((factors-module)
              :renamer (symbol-prefix-proc 'factors-module:)))

;;;#############################################################
;;;#############################################################
(define (is-square? anum)
  (begin
    (srfi-11:let-values
     (((sqrt-root sqrt-remainder)
       (exact-integer-sqrt anum)))
     (begin
       (if (zero? sqrt-remainder)
           (begin
             #t)
           (begin
             #f
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (calc-triangular-root anum)
  (begin
    (let ((tmp1 (1+ (* 8 anum))))
      (srfi-11:let-values
       (((sqrt-root sqrt-remainder)
         (exact-integer-sqrt tmp1)))
       (begin
         (if (zero? sqrt-remainder)
             (begin
               (let ((result
                      (euclidean/
                       (- sqrt-root 1) 2)))
                 (begin
                   (if (integer? result)
                       (begin
                         result)
                       (begin
                         -1
                         ))
                   )))
             (begin
               -1
               ))
         )))
    ))

;;;#############################################################
;;;#############################################################
(define (is-triangular? anum)
  (begin
    (let ((tmp1 (1+ (* 8 anum))))
      (let ((local-sqrt-root 0)
            (local-sqrt-remainder 0))
        (begin
          (call-with-values
              (lambda ()
                (begin
                  (exact-integer-sqrt tmp1)
                  ))
            (lambda (a b)
              (begin
                (set! local-sqrt-root a)
                (set! local-sqrt-remainder b)
                )))

          (if (zero? local-sqrt-remainder)
              (begin
                #t)
              (begin
                #f
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (calc-mersenne-prime kk)
  (begin
    (if (<= kk 1)
        (begin
          #f)
        (begin
          (if (sprp-module:sprp-prime? kk)
              (begin
                (let ((local-tmp (1- (expt 2 kk))))
                  (begin
                    (if (sprp-module:sprp-prime? local-tmp)
                        (begin
                          local-tmp)
                        (begin
                          #f
                          ))
                    )))
              (begin
                #f
                ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-perfect? anum)
  (begin
    (if (<= anum 1)
        (begin
          #f)
        (begin
          (let ((bnum
                 (- (factors-module:sum-of-divisors anum)
                    anum)))
            (begin
              (if (equal? anum bnum)
                  (begin
                    #t)
                  (begin
                    #f
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (calc-perfect-number-euclidean kk)
  (begin
    (if (> kk 1)
        (begin
          (let ((two-kk-m-1 (expt 2 (1- kk))))
            (let ((two-kk-minus-1
                   (1- (* 2 two-kk-m-1))))
              (begin
                (if (equal?
                     (sprp-module:sprp-prime? two-kk-minus-1)
                     #t)
                    (begin
                      (let ((tmp
                             (* two-kk-m-1
                                two-kk-minus-1)))
                        (begin
                          (if (is-perfect? tmp)
                              (begin
                                tmp)
                              (begin
                                #f
                                ))
                          )))
                    (begin
                      #f
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
