;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  curios functions module                              ###
;;;###                                                       ###
;;;###  last updated July 25, 2024                           ###
;;;###                                                       ###
;;;###  updated May 6, 2022                                  ###
;;;###                                                       ###
;;;###  updated February 27, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start curios modules
(define-module (curios-module)
  #:export (display-results
            reverse-number
            find-prime-exponent
            find-prime-exponents-list
            is-powerful-number?
            is-perfect-power?
            calc-next-int-stop
            main-loop
            ))

;;;#############################################################
;;;#############################################################
;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((sprp-module)
              :renamer (symbol-prefix-proc 'sprp-module:)))

(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

(use-modules ((factors-module)
              :renamer (symbol-prefix-proc 'factors-module:)))

;;;#############################################################
;;;#############################################################
(define (display-prime-results pp mm)
  (begin
    (let ((ndigits
           (digits-module:number-of-digits mm)))
      (begin
        (display
         (ice-9-format:format
          #f "2^(~:d) - 1 = ~:d prime!  number of digits = ~:d~%"
          pp mm ndigits))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-composite-results pp mm)
  (begin
    (let ((ndigits
           (digits-module:number-of-digits mm))
          (factors-list
           (factors-module:prime-factors-list mm)))
      (let ((factors-string
             (utils-module:number-list-to-string
              factors-list)))
        (begin
          (display
           (ice-9-format:format
            #f "2^(~:d) - 1 = ~:d composite!  "
            pp mm))
          (display
           (ice-9-format:format
            #f "number of digits = ~:d : "
            ndigits))
          (display
           (format #f "~a~%" factors-string))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (check-mersenne-prime-number pp)
  (begin
    (let ((mm (- (expt 2 pp) 1)))
      (let ((pflag
             (sprp-module:sprp-prime? mm)))
        (begin
          (if (equal? pflag #t)
              (begin
                (display-prime-results pp mm))
              (begin
                (display-composite-results pp mm)
                ))

          pflag
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop start-num end-num)
  (begin
    (let ((prime-count 0)
          (mersenne-prime-count 0)
          (mersenne-composite-count 0))
      (begin
        (do ((pp start-num (1+ pp)))
            ((> pp end-num))
          (begin
            (if (sprp-module:sprp-prime? pp)
                (begin
                  (set! prime-count (1+ prime-count))

                  (let ((pflag
                         (check-mersenne-prime-number pp)))
                    (begin
                      (if (equal? pflag #t)
                          (begin
                            (set!
                             mersenne-prime-count
                             (1+ mersenne-prime-count)))
                          (begin
                            (set!
                             mersenne-composite-count
                             (1+ mersenne-composite-count))
                            ))
                      ))
                  ))
            ))

        (newline)
        (display
         (ice-9-format:format
          #f "number of primes between ~:d and ~:d is ~:d~%"
          start-num end-num prime-count))
        (display
         (ice-9-format:format
          #f "  number of mersenne primes = ~:d~%"
          mersenne-prime-count))
        (display
         (ice-9-format:format
          #f "  number of mersenne composites = ~:d~%"
          mersenne-composite-count))
        (display
         (ice-9-format:format
          #f "  total = ~:d~%"
          (+ mersenne-prime-count mersenne-composite-count)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
