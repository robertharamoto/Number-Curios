;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for curios-module.scm                     ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated February 27, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((curios-module)
              :renamer (symbol-prefix-proc 'curios-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (simple-test-check
         shouldbe result
         extra-string extra-data
         sub-name test-label-index
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) error : ~a=~a : "
            sub-name test-label-index
            extra-string extra-data))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (let ((error-string
             (string-append err-1 err-2)))
        (begin
          (unittest2:assert?
           (equal? shouldbe result)
           sub-name
           error-string
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (curios-assert-two-lists-equal
         shouldbe-list result-list
         sub-name test-label-index result-hash-table)
  (begin
    (if (and (list? shouldbe-list)
             (list? result-list))
        (begin
          (let ((slen (length shouldbe-list))
                (rlen (length result-list))
                (err-1
                 (format
                  #f "~a : (~a) error : "
                  sub-name test-label-index)))
            (let ((err-2
                   (format
                    #f "shouldbe=~a, result=~a : "
                    shouldbe-list result-list))
                  (err-3
                   (format
                    #f "length error : shouldbe=~a, result=~a"
                    slen rlen)))
              (begin
                (unittest2:assert?
                 (equal? slen rlen)
                 sub-name
                 (string-append
                  err-1 err-2 err-3)
                 result-hash-table)

                (if (equal? slen rlen)
                    (begin
                      (do ((ii 0 (1+ ii)))
                          ((>= ii slen))
                        (begin
                          (let ((s-elem
                                 (list-ref shouldbe-list ii)))
                            (let ((rtmp (member s-elem result-list))
                                  (err-4
                                   (format
                                    #f "missing item ~a : result=~a"
                                    s-elem result-list)))
                              (begin
                                (if (equal? rtmp #f)
                                    (begin
                                      (unittest2:assert?
                                       (equal? s-elem rtmp)
                                       sub-name
                                       (string-append
                                        err-1 err-2 err-4)
                                       result-hash-table))
                                    (begin
                                      (unittest2:assert?
                                       (equal? s-elem (car rtmp))
                                       sub-name
                                       (string-append
                                        err-1 err-2 err-4)
                                       result-hash-table)
                                      ))
                                )))
                          ))
                      ))
                ))
            ))
        (begin
          (let ((err-1
                 (format
                  #f "~a : (~a) error : "
                  sub-name test-label-index)))
            (let ((err-2
                   (format
                    #f "shouldbe=~a, result=~a"
                    shouldbe-list result-list)))
              (begin
                (unittest2:assert?
                 (equal? shouldbe-list result-list)
                 sub-name
                 (string-append err-1 err-2)
                 result-hash-table)
                )))
          ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
