;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for sprp-module.scm                       ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated February 27, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((sprp-module)
              :renamer (symbol-prefix-proc 'sprp-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (sprp-simple-test-check
         shouldbe result
         extra-string extra-data
         sub-name test-label-index
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) error : ~a=~a : "
            sub-name test-label-index
            extra-string extra-data))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (let ((error-string
             (string-append err-1 err-2)))
        (begin
          (unittest2:assert?
           (equal? shouldbe result)
           sub-name
           error-string
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-mr-exp-mod-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-mr-exp-mod-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 0 2 1) (list 2 1 2 0)
           (list 3 0 11 1) (list 3 1 11 3)
           (list 3 2 11 9) (list 3 3 11 5)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((base (list-ref this-list 0))
                  (exp (list-ref this-list 1))
                  (mm (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result
                     (sprp-module:mr-exp-mod base exp mm)))
                (let ((string-1
                       (format
                        #f "base=~a, exp=~a, mm"
                        base exp)))
                  (begin
                    (sprp-simple-test-check
                     shouldbe result
                     string-1 mm
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-miller-rabin-test-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-miller-rabin-test-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 5 3 #t) (list 7 4 #t)
           (list 11 2 #t) (list 13 10 #t)
           (list 15 3 #f) (list 17 10 #t)
           (list 19 11 #t) (list 561 3 #f)
           (list 561 6 #f) (list 561 12 #f)
           (list 1105 5 #f) (list 1105 10 #f)
           (list 1105 15 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (aa (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (sprp-module:miller-rabin-test nn aa)))
                (let ((string-1
                       (format #f "nn=~a, aa" nn)))
                  (begin
                    (sprp-simple-test-check
                     shouldbe result
                     string-1 aa
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-mr-rand-prime-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-mr-rand-prime-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 #f) (list 1 #f)
           (list 2 #t) (list 3 #t) (list 4 #f)
           (list 5 #t) (list 6 #f) (list 7 #t)
           (list 8 #f) (list 9 #f) (list 10 #f)
           (list 11 #t) (list 12 #f) (list 13 #t)
           (list 14 #f) (list 15 #f) (list 16 #f)
           (list 17 #t) (list 18 #f) (list 19 #t)
           (list 20 #f) (list 21 #f) (list 22 #f)
           (list 23 #t) (list 24 #f) (list 25 #f)
           (list 26 #f) (list 27 #f) (list 28 #f)
           (list 29 #t) (list 30 #f) (list 31 #t)
           (list 32 #f) (list 33 #f) (list 34 #f)
           (list 35 #f) (list 36 #f) (list 37 #t)
           (list 38 #f) (list 39 #f) (list 40 #f)
           (list 41 #t) (list 42 #f) (list 43 #t)
           (list 44 #f) (list 45 #f) (list 46 #f)
           (list 47 #t) (list 48 #f) (list 49 #f)
           (list 50 #f) (list 51 #f) (list 52 #f)
           (list 53 #t) (list 54 #f) (list 55 #f)
           (list 56 #f) (list 57 #f) (list 58 #f)
           (list 59 #t) (list 60 #f) (list 61 #t)
           (list 62 #f) (list 63 #f) (list 64 #f)
           (list 65 #f) (list 66 #f) (list 67 #t)
           (list 68 #f) (list 69 #f) (list 70 #f)
           (list 71 #t) (list 72 #f) (list 73 #t)
           (list 74 #f) (list 75 #f) (list 76 #f)
           (list 77 #f) (list 78 #f) (list 79 #t)
           (list 80 #f) (list 81 #f) (list 82 #f)
           (list 83 #t) (list 84 #f) (list 85 #f)
           (list 86 #f) (list 77 #f) (list 78 #f)
           (list 79 #t) (list 80 #f) (list 81 #f)
           (list 82 #f) (list 83 #t) (list 84 #f)
           (list 85 #f) (list 86 #f) (list 87 #f)
           (list 88 #f) (list 89 #t) (list 90 #f)
           (list 91 #f) (list 92 #f) (list 93 #f)
           (list 94 #f) (list 95 #f) (list 96 #f)
           (list 97 #t) (list 98 #f) (list 99 #f)
           (list 100 #f)
           ))
         (times 50)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (sprp-module:mr-rand-prime? nn times)))
                (let ((string-1 "nn"))
                  (begin
                    (sprp-simple-test-check
                     shouldbe result
                     string-1 nn
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-div-two-and-odd-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-div-two-and-odd-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 (list 1 1)) (list 4 (list 2 1))
           (list 6 (list 1 3)) (list 8 (list 3 1))
           (list 10 (list 1 5)) (list 12 (list 2 3))
           (list 14 (list 1 7)) (list 16 (list 4 1))
           (list 18 (list 1 9)) (list 20 (list 2 5))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((even-num (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (sprp-module:div-two-and-odd even-num)))
                (let ((string-1 "even-num"))
                  (begin
                    (sprp-simple-test-check
                     shouldbe-list result-list
                     string-1 even-num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-probable-prime-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-probable-prime-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 5 2 3 #t) (list 7 2 3 #t) (list 9 2 3 #f)
           (list 11 2 3 #t) (list 13 2 3 #t) (list 15 2 3 #f)
           (list 17 2 3 #t) (list 19 2 3 #t) (list 21 2 3 #f)
           (list 23 2 3 #t) (list 25 2 3 #f) (list 27 2 3 #f)
           (list 29 2 3 #t) (list 31 2 3 #t) (list 33 2 3 #f)
           (list 35 2 3 #f) (list 37 2 3 #t) (list 39 2 3 #f)
           (list 41 2 3 #t) (list 43 2 3 #t) (list 45 2 3 #f)
           (list 47 2 3 #t) (list 49 2 3 #f) (list 51 2 3 #f)
           (list 53 2 3 #t) (list 55 2 3 #f) (list 57 2 3 #f)
           (list 59 2 3 #t) (list 61 2 3 #t) (list 63 2 3 #f)
           (list 65 2 3 #f) (list 67 2 3 #t) (list 69 2 3 #f)
           (list 71 2 3 #t) (list 73 2 3 #t) (list 75 2 3 #f)
           (list 77 2 3 #f) (list 79 2 3 #t) (list 81 2 3 #f)
           (list 83 2 3 #t) (list 85 2 3 #f) (list 87 2 3 #f)
           (list 89 2 3 #t) (list 91 2 3 #f) (list 93 2 3 #f)
           (list 95 2 3 #f) (list 97 2 3 #t) (list 99 2 3 #f)
           (list 101 2 3 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((num (list-ref this-list 0))
                  (aa-1 (list-ref this-list 1))
                  (aa-2 (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((sd-list
                     (sprp-module:div-two-and-odd (1- num))))
                (let ((ss (list-ref sd-list 0))
                      (dd (list-ref sd-list 1)))
                  (let ((result-1
                         (sprp-module:probable-prime? aa-1 dd num ss))
                        (result-2
                         (sprp-module:probable-prime? aa-2 dd num ss)))
                    (let ((result (and result-1 result-2))
                          (string-1
                           (format
                            #f "num=~a : base-1=~a, base-2=~a, "
                            num aa-1 aa-2))
                          (string-2
                           (format
                            #f "ss=~a, dd" ss)))
                      (begin
                        (sprp-simple-test-check
                         shouldbe result
                         (string-append
                          string-1 string-2)
                         dd
                         sub-name test-label-index
                         result-hash-table)
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sprp-prime-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-sprp-prime-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 #f) (list 1 #f)
           (list 2 #t) (list 3 #t) (list 4 #f) (list 5 #t)
           (list 6 #f) (list 7 #t) (list 8 #f) (list 9 #f)
           (list 10 #f) (list 11 #t) (list 12 #f) (list 13 #t)
           (list 14 #f) (list 15 #f) (list 16 #f) (list 17 #t)
           (list 18 #f) (list 19 #t) (list 20 #f) (list 21 #f)
           (list 22 #f) (list 23 #t) (list 24 #f) (list 25 #f)
           (list 26 #f) (list 27 #f) (list 28 #f) (list 29 #t)
           (list 30 #f) (list 31 #t) (list 32 #f) (list 33 #f)
           (list 34 #f) (list 35 #f) (list 36 #f) (list 37 #t)
           (list 38 #f) (list 39 #f) (list 40 #f) (list 41 #t)
           (list 42 #f) (list 43 #t) (list 44 #f) (list 45 #f)
           (list 46 #f) (list 47 #t) (list 48 #f) (list 49 #f)
           (list 50 #f) (list 51 #f) (list 52 #f) (list 53 #t)
           (list 54 #f) (list 55 #f) (list 56 #f) (list 57 #f)
           (list 58 #f) (list 59 #t) (list 60 #f) (list 61 #t)
           (list 62 #f) (list 63 #f) (list 64 #f) (list 65 #f)
           (list 66 #f) (list 67 #t) (list 68 #f) (list 69 #f)
           (list 70 #f) (list 71 #t) (list 72 #f) (list 73 #t)
           (list 74 #f) (list 75 #f) (list 76 #f) (list 77 #f)
           (list 78 #f) (list 79 #t) (list 80 #f) (list 81 #f)
           (list 82 #f) (list 83 #t) (list 84 #f) (list 85 #f)
           (list 86 #f) (list 77 #f) (list 78 #f) (list 79 #t)
           (list 80 #f) (list 81 #f) (list 82 #f) (list 83 #t)
           (list 84 #f) (list 85 #f) (list 86 #f) (list 87 #f)
           (list 88 #f) (list 89 #t) (list 90 #f) (list 91 #f)
           (list 92 #f) (list 93 #f) (list 94 #f) (list 95 #f)
           (list 96 #f) (list 97 #t) (list 98 #f) (list 99 #f)
           (list 100 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (sprp-module:sprp-prime? nn)))
                (let ((string-1 "nn"))
                  (begin
                    (sprp-simple-test-check
                     shouldbe result
                     string-1 nn
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
