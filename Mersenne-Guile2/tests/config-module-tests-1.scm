;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for config-module.scm                     ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated March 1, 2024                                ###
;;;###    moved test?.config files                           ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated February 27, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((config-module)
              :renamer (symbol-prefix-proc 'config-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (config-assert-value-in-hash-table
         shouldbe-key shouldbe-value shouldbe-comment
         result-htable comment-htable
         sub-name
         test-label-index
         result-hash-table)
  (begin
    (let ((result-value
           (hash-ref result-htable shouldbe-key #f))
          (result-comment
           (hash-ref comment-htable shouldbe-key #f)))
      (let ((err-1
             (format
              #f "~a : (~a) error for ~a : "
              sub-name test-label-index shouldbe-key))
            (err-2
             (format
              #f "shouldbe value=~a, result=~a"
              shouldbe-value result-value))
            (err-3
             (format
              #f "shouldbe comment=~a, result=~a"
              shouldbe-comment result-comment)))
        (begin
          (cond
           ((and
             (number? shouldbe-value)
             (number? result-value))
            (begin
              (unittest2:assert?
               (equal? shouldbe-value result-value)
               sub-name
               (string-append err-1 err-2)
               result-hash-table)
              ))
           ((and
             (string? shouldbe-value)
             (string? result-value))
            (begin
              (unittest2:assert?
               (string-ci=? shouldbe-value result-value)
               sub-name
               (string-append err-1 err-2)
               result-hash-table)
              ))
           (else
            (begin
              (unittest2:assert?
               (equal? shouldbe-value result-value)
               sub-name
               (string-append err-1 err-2)
               result-hash-table)
              )))

          (if (equal? shouldbe-comment #f)
              (begin
                (unittest2:assert?
                 (equal? shouldbe-comment result-comment)
                 sub-name
                 (string-append err-1 err-3)
                 result-hash-table))
              (begin
                (unittest2:assert?
                 (string-ci=? shouldbe-comment result-comment)
                 sub-name
                 (string-append err-1 err-3)
                 result-hash-table)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
;;;### read-config-file
(unittest2:define-tests-macro
 (test-read-config-file-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-read-config-file-1"
                  (utils-module:get-basename (current-filename))))
         (test-filename "test1.config")
         (result-htable (make-hash-table 20))
         (comment-htable (make-hash-table 20))
         (test-list
          (list
           (list "start-num" 3 ";;; start description")
           (list "end-num" 10000 ";;; end description")
           (list "bin-num" 1000 #f)
           (list "delta" 1000000000000 ";;; delta description")
           (list "outfile" "out1.txt" ";;; outfile description")
           ))
         (test-label-index 0))
     (begin
       (config-module:read-config-file
        test-filename result-htable comment-htable)

       (for-each
        (lambda (alist)
          (begin
            (let ((shouldbe-key (list-ref alist 0))
                  (shouldbe-value (list-ref alist 1))
                  (shouldbe-comment (list-ref alist 2)))
              (begin
                (config-assert-value-in-hash-table
                 shouldbe-key shouldbe-value shouldbe-comment
                 result-htable comment-htable
                 sub-name test-label-index result-hash-table)
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;;### read-config-file
(unittest2:define-tests-macro
 (test-read-config-file-2 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-read-config-file-2"
                  (utils-module:get-basename
                   (current-filename))))
         (test-filename "test2.config")
         (result-htable (make-hash-table 20))
         (comment-htable (make-hash-table 20))
         (test-list
          (list
           (list "start-num" 100000 ";;; start description")
           (list "restart-file" "restart.config" ";;; restart file")
           (list "not-a-valid-key" #f #f)
           ))
         (test-label-index 0))
     (begin
       (config-module:read-config-file
        test-filename result-htable comment-htable)

       (for-each
        (lambda (alist)
          (begin
            (let ((shouldbe-key (list-ref alist 0))
                  (shouldbe-value (list-ref alist 1))
                  (shouldbe-comment (list-ref alist 2)))
              (begin
                (config-assert-value-in-hash-table
                 shouldbe-key shouldbe-value shouldbe-comment
                 result-htable comment-htable
                 sub-name test-label-index result-hash-table)
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;;### write-config-file
(unittest2:define-tests-macro
 (test-write-config-file-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-write-config-file-1"
                  (utils-module:get-basename
                   (current-filename))))
         (test-filename "test3.config")
         (config-htable (make-hash-table 20))
         (comment-htable (make-hash-table 20))
         (result-htable (make-hash-table 20))
         (test-list
          (list
           (list "start-num" 100000 ";;; start number")
           (list "end-num" 1000000 ";;; end number")
           (list "display-num" 1000 ";;; display number")
           (list "top-prime-num" 20000 ";;; top prime")
           (list "restart-file" "restart.config" ";;; restart file")
           ))
         (test-label-index 0))
     (begin
      ;;;### first populate hash table with data from test list
       (for-each
        (lambda (alist)
          (begin
            (let ((key (list-ref alist 0))
                  (value (list-ref alist 1))
                  (comment (list-ref alist 2)))
              (begin
                (hash-set! config-htable key value)
                (hash-set! comment-htable key comment)
                ))
            )) test-list)

       ;;;### write out data
       (config-module:write-config-file
        test-filename config-htable comment-htable)

       (hash-clear! comment-htable)

       ;;;### read back the data, shouldbe identical
       (config-module:read-config-file
        test-filename result-htable comment-htable)

       (for-each
        (lambda (alist)
          (begin
            (let ((shouldbe-key (list-ref alist 0))
                  (shouldbe-value (list-ref alist 1))
                  (shouldbe-comment (list-ref alist 2)))
              (begin
                (config-assert-value-in-hash-table
                 shouldbe-key shouldbe-value shouldbe-comment
                 result-htable comment-htable
                 sub-name test-label-index result-hash-table)
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
