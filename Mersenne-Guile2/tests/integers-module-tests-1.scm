;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for integers-module.scm                   ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated February 27, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((integers-module)
              :renamer (symbol-prefix-proc 'integers-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (integers-simple-test-check
         shouldbe result
         extra-string extra-data
         sub-name test-label-index
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) error : ~a=~a : "
            sub-name test-label-index
            extra-string extra-data))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (let ((error-string
             (string-append err-1 err-2)))
        (begin
          (unittest2:assert?
           (equal? shouldbe result)
           sub-name
           error-string
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-square-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-square-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 #f) (list 3 #f) (list 4 #t) (list 5 #f)
           (list 6 #f) (list 7 #f) (list 8 #f) (list 9 #t)
           (list 10 #f) (list 11 #f) (list 12 #f) (list 13 #f)
           (list 14 #f) (list 15 #f) (list 16 #t) (list 17 #f)
           (list 18 #f) (list 19 #f) (list 20 #f) (list 21 #f)
           (list 22 #f) (list 23 #f) (list 24 #f) (list 25 #t)
           (list 26 #f) (list 27 #f) (list 28 #f) (list 29 #f)
           (list 30 #f) (list 31 #f) (list 32 #f) (list 33 #f)
           (list 34 #f) (list 35 #f) (list 36 #t) (list 37 #f)
           (list 38 #f) (list 39 #f) (list 40 #f) (list 41 #f)
           (list 42 #f) (list 43 #f) (list 44 #f) (list 45 #f)
           (list 46 #f) (list 47 #f) (list 48 #f) (list 49 #t)
           (list 50 #f) (list 51 #f) (list 52 #f) (list 53 #f)
           (list 54 #f) (list 55 #f) (list 56 #f) (list 57 #f)
           (list 58 #f) (list 59 #f) (list 60 #f) (list 61 #f)
           (list 62 #f) (list 63 #f) (list 64 #t) (list 65 #f)
           (list 66 #f) (list 67 #f) (list 68 #f) (list 69 #f)
           (list 70 #f) (list 71 #f) (list 72 #f) (list 73 #f)
           (list 74 #f) (list 75 #f) (list 76 #f) (list 77 #f)
           (list 78 #f) (list 79 #f) (list 80 #f) (list 81 #t)
           (list 82 #f) (list 83 #f) (list 84 #f) (list 85 #f)
           (list 86 #f) (list 87 #f) (list 88 #f) (list 89 #f)
           (list 90 #f) (list 91 #f) (list 92 #f) (list 93 #f)
           (list 94 #f) (list 95 #f) (list 96 #f) (list 97 #f)
           (list 98 #f) (list 99 #f) (list 100 #t) (list 101 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-bool (list-ref alist 1)))
              (let ((result-bool
                     (integers-module:is-square? test-num)))
                (let ((string-1 "test-num"))
                  (begin
                    (integers-simple-test-check
                     shouldbe-bool result-bool
                     string-1 test-num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-triangular-root-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-calc-triangular-root-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 1) (list 2 -1) (list 3 2)
           (list 4 -1) (list 5 -1) (list 6 3)
           (list 7 -1) (list 8 -1) (list 9 -1)
           (list 10 4) (list 15 5) (list 21 6)
           (list 28 7) (list 36 8) (list 45 9)
           (list 55 10) (list 66 11) (list 78 12)
           (list 91 13) (list 105 14) (list 120 15)
           (list 136 16) (list 153 17) (list 171 18)
           (list 190 19) (list 191 -1) (list 192 -1)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (integers-module:calc-triangular-root
                      test-num)))
                (let ((string-1 "test-num"))
                  (begin
                    (integers-simple-test-check
                     shouldbe result
                     string-1 test-num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-triangular-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-triangular-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 #t) (list 1 #t) (list 2 #f)
           (list 3 #t) (list 4 #f) (list 5 #f)
           (list 6 #t) (list 7 #f) (list 8 #f)
           (list 9 #f) (list 10 #t) (list 15 #t)
           (list 21 #t) (list 28 #t) (list 36 #t)
           (list 45 #t) (list 55 #t) (list 66 #t)
           (list 78 #t) (list 91 #t) (list 105 #t)
           (list 120 #t) (list 136 #t) (list 153 #t)
           (list 171 #t) (list 190 #t) (list 191 #f)
           (list 192 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-bool (list-ref alist 1)))
              (let ((result-bool
                     (integers-module:is-triangular? test-num)))
                (let ((string-1 "test-num"))
                  (begin
                    (integers-simple-test-check
                     shouldbe-bool result-bool
                     string-1 test-num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-mersenne-prime-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-calc-mersenne-prime-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 #f) (list 1 #f) (list 2 3)
           (list 3 7) (list 4 #f) (list 5 31)
           (list 6 #f) (list 7 127) (list 8 #f)
           (list 9 #f) (list 10 #f) (list 11 #f)
           (list 12 #f) (list 13 8191)
           (list 17 131071) (list 19 524287)
           (list 20 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (integers-module:calc-mersenne-prime
                      test-num)))
                (let ((string-1 "test-num"))
                  (begin
                    (integers-simple-test-check
                     shouldbe result
                     string-1 test-num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-perfect-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-perfect-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 #f) (list 1 #f) (list 2 #f)
           (list 3 #f) (list 4 #f) (list 5 #f)
           (list 6 #t) (list 7 #f) (list 8 #f)
           (list 9 #f) (list 10 #f) (list 11 #f)
           (list 27 #f) (list 28 #t) (list 29 #f)
           (list 495 #f) (list 496 #t) (list 497 #f)
           (list 8127 #f) (list 8128 #t) (list 8129 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (integers-module:is-perfect?
                      test-num)))
                (let ((string-1 "test-num"))
                  (begin
                    (integers-simple-test-check
                     shouldbe result
                     string-1 test-num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-perfect-number-euclidean-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-calc-perfect-number-euclidean-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 #f) (list 1 #f) (list 2 6)
           (list 3 28) (list 4 #f) (list 5 496)
           (list 6 #f) (list 7 8128) (list 8 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (integers-module:calc-perfect-number-euclidean
                      test-num)))
                (let ((string-1 "test-num"))
                  (begin
                    (integers-simple-test-check
                     shouldbe result
                     string-1 test-num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
