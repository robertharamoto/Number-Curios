;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for taxicab-module.scm                    ###
;;;###                                                       ###
;;;###  last updated August 14, 2024                         ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated February 15, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### regex used for regex
(use-modules ((ice-9 regex)
              :renamer (symbol-prefix-proc 'ice-9-regex:)))

;;;### srfi-19 used for time/date library
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((taxicab-module)
              :renamer (symbol-prefix-proc 'taxicab-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-populate-count-desc-htables-1 result-htable)
 (let ((sub-name
        (format
         #f "~a:test-populate-count-desc-htables-1"
         (utils-module:get-basename (current-filename))))
       (test-list
        (list
         (list 1 10000
               (list
                (list 1729 2 "1,729 = 1^3 + 12^3 = 9^3 + 10^3")
                (list 4104 2 "4,104 = 2^3 + 16^3 = 9^3 + 15^3")
                ))
         (list 10000 30000
               (list
                (list 13832 2 "13,832 = 2^3 + 24^3 = 18^3 + 20^3")
                (list 20683 2 "20,683 = 10^3 + 27^3 = 19^3 + 24^3")
                ))
         (list 87000000 88000000
               (list
                (list
                 87539319 3
                 "87,539,319 = 167^3 + 436^3 = 228^3 + 423^3 = 255^3 + 414^3")
                ))
         ))
       (count-htable (make-hash-table 1000))
       (desc-htable (make-hash-table 1000))
       (test-label-index 0))
   (begin
     (for-each
      (lambda (alist)
        (begin
          (let ((start-range (list-ref alist 0))
                (end-range (list-ref alist 1))
                (shouldbe-desc-list (list-ref alist 2)))
            (begin
              (hash-clear! count-htable)
              (hash-clear! desc-htable)

              (taxicab-module:populate-count-desc-htables
               start-range end-range count-htable desc-htable)

              (for-each
               (lambda (test-result-list)
                 (begin
                   (let ((shouldbe-taxi-num (list-ref test-result-list 0))
                         (shouldbe-taxi-count (list-ref test-result-list 1))
                         (shouldbe-string (list-ref test-result-list 2)))
                     (let ((result-count
                            (hash-ref count-htable shouldbe-taxi-num -1))
                           (result-string
                            (hash-ref desc-htable shouldbe-taxi-num "error")))
                       (let ((err-1
                              (format
                               #f "~a : (~a) : error for number ~a, "
                               sub-name test-label-index shouldbe-taxi-num))
                             (err-2
                              (format
                               #f "count shouldbe = ~a, result = ~a, "
                               shouldbe-taxi-count result-count))
                             (err-3
                              (format
                               #f "string shouldbe = ~s, result = ~s~%"
                               shouldbe-string result-string)))
                         (let ((error-string
                                (string-append err-1 err-2 err-3)))
                           (begin
                             (unittest2:assert?
                              (equal? shouldbe-taxi-count result-count)
                              sub-name
                              error-string
                              result-htable)

                             (unittest2:assert?
                              (string-ci=? shouldbe-string result-string)
                              sub-name
                              error-string
                              result-htable)
                             )))
                       ))
                   )) shouldbe-desc-list)
              ))

          (set! test-label-index (1+ test-label-index))
          )) test-list)
     )))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
