;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for results-module.scm                    ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;###  last updated June 1, 2022                            ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((results-module)
              :renamer (symbol-prefix-proc 'results-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (assert-results-value-in-hash-table
         shouldbe-key shouldbe-value result-htable
         sub-name test-label-index test-hash-table)
  (begin
    (let ((result-value
           (hash-ref result-htable shouldbe-key #f)))
      (let ((err-1
             (format
              #f "~a : (~a) : error for ~a, "
              sub-name test-label-index shouldbe-key))
            (err-2
             (format
              #f "shouldbe value = ~a, result = ~a~%"
              shouldbe-value result-value)))
        (let ((error-message
               (string-append err-1 err-2)))
          (begin
            (cond
             ((number? shouldbe-value)
              (begin
                (unittest2:assert?
                 (equal? shouldbe-value result-value)
                 sub-name
                 error-message
                 test-hash-table)
                ))
             ((string? shouldbe-value)
              (begin
                (unittest2:assert?
                 (string-ci=? shouldbe-value result-value)
                 sub-name
                 error-message
                 test-hash-table)
                ))
             (else
              (begin
                (unittest2:assert?
                 (equal? shouldbe-value result-value)
                 sub-name
                 error-message
                 test-hash-table)
                )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### load-top-results-file
(unittest2:define-tests-macro
 (test-load-top-results-from-file-1 test-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-load-top-results-from-file-1"
           (utils-module:get-basename (current-filename))))
         (test-filename "results-tests-1.txt")
         (test-list
          (list
           (list 2 5) (list 3 3) (list 4 1)
           ))
         (top-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (results-module:load-top-results-from-file
        test-filename top-htable)

       (for-each
        (lambda (alist)
          (begin
            (let ((shouldbe-key (list-ref alist 0))
                  (shouldbe-value (list-ref alist 1)))
              (begin
                (assert-results-value-in-hash-table
                 shouldbe-key shouldbe-value top-htable
                 sub-name test-label-index test-hash-table)
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
