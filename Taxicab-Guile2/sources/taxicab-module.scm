;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  Taxicab numbers module                               ###
;;;###                                                       ###
;;;###  last updated September 23, 2024                      ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start taxicab-modules

(define-module (taxicab-module)
  #:export (run-taxicab-job
            populate-count-desc-htables
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### advanced formatting
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### commafy-number
(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;### write results out to disk
(use-modules ((results-module)
              :renamer (symbol-prefix-proc 'results-module:)))

;;;### write completed start/end to config file
(use-modules ((config-module)
              :renamer (symbol-prefix-proc 'config-module:)))

;;;#############################################################
;;;#############################################################
(define (display-cubes-hash-tables
         count-htable desc-htable
         top-num top-htable)
  (begin
    (let ((taxi-list-htable (make-hash-table 1000))
          (original-top-htable (make-hash-table 1000))
          (key-list
           (sort
            (hash-map->list
             (lambda (key value) key) count-htable)
            <)))

      (begin
        ;;; copy the top seen hash table
        (hash-for-each
         (lambda (count top-count)
           (begin
             (hash-set! original-top-htable count top-count)
             )) top-htable)

        ;;; try to fill top table with results, then send over
        ;;; to results-module for writing out to file
        (for-each
         (lambda (sum)
           (begin
             (let ((count (hash-ref count-htable sum 0)))
               (begin
                 (if (> count 1)
                     (begin
                       (let ((top-count
                              (hash-ref top-htable count 0)))
                         (begin
                           (if (< top-count top-num)
                               (begin
                                 (hash-set!
                                  top-htable count (1+ top-count))

                                 (let ((tnum-string
                                        (utils-module:commafy-number sum)))
                                   (let ((tc-list
                                          (list
                                           tnum-string count
                                           (hash-ref desc-htable sum ""))))
                                     (begin
                                       (hash-set!
                                        taxi-list-htable sum tc-list)
                                       )))
                                 ))
                           ))
                       ))
                 ))
             )) key-list)

        (let ((rcount
               (display-results-to-stdout
                taxi-list-htable
                top-num original-top-htable)))
          (begin
            rcount
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-results-to-stdout
         output-htable top-num top-htable)
  (begin
    (let ((rcount 0)
          (key-list
           (sort
            (hash-map->list
             (lambda (key value)
               (begin
                 key
                 )) output-htable)
            <)))
      (begin
        (for-each
         (lambda (tnum)
           (begin
             (let ((tlist
                    (hash-ref output-htable tnum #f)))
               (begin
                 (if (not (equal? tlist #f))
                     (begin
                       (let ((nways
                              (list-ref tlist 1))
                             (tstring
                              (list-ref tlist 2)))
                         (let ((top-ways
                                (hash-ref top-htable nways 0)))
                           (begin
                             (if (< top-ways top-num)
                                 (begin
                                   (display
                                    (format
                                     #f "~a : ~a : ~a : ~a~%"
                                     nways
                                     (utils-module:commafy-number tnum)
                                     (1+ top-ways) tstring))

                                   (force-output)

                                   (hash-set!
                                    top-htable nways (1+ top-ways))
                                   (set! rcount (1+ rcount))
                                   ))
                             )))
                       ))
                 ))
             )) key-list)

        rcount
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-ii-jj-pair-macro
  (syntax-rules ()
    ((process-ii-jj-pair-macro
      ii ii-cubed jj jj-cubed
      sum
      min-range max-range
      count-htable desc-htable)
     (begin
       (set! jj-cubed (* jj jj jj))
       (set! sum (+ ii-cubed jj-cubed))

       (if (and
            (>= sum min-range)
            (<= sum max-range))
           (begin
             (let ((hc (hash-ref count-htable sum 0))
                   (tstring
                    (hash-ref
                     desc-htable sum
                     (ice-9-format:format
                      #f "~:D = ~:D^3 + ~:D^3"
                      sum ii jj))))
               (begin
                 (if (> hc 0)
                     (begin
                       (let ((t2-string
                              (ice-9-format:format
                               #f "~a = ~:D^3 + ~:D^3"
                               tstring ii jj)))
                         (begin
                           (set! tstring t2-string)
                           ))
                       ))

                 (hash-set! count-htable sum (1+ hc))
                 (hash-set! desc-htable sum tstring)
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (populate-count-desc-htables
         start-range end-range
         count-htable desc-htable)
  (begin
    (let ((local-one-third (exact->inexact (/ 1 3))))
      (let ((max-ii
             (inexact->exact
              (truncate
               (+ 1.0 (expt end-range local-one-third))
               ))))
        (begin
          (do ((ii 1 (1+ ii)))
              ((> ii max-ii))
            (begin
              (let ((ii-cubed (* ii ii ii)))
                (let ((jj-cubed ii-cubed)
                      (jj-init
                       (max
                        ii (inexact->exact
                            (truncate
                             (+ 1.0
                                (expt
                                 (max 1.0
                                      (- start-range ii-cubed 1.0))
                                 local-one-third))))))
                      (sum ii-cubed))
                  (begin
                    (do ((jj jj-init (1+ jj)))
                        ((> sum end-range))
                      (begin
                        (process-ii-jj-pair-macro
                         ii ii-cubed jj jj-cubed
                         sum
                         start-range end-range
                         count-htable desc-htable)
                        ))
                    )))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
;;;### calcluate-work-unit - split start/end range
;;;### so that the hash doesn't use a lot of memory
(define (calculate-work-unit
         start-range end-range
         top-num top-htable)
  (begin
    (let ((results-count 0)
          (count-htable (make-hash-table 10000))
          (desc-htable (make-hash-table 10000)))
      (begin
        (populate-count-desc-htables
         start-range end-range
         count-htable desc-htable)

        (let ((rcount
               (display-cubes-hash-tables
                count-htable desc-htable
                top-num top-htable)))
          (begin
            (set! results-count (+ results-count rcount))
            ))

        (gc)
        results-count
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-status-message
         ii-start max-range
         start-jday end-jday)
  (begin
    (let ((elapsed-time
           (timer-module:julian-day-difference-to-string
            end-jday start-jday)))
      (begin
        (display
         (ice-9-format:format
          #f "completed ~:D of ~:D : ~a : ~a~%"
          ii-start max-range
          elapsed-time
          (timer-module:current-date-time-string)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (run-taxicab-job
         min-range delta max-range status-num
         results-file restart-file top-num
         config-htable config-comment-htable)
  (begin
    (let ((top-htable (make-hash-table 20))
          (counter 0)
          (start-jday (srfi-19:current-julian-day)))
      (begin
        (results-module:load-top-results-from-file
         results-file top-htable)

        (do ((ii-start min-range (+ ii-start delta)))
            ((>= ii-start max-range))
          (begin
            (hash-set! config-htable "min-range" ii-start)
            (config-module:write-config-file
             restart-file config-htable config-comment-htable)

            (let ((ii-end (+ ii-start delta)))
              (begin
                (calculate-work-unit
                 ii-start ii-end top-num top-htable)
                ))

            (set! counter (+ counter delta))

            (if (zero? (modulo counter status-num))
                (begin
                  (let ((end-jday (srfi-19:current-julian-day)))
                    (begin
                      (display-status-message
                       ii-start max-range
                       start-jday end-jday)

                      (set! start-jday end-jday)
                      ))
                  ))
            ))

        (display
         (format #f "completed at ~a~%"
                 (timer-module:current-date-time-string)))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
