;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  read/write results file                              ###
;;;###                                                       ###
;;;###  last updated June 19, 2024                           ###
;;;###                                                       ###
;;;###  updated June 1, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 17, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start results modules

(define-module (results-module)
  #:export (load-top-results-from-file
            init-results-file
            write-results-file
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### regex
(use-modules ((ice-9 regex)
              :renamer (symbol-prefix-proc 'ice-9-regex:)))

;;;### read/write functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### for advanced formatting
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### time functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### commafy functions, taxicab record definitions
(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;#############################################################
;;;#############################################################
(define (load-top-results-from-file fname top-htable)
  (begin
    (if (file-exists? fname)
        (begin
          (hash-clear! top-htable)

          (with-input-from-file fname
            (lambda ()
              (begin
                (let ((comment-regex
                       (make-regexp
                        "^[ \t;#]*;" regexp/extended))
                      (empty-line-regex
                       (make-regexp "^$"))
                      (valid-line-regex
                       (make-regexp "[:]")))
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-line)
                          (ice-9-rdelim:read-line)))
                        ((eof-object? line))
                      (begin
                        (if (and
                             (not (eof-object? line))
                             (not
                              (regexp-exec
                               comment-regex line))
                             (not
                              (regexp-exec
                               empty-line-regex line))
                             (regexp-exec
                              valid-line-regex line))
                            (begin
                              (let ((sarray
                                     (string-split line #\:)))
                                (let ((nway-str
                                       (string-trim-both
                                        (list-ref sarray 0))))
                                  (begin
                                    (if (not
                                         (equal?
                                          (string->number nway-str) #f))
                                        (begin
                                          (let ((nways
                                                 (string->number nway-str)))
                                            (begin
                                              (let ((hc
                                                     (hash-ref
                                                      top-htable nways 0)))
                                                (begin
                                                  (hash-set!
                                                   top-htable nways (1+ hc))
                                                  ))
                                              ))
                                          ))
                                    )))
                              ))
                        ))
                    ))
                )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (init-results-file fname)
  (begin
    (let ((fport (open-file fname "w")))
      (begin
        (display
         (format
          #f "nways : taxicab number : rank : taxicab string~%")
         fport)
        (force-output fport)
        (close-port fport)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (write-results-file
         fname output-htable top-num top-htable)
  (begin
    (let ((rcount 0)
          (key-list
           (sort
            (hash-map->list
             (lambda (key value)
               (begin
                 key
                 )) output-htable)
            <))
          (fport (open-file fname "a")))
      (begin
        (for-each
         (lambda (tnum)
           (begin
             (let ((tlist
                    (hash-ref output-htable tnum #f)))
               (begin
                 (if (not (equal? tlist #f))
                     (begin
                       (let ((nways
                              (list-ref tlist 1))
                             (tstring
                              (list-ref tlist 2)))
                         (let ((top-ways
                                (hash-ref top-htable nways 0)))
                           (begin
                             (if (< top-ways top-num)
                                 (begin
                                   (display
                                    (format
                                     #f "~a : ~a : ~a : ~a~%"
                                     nways
                                     (utils-module:commafy-number tnum)
                                     (1+ top-ways) tstring)
                                    fport)

                                   (hash-set!
                                    top-htable nways (1+ top-ways))
                                   (set! rcount (1+ rcount))
                                   ))
                             )))
                       ))
                 ))
             )) key-list)

        (force-output fport)
        (close-port fport)
        rcount
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
